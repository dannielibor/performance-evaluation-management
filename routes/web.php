<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/seed', 'SeedController@index');

Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');

Route::get('/', function () {

    $twoauth = App\TwoAuth::all();
    foreach($twoauth as $item){
        if($item->created_at < Carbon\Carbon::now()->subMinutes(1)->toDateTimeString()){
            $item->delete();
        }
    }
    
    if(Auth::check()){
        if(Session::get('type') == 'Admin' || Session::get('type') == 'Super Admin'){
            return redirect('/dashboard');
        }else{
            return redirect('/home');
        }
    }else{
        return view('authenticate');
    }

});

Route::get('/authenticate', 'AuthenticationController@index');
Route::get('/resend/code', 'AuthenticationController@resend');

// Auth::routes();

// Authentication Routes...
$this->get('login', 'Auth\LoginController@showLoginForm')->name('login');
$this->post('login', 'Auth\LoginController@login');
$this->post('logout', 'Auth\LoginController@logout')->name('logout');

// Password Reset Routes...
$this->get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm');
Route::post('/password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
$this->get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm');
$this->get('admin/password/reset/{token}', 'Auth\ResetPasswordController@showAdminResetForm');
$this->post('password/reset', 'Auth\ResetPasswordController@reset')->name('password.reset');

// registration activation routes
Route::get('activation/key/{activation_key}', ['as' => 'activation_key', 'uses' => 'Auth\ActivationKeyController@activateKey']);
Route::get('activation/resend', ['as' =>  'activation_key_resend', 'uses' => 'Auth\VerificationController@resend']);
Route::post('activation/resend', ['as' =>  'activation_key_resend.post', 'uses' => 'Auth\ActivationKeyController@resendKey']);

Route::get('/departmentRate', 'Admin\MainController@departmentRate');

Route::get('/admin', function(){
    return view('admin.layouts.login');
});

Route::group(['middleware' => 'admin'], function(){

    Route::resource('/tutorials', 'Admin\TutorialController');

    Route::get('/entries/load/feedback', 'Admin\EntryController@feedback');
    Route::get('/entries/load/entry', 'Admin\EntryController@entry');
    Route::post('/entries/show', 'Admin\EntryController@show');
    Route::get('/entries/sort', 'Admin\EntryController@edit');
    Route::resource('/entries', 'Admin\EntryController');

    Route::get('/rank/load', 'Admin\RankController@load');
    Route::resource('/rank', 'Admin\RankController');

    Route::get('/feedback/edit', 'Admin\FeedbackController@edit');
    Route::get('/feedback/update', 'Admin\FeedbackController@update');
    Route::get('/feedback/store', 'Admin\FeedbackController@store');
    Route::get('/feedback/destroy', 'Admin\FeedbackController@destroy');

    Route::get('/summative-report/subelement/update/edit', 'Admin\SubElementController@edit');
    Route::get('/summative-report/subelement/update/destroy', 'Admin\SubElementController@destroy');
    Route::get('/summative-report/subelement/update', 'Admin\SubElementController@update');
    Route::get('/summative-report/subelement/store', 'Admin\SubElementController@store');
    Route::resource('/summative-report/subelement', 'Admin\SubElementController');

    Route::get('/summative-report/getDepartment', 'Admin\SummativeReportController@edit');
    Route::resource('/summative-report/load', 'Admin\SummativeReportController');

    Route::get('/summative-report/element/destroy', 'Admin\ElementController@destroy');
    Route::get('/summative-report/element/rate', 'Admin\ElementController@rate');
    Route::get('/summative-report/element/title', 'Admin\ElementController@title');
    Route::get('/summative-report/element/store', 'Admin\ElementController@store');
    Route::get('/summative-report/element/load', 'Admin\ElementController@show');
    Route::get('/summative-report/edit', 'Admin\ElementController@edit');
    Route::resource('/summative-report', 'Admin\ElementController');

    Route::get('/forms/manage-relationship/edit', 'Admin\FormRelationshipController@edit');
    Route::get('/forms/manage-relationship/selfEval', 'Admin\FormRelationshipController@selfEval');
    Route::get('/forms/manage-relationship/evaluatee', 'Admin\FormRelationshipController@evaluatee');
    Route::get('/forms/manage-relationship/evaluator', 'Admin\FormRelationshipController@evaluator');
    Route::get('/forms/manage-relationship/status', 'Admin\FormRelationshipController@status');
    Route::get('/forms/manage-relationship/{id}', 'Admin\FormRelationshipController@show');
    Route::resource('/forms/manage-relationship', 'Admin\FormRelationshipController');

    Route::resource('/class-load', 'Admin\ClassLoadController');

    Route::get('/content/edit', 'Admin\ContentController@edit');
    Route::get('/content/update', 'Admin\ContentController@update');
    Route::get('/content/destroy', 'Admin\ContentController@destroy');
    Route::get('/content/store', 'Admin\ContentController@store');

    Route::get('/edit/chapter', 'Admin\BodyController@edit');
    Route::get('/destroy/chapter', 'Admin\BodyController@destroy');
    Route::get('/store/chapter', 'Admin\BodyController@store');
    Route::get('/update/chapter', 'Admin\BodyController@update');

    Route::get('/forms/restore/{id}', 'Admin\FormController@restore');
    Route::get('/forms/paginate', 'Admin\FormController@paginate');
    Route::get('/publish/{id}', 'Admin\FormController@publish');
    Route::get('/forms/edit/{id}', 'Admin\FormController@edit');
    Route::post('/forms/update/{id}', 'Admin\FormController@update');
    Route::resource('/forms', 'Admin\FormController');

    Route::resource('/certificate-of-registration', 'Admin\CorController');

    Route::get('/faculty/2fa', 'Admin\FacultyController@twoFa');
    Route::get('/faculty/delete/{id}', 'Admin\FacultyController@destroy');
    Route::get('/faculty/forceDel/{id}', 'Admin\FacultyController@forcedelete');
    Route::get('/faculty/restore/{id}', 'Admin\FacultyController@restore');
    Route::get('/faculty/type', 'Admin\FacultyController@type');
    Route::get('/load/faculty', 'Admin\FacultyController@load');
    Route::get('/faculty/edit/{id}', 'Admin\FacultyController@edit');
    Route::resource('/faculty', 'Admin\FacultyController');

    Route::get('/students/2fa', 'Admin\StudentsController@twoFa');
    Route::get('/students/delete/{id}', 'Admin\StudentsController@destroy');
    Route::get('/students/forceDel/{id}', 'Admin\StudentsController@forcedelete');
    Route::get('/students/restore/{id}', 'Admin\StudentsController@restore');
    Route::get('/load/students', 'Admin\StudentsController@load');
    Route::get('/students/edit/{id}', 'Admin\StudentsController@edit');
    Route::resource('/students', 'Admin\StudentsController');

    Route::get('/admin/2fa', 'Admin\ProfileController@edit');
    Route::get('/admin/profile/settings', 'Admin\ProfileController@show');
    Route::resource('/admin/profile', 'Admin\ProfileController');

    Route::resource('/admins', 'Admin\AdminsController');

    Route::get('/getSession', 'Admin\MainController@getSession');
    Route::get('/entry-export', 'Admin\MainController@export');
    Route::get('/dashboard/selectSection', 'Admin\MainController@selectSection');
    Route::get('/dashboard/chooseSection', 'Admin\MainController@chooseSection');
    Route::get('/dashboard/setStudents', 'Admin\MainController@setStudents');
    Route::get('/dashboard/setSchedules', 'Admin\MainController@setSchedules');
    Route::get('/dashboard/setFaculty', 'Admin\MainController@setFaculty');
    Route::get('/dashboard', 'Admin\MainController@index');
    Route::resource('/dashboard', 'Admin\MainController');

});

Route::group(['middleware' => 'user'], function(){

    Route::get('/search', 'Guest\MainController@search');

    Route::get('/summary/summative_report/load', 'Guest\SummativeReportController@update');
    Route::get('/summary/summative_report/getDepartment', 'Guest\SummativeReportController@edit');
    Route::resource('/summary/summative_report', 'Guest\SummativeReportController');

    Route::get('/summary/rank/load/feedback', 'Guest\RankController@feedback');
    Route::get('/summary/rank/load/entry', 'Guest\RankController@entry');
    Route::get('/summary/rank/load', 'Guest\RankController@load');
    Route::resource('/summary/rank', 'Guest\RankController');

    // Route::get('/summative_report/load', 'Guest\SummativeReportController@update');
    // Route::get('/summative_report/getDepartment', 'Guest\SummativeReportController@edit');
    // Route::resource('/summative_report', 'Guest\SummativeReportController');

    Route::get('/profile/settings', 'Guest\ProfileController@show');
    Route::get('/2fa', 'Guest\ProfileController@edit');
    Route::resource('/profile', 'Guest\ProfileController');

    Route::get('/home', 'Guest\MainController@index');

    Route::get('/pagination/fetch_data', 'Guest\FormsController@fetch_data');
    Route::get('/paginate', 'Guest\FormsController@paginate');
    Route::post('/entry', 'Guest\FormsController@store');
    Route::get('/forms/{title}/{formId}/faculty/{as}/{userId}', 'Guest\FormsController@show');
    Route::get('/forms/{title}/{formId}/faculty/{as}', 'Guest\FormsController@edit');
    Route::resource('/forms/{title}/{formId}', 'Guest\FormsController');

});