<div class="m-content">
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head" style = "background-color:#f4f4f4">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        <div id = "default" style = "display: block">
                            <a href="javascript:history.back()" id = "cancel" style = "background:none;border:none" class="btn btn-secondary m-btn m-btn--custom m-btn--icon">
                            <span>
                                <i class="la la-arrow-left"></i>
                                &nbsp;&nbsp;    
                                <span id = "default_title">{{ $form->title }} </span>
                            </span>
                            <a href="#" <?php if($form->status != 'active'){ echo 'id = "editBtn"'; } ?> class="m-portlet__nav-link btn m-btn m-btn--hover-warning m-btn--icon m-btn--icon-only m-btn--pill" title="Cancel">
                                <i class="fa fa-pencil"></i>
                            </a>
                        </div>
                        <div id = "edit" style = "display:none">
                            <div class = "row">
                                <div class = "col">
                                    <input type="text" id = "edit_title" value = "{{ $form->title }}" class = "form-control m-input--air">
                                </div>
                                <div class = "col">
                                    <a href="#" id = "saveBtn" class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill" title="Submit">
                                        <i class="fa fa-check"></i>
                                    </a>
                                    <a href="#" id = "cancelBtn" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Cancel">
                                        <i class="fa fa-remove"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </h3> 
                </div>
            </div>
        </div>
        <div class="m-portlet__body">
            <div class="row align-items-center">
                <div class="col-xl-8 order-2 order-xl-1">
                    <div class="form-group m-form__group row align-items-center">
                        <div class="col-md-3">
                            <div class="m-form__group m-form__group--inline">
                                <div class="m-input-icon m-input-icon--left">
                                    <button id = "chapter" <?php if($form->status == 'active'){ echo 'disabled'; } ?> style = "background-color:#800;border:none" class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--pill">
                                        <span>
                                            <i class="fl flaticon-signs"></i>
                                            <span>
                                                New Chapter
                                            </span>
                                        </span>
                                    </button>
                                </div>
                            </div>
                            <div class="d-md-none m--margin-bottom-10"></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="m-portlet__body">
                @if($form->status == 'active')
                <fieldset disabled="disabled">
                @endif
                <!--begin::Section-->
                <div class="m-accordion m-accordion--default" id="m_accordion_1" role="tablist">
                    <div <?php if($form->status != 'active'){ echo 'id="m_sortable_portlets"'; } ?>>
                        <!--begin::Item-->
                        @foreach (App\Body::withTrashed()->where('formId', $form->id)->orderByRaw('LENGTH(position)', 'asc')->orderBy('position', 'asc')->get() as $item)
                            <div id = "item" data-id = "{{$item->id}}">
                                <div class="m-portlet m-portlet--mobile m-portlet--sortable m-accordion__item">
                                    <a href="#" style = "float:right" data-toggle="modal" data-target="#m_modal_delete_{{$item->id}}" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete">
                                        <i class="la la-trash"></i>
                                    </a>
                                    <div style = "background-color:#F2F3F4" class="m-accordion__item-head collapsed" role="tab" id="m_accordion_1_item_1_head_{{$item->id}}" data-toggle="collapse" href="#m_accordion_1_item_1_body_{{$item->id}}" aria-expanded="false">
                                        <span class="m-accordion__item-icon" style = "cursor:move">
                                            <i class = "fa fa-sort"></i>
                                        </span>
                                        <span class="m-accordion__item-title">
                                            <div class="m-input-icon m-input-icon--left">
                                                <input type="text" value = "{{$item->header}}" id = "chapter_title" data-id = "{{$item->id}}" class="form-control m-input--pill" placeholder="Text Here" style = "width:50%">
                                                <span class="m-input-icon__icon m-input-icon__icon--left">
                                                    <span>
                                                        <i class="la" id = "sort" data = "{{$item->sort}}" data-id = "{{$item->id}}">{{$item->sort}}.</i>
                                                    </span>
                                                </span>
                                            </div>
                                        </span>
                                        <span class="m-accordion__item-mode"></span>
                                    </div>
                                    <div class="m-accordion__item-body collapse" id="m_accordion_1_item_1_body_{{$item->id}}" class=" " role="tabpanel" aria-labelledby="m_accordion_1_item_1_head_{{$item->id}}" data-parent="#m_accordion_1_{{$item->id}}">
                                        <div class="m-accordion__item-content">
                                            <div class="row align-items-center">
                                                <div class="col-xl-8 order-2 order-xl-1">
                                                    <div class="form-group m-form__group row align-items-center">
                                                        <div class="col-md-3">
                                                            <div class="m-form__group m-form__group--inline">
                                                                <div class="m-input-icon m-input-icon--left">
                                                                    <button id = "header" data-id = "{{$item->id}}" style = "background-color:#800;border:none" class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--pill">
                                                                        <span>
                                                                            <i class="fl flaticon-signs"></i>
                                                                            <span>
                                                                                New Header
                                                                            </span>
                                                                        </span>
                                                                    </button>
                                                                </div>
                                                            </div>
                                                            <div class="d-md-none m--margin-bottom-10"></div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="m-form__group m-form__group--inline">
                                                                <div class="m-input-icon m-input-icon--left">
                                                                    <button id = "question" data-id = "{{$item->id}}" style = "background-color:#800;border:none" class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--pill">
                                                                        <span>
                                                                            <i class="fl flaticon-signs"></i>
                                                                            <span>
                                                                                New Question
                                                                            </span>
                                                                        </span>
                                                                    </button>
                                                                </div>
                                                            </div>
                                                            <div class="d-md-none m--margin-bottom-10"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <table class = "table table-striped">
                                                <thead>
                                                    <tr>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                    </tr>
                                                </thead>
                                                <tbody id = "input" data-id = "{{$item->id}}">
                                                    @foreach (App\Content::withTrashed()->where('bodyId', $item->id)->orderByRaw('LENGTH(position)', 'asc')->orderBy('position', 'asc')->get() as $content)
                                                        @if($content->type == 'header')
                                                            <tr id = "row" data-id = "{{$item->id}}" data = "{{$content->id}}" data-type = "header">
                                                                <td style = "width:1%;cursor:move">
                                                                    <center>
                                                                        <i class = "fa fa-sort"></i>
                                                                    </center>
                                                                </td>
                                                                <td style = "width:50%" colspan = "3">
                                                                    <div class="m-input-icon m-input-icon--left">
                                                                        <input type="text" value = "{{$content->content}}" id = "input_title" data-id = "{{$content->id}}" class="form-control m-input--pill" placeholder="Text Here" style = "width:100%">
                                                                        <span class="m-input-icon__icon m-input-icon__icon--left">
                                                                            <span>
                                                                                <i class="la" id = "label" data = "{{$content->id}}">{{$content->sort}}.</i>
                                                                            </span>
                                                                        </span>
                                                                    </div>
                                                                </td>
                                                                <td style = "width:3%;cursor:move">
                                                                    <center>
                                                                        <div id = "del" data-type = "header" data-id = "{{$content->id}}" data = "{{$item->id}}" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill">
                                                                            <i class="la la-trash-o"></i>
                                                                        </div>
                                                                    </center>
                                                                </td>
                                                            </tr>
                                                        @elseif($content->type == 'question')
                                                            <tr id = "row" data-id = "{{$item->id}}" data = "{{$content->id}}" data-type = "question">
                                                                <td style = "width:1%;cursor:move"></td>
                                                                <td style = "width:1%;cursor:move">
                                                                    <center>
                                                                        <i class = "fa fa-sort"></i>
                                                                    </center>
                                                                </td>
                                                                <td style = "width:50%;" colspan = "2">
                                                                    <input type="text" value = "{{$content->content}}" id = "input_title" data-id = "{{$content->id}}" class="form-control m-input--pill" placeholder="Text Here" style = "width:100%">
                                                                </td>
                                                                <td style = "width:3%;cursor:move">
                                                                    <center>
                                                                        <div id = "del" data-type = "question" data-id = "{{$content->id}}" data = "{{$item->id}}" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill">
                                                                            <i class="la la-trash-o"></i>
                                                                        </div>
                                                                    </center>
                                                                </td>
                                                            </tr>
                                                        @endif
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                                {{-- delete modal --}}
                                <div class="modal fade" id="m_modal_delete_{{ $item->id }}" tabindex="-1" role="dialog" aria-labelledby="delete_{{ $item->id }}" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                        <div class="modal-header" style = "background-color:#FAA900">
                                            <h5 class="modal-title" id="delete_{{ $item->id }}"><i class = "fa fa-warning"></i> &nbsp; Delete Chapter</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body" style = "text-align:center">
                                            This chapter will be deleted including its contents!
                                            <br>
                                            Do you wish to continue?
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" data-dismiss="modal" id = "delete" data-id = "{{$item->id}}" class="btn m-btn--pill btn-danger">
                                                Delete
                                            </button>
                                            <button type="button" class="btn btn-metal m-btn--pill" data-dismiss="modal">
                                                Cancel
                                            </button>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                        <!--end::Item-->
                </div>
                @if($form->status == 'active')
                </fieldset>
                @endif
                <!--end::Section-->
            </div>
            <!--end: Search Form -->
        </div>
    </div>
</div>