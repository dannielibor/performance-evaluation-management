@extends('admin.layouts.app')

@section('title', 'Evaluation Management')

@section('content')

<style>
.container{
    width:30%;
}
.m-loader{
    position: fixed;
    z-index: 999;
    height: 2em;
    width: 2em;
    overflow: visible;
    margin: auto;
    top: 0;
    left: 0;
    bottom: 0;
    right: 0;
}
#more:hover > i{
    color: white !important;
}
#more:hover{
    background-color: #800 !important;
}

#drag:hover{
    cursor: move;
}

</style>

@if($form->status == 'active')

    <div class="alert alert-warning alert-dismissible fade show" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
        <i class="la la-warning"></i>&nbsp;
        This form is currently active.
    </div>

@endif

<input type="hidden" value = "{{$form->title}}" id = "title">
<input type="hidden" value = "{{$form->id}}" id = "formId">
    
    <!-- BEGIN: Subheader -->
    <div class="m-subheader">
        <div class="d-flex align-items-center">
            <div>
                <div class="m-input-icon m-input-icon--left">
                    <button <?php if($form->status == 'active'){ echo 'disabled'; } ?> class="btn btn-success m-btn m-btn--custom m-btn--icon m-btn--pill" data-toggle="modal" data-target="#publish">
                        <span>
                            <i class="m-nav__link-icon flaticon-calendar-2"></i>
                            <span>
                                Publish
                            </span>
                        </span>
                    </button>
                </div>
            </div>
        </div>
    </div>
    <!-- END: Subheader -->

    @include('admin.forms.form')
    @include('admin.forms.feedback')

<div class="modal fade" id="publish" tabindex="-1" role="dialog" aria-labelledby="publish" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <form action="/publish/{{$form->id}}" method = "get" class = "m-form m-form--fit m-form--label-align-right">
                @csrf
                <div class="modal-header" style = "background-color:#f4f4f4">
                    <h5 class="modal-title" id="publish">Publish</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group m-form__group row m--margin-bottom-20">
                        <label class="col-form-label col-lg-3 col-sm-12">
                            Schedule
                        </label>
                        <div class="col-lg-6 col-md-9 col-sm-12">
                            <div class='input-group' id='m_daterangepicker_2'>
                                <input type='text' class="form-control m-input" readonly  placeholder="Select date range" name = "schedule" value = "{{$form->from}} / {{$form->to}}"/>
                                <div class="input-group-append">
                                    <span class="input-group-text">
                                        <i class="la la-calendar-check-o"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group m-form__group row m--margin-bottom-20">
                        <label class="col-form-label col-lg-3 col-sm-12">
                            Pagination
                        </label>
                        <div class="col-lg-6 col-md-9 col-sm-12">
                            <input id="m_touchspin_1" readonly type="text" class="form-control" value = "{{$form->pagination}}" name="paginate" placeholder="3" type="text">
                        </div>
                    </div>
                        <?php
                            $total = App\Body::withTrashed()->where('formId', $form->id)->sum('rate');
                        ?>
                    <p style = "float:right">Total: <span id = "total">{{$total}}</span> %</p>
                    <table class = "table table-striped">
                        <thead>
                            <tr>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody id = "touchspin">
                            @foreach (App\Body::withTrashed()->where('formId', $form->id)->orderByRaw('LENGTH(position)', 'asc')->orderBy('position', 'asc')->get() as $item)
                                <tr id = "spin" data-id = "{{$item->id}}">
                                    <td colspan="3" style = "width:45%" id = "touchspin_title" data-id = "{{$item->id}}">
                                        <input type="text" class = "form-control" id = "header_title" data-id = "{{$item->id}}" value = "{{$item->header}}" placeholder="Text here">
                                    </td>
                                    <td>
                                        <input id="m_touchspin_3" readonly type="text" class="form-control" value="{{$item->rate}}" name="rate[]" placeholder="Input Rate" data-id = "{{$item->id}}" type="text">
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="submit" id = "publishBtn" class="btn m-btn--pill btn-success">Publish</button>
                    <button type="button" class="btn m-btn--pill btn-danger" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js"></script>
<script src="{{ asset('metronic/assets/demo/default/custom/components/forms/widgets/bootstrap-daterangepicker.js') }}" type="text/javascript"></script>
<script src="{{ asset('metronic/assets/demo/default/custom/components/forms/widgets/bootstrap-touchspin.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/create_form.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/create_feedback.js') }}" type="text/javascript"></script>

@endsection