@extends('admin.layouts.app')

@section('title', 'Evaluation Management')

@section('content')

    <div class="m-content">
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head" style = "background-color:#f4f4f4">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            <a href="javascript:history.back()" id = "cancel" style = "background:none;border:none" class="btn btn-secondary m-btn m-btn--custom m-btn--icon">
                                <span>
                                    <i class="la la-arrow-left"></i>
                                    &nbsp;&nbsp;
                                    <span>
                                        Manage Relationship
                                    </span>
                                </span>
                            </a>
                        </h3>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body">

                @if($form->status == 'active')

                <fieldset disabled="disabled">
                
                @endif

                <!--begin: Search Form -->
                <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                    <div class="row align-items-center">
                        <div class="col-xl-4 order-1 order-xl-2 m--align-left">
                            {!! Form::open(['action' => 'Admin\FormRelationshipController@store', 'method' => 'post']) !!}
                                @csrf
                                <input type="hidden" value = "{{$form->id}}" name = "formId" id = "formId">
                            <button type = "submit" style = "background-color: #800;border:none" class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--pill">
                                <span>
                                    <i class="fl flaticon-file"></i>
                                    <span>
                                        New Relationship
                                    </span>
                                </span>
                            </button>
                            {!! Form::close() !!}
                            <div class="m-separator m-separator--dashed d-xl-none"></div>
                        </div>
                        <div class="col-xl-8 order-1 order-xl-2 m--align-right">
                            <div class="dropdown">
                                <button style = "color:white;background-color:#800;border:none" class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--pill dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class = "fl flaticon-list-1"></i>
                                    Self Evaluation
                                </button>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton" style = "padding:15px">
                                    <div class="m-checkbox-list">
                                        @foreach (App\Faculty::withTrashed()->distinct()->get(['type']) as $type)
                                            <label class="m-checkbox">
                                                <input type="checkbox" value = "{{$type->type}}" id = "self" 
                                                    <?php 
                                                        $validate = App\FormRestriction::withTrashed()->where('formId', $form->id)->where('type', $type->type)->first();
                                                        if(!empty($validate) && $validate->selfEvaluation == 1){echo'checked';} 
                                                    ?>>
                                                    {{$type->type}}
                                                <span></span>
                                            </label>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            <div class="m-separator m-separator--dashed d-xl-none"></div>
                        </div>
                    </div>
                </div>
                <!--end: Search Form -->
                <!--begin: Datatable -->
                <table class = "table table-striped" width="100%">
                    <thead style = "text-align:center">
                        <tr>
                            <th title="Field #1">
                                Evaluator
                            </th>
                            <th title="Field #2">
                                Evaluatee
                            </th>
                            <th title = "Field #3">
                                Option
                            </th>
                            <th title = "Field #4">
                                Action
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($relation) > 0)
                            @foreach ($relation as $item)
                                <tr>
                                    <td>
                                        <select class = "form-control" id = "evaluator" data = "{{$item->id}}">
                                            <option value="">Choose...</option>
                                            <option value="Dean" <?php if($item->evaluator=='Dean'){echo'selected';} ?> >Dean</option>
                                            <option value="Vice Dean" <?php if($item->evaluator=='Vice Dean'){echo'selected';} ?> >Vice Dean</option>
                                            <option value="Department Chair" <?php if($item->evaluator=='Department Chair'){echo'selected';} ?> >Department Chair</option>
                                            <option value="Professor" <?php if($item->evaluator=='Professor'){echo'selected';} ?> >Professor</option>
                                            <option value="Student" <?php if($item->evaluator=='Student'){echo'selected';} ?> >Student</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select class = "form-control" id = "evaluatee" data = "{{$item->id}}">
                                            <option value="">Choose...</option>
                                            <option value="Dean" <?php if($item->evaluatee=='Dean'){echo'selected';} ?> >Dean</option>
                                            <option value="Vice Dean" <?php if($item->evaluatee=='Vice Dean'){echo'selected';} ?> >Vice Dean</option>
                                            <option value="Department Chair" <?php if($item->evaluatee=='Department Chair'){echo'selected';} ?> >Department Chair</option>
                                            <option value="Professor" <?php if($item->evaluatee=='Professor'){echo'selected';} ?> >Professor</option>
                                        </select>
                                    </td>
                                    <td>
                                        <center>
                                            <div class = "m-form__group form-group row">
                                                <label class="col-3 col-form-label" id = "label_relation" data = "{{$item->id}}">
                                                    {{$item->status}}
                                                </label>
                                                <div class="col-3">
                                                    <span class="m-switch m-switch--outline m-switch--success">
                                                        <label>
                                                            <input type="checkbox" <?php if($item->status =='Distinct'){echo'checked';} ?> id = "relationship" data = "{{$item->id}}">
                                                            <span></span>
                                                        </label>
                                                    </span>
                                                </div>
                                            </div>
                                        </center>
                                    </td>
                                    <td>
                                        <center>
                                            <a href="#" data-toggle="modal" data-target="#delete_{{$item->id}}" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete">
                                                <i class="la la-trash"></i>
                                            </a>
                                        </center>
                                    </td>
                                </tr>

                                {{-- delete --}}
                                <div class="modal fade" id="delete_{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            {!! Form::open(['action' => ['Admin\FormRelationshipController@destroy', $item->id], 'method' => 'post']) !!}
                                                @csrf
                                                {!! Form::hidden('_method', 'DELETE') !!}
                                            <div class="modal-header" style = "background-color:#FAA900">
                                                <h5 class="modal-title" id="">
                                                    <i class = "fa fa-warning"></i> &nbsp; Delete Relationship!
                                                </h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true" class="la la-remove"></span>
                                                </button>
                                            </div>
                                            <div class="modal-body" style = "text-align:center">
                                                This form relationship will be deleted!
                                                <br>
                                                Are you sure?
                                            </div>
                                            <div class="modal-footer">
                                                <button type="submit" class="m-btn--pill btn btn-danger m-btn">
                                                    Delete
                                                </button>
                                                <button type="button" class="m-btn--pill btn btn-metal m-btn" data-dismiss="modal">
                                                    Close
                                                </button>
                                            </div>
                                            {!! Form::close() !!}
                                        </div>
                                    </div>
                                </div>

                            @endforeach
                        @endif
                    </tbody>
                </table>
                <!--end: Datatable -->
                @if($form->status == 'active')

                </fieldset>

                @endif
            </div>
        </div>
    </div>

<script>
    $('input[type="checkbox"][id="self"]').click(function(){
        var array = [];
        $('#self:checked').each(function(){
            array.push($(this).val());
        });
        $.ajax({
            url: '/forms/manage-relationship/edit',
            type: 'get',
            async: false,
            data: { id: $('#formId').val(), type: array }
        });
    });
    $('.dropdown-menu').click(function(e) {
        e.stopPropagation();
    });
    $('#relationship[data]').change(function(){
        var status = ''; 
        var data = $(this).attr('data');
        if($('#label_relation[data="'+data+'"]').text() == 'Indistinct'){
            status = 'Distinct';
        }else{
            status = 'Indistinct';
        } 
        $.ajax({
            url: '/forms/manage-relationship/status',
            type: 'get',
            async: false,
            data: { id:$(this).attr('data'), value: status },
            success: function(response){ 
                $('#label_relation[data="'+data+'"]').text(status);
            }
        });
    });
    $('#selfEval[data]').change(function(){
        $.ajax({
            url: '/forms/manage-relationship/selfEval',
            type: 'get',
            data: { id:$(this).attr('data') }
        });
    });
    $('#evaluator[data]').change(function(){
        $.ajax({
            url: '/forms/manage-relationship/evaluator',
            type: 'get',
            data: { id:$(this).attr('data'), value: $(this).val(), compare:$('#evaluatee[data="'+$(this).attr('data')+'"]').val(), formId: $('#formId').val() },
            success: function(response){
                if(response == 'false'){
                    $('#evaluator[data="'+id+'"]').css('border','1px solid red');
                        toastr.options = {
                        "closeButton": false,
                        "debug": false,
                        "newestOnTop": false,
                        "progressBar": false,
                        "positionClass": "toast-bottom-right",
                        "preventDuplicates": false,
                        "onclick": null,
                        "showDuration": "300",
                        "hideDuration": "1000",
                        "timeOut": "5000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                    };

                    toastr.error("Relationship already exist?");
                }
            }
        });
    });
    $('#evaluatee[data]').change(function(){
        var id = $(this).attr('data');
        $.ajax({
            url: '/forms/manage-relationship/evaluatee',
            type: 'get',
            data: { id:$(this).attr('data'), value: $(this).val(), compare:$('#evaluator[data="'+$(this).attr('data')+'"]').val(), formId: $('#formId').val() },
            success: function(response){
                if(response == 'false'){
                    $('#evaluatee[data="'+id+'"]').css('border','1px solid red');
                        toastr.options = {
                        "closeButton": false,
                        "debug": false,
                        "newestOnTop": false,
                        "progressBar": false,
                        "positionClass": "toast-bottom-right",
                        "preventDuplicates": false,
                        "onclick": null,
                        "showDuration": "300",
                        "hideDuration": "1000",
                        "timeOut": "5000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                    };

                    toastr.error("Relationship already exist?");
                }
            }
        });
    });
</script>

@endsection