<div class="m-content">
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head" style = "background-color:#f4f4f4">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        <span>Feedback</span>
                    </h3> 
                </div>
            </div>
        </div>
        <div class="m-portlet__body">
            <div class="row align-items-center">
                <div class="col-xl-8 order-2 order-xl-1">
                    <div class="form-group m-form__group row align-items-center">
                        <div class="col-md-3">
                            <div class="m-form__group m-form__group--inline">
                                <div class="m-input-icon m-input-icon--left">
                                    <button id = "textArea" <?php if($form->status == 'active'){ echo 'disabled'; } ?> style = "background-color:#800;border:none" class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--pill">
                                        <span>
                                            <i class="fl flaticon-signs"></i>
                                            <span>
                                                New Text Area
                                            </span>
                                        </span>
                                    </button>
                                </div>
                            </div>
                            <div class="d-md-none m--margin-bottom-10"></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="m-portlet__body">
                <!--begin::Section-->
                @if($form->status == 'active')
                <fieldset disabled="disabled">
                @endif
                <div class="m-accordion m-accordion--default" id="m_accordion_1" role="tablist">
                    <div id="m_sortable_portlets">
                        <!--begin::Item-->
                        <table class = "table table-striped" <?php if($form->status != 'active'){ echo 'id = "sort"'; } ?>>
                            <thead style = "text-align:center">
                                <tr>
                                    <th></th>
                                    <th title="Field #1">
                                        Header
                                    </th>
                                    <th title="Field #2">
                                        Actions
                                    </th>
                                </tr>
                            </thead>
                            <tbody id = "feedback">
                                @foreach ($feedback as $item)
                                    <tr data-id = "{{$item->id}}">
                                        <td>
                                            <i class = "fa fa-sort" id = "drag"></i>
                                        </td>
                                        <td>
                                            <input style = "text-align:center" type = "text" class = "form-control" placeholder = "Text here" id = "feedback_header" data-id = "{{$item->id}}" value = "{{$item->header}}">
                                        </td>
                                        <td>
                                            <center>
                                                <a id = "del-feedback" data-id = "{{$item->id}}" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete">
                                                    <i class="la la-trash"></i>
                                                </a>
                                            </center>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                        <!--end::Item-->
                </div>
                @if($form->status == 'active')
                </fieldset>
                @endif
                <!--end::Section-->
            </div>
            <!--end: Search Form -->
        </div>
    </div>
</div>