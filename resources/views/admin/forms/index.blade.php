@extends('admin.layouts.app')

@section('title', 'Evaluation Management')

@section('content')

    <div class="m-content">
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head" style = "background-color:#f4f4f4">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Forms
                        </h3>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body">
                <!--begin: Search Form -->
                <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                    <div class="row align-items-center">
                        <div class="col-xl-8 order-2 order-xl-1">
                            <div class="form-group m-form__group row align-items-center">
                                <div class="col-md-4">
                                    <div class="m-form__group m-form__group--inline">
                                        <div class="m-form__label">
                                            <label>
                                                Status:
                                            </label>
                                        </div>
                                        <div class="m-form__control">
                                            <select class="form-control m-bootstrap-select" id="m_form_status">
                                                <option value="">
                                                    All
                                                </option>
                                                <option value="3">
                                                    Inactive
                                                </option>
                                                <option value="2">
                                                    Active
                                                </option>
                                                <option value="1">
                                                    Pending
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="d-md-none m--margin-bottom-10"></div>
                                </div>
                                <div class="col-md-8">
                                    <div class="m-input-icon m-input-icon--left">
                                        <input type="text" class="form-control m-input" placeholder="Search..." id="generalSearch">
                                        <span class="m-input-icon__icon m-input-icon__icon--left">
                                            <span>
                                                <i class="la la-search"></i>
                                            </span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 order-1 order-xl-2 m--align-right">
                            <button style = "background-color: #800;border:none" class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--pill" data-toggle="modal" data-target="#newForm">
                                <span>
                                    <i class="fl flaticon-file"></i>
                                    <span>
                                        New Form
                                    </span>
                                </span>
                            </button>
                            <div class="m-separator m-separator--dashed d-xl-none"></div>
                        </div>
                        <div class="modal fade" id="newForm" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-header" style = "background-color:#f4f4f4">
                                        <h5 class="modal-title" id="">
                                            New Form
                                        </h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true" class="la la-remove"></span>
                                        </button>
                                    </div>
                                    {!! Form::open(['action' => 'Admin\FormController@store', 'method' => 'post']) !!}
                                        @csrf
                                        <div class="modal-body">
                                            <div class="form-group m-form__group row m--margin-top-20">
                                                <label class="col-form-label col-lg-3 col-sm-12">
                                                    Title
                                                </label>
                                                <div class="col-lg-7 col-md-9 col-sm-12">
                                                    <input type='text' class="form-control" placeholder="Enter Form Title" name = "title" required/>
                                                </div>
                                            </div>
                                            <div class="m-form__group form-group row">
                                                <label class="col-3 col-form-label">
                                                    Self Evaluation
                                                </label>
                                                <div class="col-9">
                                                    <div class="m-checkbox-list">
                                                        <label class="m-checkbox">
                                                            <input type="checkbox" name = "self[]" value = "Dean">
                                                            Dean
                                                            <span></span>
                                                        </label>
                                                        <label class="m-checkbox">
                                                            <input type="checkbox" name = "self[]" value = "Vice Dean">
                                                            Vice Dean
                                                            <span></span>
                                                        </label>
                                                        <label class="m-checkbox">
                                                            <input type="checkbox" name = "self[]" value = "Department Chair">
                                                            Department Chair
                                                            <span></span>
                                                        </label>
                                                        <label class="m-checkbox">
                                                            <input type="checkbox" name = "self[]" value = "Professor">
                                                            Professor
                                                            <span></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" class="m-btn--pill btn btn-success m-btn">
                                                Submit
                                            </button>
                                            <button type="button" class="m-btn--pill btn btn-danger m-btn" data-dismiss="modal">
                                                Close
                                            </button>
                                        </div>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end: Search Form -->
                <!--begin: Datatable -->
                <table class = "m-datatable" id="html_table" width="100%">
                    <thead style = "text-align:center">
                        <tr>
                            <th title="Field #1">
                                Title
                            </th>
                            <th title="Field #2">
                                Status
                            </th>
                            <th title = "Field #3">
                                Schedule
                            </th>
                            <th title = "Field #4">
                                Created at
                            </th>
                            <th title = "Field #5">
                                Updated at
                            </th>
                            <th title = "Field #6">
                                Actions
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($forms as $form)
                            <tr>
                                <td>
                                    {{ $form->title }}
                                </td>
                                <td>
                                    @if($form->status == 'pending')
                                        1
                                    @elseif($form->status == 'active')
                                        2
                                    @elseif(Session::get('type') == 'Super Admin' && $form->trashed())
                                        5
                                    @else 
                                        3
                                    @endif
                                </td>
                                <td>
                                    <center>
                                        @if(!empty($form->from))
                                            {{ Carbon\Carbon::parse($form->from)->format('M. d, Y h:m A') }} - {{ Carbon\Carbon::parse($form->to)->format('M. d, Y h:m A') }}
                                        @else 
                                            ---
                                        @endif
                                    </center>
                                </td>
                                <td>
                                    <center>
                                        {{$form->created_at->format('M d, Y')}}
                                    </center>
                                </td>
                                <td>
                                    <center>
                                        {{$form->updated_at->format('M d, Y')}}
                                    </center>
                                </td>
                                <td>
                                    <center>
                                        <a href="/forms/edit/{{$form->id}}" class="m-portlet__nav-link btn m-btn m-btn--hover-warning m-btn--icon m-btn--icon-only m-btn--pill" title="Edit details">
                                            <i class="la la-edit"></i>
                                        </a>
                                        <a href="/forms/manage-relationship/{{$form->id}}" class="m-portlet__nav-link btn m-btn m-btn--hover-primary m-btn--icon m-btn--icon-only m-btn--pill" title="Manage">
                                            <i class="la la-exclamation-circle"></i>
                                        </a>
                                        @if($form->trashed())
                                            <a href="#" data-toggle="modal" data-target="#m_modal_forceDel_{{$form->id}}" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Force Delete">
                                                <i class="la la-trash"></i>
                                            </a>
                                            <a href="#" data-toggle="modal" data-target="#m_modal_restore_{{ $form->id }}" class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill" title="Restore">
                                                <i class="la la-undo"></i>
                                            </a>
                                        @else 
                                            <a href="#" data-toggle="modal" data-target="#m_modal_delete_{{$form->id}}" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete">
                                                <i class="la la-trash"></i>
                                            </a>
                                        @endif
                                    </center>
                                </td>
                                {{-- delete modal --}}
                                <div class="modal fade" id="m_modal_delete_{{ $form->id }}" tabindex="-1" role="dialog" aria-labelledby="delete_{{ $form->id }}" aria-hidden="true">
                                    {!! Form::open(['action' => ['Admin\FormController@destroy', $form->id], 'method' => 'post']) !!}
                                        @csrf
                                        {!! Form::hidden('_method', 'DELETE') !!}
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                        <div class="modal-header" style = "background-color:#FAA900">
                                            <h5 class="modal-title" id="delete_{{ $form->id }}"><i class = "fa fa-warning"></i> &nbsp; Delete Form!</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body" style = "text-align:center">
                                            <p>All data from this form will be deleted including entries!</p>
                                            <p>Do you want to delete this form?</p>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" class="m-btn--pill btn btn-danger">
                                                Yes
                                            </button>
                                            <button type="button" class="m-btn--pill btn btn-metal" data-dismiss="modal">
                                                No
                                            </button>
                                        </div>
                                        </div>
                                    </div>
                                    {!! Form::close() !!}
                                </div>
                                {{-- force delete modal --}}
                                <div class="modal fade" id="m_modal_forceDel_{{ $form->id }}" tabindex="-1" role="dialog" aria-labelledby="m_modal_forceDel_{{ $form->id }}" aria-hidden="true">
                                    {!! Form::open(['action' => 'Admin\FormController@create', 'method' => 'get']) !!}
                                        @csrf
                                        <input type="hidden" value = "{{$form->id}}" name = "id">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                        <div class="modal-header" style = "background-color:#FAA900">
                                            <h5 class="modal-title" id="m_modal_forceDel_{{ $form->id }}"><i class = "fa fa-warning"></i> &nbsp; Delete Form!</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body" style = "text-align:center">
                                            <p>All data from this form will be deleted including entries permanently!</p>
                                            <p>Do you want to delete this form?</p>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" class="m-btn--pill btn btn-danger">
                                                Yes
                                            </button>
                                            <button type="button" class="m-btn--pill btn btn-metal" data-dismiss="modal">
                                                No
                                            </button>
                                        </div>
                                        </div>
                                    </div>
                                    {!! Form::close() !!}
                                </div>
                                {{-- restore modal --}}
                                <div class="modal fade" id="m_modal_restore_{{ $form->id }}" tabindex="-1" role="dialog" aria-labelledby="m_modal_restore_{{ $form->id }}" aria-hidden="true">
                                    <form action = "/forms/restore/{{$form->id}}" method = "get">
                                        @csrf
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                            <div class="modal-header" style = "background-color:#FAA900">
                                                <h5 class="modal-title" id="m_modal_restore_{{ $form->id }}"><i class = "fa fa-warning"></i> &nbsp; Restore</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body" style = "text-align:center">
                                                This form will be restored!
                                                <br>
                                                Do you wish to continue?
                                            </div>
                                            <div class="modal-footer">
                                                <button type="submit" class="m-btn--pill btn btn-warning">
                                                    Restore
                                                </button>
                                                <button type="button" class="m-btn--pill btn btn-metal" data-dismiss="modal">
                                                    Cancel
                                                </button>
                                            </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                <!--end: Datatable -->
            </div>
        </div>
    </div>

<script src="{{ asset('metronic/assets/demo/default/custom/components/datatables/base/html-table.js') }}" type="text/javascript"></script>

@endsection