@extends('admin.layouts.app')

@section('title', 'User Management')

@section('content')

<style>

  .avatar-upload {
    position: relative;
    max-width: 205px;
    margin: 10px auto;
    }
    .avatar-upload .avatar-edit {
    position: absolute;
    right: 12px;
    z-index: 1;
    top: 10px;
    }
    .avatar-upload .avatar-edit input, #imageUpload {
    display: none;
    }
    .avatar-upload .avatar-edit input + label {
    display: inline-block;
    width: 34px;
    height: 34px;
    margin-bottom: 0;
    border-radius: 100%;
    background: #FFFFFF;
    border: 1px solid transparent;
    box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.12);
    cursor: pointer;
    font-weight: normal;
    transition: all 0.2s ease-in-out;
    }
    .avatar-upload .avatar-edit input + label:hover {
    background: #f1f1f1;
    border-color: #d6d6d6;
    }
    .avatar-upload .avatar-edit input + label:after {
    content: "\f040";
    font-family: 'FontAwesome';
    color: #757575;
    position: absolute;
    top: 10px;
    left: 0;
    right: 0;
    text-align: center;
    margin: auto;
    }
    .avatar-upload .avatar-preview {
    width: 192px;
    height: 192px;
    position: relative;
    border-radius: 100%;
    border: 6px solid #F8F8F8;
    box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.1);
    }
    .avatar-upload .avatar-preview > div {
    width: 100%;
    height: 100%;
    border-radius: 100%;
    background-size: cover;
    background-repeat: no-repeat;
    background-position: center;
    }

    input[type='text']{
        text-transform: capitalize;
    }

    .faculty-img{
        border-radius:100%;
        width: 15%;
        margin-right:20px;
    }

    #html_table .btn{
        border-radius:50px;
    }
    i.fa.fa-caret-right{
        color: #800 !important;
    }
    i.fa.fa-caret-down{
        color: #800 !important;
    }
</style>

<div class="m-content">
        <!--begin:: Widgets/Stats-->
    <div class="m-portlet ">
        <div class="m-portlet__body  m-portlet__body--no-padding">
            <div class="row m-row--no-padding m-row--col-separator-xl">
                <div class="col-md-12 col-lg-6 col-xl-3">
                    <!--begin::Total Profit-->
                    <div class="m-widget24">
                        <div class="m-widget24__item">
                            <h4 class="m-widget24__title">
                                Faculties
                            </h4>
                            <br>
                            <span class="m-widget24__desc">
                                All Faculties
                            </span>
                            <span class="m-widget24__stats m--font-accent">
                                <?php
                                    $allFaculty = App\Faculty::all();
                                ?>
                                {{ count($allFaculty) }}
                            </span>
                            <div class="m--space-10"></div>
                            <?php
                                $class = App\ClassLoad::distinct()->get(['facultyNo']);
                                $enrolled = 0;
                                foreach($class as $load){
                                    $faculty = App\Faculty::where('facultyNo', $load->facultyNo)->first();
                                    if(!empty($faculty)){
                                        $enrolled = ++$enrolled;
                                    }
                                }
                            ?>
                            @if( $enrolled == 0 && count($allFaculty) == 0 )
                                <div class="progress m-progress--sm">
                                    <div class="progress-bar m--bg-accent" role="progressbar" style="width: 0%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <span class="m-widget24__change">
                                    Current Faculties
                                </span>
                                <span class="m-widget24__number">
                                    0%
                                </span>
                            @else 
                                <div class="progress m-progress--sm">
                                    <div class="progress-bar m--bg-accent" role="progressbar" style="width: {{ ($enrolled/count($allFaculty)) * 100 }}%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <span class="m-widget24__change">
                                    Current Faculties
                                </span>
                                <span class="m-widget24__number">
                                    {{ round(($enrolled/count($allFaculty)) * 100) }}%
                                </span>
                            @endif
                        </div>
                    </div>
                    <!--end::Total Profit-->
                </div>
                <div class="col-md-12 col-lg-6 col-xl-3" style = "padding:10px">
                    <table class = "table" style = "text-align:left">
                        <thead>
                            <tr>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <i class = "fa fa-user"></i> &nbsp; Dean
                                </td>
                                <td>
                                    <?php
                                        $dean = App\Faculty::where('type', 'Dean')->get();
                                        echo count($dean);
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <i class = "fa fa-user"></i> &nbsp; Vice Dean
                                </td>
                                <td>
                                    <?php
                                        $vice = App\Faculty::where('type', 'Vice Dean')->get();
                                        echo count($vice);
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <i class = "fa fa-user"></i> &nbsp; Department Chair
                                </td>
                                <td>
                                    <?php
                                        $dept = App\Faculty::where('type', 'Department Chair')->get();
                                        echo count($dept);
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <i class = "fa fa-user"></i> &nbsp; Professor
                                </td>
                                <td>
                                    <?php
                                        $prof = App\Faculty::where('type', 'Professor')->get();
                                        echo count($prof);
                                    ?>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-md-12 col-lg-6 col-xl-3">
                    <!--begin::New Feedbacks-->
                    <div class="m-widget24">
                        <div class="m-widget24__item">
                            <h4 class="m-widget24__title">
                                Pending
                            </h4>
                            <br>
                            <span class="m-widget24__desc">
                                Status
                            </span>
                            <span class="m-widget24__stats m--font-danger">
                                <?php
                                    $penRes = 0;
                                    $penTot = 0;
                                    $pending = App\Faculty::all();
                                    foreach($pending as $pen){
                                        if($pen->status == 'pending'){
                                            $penRes = ++$penRes;
                                        }
                                        $penTot = ++$penTot;
                                    }
                                ?>
                                {{ $penRes }}
                            </span>
                            <div class="m--space-10"></div>
                            @if( $penRes == 0 && $penTot == 0 )
                                <div class="progress m-progress--sm">
                                    <div class="progress-bar m--bg-danger" role="progressbar" style="width: 0%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <span class="m-widget24__change">
                                    Progress
                                </span>
                                <span class="m-widget24__number">
                                    0%
                                </span>
                            @else 
                                <div class="progress m-progress--sm">
                                    <div class="progress-bar m--bg-danger" role="progressbar" style="width: {{ ($penRes/$penTot) * 100 }}%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <span class="m-widget24__change">
                                    Progress
                                </span>
                                <span class="m-widget24__number">
                                    {{ round(($penRes/$penTot) * 100) }}%
                                </span>
                            @endif
                        </div>
                    </div>
                    <!--end::New Feedbacks-->
                </div>
                <div class="col-md-12 col-lg-6 col-xl-3">
                    <!--begin::New Orders-->
                    <div class="m-widget24">
                        <div class="m-widget24__item">
                            <h4 class="m-widget24__title">
                                Completed
                            </h4>
                            <br>
                            <span class="m-widget24__desc">
                                Status
                            </span>
                            <span class="m-widget24__stats m--font-success">
                                <?php
                                    $comRes = 0;
                                    $comTot = 0;
                                    $completed = App\Faculty::all();
                                    foreach($completed as $com){
                                        if($com->status == 'completed'){
                                            $comRes = ++$comRes;
                                        }
                                        $comTot = ++$comTot;
                                    }
                                ?>
                                {{ $comRes }}
                            </span>
                            <div class="m--space-10"></div>
                            @if( $comRes == 0 && $comTot == 0 )
                                <div class="progress m-progress--sm">
                                    <div class="progress-bar m--bg-success" role="progressbar" style="width: 0%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <span class="m-widget24__change">
                                    Progress
                                </span>
                                <span class="m-widget24__number">
                                    0%
                                </span>
                            @else 
                                <div class="progress m-progress--sm">
                                    <div class="progress-bar m--bg-success" role="progressbar" style="width: {{ ($comRes/$comTot) * 100 }}%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <span class="m-widget24__change">
                                    Progress
                                </span>
                                <span class="m-widget24__number">
                                    {{ round(($comRes/$comTot) * 100) }}%
                                </span>
                            @endif
                        </div>
                    </div>
                    <!--end::New Orders-->
                </div>
            </div>
        </div>
    </div>
    <!--end:: Widgets/Stats--> 
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head" style = "background-color:#f4f4f4">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        Faculty
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-portlet__body">
            <!--begin: Search Form -->
            <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                <div class="row align-items-center">
                    <div class="col-xl-8 order-2 order-xl-1">
                        <div class="form-group m-form__group row align-items-center">
                            <div class="col-md-3">
                                <div class="m-form__group m-form__group--inline">
                                    <div class="m-form__label">
                                        <label>
                                            Type:
                                        </label>
                                    </div>
                                    <div class="m-form__control">
                                        <select class="form-control m-bootstrap-select" id="m_form_type">
                                            <option value="">
                                                All
                                            </option>
                                            <option value="dean">
                                                Dean
                                            </option>
                                            <option value="vice dean">
                                                Vice Dean
                                            </option>
                                            <option value="department chair">
                                                Department Chair
                                            </option>
                                            <option value="professor">
                                                Professor
                                            </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="d-md-none m--margin-bottom-10"></div>
                            </div>
                            <div class="col-md-4">
                                <div class="m-form__group m-form__group--inline">
                                    <div class="m-form__label">
                                        <label>
                                            Department:
                                        </label>
                                    </div>
                                    <div class="m-form__control">
                                        <select class="form-control m-bootstrap-select" id="m_form_department">
                                            <option value="">
                                                All
                                            </option>
                                            @foreach ($departments as $item)
                                                <option value = "{{$item->department}}">{{$item->department}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="d-md-none m--margin-bottom-10"></div>
                            </div>
                            <div class="col-md-5">
                                <div class="m-input-icon m-input-icon--left">
                                    <input type="text" class="form-control m-input" placeholder="Search..." id="generalSearch">
                                    <span class="m-input-icon__icon m-input-icon__icon--left">
                                        <span>
                                            <i class="la la-search"></i>
                                        </span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 order-1 order-xl-2 m--align-right">
                        <button style = "background-color:#800;border:none" id = "assign" class="btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--pill" data-toggle="modal" data-target="#m_assign">
                            <span>
                                <i class="fl flaticon-user-add"></i>
                                <span>
                                    Assign Faculty
                                </span>
                            </span>
                        </button>
                        <div class="m-separator m-separator--dashed d-xl-none"></div>
                    </div>

                    <div class="modal fade" id="m_assign" tabindex="-1" role="dialog" aria-labelledby="m_assign" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-header" style = "background-color:#f4f4f4">
                                    <h5 class="modal-title" id="m_assign">
                                        Assign Faculty
                                    </h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">
                                            &times;
                                        </span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <table class = "table">
                                        <thead style = "text-align:center">
                                            <tr>
                                                <th title="Field #1">
                                                    Name
                                                </th>
                                                <th title="Field #2">
                                                    Type
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody id = "tbody">
                                        </tbody>
                                    </table>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" id = "submitAssign" class="m-btn--pill btn btn-success">
                                        Submit
                                    </button>
                                    <button type="button" class="m-btn--pill btn btn-danger" data-dismiss="modal">
                                        Close
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <!--end: Search Form -->
            <!--begin: Datatable -->
            <div class="m_datatable_faculty" id="child_data_local"></div>
            <!--end: Datatable -->
        </div>
    </div>
</div>

<script src="{{ asset('metronic/assets/demo/default/custom/components/datatables/child/data-local.js') }}" type="text/javascript"></script>

<script>

    $('#child_data_local').on('click', 'input:checkbox[data-id]', function(){

        $('#tbody > tr').each(function(){
            $(this).detach();
        });
        
        $('#child_data_local input:checkbox[name=select]:checked').each(function(){

            var html = '';

            if($(this).attr('data-type') == 'Professor'){
                html = '<tr>' +
                            '<td>' + $(this).val() + '</td>' +
                            '<td>' +
                                '<select class="form-control" id="type" data-id = "'+$(this).attr('data-id')+'">' +
                                    '<option value = "Dean">Dean</option>' +
                                    '<option value = "Vice Dean">Vice Dean</option>' +
                                    '<option value = "Department Chair">Department Chair</option>' +
                                    '<option value = "Professor" selected>Professor</option>' +
                                '</select>' +
                            '</td>' +
                        '</tr>';
            }else if ($(this).attr('data-type') == 'Dean'){
                html = '<tr>' +
                            '<td>' + $(this).val() + '</td>' +
                            '<td>' +
                                '<select class="form-control" id="type" data-id = "'+$(this).attr('data-id')+'">' +
                                    '<option value = "Dean" selected>Dean</option>' +
                                    '<option value = "Vice Dean">Vice Dean</option>' +
                                    '<option value = "Department Chair">Department Chair</option>' +
                                    '<option value = "Professor">Professor</option>' +
                                '</select>' +
                            '</td>' +
                        '</tr>';
            }else if ($(this).attr('data-type') == 'Vice Dean'){
                html = '<tr>' +
                            '<td>' + $(this).val() + '</td>' +
                            '<td>' +
                                '<select class="form-control" id="type" data-id = "'+$(this).attr('data-id')+'">' +
                                    '<option value = "Dean">Dean</option>' +
                                    '<option value = "Vice Dean" selected>Vice Dean</option>' +
                                    '<option value = "Department Chair">Department Chair</option>' +
                                    '<option value = "Professor">Professor</option>' +
                                '</select>' +
                            '</td>' +
                        '</tr>';
            }else{
                html = '<tr>' +
                            '<td>' + $(this).val() + '</td>' +
                            '<td>' +
                                '<select class="form-control" id="type" data-id = "'+$(this).attr('data-id')+'">' +
                                    '<option value = "Dean">Dean</option>' +
                                    '<option value = "Vice Dean" >Vice Dean</option>' +
                                    '<option value = "Department Chair" selected>Department Chair</option>' +
                                    '<option value = "Professor">Professor</option>' +
                                '</select>' +
                            '</td>' +
                        '</tr>';
            }

            
            $('#tbody').append(html);
        });
    });

    $('#submitAssign').click(function(){
        var array = [];
        $('#tbody > tr').each(function(){
            array.push({'id':$('#tbody #type[data-id]').attr('data-id'), 'type':$('#tbody #type[data-id]').val()});
        });
        $.ajax({ 
            url: '/faculty/type',
            type: 'get',
            data: { array: array },
            success: function(){
                location.reload();
            }
        });
    });

</script>

@endsection