@extends('admin.layouts.app')

@section('title', 'User Management')

@section('content')

<style>

    .avatar-upload {
    position: relative;
    max-width: 205px;
    margin: 10px auto;
    }
    .avatar-upload .avatar-edit {
    position: absolute;
    right: 12px;
    z-index: 1;
    top: 10px;
    }
    .avatar-upload .avatar-edit input, #imageUpload {
    display: none;
    }
    .avatar-upload .avatar-edit input + label {
    display: inline-block;
    width: 34px;
    height: 34px;
    margin-bottom: 0;
    border-radius: 100%;
    background: #FFFFFF;
    border: 1px solid transparent;
    box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.12);
    cursor: pointer;
    font-weight: normal;
    transition: all 0.2s ease-in-out;
    }
    .avatar-upload .avatar-edit input + label:hover {
    background: #f1f1f1;
    border-color: #d6d6d6;
    }
    .avatar-upload .avatar-edit input + label:after {
    content: "\f040";
    font-family: 'FontAwesome';
    color: #757575;
    position: absolute;
    top: 10px;
    left: 0;
    right: 0;
    text-align: center;
    margin: auto;
    }
    .avatar-upload .avatar-preview {
    width: 192px;
    height: 192px;
    position: relative;
    border-radius: 100%;
    border: 6px solid #F8F8F8;
    box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.1);
    }
    .avatar-upload .avatar-preview > div {
    width: 100%;
    height: 100%;
    border-radius: 100%;
    background-size: cover;
    background-repeat: no-repeat;
    background-position: center;
    }

    input[type='number']::-webkit-outer-spin-button,
    input[type='number']::-webkit-inner-spin-button {
        -webkit-appearance: none;
    }

    #fullname, #setFname{
        text-transform: capitalize;
    }

</style>

    <div class="m-content">
        <div class="row">
            <div class="col-xl-3 col-lg-4">
                <div class="m-portlet m-portlet--full-height  ">
                    <div class="m-portlet__body">
                        <div class="m-card-profile">
                            <div class="m-card-profile__title m--hide">
                                Your Profile
                            </div>
                            <div class="m-card-profile__pic">
                                <div class="avatar-upload">
                                    <div class="avatar-edit">
                                        <input type='file'/>
                                        <label onclick = "$('.input').click();"></label>
                                    </div>
                                    <div class="avatar-preview">
                                        <div id="imagePreview" style="background-image: url('/storage/profiles/{{ $user->image }}');">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="m-card-profile__details">
                                <span class="m-card-profile__name">
                                    <span id = "setFname">{{ $user->name }}</span>
                                </span>
                                <a href="" class="m-card-profile__email m-link" id = "setEmail">
                                    <span id = "setEmail">{{ $user->email }}</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-9 col-lg-8">
                <div class="m-portlet m-portlet--full-height m-portlet--tabs  ">
                    <div class="m-portlet__head" style = "background-color:#f4f4f4">
                        <div class="m-portlet__head-tools">
                            <ul class="nav nav-tabs m-tabs m-tabs-line   m-tabs-line--left m-tabs-line--default" role="tablist">
                                <li class="nav-item m-tabs__item">
                                    <a class="nav-link m-tabs__link active" data-toggle="tab" href="#m_user_profile_tab_1" role="tab">
                                        <i class="flaticon-share m--hide"></i>
                                        Faculty Profile
                                    </a>
                                </li>
                                <li class="nav-item m-tabs__item">
                                    <a class="nav-link m-tabs__link" data-toggle="tab" href="#m_user_profile_tab_3" role="tab">
                                        Settings
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="tab-content">
                        <div class="tab-pane active" id="m_user_profile_tab_1">
                            {!! Form::open(['action' => ['Admin\FacultyController@update', $user->id], 'enctype' => 'multipart/form-data', 'method' => 'post', 'class' => 'm-form m-form--fit m-form--label-align-right']) !!}
                                @csrf
                                {!! Form::hidden('_method', 'PUT') !!}
                                <input type="hidden" name = "image" value = "{{ $user->image }}">
                                <input type='file' name = "img" id="imageUpload" accept=".png, .jpg, .jpeg" class = "input" capture/>
                            <div class="m-portlet__body">
                                <div class="form-group m-form__group row">
                                    <div class="col-10 ml-auto">
                                        <h3 class="m-form__section">
                                            Faculty Details
                                        </h3>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-2 col-form-label">
                                        Title
                                    </label>
                                    <div class="col">
                                        <?php
                                            $split = explode('.', $user->name);
                                        ?>
                                        <select class="form-control m-input m-input--air" id = "title" name = "title">
                                            <option value="">Choose...</option>
                                            <option value="Prof" <?php if($split[0] == 'Prof'){echo'selected';} ?>>Prof.</option>
                                            <option value="Dr" <?php if($split[0] == 'Dr'){echo'selected';} ?>>Dr.</option>
                                            <option value="Eng" <?php if($split[0] == 'Eng'){echo'selected';} ?>>Eng.</option>
                                            <option value="Atty" <?php if($split[0] == 'Atty'){echo'selected';} ?>>Atty.</option>
                                        </select>
                                    </div>
                                    <label for="example-text-input" class="col-2 col-form-label">
                                        Full Name
                                    </label>
                                    <div class="col">
                                        <input class="form-control m-input m-input--air" id = "fullname" type="text" value="{{ $user->name }}" name = "name" required>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-2 col-form-label">
                                        Faculty Number
                                    </label>
                                    <div class="col">
                                        @if(Session::get('type') == 'Super Admin')
                                            <input class="form-control m-input m-input--air" type="text" value="{{$user->facultyTrashed->facultyNo}}" name = "facultyNo" required>
                                        @else 
                                            <input class="form-control m-input m-input--air" type="text" value="{{$user->faculty->facultyNo}}" name = "facultyNo" required>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-2 col-form-label">
                                        Department
                                    </label>
                                    <div class="col-4">
                                        <select class="form-control m-input m-input--air" name = "department">
                                            @foreach($departments as $item)
                                                @if(Session::get('type') == 'Super Admin')
                                                    <option value="{{$item->department}}" <?php if($user->facultyTrashed->department == $item->department){ echo 'selected'; } ?>>{{$item->department}}</option>
                                                @else 
                                                    <option value="{{$item->department}}" <?php if($user->faculty->department == $item->department){ echo 'selected'; } ?>>{{$item->department}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="m-form__seperator m-form__seperator--dashed m-form__seperator--space-2x"></div>
                                <div class="form-group m-form__group row">
                                    <div class="col-10 ml-auto">
                                        <h3 class="m-form__section">
                                            Account
                                        </h3>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-2 col-form-label">
                                        Username
                                    </label>
                                    <div class="col">
                                        <input class="form-control m-input m-input--air" type="text" id = "username" name = "username" value = "{{$user->username}}" required>
                                    </div>
                                    <label for="example-text-input" class="col-2 col-form-label">
                                        Email
                                    </label>
                                    <div class="col">
                                        <input class="form-control m-input m-input--air" id = "email" type="email" value="{{ $user->email }}" name = "email">
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-2 col-form-label">
                                        Change Password
                                    </label>
                                    <div class="col-4">
                                        <input class="form-control m-input m-input--air" type="password" id = "password" name = "password">
                                    </div>
                                    <label for="example-text-input" class="col-2 col-form-label">
                                        Confirm Password
                                    </label>
                                    <div class="col-4">
                                        <input class="form-control m-input m-input--air" type="password" id = "confirm_password">
                                    </div>
                                </div>
                            </div>
                            <div class="m-portlet__foot m-portlet__foot--fit">
                                <div class="m-form__actions">
                                    <div class="row">
                                        <div class="col-2"></div>
                                        <div class="col-7">
                                            <button type="submit" class="m-btn--pill btn btn-success m-btn m-btn--air m-btn--custom">
                                                Submit
                                            </button>
                                            &nbsp;&nbsp;
                                            <a href = "/faculty" class="m-btn--pill btn btn-danger m-btn m-btn--air m-btn--custom">
                                                Cancel
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                        <div class="tab-pane " id="m_user_profile_tab_3">
                            <div class="m-portlet__body">
                                <div class="form-group m-form__group row">
                                    <div class="col-10 ml-auto">
                                        <h3 class="m-form__section">
                                            Privacy
                                        </h3>
                                    </div>
                                </div>
                                <div class="m-form__group form-group row">
                                    <label class="col-3 col-form-label">
                                        Two-Factor Authentication (2FA)
                                    </label>
                                    <div class="col-3">
                                        <span class="m-switch m-switch--outline m-switch--icon m-switch--success">
                                            <label>
                                                <input type="checkbox" data = "{{ $user->id }}" id = "2fa" 
                                                    <?php
                                                        if($user->twoFA == 1){
                                                            echo 'checked';
                                                        }
                                                    ?>>
                                                <span></span>
                                            </label>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<script>

    $('#2fa').change(function(){
        $.ajax({
            url: '/faculty/2fa',
            type: 'get',
            data: { id: $(this).attr('data') }
        });
    });

    function readURL(input) { 
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#imagePreview').css('background-image', 'url(' + e.target.result + ')');
                $('#imagePreview').hide();
                $('#imagePreview').fadeIn(650);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#imageUpload").change(function () {
        readURL(this);
    });

    $("#fullname").keyup(function(){
        $("#setFname").html($(this).val());
    });

    $("#email").keyup(function(){
        $("#setEmail").html($(this).val());
    });

    var password = document.getElementById("password")
    , confirm_password = document.getElementById("confirm_password");

    function validatePassword(){
    if(password.value != confirm_password.value) {
        confirm_password.setCustomValidity("Passwords Don't Match");
    } else {
        confirm_password.setCustomValidity('');
    }
    }

    password.onchange = validatePassword;
    confirm_password.onkeyup = validatePassword;

</script>

@endsection