<?php
    $blocks = [];
    $students = App\Student::where('program', $program)->get();
?>

@foreach ($students as $student)
    <?php array_push($blocks, $student->studentNo); ?>
@endforeach

<?php $cor = App\COR::whereIn('studentNo', $blocks)->distinct()->get(['section']); ?>

@foreach ($cor as $load)
    <tr>
        <td>
            {{ $load->section }}
        </td>
        <td>
            <?php $count = 0; $completed = 0; ?>
            @foreach ($blocks as $block)
                @foreach (App\COR::where('studentNo', $block)->where('section', $load->section)->distinct()->get(['studentNo']) as $class)
                    @if($class->student->status == 'completed')
                        <?php $completed = ++$completed; ?>
                    @endif
                    <?php $count = ++$count; ?>
                @endforeach
            @endforeach

            <?php $total = round(($completed/$count) * 100) ?>
            
            @if($total < 50)
                <?php 
                $rate = 'danger'; 
                ?>
            @elseif($total < 100)
                <?php 
                $rate = 'warning'; 
                ?>
            @else 
                <?php
                    $rate = 'success'; 
                    ?>
            @endif
            <span class="m-widget1__number m--font-{{$rate}}">
                <strong>
                    {{$total}}%
                </strong>
            </span>
            <div class="progress m-progress--sm">
                <div class="progress-bar m--bg-{{$rate}}" role="progressbar" style="width: {{$total}}%;" aria-valuenow="{{$total}}" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
        </td>
    </tr>
@endforeach