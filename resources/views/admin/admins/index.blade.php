@extends('admin.layouts.app')

@section('title', 'User Management')

@section('content')

<style>

  .avatar-upload {
    position: relative;
    max-width: 205px;
    margin: 10px auto;
    }
    .avatar-upload .avatar-edit {
    position: absolute;
    right: 12px;
    z-index: 1;
    top: 10px;
    }
    .avatar-upload .avatar-edit input, #imageUpload {
    display: none;
    }
    .avatar-upload .avatar-edit input + label {
    display: inline-block;
    width: 34px;
    height: 34px;
    margin-bottom: 0;
    border-radius: 100%;
    background: #FFFFFF;
    border: 1px solid transparent;
    box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.12);
    cursor: pointer;
    font-weight: normal;
    transition: all 0.2s ease-in-out;
    }
    .avatar-upload .avatar-edit input + label:hover {
    background: #f1f1f1;
    border-color: #d6d6d6;
    }
    .avatar-upload .avatar-edit input + label:after {
    content: "\f040";
    font-family: 'FontAwesome';
    color: #757575;
    position: absolute;
    top: 10px;
    left: 0;
    right: 0;
    text-align: center;
    margin: auto;
    }
    .avatar-upload .avatar-preview {
    width: 192px;
    height: 192px;
    position: relative;
    border-radius: 100%;
    border: 6px solid #F8F8F8;
    box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.1);
    }
    .avatar-upload .avatar-preview > div {
    width: 100%;
    height: 100%;
    border-radius: 100%;
    background-size: cover;
    background-repeat: no-repeat;
    background-position: center;
    }

    .fname, .lname, .mname{
        text-transform: capitalize;
    }

    .user-img{
        border-radius:100%;
        width: 15%;
        margin-right:20px;
    }

    #html_table button{
        border-radius:50px;
    }
</style>

<div class="m-content">
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head" style = "background-color:#f4f4f4">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        Admins
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-portlet__body">
            <!--begin: Search Form -->
            <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                <div class="row align-items-center">
                    <div class="col-xl-8 order-2 order-xl-1">
                        <div class="form-group m-form__group row align-items-center">
                            <div class="col-md-12">
                                <div class="m-input-icon m-input-icon--left">
                                    <input type="text" class="form-control m-input" placeholder="Search..." id="generalSearch">
                                    <span class="m-input-icon__icon m-input-icon__icon--left">
                                        <span>
                                            <i class="la la-search"></i>
                                        </span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 order-1 order-xl-2 m--align-right">
                        <button style = "background-color:#800;border:none" class="btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--pill" data-toggle="modal" data-target="#m_modal_5">
                            <span>
                                <i class="fl flaticon-user-add"></i>
                                <span>
                                    New Admin
                                </span>
                            </span>
                        </button>
                        <div class="m-separator m-separator--dashed d-xl-none"></div>
                    </div>
                    <div class="modal fade" id="m_modal_5" tabindex="-1" role="dialog" aria-labelledby="m_modal_5" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                {!! Form::open(['action' => 'Admin\AdminsController@store', 'method' => 'post', 'enctype' => 'multipart/form-data']) !!}
                                    @csrf
                                <div class="modal-header" style = "background-color:#f4f4f4">
                                    <h5 class="modal-title" id="m_modal_5">
                                        New Admin
                                    </h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">
                                            &times;
                                        </span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="avatar-upload">
                                        <div class="avatar-edit">
                                            <input type='file' name = "img" id="imageUpload-newAdmin" accept=".png, .jpg, .jpeg" capture/>
                                            <label for="imageUpload-newAdmin"></label>
                                        </div>
                                        <div class="avatar-preview">
                                            <div id="imagePreview-newAdmin" style="background-image: url('/storage/profiles/default_profile.jpg');">
                                            </div>
                                        </div>
                                    </div>
                                    <div class = "form-group">
                                        <div class = "row">
                                            <div class = "col">
                                                <label for="firstname">Full Name</label>
                                                <input type="text" class="form-control m-input m-input--air fname" name = "name" placeholder="John Doe" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class = "form-group">
                                        <div class = "row">
                                            <div class = "col">
                                                <label for="username">Username</label>
                                                <input type="text" class = "form-control m-input m-input--air" name = "username" placeholder="johndoe" required>
                                            </div>
                                            <div class = "col">
                                                <label for="email">Email Address</label>
                                                <input type="email" class = "form-control m-input m-input--air" name = "email" placeholder="johndoe@example.com" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class = "form-group">
                                        <div class = "row">
                                            <div class = "col">
                                                <label for="password">Password</label>
                                                <input type="password" id = "password" name = "password" placeholder = "Password" class = "form-control m-input m-input--air" required>
                                            </div>
                                            <div class = "col">
                                                <label for="confirm_password">Confirm Password</label>
                                                <input type="password" id = "confirm_password" placeholder = "Confirm Password" class = "form-control m-input m-input--air" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class = "form-group">
                                        <label class="m-checkbox">
                                            <input type="checkbox" name = "isSuperAdmin">
                                            Super Admin
                                            <span></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="submit" class="m-btn--pill btn btn-success">
                                        Submit
                                    </button>
                                    <button type="button" class="m-btn--pill btn btn-danger" data-dismiss="modal">
                                        Close
                                    </button>
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end: Search Form -->
            <!--begin: Datatable -->
            <table class = "m-datatable" id="html_table" width="100%">
                <thead style = "text-align:center">
                    <tr>
                        <th title="Field #1">
                            Name
                        </th>
                        <th title="Field #2">
                            Email
                        </th>
                        <th title="Field #3">
                            Created At
                        </th>
                        <th title = "Field #4">
                            Status
                        </th>
                        <th title = "Field #5">
                            Type
                        </th>
                        <th title="Field #6">
                            Actions
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($users as $user)
                        <tr>
                            <td>
                                <img src="/storage/profiles/{{ $user->image }}" class = "user-img" alt="{{ $user->image }}">{{ $user->username }}
                            </td>
                            <td>
                                <center>
                                    {{ $user->email }}
                                </center>
                            </td>
                            <td>
                                <center>
                                    {{ $user->created_at->format('M. d, Y') }}
                                </center>
                            </td>
                            <td>
                                @if($user->activated == 1)
                                    4
                                @elseif($user->trashed())
                                    5
                                @else 
                                    1
                                @endif
                            </td>
                            <td>
                                @if($user->isAdmin == 1)
                                    1
                                @else
                                    2
                                @endif
                            </td>
                            <td>
                                <center>
                                    <a href="#" data-toggle="modal" data-target="#m_modal_{{ $user->id }}" class="m-portlet__nav-link btn m-btn m-btn--hover-warning m-btn--icon m-btn--icon-only m-btn--pill" title="Edit">
                                        <i class="la la-edit"></i>
                                    </a>
                                    @if(auth()->user()->id != $user->id)
                                        @if($user->trashed())
                                            <a href="#" data-toggle="modal" data-target="#m_modal_forceDel_{{ $user->id }}" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Force Delete">
                                                <i class="la la-trash"></i>
                                            </a>
                                            <a href="#" data-toggle="modal" data-target="#m_modal_restore_{{ $user->id }}" class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill" title="Restore">
                                                <i class="la la-undo"></i>
                                            </a>
                                        @else 
                                            <a href="#" data-toggle="modal" data-target="#m_modal_delete_{{ $user->id }}" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete">
                                                <i class="la la-trash"></i>
                                            </a>
                                        @endif
                                    @elseif(auth()->user()->id == $user->id)
                                        <a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-metal m-btn--icon m-btn--icon-only m-btn--pill" title="Soft Delete">
                                            <i class="la la-trash"></i>
                                        </a>
                                    @endif
                                </center>
                            </td>

                            {{-- delete modal --}}
                            <div class="modal fade" id="m_modal_delete_{{ $user->id }}" tabindex="-1" role="dialog" aria-labelledby="delete_{{ $user->id }}" aria-hidden="true">
                                {!! Form::open(['action' => ['Admin\AdminsController@destroy', $user->id], 'method' => 'post']) !!}
                                    @csrf
                                    {!! Form::hidden('_method', 'DELETE') !!}
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                    <div class="modal-header" style = "background-color:#FAA900">
                                        <h5 class="modal-title" id="delete_{{ $user->id }}"><i class = "fa fa-warning"></i> &nbsp; Soft Delete</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body" style = "text-align:center">
                                        This user will be deleted temporarily!
                                        <br>
                                        Do you wish to continue?
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" class="m-btn--pill btn btn-danger">
                                            Delete
                                        </button>
                                        <button type="button" class="m-btn--pill btn btn-metal" data-dismiss="modal">
                                            Cancel
                                        </button>
                                    </div>
                                    </div>
                                </div>
                                {!! Form::close() !!}
                            </div>

                            {{-- force delete modal --}}
                            <div class="modal fade" id="m_modal_forceDel_{{ $user->id }}" tabindex="-1" role="dialog" aria-labelledby="forceDel_{{ $user->id }}" aria-hidden="true">
                                {!! Form::open(['action' => ['Admin\AdminsController@destroy', $user->id], 'method' => 'post']) !!}
                                    @csrf
                                    {!! Form::hidden('_method', 'DELETE') !!}
                                    <input type="hidden" value = "1" name = "force">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                    <div class="modal-header" style = "background-color:#FAA900">
                                        <h5 class="modal-title" id="forceDel_{{ $user->id }}"><i class = "fa fa-warning"></i> &nbsp; Force Delete</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body" style = "text-align:center">
                                        This user will be deleted permanently!
                                        <br>
                                        Do you wish to continue?
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" class="m-btn--pill btn btn-danger">
                                            Delete
                                        </button>
                                        <button type="button" class="m-btn--pill btn btn-metal" data-dismiss="modal">
                                            Cancel
                                        </button>
                                    </div>
                                    </div>
                                </div>
                                {!! Form::close() !!}
                            </div>

                            {{-- restore modal --}}
                            <div class="modal fade" id="m_modal_restore_{{ $user->id }}" tabindex="-1" role="dialog" aria-labelledby="m_modal_restore_{{ $user->id }}" aria-hidden="true">
                                {!! Form::open(['action' => ['Admin\AdminsController@edit', $user->id], 'method' => 'get']) !!}
                                    @csrf
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                    <div class="modal-header" style = "background-color:#FAA900">
                                        <h5 class="modal-title" id="m_modal_restore_{{ $user->id }}"><i class = "fa fa-warning"></i> &nbsp; Restore</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body" style = "text-align:center">
                                        This user will be restored!
                                        <br>
                                        Do you wish to continue?
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" class="m-btn--pill btn btn-warning">
                                            Restore
                                        </button>
                                        <button type="button" class="m-btn--pill btn btn-metal" data-dismiss="modal">
                                            Cancel
                                        </button>
                                    </div>
                                    </div>
                                </div>
                                {!! Form::close() !!}
                            </div>

                            {{-- edit modal --}}
                            <div class="modal fade" id="m_modal_{{ $user->id }}" tabindex="-1" role="dialog" aria-labelledby="edit_{{ $user->id }}" aria-hidden="true">
                                {!! Form::open(['action' => ['Admin\AdminsController@update', $user->id], 'method' => 'post', 'enctype' => 'multipart/form-data']) !!}
                                    @csrf
                                    {!! Form::hidden('_method', 'PUT') !!}
                                    <input type="hidden" name = "image" value = "{{ $user->image }}">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header" style = "background-color:#f4f4f4">
                                            <h5 class="modal-title" id="edit_{{ $user->id }}">
                                                Edit Admin
                                            </h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">
                                                    &times;
                                                </span>
                                            </button>
                                        </div>
                                        <div class="modal-body">

                                            <input type='file' name = "img" id="imageUpload" class = "input{{$user->id}}" data = "{{$user->id}}" accept=".png, .jpg, .jpeg" capture/>
                                            
                                            <div class="avatar-upload" >
                                                <div class="avatar-edit">
                                                    <input type='hidden'/>
                                                    <label onclick = "$('.input{{$user->id}}').click();"></label>
                                                </div>
                                                <div class="avatar-preview">
                                                    <div id="imagePreview" data = "{{$user->id}}" style="background-image: url(/storage/profiles/{{$user->image}});">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class = "form-group">
                                                <div class = "row">
                                                    <div class = "col">
                                                        <label for="firstname">Full Name</label>
                                                        <input type="text" class="form-control m-input m-input--air fname" name = "name" value = "{{ $user->name }}" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class = "form-group">
                                                <div class = "row">
                                                    <div class = "col">
                                                        <label for="username">Username</label>
                                                        <input type="text" class = "form-control m-input m-input--air" name = "username" value = "{{ $user->username }}" required>
                                                    </div>
                                                    <div class = "col">
                                                        <label for="email">Email Address</label>
                                                        <input type="email" class = "form-control m-input m-input--air" name = "email" value = "{{ $user->email }}" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class = "form-group">
                                                <div class = "row">
                                                    <div class = "col">
                                                        <label for="password">Change Password</label>
                                                        <input type="password" id = "pass" placeholder = "Passowrd" class = "form-control m-input m-input--air" name = "password">
                                                    </div>
                                                    <div class = "col">
                                                        <label for="confirm_password">Confirm Password</label>
                                                        <input type="password" id = "rpass" placeholder = "Confirm Password" class = "form-control m-input m-input--air">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" class="m-btn--pill btn btn-success">
                                                Save changes
                                            </button>
                                            <button type="button" class="m-btn--pill btn btn-danger" data-dismiss="modal">
                                                Close
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <!--end: Datatable -->
        </div>
    </div>
</div>

<script src="{{ asset('metronic/assets/demo/default/custom/components/datatables/base/html-table.js') }}" type="text/javascript"></script>

<script>
    
    $("#imageUpload[data]").change(function () { 
        if (this.files && this.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#imagePreview[data]').css('background-image', 'url(' + e.target.result + ')');
                $('#imagePreview[data]').hide();
                $('#imagePreview[data]').fadeIn(650);
            };
            reader.readAsDataURL(this.files[0]);
        }
    });

    function readURL(input) { 
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#imagePreview-newAdmin').css('background-image', 'url(' + e.target.result + ')');
                $('#imagePreview-newAdmin').hide();
                $('#imagePreview-newAdmin').fadeIn(650);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#imageUpload-newAdmin").change(function () {
        readURL(this);
    });
    
    var password = document.getElementById("password")
    , confirm_password = document.getElementById("confirm_password");

    function validatePassword(){
    if(password.value != confirm_password.value) {
        confirm_password.setCustomValidity("Passwords Don't Match");
    } else {
        confirm_password.setCustomValidity('');
    }
    }

    password.onchange = validatePassword;
    confirm_password.onkeyup = validatePassword;

    var pass = document.getElementById("pass")
    , rpass = document.getElementById("rpass");

    function validatePassword(){
    if(pass.value != rpass.value) {
        rpass.setCustomValidity("Passwords Don't Match");
    } else {
        rpass.setCustomValidity('');
    }
    }

    pass.onchange = validatePassword;
    rpass.onkeyup = validatePassword;
</script>

@endsection