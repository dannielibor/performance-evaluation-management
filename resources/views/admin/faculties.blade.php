<table class = "m-datatable_facultyProgress" id="html_table" width="100%">
    <thead>
        <tr>
            <th></th>
        </tr>
    </thead>
    <tbody>
        @foreach (App\Faculty::where('department', $getDepartment)->get() as $faculty)
            <tr>
                <td>
                    <div class="m-widget4__item">
                        <div class="m-widget4__img m-widget4__img--pic">
                            <img src="/storage/profiles/{{$faculty->user->image}}" alt="{{$faculty->user->image}}">
                        </div>
                        <div class="m-widget4__info">
                            <span class="m-widget4__title">
                                {{$faculty->user->name}}
                            </span>
                            <br>
                            <span class="m-widget4__sub">
                                {{$faculty->department}}
                            </span>
                        </div>
        
                        <?php $completed = 0; $allForms = 0; ?>
        
                        @foreach (App\FormProgress::where('evaluator', $faculty->userId)->where('formId', $setForm)->get() as $progress)
                            @if($progress->status == 'completed')
                                <?php $completed = ++$completed; ?>
                            @endif
                            <?php $allForms = ++$allForms; ?>
                        @endforeach
        
                        @if( $completed == 0 && $allForms == 0 )
                            <?php $percent = 0; ?>
                        @else 
                            <?php 
                            $percent = round(($completed/$allForms)*100); 
                            ?>
                        @endif
        
                        @if($percent < 50)
                            <?php 
                            $rate = 'danger';
                                ?>
                        @elseif($percent < 100)
                            <?php 
                            $rate = 'warning';
                                ?>
                        @else 
                            <?php
                                $rate = 'success';
                                ?>
                        @endif
                        
                        <div class="m-widget4__progress">
                            <div class="m-widget4__progress-wrapper">
                                <span class="m-widget17__progress-number">
                                    {{ $percent }}%
                                </span>
                            </div>
                            <div class="progress m-progress--sm">
                                <div class="progress-bar m--bg-{{$rate}}" role="progressbar" style="width: {{$percent}}%;" aria-valuenow="{{$percent}}" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>
                        <div class="m-widget4__ext">
                            <a href="/faculty/edit/{{$faculty->userId}}" class="m-btn m-btn--pill btn btn-sm btn-metal" id = "hoverProfile">
                                Profile
                            </a>
                        </div>
                    </div>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>

<script src="{{ asset('metronic/assets/demo/default/custom/components/datatables/base/html-table.js') }}" type="text/javascript"></script>