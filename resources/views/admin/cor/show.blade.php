
    <div class="m-content" id = "m_1" style = "display:block">
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head" style = "background-color:#f4f4f4">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            COR Management
                        </h3>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body">
                <!--begin: Search Form -->
                <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                    <div class="row align-items-center">
                        <div class="col-xl-8 order-2 order-xl-1">
                            <div class="form-group m-form__group row align-items-center">
                                <div class="col-md-8">
                                    <div class="m-input-icon m-input-icon--left">
                                        <input type="text" class="form-control m-input" placeholder="Search..." id="generalSearch">
                                        <span class="m-input-icon__icon m-input-icon__icon--left">
                                            <span>
                                                <i class="la la-search"></i>
                                            </span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 order-1 order-xl-2 m--align-right">
                            <button id = "import" class="btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--pill" style = "background-color:#800;border:none">
                                <span>
                                    <i class="fl flaticon-interface-4"></i>
                                    <span>
                                        Import COR
                                    </span>
                                </span>
                            </button>
                            <div class="m-separator m-separator--dashed d-xl-none"></div>
                        </div>
                    </div>
                </div>
                <!--end: Search Form -->
                <!--begin: Datatable -->
                <table class = "m-datatable" id="html_table" width="100%">
                    <thead style = "text-align:center">
                        <tr>
                            <th title="Field #1">
                                Student Number
                            </th>
                            <th title="Field #2">
                                Name
                            </th>
                            <th title="Field #3">
                                Code
                            </th>
                            <th title = "Field #4">
                                Subject
                            </th>
                            <th title = "Field #5">
                                Section
                            </th>
                            <th title = "Field #6">
                                Schedule
                            </th>
                            <th title = "Field #7">
                                Room
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($cor as $item)
                            <tr>
                                <td>
                                    <center>
                                        {{ $item->studentNo }}
                                    </center>
                                </td>
                                <td>
                                    <img src="/storage/profiles/{{ $item->student->user->image }}" class = "user-img" alt="{{ $item->student->user->image }}">&nbsp;{{ $item->student->user->name }}
                                </td>
                                <td>
                                    <center>
                                        {{ $item->code }}
                                    </center>
                                </td>
                                <td>
                                    <center>
                                        {{ $item->subject }}
                                    </center>
                                </td>
                                <td>
                                    <center>
                                        {{$item->section}}
                                    </center>
                                </td>
                                <td>
                                    <center>
                                        {{ $item->schedule }}
                                    </center>
                                </td>
                                <td>
                                    <center>
                                        {{ $item->room }}
                                    </center>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                <!--end: Datatable -->
            </div>
        </div>
    </div>


    <div class="m-content" id = "m_2" style = "display:none">
        <!--Begin::Main Portlet-->
        <div class="m-portlet m-portlet--full-height">
            <!--begin: Portlet Head-->
            <div class="m-portlet__head" style = "background-color:#f4f4f4">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            <a href="#" id = "cancel" style = "background:none;border:none" class="btn btn-secondary m-btn m-btn--custom m-btn--icon" data-wizard-action="prev">
                                <span>
                                    <i class="la la-arrow-left"></i>
                                    &nbsp;&nbsp;
                                    <span>
                                        Import Certificate of Registration
                                    </span>
                                </span>
                            </a>
                        </h3>
                    </div>
                </div>
                <div class="m-portlet__head-tools">
                    <ul class="m-portlet__nav">
                        <li class="m-portlet__nav-item">
                            <a href="#" data-toggle="m-tooltip" class="m-portlet__nav-link m-portlet__nav-link--icon" data-direction="left" data-width="auto" title="Fill it up to import the csv file">
                                <i class="flaticon-info m--icon-font-size-lg3"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <!--end: Portlet Head-->
            <!--begin: Form Wizard-->
            <div class="m-wizard m-wizard--2 m-wizard--success" id="m_wizard">
                <!--begin: Message container -->
                <div class="m-portlet__padding-x">
                    <!-- Here you can put a message or alert -->
                </div>
                <!--end: Message container -->
                <!--begin: Form Wizard Head -->
                <div class="m-wizard__head m-portlet__padding-x">
                    <!--begin: Form Wizard Progress -->
                    <div class="m-wizard__progress">
                        <div class="progress">
                            <div class="progress-bar" role="progressbar"  aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                    </div>
                    <!--end: Form Wizard Progress -->  
                    <!--begin: Form Wizard Nav -->
                    <div class="m-wizard__nav">
                        <div class="m-wizard__steps">
                            <div class="m-wizard__step m-wizard__step--current"  data-wizard-target="#m_wizard_form_step_1">
                                <a href="#" class="m-wizard__step-number">
                                    <span>
                                        <i class="la la-info"></i>
                                    </span>
                                </a>
                                <div class="m-wizard__step-info">
                                    <div class="m-wizard__step-title">
                                        1. Academic Year Information
                                    </div>
                                    <div class="m-wizard__step-desc">
                                        Fill up information for this
                                        <br>
                                        academic year and term
                                    </div>
                                </div>
                            </div>
                            <div class="m-wizard__step" data-wizard-target="#m_wizard_form_step_2">
                                <a href="#" class="m-wizard__step-number">
                                    <span>
                                        <i class="la la-upload"></i>
                                    </span>
                                </a>
                                <div class="m-wizard__step-info">
                                    <div class="m-wizard__step-title">
                                        2. Import CSV file
                                    </div>
                                    <div class="m-wizard__step-desc">
                                        Browse CSV file
                                        <br>
                                        to be imported
                                    </div>
                                </div>
                            </div>
                            <div class="m-wizard__step" data-wizard-target="#m_wizard_form_step_3">
                                <a href="#" class="m-wizard__step-number">
                                    <span>
                                        <i class="la la-save"></i>
                                    </span>
                                </a>
                                <div class="m-wizard__step-info">
                                    <div class="m-wizard__step-title">
                                        3. Confirmation
                                    </div>
                                    <div class="m-wizard__step-desc">
                                        Kindly confirm all
                                        <br>
                                        imported data
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--end: Form Wizard Nav -->
                </div>
                <!--end: Form Wizard Head -->  
                <!--begin: Form Wizard Form-->
                <div class="m-wizard__form">
                    {!! Form::open(['action' => 'Admin\CorController@store', 'method' => 'post', 'enctype' => 'multipart/form-data', 'class' => 'm-form m-form--label-align-left- m-form--state-', 'id' => 'm_form']) !!}
                        @csrf
                        <!--begin: Form Body -->
                        <div class="m-portlet__body">
                            <!--begin: Form Wizard Step 1-->
                            <div class="m-wizard__form-step m-wizard__form-step--current" id="m_wizard_form_step_1">
                                <div class="row">
                                    <div class="col-xl-8 offset-xl-2">
                                        <div class="m-form__section m-form__section--first">
                                            <div class="m-form__heading">
                                                <h3 class="m-form__heading-title">
                                                    Academic Year / Term Details
                                                </h3>
                                            </div>
                                            <div class = "row">
                                                <div class = "col">
                                                    <div class="form-group m-form__group row">
                                                        <label class="col-xl-3 col-lg-3 col-form-label">
                                                            Year:
                                                        </label>
                                                        <div class="col-xl-9 col-lg-9">
                                                            <select name="year" id = "year" class = "form-control m-input m-input--air">
                                                                <option value="">Choose...</option>
                                                                <option value="{{ Carbon\Carbon::now()->format('Y')-1 }}-{{ Carbon\Carbon::now()->format('Y') }}">{{ Carbon\Carbon::now()->format('Y')-1 }}-{{ Carbon\Carbon::now()->format('Y') }}</option>
                                                                <option value="{{ Carbon\Carbon::now()->format('Y')-2 }}-{{ Carbon\Carbon::now()->format('Y')-1 }}">{{ Carbon\Carbon::now()->format('Y')-2 }}-{{ Carbon\Carbon::now()->format('Y')-1 }}</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class = "col">
                                                    <div class="form-group m-form__group row">
                                                        <label class="col-xl-3 col-lg-3 col-form-label">
                                                            Term:
                                                        </label>
                                                        <div class="col-xl-9 col-lg-9">
                                                            <select name="term" id="term" class = "form-control m-input m-input--air">
                                                                <option value="">Choose...</option>
                                                                <option value="1st">1st Semester</option>
                                                                <option value="2nd">2nd Semester</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--end: Form Wizard Step 1-->
                            <!--begin: Form Wizard Step 2-->
                            <div class="m-wizard__form-step" id="m_wizard_form_step_2">
                                <div class="row">
                                    <div class="col-xl-8 offset-xl-2">
                                        <div class="m-form__section m-form__section--first">
                                            <div class="file-upload">
                                                <div class="file-select">
                                                    <div class="file-select-button" id="fileName">Choose File</div>
                                                    <div class="file-select-name" id="noFile">No file chosen...</div> 
                                                    <input type="file" name="chooseFile" id="chooseFile">
                                                </div>
                                                <label>You can drag and drop here</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--end: Form Wizard Step 2--> 
                            <!--begin: Form Wizard Step 3-->
                            <div class="m-wizard__form-step" id="m_wizard_form_step_3">
                                <div class="row">
                                    <div class="col-xl-8 offset-xl-2">
                                        <!--begin::Section-->
                                        <div class="tab-content m--margin-top-40">
                                            <div class="tab-pane active" id="m_form_confirm_1" role="tabpanel">
                                                <div class="m-form__section m-form__section--first">
                                                    <div class="m-form__heading">
                                                        <h4 class="m-form__heading-title">
                                                            Academic Year / Term Details
                                                        </h4>
                                                    </div>
                                                    <div class="form-group m-form__group m-form__group--sm row">
                                                        <label class="col-xl-3 col-lg-3 col-form-label">
                                                            Year:
                                                        </label>
                                                        <div class="col-xl-9 col-lg-9">
                                                            <span class="m-form__control-static" id = "getYear"></span>
                                                        </div>
                                                    </div>
                                                    <div class="form-group m-form__group m-form__group--sm row">
                                                        <label class="col-xl-3 col-lg-3 col-form-label">
                                                            Term:
                                                        </label>
                                                        <div class="col-xl-9 col-lg-9">
                                                            <span class="m-form__control-static" id = "getTerm"></span>&nbsp;<span class="m-form__control-static">Semester</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="m-separator m-separator--dashed m-separator--lg"></div>
                                                <div class="m-form__section">
                                                    <div class="m-form__heading">
                                                        <h4 class="m-form__heading-title">
                                                            CSV File
                                                            <i data-toggle="m-tooltip" data-width="auto" class="m-form__heading-help-icon flaticon-info" title="File Details"></i>
                                                        </h4>
                                                    </div>
                                                    <div class="form-group m-form__group m-form__group--sm row">
                                                        <label class="col-xl-3 col-lg-3 col-form-label">
                                                            File Name:
                                                        </label>
                                                        <div class="col-xl-9 col-lg-9">
                                                            <span class="m-form__control-static" id = "name"></span>
                                                        </div>
                                                    </div>
                                                    <div class="form-group m-form__group m-form__group--sm row">
                                                        <label class="col-xl-3 col-lg-3 col-form-label">
                                                            Type:
                                                        </label>
                                                        <div class="col-xl-9 col-lg-9">
                                                            <span class="m-form__control-static" id = "type"></span>
                                                        </div>
                                                    </div>
                                                    <div class="form-group m-form__group m-form__group--sm row">
                                                        <label class="col-xl-3 col-lg-3 col-form-label">
                                                            Size:
                                                        </label>
                                                        <div class="col-xl-9 col-lg-9">
                                                            <span class="m-form__control-static" id = "size"></span>
                                                        </div>
                                                    </div>
                                                    <div class="form-group m-form__group m-form__group--sm row">
                                                        <label class="col-xl-3 col-lg-3 col-form-label">
                                                            Last Modified:
                                                        </label>
                                                        <div class="col-xl-9 col-lg-9">
                                                            <span class="m-form__control-static" id = "modified"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--end::Section-->
                                        <!--end::Section-->
                                        <div class="m-separator m-separator--dashed m-separator--lg"></div>
                                        <div class="form-group m-form__group m-form__group--sm row">
                                            <div class="col-xl-12">
                                                <div class="m-checkbox-inline">
                                                    <label class="m-checkbox m-checkbox--solid m-checkbox--info">
                                                        <input type="checkbox" name="accept" value="1">
                                                        Click here to indicate that you have read and agree to the terms presented in the Terms and Conditions agreement
                                                        <span></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--end: Form Wizard Step 3-->
                        </div>
                        <!--end: Form Body -->
                        <!--begin: Form Actions -->
                        <div class="m-portlet__foot m-portlet__foot--fit m--margin-top-40">
                            <div class="m-form__actions">
                                <div class="row">
                                    <div class="col-lg-2"></div>
                                    <div class="col-lg-4 m--align-left">
                                        <a href="#" class="btn btn-secondary m-btn m-btn--custom m-btn--icon" data-wizard-action="prev">
                                            <span>
                                                <i class="la la-arrow-left"></i>
                                                &nbsp;&nbsp;
                                                <span>
                                                    Back
                                                </span>
                                            </span>
                                        </a>
                                    </div>
                                    <div class="col-lg-4 m--align-right">
                                        <button type = "submit" class="btn btn-success m-btn m-btn--custom m-btn--icon" data-wizard-action="submit">
                                            <span>
                                                <i class="la la-check"></i>
                                                &nbsp;&nbsp;
                                                <span>
                                                    Submit
                                                </span>
                                            </span>
                                        </button>
                                        <a href = "#" class="btn btn-warning m-btn m-btn--custom m-btn--icon" data-wizard-action="next">
                                            <span>
                                                <span>
                                                    Save & Continue
                                                </span>
                                                &nbsp;&nbsp;
                                                <i class="la la-arrow-right"></i>
                                            </span>
                                        </a>
                                    </div>
                                    <div class="col-lg-2"></div>
                                </div>
                            </div>
                        </div>
                        <!--end: Form Actions -->
                    {!! Form::close() !!}
                </div>
                <!--end: Form Wizard Form-->
            </div>
            <!--end: Form Wizard-->
        </div>
        <!--End::Main Portlet-->
    </div>