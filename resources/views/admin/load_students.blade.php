<table class = "m-datatable_studentProgress" id="html_table" width="100%">
    <thead>
        <tr>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <?php
            $blocks = [];
            $students = App\Student::where('program', $program)->get();
        ?>
        
        @foreach ($students as $student)
            <?php array_push($blocks, $student->studentNo); ?>
        @endforeach
        
        <?php 
            if(isset($getSection)){
                $setClass = App\COR::whereIn('studentNo', $blocks)->where('section', $getSection)->where('code', $getCode)->get();
            }
        ?>

    @if(isset($setClass))
        @foreach ($setClass as $getClass)
            <tr>
                <td>
                    <div class="m-widget4__item">
                        <div class="m-widget4__img m-widget4__img--pic">
                            <img src="/storage/profiles/{{$getClass->student->user->image}}" alt="{{$getClass->student->user->image}}">
                        </div>
                        <div class="m-widget4__info">
                            <span class="m-widget4__title">
                                {{ $getClass->student->user->name }}
                            </span>
                            <br>
                            <span class="m-widget4__sub">
                                {{ $getClass->student->studentNo }}
                            </span>
                        </div>
        
                        <?php
                            $count = 0;
                            $completed = 0;
                            $result = 0;
                            $rate = 0;
                            $prof = App\ClassLoad::where('code', $getCode)->where('section', $getSection)->first();
                        ?>
                        @if(!empty($prof->faculty))
                            <?php $progresses = App\FormProgress::where('formId', $formId)->where('evaluator', $getClass->student->userId)->where('evaluatee', $prof->faculty->userId)->get(); ?>
                            @foreach ($progresses as $progress)
                                @if($progress->status == 'completed')
                                    <?php $completed = ++$completed; ?>
                                @endif
                                <?php $count = ++$count; ?>
                            @endforeach
            
                            @if($completed == 0 && $count == 0)
                                <?php $result = 0; ?>
                            @else 
                                <?php $result = round(($completed / $count) * 100) ?>
                            @endif
            
                            @if($result < 50)
                                <?php 
                                $rate = 'danger'; 
                                ?>
                            @elseif($result < 100)
                                <?php 
                                $rate = 'warning'; 
                                ?>
                            @else 
                                <?php 
                                $rate = 'success'; 
                                ?>
                            @endif
                        @endif
        
                        <div class="m-widget4__progress">
                            <div class="m-widget4__progress-wrapper">
                                <span class="m-widget17__progress-number">
                                    {{ $result }}%
                                </span>
                            </div>
                            <div class="progress m-progress--sm">
                                <div class="progress-bar m--bg-{{$rate}}" role="progressbar" style="width: {{$result}}%;" aria-valuenow="{{$result}}" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>
                        <div class="m-widget4__ext">
                            <a href="/students/edit/{{$getClass->student->userId}}" class="m-btn m-btn--pill btn btn-sm btn-metal" id = "hoverProfile">
                                Profile
                            </a>
                        </div>
        
                    </div>
                </td>
            </tr>
        @endforeach
    @endif
    </tbody>
</table>

<script src="{{ asset('metronic/assets/demo/default/custom/components/datatables/base/html-table.js') }}" type="text/javascript"></script>