<br>

@if(isset($elements) && count($elements) > 0)

<input type="hidden" value = "{{$total->total}}" id = "setTotal">

    @foreach ($elements as $element)

        <div id = "report" data-id = "{{$element->id}}">
            <div class="m-portlet m-portlet--mobile m-portlet--sortable m-accordion__item">
                <a href="#" style = "float:right" data-toggle="modal" data-target="#m_modal_delete_{{$element->id}}" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete">
                    <i class="la la-trash"></i>
                </a>
                <div style = "background-color:#F2F3F4" class="m-accordion__item-head collapsed" role="tab" id="m_accordion_1_item_1_head_{{$element->id}}" data-toggle="collapse" href="#m_accordion_1_item_1_body_{{$element->id}}" aria-expanded="false">
                    <span class="m-accordion__item-title">
                        <div class="d-flex flex-row bd-highlight mb-3">
                            <div class="p-2 bd-highlight">
                                <select class = "form-control" id="forms" data-id = "{{$element->id}}">
                                    <option value="">Choose...</option>
                                    @foreach ($forms as $form)
                                        <option value="{{$form->id}}" <?php if($form->id == $element->formId && $element->selfEvaluation == 0){echo'selected';} ?> >{{$form->title}}</option>
                                        @foreach (App\FormRestriction::where('formId', $form->id)->where('type', $element->type)->where('selfEvaluation', 1)->get() as $self)
                                            <option value="{{$self->formId}}|s" <?php if($form->id == $element->formId && $element->selfEvaluation == 1){echo'selected';} ?> >Self Evaluation - {{$self->form->title}}</option>
                                        @endforeach
                                    @endforeach
                                </select>
                            </div>
                            <div class="p-2 bd-highlight" id = "spin" data-id = "{{$element->id}}">
                                <input id="m_touchspin_3" readonly type="text" class="form-control elementRate" value="{{$element->rate}}" data-id = "{{$element->id}}">
                            </div>
                        </div>
                    </span>
                    <span class="m-accordion__item-mode"></span>
                </div>
                <div class="m-accordion__item-body collapse" id="m_accordion_1_item_1_body_{{$element->id}}" class=" " role="tabpanel" aria-labelledby="m_accordion_1_item_1_head_{{$element->id}}" data-parent="#m_accordion_1_{{$element->id}}">
                    <div class="m-accordion__item-content">
                        <div class="row align-items-center">
                            <div class="col-xl-8 order-2 order-xl-1">
                                <div class="form-group m-form__group row align-items-center">
                                    <div class="col-md-3">
                                        <div class="m-form__group m-form__group--inline">
                                            <div class="m-input-icon m-input-icon--left">
                                                <button id = "subelement" data-id = "{{$element->id}}" style = "background-color:#800;border:none" class="btn btn-success m-btn m-btn--custom m-btn--icon m-btn--pill">
                                                    <span>
                                                        <i class="la la-plus-circle"></i>
                                                        <span>
                                                            New Sub Element
                                                        </span>
                                                    </span>
                                                </button>
                                            </div>
                                        </div>
                                        <div class="d-md-none m--margin-bottom-10"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <table class = "table table-striped">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody id = "subposition" data-id = "{{$element->id}}">
                                <?php $elementId = $element->id; ?>
                                @include('admin.data_analysis.summative_report.sub_element')
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        {{-- delete modal --}}
        <div class="modal fade" id="m_modal_delete_{{ $element->id }}" tabindex="-1" role="dialog" aria-labelledby="delete_{{ $element->id }}" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                <div class="modal-header" style = "background-color:#FAA900">
                    <h5 class="modal-title" id="delete_{{ $element->id }}"><i class = "fa fa-warning"></i> &nbsp; Delete Element</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" style = "text-align:center">
                    This element will be deleted including its sub elements!
                    <br>
                    Do you wish to continue?
                </div>
                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" id = "delete" data-id = "{{$element->id}}" class="btn m-btn--pill btn-danger">
                        Delete
                    </button>
                    <button type="button" class="btn btn-metal m-btn--pill" data-dismiss="modal">
                        Cancel
                    </button>
                </div>
                </div>
            </div>
        </div>

    @endforeach

@endif

<script src="{{ asset('metronic/assets/demo/default/custom/components/forms/widgets/bootstrap-touchspin.js') }}" type="text/javascript"></script>

<script>

$(document).ready(function(){

    $('#m_sortable_portlets_report #delete[data-id]').click(function(){
        var id = $(this).attr('data-id');
        $.ajax({
            url: '/summative-report/element/destroy',
            type: 'get',
            data: { id: $(this).attr('data-id') },
            success: function(response){
                $('#m_sortable_portlets_report #report[data-id="'+id+'"]').detach();
                toastr.options = {
                    "closeButton": false,
                    "debug": false,
                    "newestOnTop": false,
                    "progressBar": false,
                    "positionClass": "toast-bottom-right",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                };
                toastr.success("Element has been successfully deleted!");
            }
        });
    });

    $('#subelement[data-id]').click(function(){
        var id = $(this).attr('data-id');
        $.ajax({
            url: '/summative-report/subelement/store',
            type: 'get',
            data: { id: $(this).attr('data-id') },
            success: function(response){
                $('#subposition[data-id="'+id+'"]').html(response);
                toastr.options = {
                    "closeButton": false,
                    "debug": false,
                    "newestOnTop": false,
                    "progressBar": false,
                    "positionClass": "toast-bottom-right",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                };
                toastr.success("New subelement has been successfully added!");
            }
        });
    });

    $('#total').text($('#setTotal').val());

    $('#forms[data-id]').click(function(e){
        e.stopPropagation();
    });

    $('#spin[data-id]').click(function(e){
        e.stopPropagation();
    });

    $('.elementRate[data-id]').change(function(){
        var num = $(this).val();
        var all = 0;
        $('.elementRate[data-id]').each(function(){
            all = all + parseInt($(this).val());
        });
        if(parseInt(all) > 100){
            $('#total').css('color', 'red');
            $(this).css('border', '1px solid red');
        }else{
            $('#total').css('color', '');
            $(this).css('border', '');
            $.ajax({
                url: '/summative-report/element/rate',
                type: 'get',
                data: { id: $(this).attr('data-id'), value: num }
            });
        }
        $('#total').text(parseInt(all));
    });

    $('#forms[data-id]').change(function(){
        $.ajax({
            url: '/summative-report/element/title',
            type: 'get',
            data: { id: $(this).attr('data-id'), value: $(this).val() }
        });
    });

});

</script>