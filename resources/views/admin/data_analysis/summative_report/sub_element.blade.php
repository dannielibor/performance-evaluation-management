@foreach (App\SubElement::where('elementId', $elementId)->get() as $sub)
    <tr id = "table_subEl" data-id = "{{$elementId}}" data = "{{$sub->id}}">
        <td style = "width:50%;" colspan = "2">
            <div class="d-flex flex-row bd-highlight mb-3">
                <div class="p-2 bd-highlight">
                    <select class = "form-control" id="types_subelement" data-id = "{{$elementId}}" data = "{{$sub->id}}">
                        <option value="">Choose...</option>
                        <option value="Dean/VDAA" <?php if('Dean/VDAA' == $sub->type){echo'selected';} ?>>Dean/VDAA</option>
                        <option value="Department Chair" <?php if('Department Chair' == $sub->type){echo'selected';} ?>>Department Chair</option>
                    </select>
                </div>
                <div class="p-2 bd-highlight" id = "Subspin" data-id = "{{$sub->id}}"> 
                    <input id="m_touchspin_3" readonly type="text" class="form-control subRate" value="{{$sub->rate}}" data-id = "{{$sub->id}}" data = "{{$elementId}}">
                </div>
            </div>
        </td>
        <td style = "width:3%">
            <center>
                <div id = "del" data-id = "{{$sub->id}}" data = "{{$elementId}}" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill">
                    <i class="la la-trash-o"></i>
                </div>
            </center>
        </td>
    </tr>
@endforeach

<script src="{{ asset('metronic/assets/demo/default/custom/components/forms/widgets/bootstrap-touchspin.js') }}" type="text/javascript"></script>

<script>

$(document).ready(function(){

    $('#m_sortable_portlets_report #types_subelement[data]').change(function(){
        $.ajax({
            url: '/summative-report/subelement/update',
            type: 'get',
            data: { id: $(this).attr('data'), value: $(this).val() }
        });
    });

    $('#m_sortable_portlets_report #del[data]').click(function(){
        $.ajax({
            url: '/summative-report/subelement/update/destroy',
            type: 'get',
            data: { id: $(this).attr('data-id') },
            success: function(){
                toastr.options = {
                    "closeButton": false,
                    "debug": false,
                    "newestOnTop": false,
                    "progressBar": false,
                    "positionClass": "toast-bottom-right",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                };
                toastr.success("Subelement has been successfully deleted!");
            }
        });
        $('#m_sortable_portlets_report #table_subEl[data="'+$(this).attr('data-id')+'"]').detach();
    });

    $('#m_sortable_portlets_report .subRate[data-id]').change(function(){
        var array = [];
        $('#m_sortable_portlets_report .subRate[data-id][data="'+$(this).attr('data')+'"]').each(function(){
            array.push(parseInt($(this).val()));
        });
        var total = 0;
        for (var i = 0; i < array.length; i++) {
            total += array[i] << 0;
        }   
        if( total > parseInt($('#m_sortable_portlets_report .elementRate[data-id="'+$(this).attr('data')+'"]').val()) ){
            $(this).css('border', '1px solid red');
        }else{
            $(this).css('border', '');
            $.ajax({
                url: '/summative-report/subelement/update/edit',
                type: 'get',
                data: { id: $(this).attr('data-id'), value: $(this).val() }
            });
        }
    });

});

</script>