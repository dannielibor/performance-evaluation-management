@extends('admin.layouts.app')

@section('title', 'Data Analysis')

@section('content')

    <div class="m-content">
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head" style = "background-color:#f4f4f4">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            <a href="javascript:history.back()" id = "cancel" style = "background:none;border:none" class="btn btn-secondary m-btn m-btn--custom m-btn--icon">
                                <span>
                                    <i class="la la-arrow-left"></i>
                                    &nbsp;&nbsp;
                                    <span>
                                        Edit Summative Report
                                    </span>
                                </span>
                            </a>
                        </h3>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body">
                <div class = "row align-items-center">
                    <div class="col-xl-4 order-2 order-xl-1">
                        <div class="form-group m-form__group row align-items-center">
                            <div class="col-md-12">
                                <select class="form-control" id = "elementType">
                                    <option value="">Choose...</option>
                                    <!-- @foreach ($types as $type)
                                        <option value="{{$type->type}}">
                                            {{$type->type}}
                                        </option>
                                    @endforeach -->
                                    <option value="Professor">Professor</option>
                                    <option value="Department Chair">Department Chair</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <center> 
                    <div class="d-flex justify-content-center align-items-center p-3 my-3">
                        <img class="mr-3" src="/images/sbca.png" alt="" width="50">
                        <div class="lh-100">
                            <h6 class="mb-0 lh-100"><font face="Old English Text MT" size="5">San Beda College Alabang</font></h6>
                            <small>Alabang Hills Village, Muntinlupa City</small>
                        </div>
                    </div>
                    <h2>FACULTY PERFORMANCE APPRAISAL <br> SUMMATIVE REPORT</h2>
                </center>

                <br><br>

                <div class = "row align-items-center">
                    <div class="col-xl-4 order-2 order-xl-1">
                        <button class="btn btn-success m-btn m-btn--custom m-btn--icon m-btn--pill" style = "background-color:#800;border:none" id = "newElement">
                            <span>
                                <i class="la la-plus-circle"></i>
                                <span>
                                    New Element
                                </span>
                            </span>
                        </button>
                    </div>
                    <div class="col-xl-8 order-1 order-xl-2 m--align-right">
                        <span>
                            Total: <span id = "total">0</span>%
                        </span>
                        <div class="m-separator m-separator--dashed d-xl-none"></div>
                    </div>
                </div>
                
                <div class="m-accordion m-accordion--default" id="m_accordion_1" role="tablist">
                    <div id="m_sortable_portlets_report" class = "element">
                        @include('admin.data_analysis.summative_report.element')
                    </div>
                </div>
                
            </div>
        </div>
    </div>
    
    <script src="{{ asset('metronic/assets/demo/default/custom/components/forms/widgets/bootstrap-touchspin.js') }}" type="text/javascript"></script>
    
    <script>

        $(document).ready(function(){

            $('#elementType').change(function(){
                $.ajax({
                    url: '/summative-report/element/load',
                    type: 'get',
                    data: { type: $(this).val() },
                    success: function(data){
                        $('.element').html(data);
                    }
                });
            });

            $('#newElement').click(function(){
                $.ajax({
                    url: '/summative-report/element/store',
                    type: 'get',
                    async: false,
                    data: { type: $('#elementType').val() },
                    success: function(response){ 
                        $('.element').html(response);
                        toastr.options = {
                            "closeButton": false,
                            "debug": false,
                            "newestOnTop": false,
                            "progressBar": false,
                            "positionClass": "toast-bottom-right",
                            "preventDuplicates": false,
                            "onclick": null,
                            "showDuration": "300",
                            "hideDuration": "1000",
                            "timeOut": "5000",
                            "extendedTimeOut": "1000",
                            "showEasing": "swing",
                            "hideEasing": "linear",
                            "showMethod": "fadeIn",
                            "hideMethod": "fadeOut"
                        };
                        toastr.success("New element has been successfully added!");
                    }
                });
            });

        });

    </script>

@endsection