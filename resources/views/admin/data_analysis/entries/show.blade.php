@extends('admin.layouts.app')

@section('title', 'Data Analysis')

@section('content')

<div class = "m-content">
    <input type="hidden" value = "{{$form->id}}" id = "formId">
    <input type="hidden" value = "{{$year}}" id = "passYear">
    <input type="hidden" value = "{{$term}}" id = "passTerm">
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__body">
            <!--begin: Search Form -->
            <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                <div class="m-form__group m-form__group--inline">
                    <div class="m-form__label">
                        <label>
                            Faculty:
                        </label>
                    </div>
                    <div class="m-form__control">
                        <select class="form-control m-select2 faculty" id="m_select2_2" style = "width:100%">
                            <option value="">Choose...</option>
                            @foreach (App\Faculty::distinct()->get(['department']) as $department)
                                <optgroup label="{{$department->department}}">
                                    @foreach (App\Faculty::where('department', $department->department)->get() as $faculty)
                                        <option value="{{$faculty->userId}}">
                                            {{$faculty->user->name}}
                                        </option>
                                    @endforeach
                                </optgroup>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <!--end: Search Form -->
        </div>
    </div>

    <!--Begin::Section-->
    <div class="row">
        <div class="col-xl-6">
            <!--begin:: Widgets/Top Products-->
            <div class="m-portlet m-portlet--bordered-semi m-portlet--full-height ">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                Entries
                            </h3>
                        </div>
                    </div>
                </div>
                <div class="m-portlet__body" id = "entries">
                    <!--begin::Section-->
                    @if(isset($user))
                        @include('admin.data_analysis.entries.entries')
                    @else 
                        <div class="m-accordion m-accordion--default m-accordion--solid" id="m_accordion_3" role="tablist">
                            @foreach (App\Body::where('formId', $form->id)->orderByRaw('LENGTH(position)', 'asc')->orderBy('position', 'asc')->get() as $body)
                                <!--begin::Item-->
                                <div class="m-accordion__item">
                                    <div class="m-accordion__item-head collapsed" style = "background-color:#f4f4f4" role="tab" id="m_accordion_3_item_1_head{{$body->id}}" data-toggle="collapse" href="#m_accordion_3_item_1_body{{$body->id}}" aria-expanded="    false">
                                        <span class="m-accordion__item-title">
                                            {{ $body->header }} ({{ $body->rate }}%)
                                        </span>
                                        <span class="m-accordion__item-mode"></span>
                                    </div>
                                    <div class="m-accordion__item-body collapse" id="m_accordion_3_item_1_body{{$body->id}}" class=" " role="tabpanel" aria-labelledby="m_accordion_3_item_1_head{{$body->id}}" data-parent="#m_accordion_3">
                                        <div class="m-accordion__item-content">
                                            <table class = "table table-striped">
                                                <thead>
                                                    <tr style = "text-align:center">
                                                        <th></th>
                                                        <th></th>
                                                        <th>
                                                            Outstanding
                                                        </th>
                                                        <th>
                                                            Very Satisfactory
                                                        </th>
                                                        <th>
                                                            Satisfactory
                                                        </th>
                                                        <th>
                                                            Moderately Satisfactory
                                                        </th>
                                                        <th>
                                                            Needs Improvement
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <!--end::Item-->
                            @endforeach
                        </div>
                    @endif
                    <!--end::Section-->
                </div>
            </div>
            <!--end:: Widgets/Top Products-->
        </div>
        <div class="col-xl-6">
            <!--begin:: Widgets/Top Products-->
            <div class="m-portlet m-portlet--bordered-semi m-portlet--full-height ">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                Feedbacks
                            </h3>
                        </div>
                    </div>
                </div>
                <div class="m-portlet__body" id = "feedback">
                    <!--begin::Section-->
                    @if(isset($user))
                        @include('admin.data_analysis.entries.feedback')
                    @endif
                    <!--end::Section-->
                </div>
            </div>
            <!--end:: Widgets/Top Products-->
        </div>
    </div>
    <!--End::Section-->
</div>

<script src="{{asset('metronic/assets/demo/default/custom/components/forms/widgets/select2.js')}}" type="text/javascript"></script>

<script>

$(document).ready(function(){

    $('.faculty').change(function(){

        $.ajax({

            url: '/entries/load/entry',
            type: 'get',
            data: { id: $(this).val(), form: $('#formId').val(), year: $('#passYear').val(), term: $('#passTerm').val() },
            success: function(response){ 
                $('#entries').html(response);
            }

        });

        $.ajax({

            url: '/entries/load/feedback',
            type: 'get',
            data: { id: $(this).val(), form: $('#formId').val(), year: $('#passYear').val(), term: $('#passTerm').val() },
            success: function(response){ 
                $('#feedback').html(response);
            }

        });

    });

});

</script>

@endsection