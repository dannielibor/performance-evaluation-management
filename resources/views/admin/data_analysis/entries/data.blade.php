@foreach ($forms as $form)
<form action="/entries/show" method = "post" data-id = "{{$form->id}}">
    <input type="hidden" value = "{{$form->id}}" name = "id">
    <input type="hidden" name = "year" id = "passYear" data-id = "{{$form->id}}">
    <input type="hidden" name = "term" id = "passTerm" data-id=  "{{$form->id}}">
    @csrf
    <tr id = "row" data-id = "{{$form->id}}" style = "cursor:pointer">
        <td style = "text-align:left">
            {{ $form->title }}
        </td>
        <td>
            <?php
                if(isset($getYear)){
                    $progress = App\FormProgress::where('formId', $form->id)->where('status', 'completed')->where('year', $getYear)->get();
                }elseif(isset($getTerm)){
                    $progress = App\FormProgress::where('formId', $form->id)->where('status', 'completed')->where('term', $getTerm)->get();
                }elseif(isset($getYear) && isset($getTerm)){
                    $progress = App\FormProgress::where('formId', $form->id)->where('status', 'completed')->where('term', $getTerm)->where('year', $getYear)->get();
                }
                echo count($progress);
            ?>
        </td>
    </tr>
    <button style = "display:none" type = "submit" id = "submit" data-id = "{{$form->id}}"></button>
</form>
@endforeach

<script>
    $(document).ready(function(){

        $('#row[data-id]').click(function(){

            $('#passYear[data-id="'+$(this).attr('data-id')+'"]').val( $('#setYear').val() );
            $('#passTerm[data-id="'+$(this).attr('data-id')+'"]').val( $('#setTerm').val() );
            $('#submit[data-id="'+$(this).attr('data-id')+'"]').click();

        });

    });
</script>