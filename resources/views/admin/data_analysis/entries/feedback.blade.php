@foreach (App\FeedbackResult::where('formId', $form)->where('year', $year)->where('term', $term)->where('evaluatee', $user->id)->get() as $feedback)

    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        Anonymous
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-portlet__body">
            {{ $feedback->result }}
        </div>
    </div>

@endforeach