@extends('admin.layouts.app')

@section('title', 'Data Analysis')

@section('content')

<div class="m-content">
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head" style = "background-color:#f4f4f4">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        Entries
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-portlet__body">
                    <!--begin: Search Form -->
        <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                <div class="row align-items-center">
                    <div class="col-xl-8 order-2 order-xl-1">
                        <div class="form-group m-form__group row align-items-center">
                            <div class="col-md-3">
                                <div class="m-form__group m-form__group--inline">
                                    <div class="m-form__label">
                                        <label>
                                            Year:
                                        </label>
                                    </div>
                                    <div class="m-form__control">
                                        <select class="form-control" id = "setYear">
                                            @foreach (App\FormProgress::distinct()->get(['year']) as $year)
                                                <?php
                                                    $cor = App\COR::orderBy('id', 'desc')->first();
                                                ?>
                                                <option value="{{$year->year}}" <?php if($cor->year == $year->year){echo'selected';} ?>>{{$year->year}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="d-md-none m--margin-bottom-10"></div>
                            </div>
                            <div class="col-md-3">
                                <div class="m-form__group m-form__group--inline">
                                    <div class="m-form__label">
                                        <label>
                                            Term:
                                        </label>
                                    </div>
                                    <div class="m-form__control">
                                        <select class="form-control" id = "setTerm">
                                            @foreach (App\FormProgress::distinct()->get(['term']) as $term)
                                                <?php
                                                    $cor = App\COR::orderBy('id', 'desc')->first();
                                                ?>
                                                <option value="{{$term->term}}" <?php if($cor->term == $term->term){echo'selected';} ?>>{{$term->term}} semester</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="d-md-none m--margin-bottom-10"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end: Search Form -->
            <!--begin: Datatable -->
            <table class = "table table-hover" style = "text-align:center">
                <thead>
                    <tr>
                        <th scope = "col">
                            Form
                        </th>
                        <th scope = "col">
                            Entries
                        </th>
                    </tr>
                </thead>
                <tbody id = "entries">
                    @include('admin.data_analysis.entries.data')
                </tbody>
            </table>
            <!--end: Datatable -->
        </div>
    </div>
</div>

<script>

    $('#setYear').change(function(){ console.log();
        $.ajax({
            url: '/entries/sort',
            type: 'get',
            data: { year: $('#setYear').val(), term: $('#setTerm').val() },
            success: function(response){
                $('#entries').html(response);
            }
        });
    });

    $('#setTerm').change(function(){
        $.ajax({
            url: '/entries/sort',
            type: 'get',
            data: { year: $('#setYear').val(), term: $('#setTerm').val() },
            success: function(response){
                $('#entries').html(response);
            }
        });
    });

</script>

@endsection