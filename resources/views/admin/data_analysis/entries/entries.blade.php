<div class="m-accordion m-accordion--default m-accordion--solid" id="m_accordion_3" role="tablist">
    @foreach (App\Body::where('formId', $form)->orderByRaw('LENGTH(position)', 'asc')->orderBy('position', 'asc')->get() as $body)
        <!--begin::Item-->
        <div class="m-accordion__item">
            <div class="m-accordion__item-head collapsed"  role="tab" id="m_accordion_3_item_1_head{{$body->id}}" data-toggle="collapse" href="#m_accordion_3_item_1_body{{$body->id}}" aria-expanded="    false">
                <span class="m-accordion__item-title">
                    {{ $body->header }} ({{ $body->rate }}%)
                </span>
                <span class="m-accordion__item-mode"></span>
            </div>
            <div class="m-accordion__item-body collapse" id="m_accordion_3_item_1_body{{$body->id}}" class=" " role="tabpanel" aria-labelledby="m_accordion_3_item_1_head{{$body->id}}" data-parent="#m_accordion_3">
                <div class="m-accordion__item-content scroll">
                    <table class = "table table-striped">
                        <thead>
                            <tr style = "text-align:center">
                                <th></th>
                                <th></th>
                                <th>
                                    Outstanding
                                </th>
                                <th>
                                    Very Satisfactory
                                </th>
                                <th>
                                    Satisfactory
                                </th>
                                <th>
                                    Moderately Satisfactory
                                </th>
                                <th>
                                    Needs Improvement
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach (App\Content::where('bodyId', $body->id)->orderByRaw('LENGTH(position)', 'asc')->orderBy('position', 'asc')->get() as $content)
                                @if($content->type == 'header')
                                    <tr>
                                        <td>
                                            {{ $content->sort }}. {{ $content->content }}
                                        </td>
                                    </tr>
                                @else 
                                    <tr>
                                        <td></td>
                                        <td>
                                            {{ $content->content }}
                                        </td>
                                        <td>
                                            <?php 
                                                $ans = 0; $quest = 0; 
                                                if(isset($self)){
                                                    $formProgress = App\FormProgress::where('formId', $form)->where('year', $passYear)->where('term', $passTerm)->where('status', 'completed')->where('evaluatee', $user->id)->where('evaluator', $user->id)->get(); 
                                                }else{
                                                    $formProgress = App\FormProgress::where('formId', $form)->where('year', $passYear)->where('term', $passTerm)->where('status', 'completed')->where('evaluatee', $user->id)->get(); 
                                                }
                                            ?>
                                            @foreach($formProgress as $progress)
                                                @foreach(App\Entry::where('contentId', $content->id)->where('formProgressId', $progress->id)->get() as $entry)
                                                    @if($entry->result == '5')
                                                        <?php $ans = ++$ans; ?>
                                                    @endif
                                                    <?php $quest = ++$quest; ?>
                                                @endforeach
                                            @endforeach
                                            <center>
                                                @if($ans == 0 && $quest == 0)
                                                    0% 
                                                @else 
                                                    {{ round(($ans * 100) / $quest) }}%
                                                @endif
                                            </center>
                                        </td>
                                        <td>
                                            <?php 
                                                $ans = 0; $quest = 0; 
                                                if(isset($self)){
                                                    $formProgress = App\FormProgress::where('formId', $form)->where('year', $passYear)->where('term', $passTerm)->where('status', 'completed')->where('evaluatee', $user->id)->where('evaluator', $user->id)->get(); 
                                                }else{
                                                    $formProgress = App\FormProgress::where('formId', $form)->where('year', $passYear)->where('term', $passTerm)->where('status', 'completed')->where('evaluatee', $user->id)->get(); 
                                                }
                                            ?>
                                            @foreach($formProgress as $progress)
                                                @foreach(App\Entry::where('contentId', $content->id)->where('formProgressId', $progress->id)->get() as $entry)
                                                    @if($entry->result == '4')
                                                        <?php $ans = ++$ans; ?>
                                                    @endif
                                                    <?php $quest = ++$quest; ?>
                                                @endforeach
                                            @endforeach
                                            <center>
                                                @if($ans == 0 && $quest == 0)
                                                    0%
                                                @else 
                                                    {{ round(($ans * 100) / $quest) }}%
                                                @endif
                                            </center>
                                        </td>
                                        <td>
                                            <?php 
                                                $ans = 0; $quest = 0; 
                                                if(isset($self)){
                                                    $formProgress = App\FormProgress::where('formId', $form)->where('year', $passYear)->where('term', $passTerm)->where('status', 'completed')->where('evaluatee', $user->id)->where('evaluator', $user->id)->get(); 
                                                }else{
                                                    $formProgress = App\FormProgress::where('formId', $form)->where('year', $passYear)->where('term', $passTerm)->where('status', 'completed')->where('evaluatee', $user->id)->get(); 
                                                }
                                            ?>
                                            @foreach($formProgress as $progress)
                                                @foreach(App\Entry::where('contentId', $content->id)->where('formProgressId', $progress->id)->get() as $entry)
                                                    @if($entry->result == '3')
                                                        <?php $ans = ++$ans; ?>
                                                    @endif
                                                    <?php $quest = ++$quest; ?>
                                                @endforeach
                                            @endforeach
                                            <center>
                                                @if($ans == 0 && $quest == 0)
                                                    0%
                                                @else 
                                                    {{ round(($ans * 100) / $quest) }}%
                                                @endif
                                            </center>
                                        </td>
                                        <td>
                                            <?php 
                                                $ans = 0; $quest = 0; 
                                                if(isset($self)){
                                                    $formProgress = App\FormProgress::where('formId', $form)->where('year', $passYear)->where('term', $passTerm)->where('status', 'completed')->where('evaluatee', $user->id)->where('evaluator', $user->id)->get(); 
                                                }else{
                                                    $formProgress = App\FormProgress::where('formId', $form)->where('year', $passYear)->where('term', $passTerm)->where('status', 'completed')->where('evaluatee', $user->id)->get(); 
                                                }
                                            ?>
                                            @foreach($formProgress as $progress)
                                                @foreach(App\Entry::where('contentId', $content->id)->where('formProgressId', $progress->id)->get() as $entry)
                                                    @if($entry->result == '2')
                                                        <?php $ans = ++$ans; ?>
                                                    @endif
                                                    <?php $quest = ++$quest; ?>
                                                @endforeach
                                            @endforeach
                                            <center>
                                                @if($ans == 0 && $quest == 0)
                                                    0%
                                                @else 
                                                    {{ round(($ans * 100) / $quest) }}%
                                                @endif
                                            </center>
                                        </td>
                                        <td>
                                            <?php 
                                                $ans = 0; $quest = 0; 
                                                if(isset($self)){
                                                    $formProgress = App\FormProgress::where('formId', $form)->where('year', $passYear)->where('term', $passTerm)->where('status', 'completed')->where('evaluatee', $user->id)->where('evaluator', $user->id)->get(); 
                                                }else{
                                                    $formProgress = App\FormProgress::where('formId', $form)->where('year', $passYear)->where('term', $passTerm)->where('status', 'completed')->where('evaluatee', $user->id)->get(); 
                                                }
                                            ?>
                                            @foreach($formProgress as $progress)
                                                @foreach(App\Entry::where('contentId', $content->id)->where('formProgressId', $progress->id)->get() as $entry)
                                                    @if($entry->result == '1')
                                                        <?php $ans = ++$ans; ?>
                                                    @endif
                                                    <?php $quest = ++$quest; ?>
                                                @endforeach
                                            @endforeach
                                            <center>
                                                @if($ans == 0 && $quest == 0)
                                                    0%
                                                @else 
                                                    {{ round(($ans * 100) / $quest) }}%
                                                @endif
                                            </center>
                                        </td>
                                    </tr>
                                @endif
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!--end::Item-->
    @endforeach
</div>