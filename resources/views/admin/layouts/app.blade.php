<?php
    $user = App\User::find(auth()->user()->id);
?>

<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
	<!-- begin::Head -->
	<head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

		<title>
			SBCA - FPES | @yield('title')
		</title>
		
		{{-- <script src="{{ asset('js/app.js') }}"></script> --}}
		
        <!-- Fonts -->
        <link rel="dns-prefetch" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
		<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
		{{-- <link rel="stylesheet" href="/resources/demos/style.css"> --}}
        <!-- Styles -->

		<!--begin::Web font -->
		<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
		<script>
			WebFont.load({
				google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
				active: function() {
					sessionStorage.fonts = true;
				}
			});
		</script>
		<!--end::Web font -->
		<link href="{{ asset('metronic/assets/vendors/custom/jquery-ui/jquery-ui.bundle.css') }}" rel="stylesheet" type="text/css" />
		<!--begin::Base Styles -->  
		<link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">
		<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
		<script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
        <!--begin::Page Vendors -->
		<link href="{{asset('metronic/assets/vendors/custom/fullcalendar/fullcalendar.bundle.css')}}" rel="stylesheet" type="text/css" />
		<!--end::Page Vendors -->
		<link href="{{asset('metronic/assets/vendors/base/vendors.bundle.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{asset('metronic/assets/demo/default/base/style.bundle.css')}}" rel="stylesheet" type="text/css" />
		<!--end::Base Styles -->
		<link rel="shortcut icon" href="{{ asset('images/sbca.png') }}" />

		<style>
			.m-brand.m-brand--skin-dark .m-brand__tools .m-brand__toggler span {
				  background: black;
			}
			.m-brand.m-brand--skin-dark .m-brand__tools .m-brand__toggler span::before, .m-brand.m-brand--skin-dark .m-brand__tools .m-brand__toggler span::after {
				background: black;
			}
			.m-brand.m-brand--skin-dark .m-brand__tools .m-brand__toggler:hover span::before, .m-brand.m-brand--skin-dark .m-brand__tools .m-brand__toggler:hover span::after {
				background: #800; 
			}
			.m-brand.m-brand--skin-dark .m-brand__tools .m-brand__toggler:hover span {
				  background: #800; 
			}
	  		.m-brand.m-brand--skin-dark {
				background: #DCDCDC; 
			}
			.m-brand.m-brand--skin-dark .m-brand__tools .m-brand__toggler.m-brand__toggler--active span {
				  background: #800; 
			}
      		.m-brand.m-brand--skin-dark .m-brand__tools .m-brand__toggler.m-brand__toggler--active span::before, .m-brand.m-brand--skin-dark .m-brand__tools .m-brand__toggler.m-brand__toggler--active span::after {
				background: #800; 
			}
			.m-aside-left-close.m-aside-left-close--skin-dark {
				  background-color: #191919; 
			}
			.m-aside-left-close.m-aside-left-close--skin-dark > i {
				color: white; 
			}
			.m-aside-left.m-aside-left--skin-dark {
				background-color: #F3F3F3;
				box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 25px 0 rgba(0, 0, 0, 0.19);
			}
			/* Scrollbar styles */
			::-webkit-scrollbar {
				width: 12px;
				height: 12px;
			}

			::-webkit-scrollbar-track {
				border-radius: 12px;
			}

			::-webkit-scrollbar-track:vertical {
				background: #f1f2f7; 
			}

			::-webkit-scrollbar-thumb {
				background: #5a5a5a;  
				border-radius: 12px;
			}
			.m-menu__link-icon, .m-menu__link-text{
				color:black !important;
			}
			.m-topbar .m-topbar__nav.m-nav > .m-nav__item > .m-nav__link .m-nav__link-icon > i:before{
				background-color: black !important;
			}
			.m-menu__section-text{
				color: black !important;
			}
			.m-aside-left-close.m-aside-left-close--skin-dark:hover{
				background-color:#DCDCDC !important;
			}
			.m-aside-left-close.m-aside-left-close--skin-dark:hover > i {
				color: black; 
			}
			.m-brand.m-brand--skin-dark .m-brand__tools .m-brand__icon > i {
				  color: black;
			}
			.m-grid__item, .m-grid__item--fluid, .m-wrapper{
				background-color:#F2F3F4;
				box-shadow: inset 5px 0 10px -10px rgba(0,0,0,0.7);
			}
			.m-card-user.m-card-user--skin-dark .m-card-user__details .m-card-user__email {
				color: grey; 
			}
			.m-card-user.m-card-user--skin-dark .m-card-user__details .m-card-user__email:hover {
				color: black; 
			}
			.m-card-user.m-card-user--skin-dark .m-card-user__details .m-card-user__email:hover:after {
				border-bottom: 1px solid black;
				opacity: 0.3 ;
				filter: alpha(opacity=30); 
			}
			.m-card-user.m-card-user--skin-dark .m-card-user__details .m-card-user__name {
				color: #000; 
			}
			.m-nav .m-nav__item:hover:not(.m-nav__item--disabled) > .m-nav__link .m-nav__link-icon,
			.m-nav .m-nav__item:hover:not(.m-nav__item--disabled) > .m-nav__link .m-nav__link-text,
			.m-nav .m-nav__item:hover:not(.m-nav__item--disabled) > .m-nav__link .m-nav__link-arrow, .m-nav .m-nav__item.m-nav__item--active > .m-nav__link .m-nav__link-icon,
			.m-nav .m-nav__item.m-nav__item--active > .m-nav__link .m-nav__link-text,
			.m-nav .m-nav__item.m-nav__item--active > .m-nav__link .m-nav__link-arrow {
			color: #000 !important; }
			.m-scroll-top:hover > i {
				color: #800 !important; 
			}
			.m-aside-menu.m-aside-menu--skin-dark .m-menu__nav > .m-menu__item:not(.m-menu__item--parent):not(.m-menu__item--open):not(.m-menu__item--expanded):not(.m-menu__item--active):hover {
			-webkit-transition: background-color 0.3s;
			-moz-transition: background-color 0.3s;
			-ms-transition: background-color 0.3s;
			-o-transition: background-color 0.3s;
			transition: background-color 0.3s;
			background-color: #DCDCDC; }

			a.m-datatable__pager-link.m-datatable__pager-link-number.m-datatable__pager-link--active{
				background-color: #800 !important;
			}

			ul.m-datatable__pager-nav > li a:hover{
				background-color: #800 !important;
			}

			button.btn.dropdown-toggle.btn-default:hover{
				background-color: #800 !important;
				color: white !important;
			}

			button.btn.dropdown-toggle.btn-default{
				background-color: #EBE9F2 !important;
				color: black !important;
			}
			.m-link:hover:after {
			border-bottom: 1px solid #000;
			opacity: 0.3 ;
			filter: alpha(opacity=30) ; }

			.m-menu__item--active{
				background-color: #c6c6c6 !important;
			}

			.search-img{
				border-radius:100%;
				width: 25px;
				margin-right:10px;
			}
		</style>

	</head>
	<!-- end::Head -->
    <!-- end::Body -->
	<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
		<!-- begin:: Page -->
		{!! Toastr::render() !!}
		<div class="m-grid m-grid--hor m-grid--root m-page">
			<!-- BEGIN: Header -->
			<header class="m-grid__item m-header"  data-minimize-offset="200" data-minimize-mobile-offset="200" >
				<div class="m-container m-container--fluid m-container--full-height">
					<div class="m-stack m-stack--ver m-stack--desktop" id = "headnav">
						<!-- BEGIN: Brand -->
						<div class="m-stack__item m-brand  m-brand--skin-dark" style = "border-bottom:1px solid #800">
							<div class="m-stack m-stack--ver m-stack--general">
								<div class="m-stack__item m-stack__item--middle m-brand__logo">
									<a href="/" class="m-brand__logo-wrapper" style = "text-decoration:none">
										<img alt="San Beda College Alabang" width = "30" src="{{asset('images/sbca.png')}}"/>&nbsp;<span style = "color:#000;font-size:15px">&nbsp;Admin</span>
									</a>
								</div>
								<div class="m-stack__item m-stack__item--middle m-brand__tools">
									<!-- BEGIN: Left Aside Minimize Toggle -->
									<a href="javascript:;" id="m_aside_left_minimize_toggle" class="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-desktop-inline-block">
										<span></span>
									</a>
									<!-- END -->
							<!-- BEGIN: Responsive Aside Left Menu Toggler -->
									<a href="javascript:;" id="m_aside_left_offcanvas_toggle" class="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-tablet-and-mobile-inline-block">
										<span></span>
									</a>
									<!-- END -->
							<!-- BEGIN: Topbar Toggler -->
									<a id="m_aside_header_topbar_mobile_toggle" href="javascript:;" class="m-brand__icon m--visible-tablet-and-mobile-inline-block">
										<i class="flaticon-more"></i>
									</a>
									<!-- BEGIN: Topbar Toggler -->
								</div>
							</div>
						</div>
						<!-- END: Brand -->
						<div class="m-stack__item m-stack__item--fluid m-header-head" id="m_header_nav" style = "background:#DDDDDD">							
							<!-- BEGIN: Topbar -->
							<div id="m_header_topbar" class="m-topbar  m-stack m-stack--ver m-stack--general">
								<div class="m-stack__item m-topbar__nav-wrapper">
									<ul class="m-topbar__nav m-nav m-nav--inline">
										<li class="m-nav__item m-dropdown m-dropdown--large m-dropdown--arrow m-dropdown--align-center m-dropdown--mobile-full-width m-dropdown--skin-light	m-list-search m-list-search--skin-light" data-dropdown-toggle="click" data-dropdown-persistent="true" id="m_quicksearch" data-search-type="dropdown">
											<a href="#" class="m-nav__link m-dropdown__toggle">
												<span class="m-nav__link-icon">
													<i class="flaticon-search-1"></i>
												</span>
											</a>
											<div class="m-dropdown__wrapper">
												<span class="m-dropdown__arrow m-dropdown__arrow--center"></span>
												<div class="m-dropdown__inner ">
													<div class="m-dropdown__header">
														<form action = "/search" class="m-list-search__form">
															<div class="m-list-search__form-wrapper">
																<span class="m-list-search__form-input-wrapper">
																	<input id="m_quicksearch_input" autocomplete="off" type="text" name = "q" class="m-list-search__form-input" value="" placeholder="Search...">
																</span>
																<span class="m-list-search__form-icon-close" id="m_quicksearch_close">
																	<i class="la la-remove"></i>
																</span>
															</div>
															<div class = "dropdown">
																<div id="myDropdown1" class="dropdown-menu" style = "max-height:200px;overflow:scroll;">
																	@foreach (App\Faculty::all() as $faculty)
																		<a href="/faculty/edit/{{$faculty->userId}}" class="dropdown-item"><img src="/storage/profiles/{{$faculty->user->image}}" class = "search-img">{{ $faculty->user->name }}</a>
																	@endforeach
																	@foreach (App\Student::all() as $students)
																		<a href="/students/edit/{{$students->userId}}" class="dropdown-item"><img src="/storage/profiles/{{$students->user->image}}" class = "search-img">{{ $students->user->name }}</a>
																	@endforeach
																</div>
															</div>
														</form>
													</div>
													<div class="m-dropdown__body">
														<div class="m-dropdown__scrollable m-scrollable" data-max-height="300" data-mobile-max-height="200">
															<div class="m-dropdown__content"></div>
														</div>
													</div>
												</div>
											</div>
										</li>
										<li class="m-nav__item m-topbar__quick-actions m-topbar__quick-actions--img m-dropdown m-dropdown--large m-dropdown--header-bg-fill m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push m-dropdown--mobile-full-width m-dropdown--skin-light"  data-dropdown-toggle="click">
											<a href="#" class="m-nav__link m-dropdown__toggle">
												<span class="m-nav__link-badge m-badge m-badge--dot m-badge--info m--hide"></span>
												<span class="m-nav__link-icon">
													<i class="flaticon-share"></i>
												</span>
											</a>
											<div class="m-dropdown__wrapper">
												<span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust" style = "color:black !important"></span>
												<div class="m-dropdown__inner">
													<div class="m-dropdown__header m--align-center" style="background: url({{asset('images/one.jpg')}}); background-size: cover;">
														<span class="m-dropdown__header-title" style = "color:black !important">
															Quick Actions
														</span>
														<span class="m-dropdown__header-subtitle" style = "color:black !important">
															Shortcuts
														</span>
													</div>
													<div class="m-dropdown__body m-dropdown__body--paddingless">
														<div class="m-dropdown__content">
															<div class="m-scrollable" data-scrollable="false" data-max-height="380" data-mobile-max-height="200">
																<div class="m-nav-grid m-nav-grid--skin-light">
																	<div class="m-nav-grid__row">
																		<a href="/summative-report" class="m-nav-grid__item">
																			<i class="m-nav-grid__icon fa fa-bar-chart-o" style = "color:black !important"></i>
																			<span class="m-nav-grid__text" style = "color:black !important">
																				Generate Summative Report
																			</span>
																		</a>
																		<a href="#" data-toggle="modal" data-target="#shrtNewForm" class="m-nav-grid__item">
																			<i class="m-nav-grid__icon fa fa-list-alt" style = "color:black !important"></i>
																			<span class="m-nav-grid__text" style = "color:black !important">
																				Create New Form
																			</span>
																		</a>
																	</div>
																	<div class="m-nav-grid__row">
																		<a href="/certificate-of-registration" class="m-nav-grid__item">
																			<i class="m-nav-grid__icon fa fa-database" style = "color:black !important"></i>
																			<span class="m-nav-grid__text" style = "color:black !important">
																				Import Certificate of Registration
																			</span>
																		</a>
																		<a href="/class-load" class="m-nav-grid__item">
																			<i class="m-nav-grid__icon fa fa-database" style = "color:black !important"></i>
																			<span class="m-nav-grid__text" style = "color:black !important">
																				Import Class Load
																			</span>
																		</a>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</li>
										<li class="m-nav__item m-topbar__user-profile m-topbar__user-profile--img  m-dropdown m-dropdown--medium m-dropdown--arrow m-dropdown--header-bg-fill m-dropdown--align-right m-dropdown--mobile-full-width m-dropdown--skin-light" data-dropdown-toggle="click">
											<a href="#" class="m-nav__link m-dropdown__toggle">
												<span class="m-topbar__userpic">
													<img src="/storage/profiles/{{ $user->image }}" class="m--img-rounded m--marginless m--img-centered" alt=""/>
												</span>
												<span class="m-topbar__username m--hide">
													Nick
												</span>
											</a>
											<div class="m-dropdown__wrapper">
												<span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust" style = "color:#F3F3F3"></span>
												<div class="m-dropdown__inner">
													<div class="m-dropdown__header m--align-center" style="background: #F3F3F3; background-size: cover;">
														<div class="m-card-user m-card-user--skin-dark">
															<div class="m-card-user__pic">
																<img src="/storage/profiles/{{ $user->image }}" class="m--img-rounded m--marginless" alt=""/>
															</div>
															<div class="m-card-user__details">
																<span class="m-card-user__name m--font-weight-500">
                                                                    @if($user->name == 'admin')
                                                                        Admin
                                                                    @else
                                                                        {{ $user->name }}
                                                                    @endif
																</span>
																<a href="" class="m-card-user__email m--font-weight-300 m-link">
																	{{ $user->email }}
																</a>
															</div>
														</div>
													</div>
													<div class="m-dropdown__body">
														<div class="m-dropdown__content">
															<ul class="m-nav m-nav--skin-light">
																<li class="m-nav__section m--hide">
																	<span class="m-nav__section-text">
																		Section
																	</span>
																</li>
																<li class="m-nav__item">
																	<a href = "/admin/profile" class="m-nav__link">
																		<i class="m-nav__link-icon fl flaticon-user"></i>
																		<span class="m-nav__link-title">
																			<span class="m-nav__link-wrap">
																				<span class="m-nav__link-text">
																					My Profile
																				</span>
																			</span>
																		</span>
																	</a>
																</li>
																<li class="m-nav__item">
																	<a href = "/admin/profile/settings" class="m-nav__link">
																		<i class="m-nav__link-icon fl flaticon-settings-1"></i>
																		<span class="m-nav__link-title">
																			<span class="m-nav__link-wrap">
																				<span class="m-nav__link-text">
																					Settings
																				</span>
																			</span>
																		</span>
																	</a>
																</li>
																<li class="m-nav__separator m-nav__separator--fit"></li>
																<li class="m-nav__item">
                                                                    <a class="btn m-btn--pill btn-secondary m-btn m-btn--custom m-btn--label-brand m-btn--bolder" style = "color:grey" href="{{ route('logout') }}"
                                                                    onclick="event.preventDefault();
                                                                                    document.getElementById('logout-form').submit();">
                                                                        Sign out
                                                                    </a>
                                                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                                        @csrf
                                                                    </form>
																</li>
															</ul>
														</div>
													</div>
												</div>
											</div>
										</li>
									</ul>
								</div>
							</div>
							<!-- END: Topbar -->
						</div>
					</div>
				</div>
			</header>
			<!-- END: Header -->		
		<!-- begin::Body -->
			<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
				<!-- BEGIN: Left Aside -->
				<button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn">
					<i class="la la-close"></i>
				</button>
				<div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-dark ">
					<!-- BEGIN: Aside Menu -->
	<div id="m_ver_menu" class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark " data-menu-vertical="true" data-menu-scrollable="false" data-menu-dropdown-timeout="500">
						<ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow" id = "submenu_active">
							<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  data-menu-submenu-toggle="hover">
								<a  href="/dashboard" class="m-menu__link m-menu__toggle">
									<i class="m-menu__link-icon fa fa-dashboard"></i>
									<span class="m-menu__link-text">
										Dashboard
									</span>
								</a>
							</li>
							<li class="m-menu__section">
								<h4 class="m-menu__section-text">
									User Management
								</h4>
								<i class="m-menu__section-icon flaticon-more-v3"></i>
							</li>
							@if(Session::get('type') == 'Super Admin')
								<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  data-menu-submenu-toggle="hover">
									<a  href="/admins" class="m-menu__link m-menu__toggle">
										<i class="m-menu__link-icon fa fa-user-secret"></i>
										<span class="m-menu__link-text">
											Admins
										</span>
									</a>
								</li>
							@endif
							<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  data-menu-submenu-toggle="hover">
								<a  href="/students" class="m-menu__link m-menu__toggle">
									<i class="m-menu__link-icon fa fa-users"></i>
									<span class="m-menu__link-text">
										Students
									</span>
								</a>
							</li>
							<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  data-menu-submenu-toggle="hover">
								<a  href="/faculty" class="m-menu__link m-menu__toggle">
									<i class="m-menu__link-icon fa 	fa-briefcase"></i>
									<span class="m-menu__link-text">
										Faculty
									</span>
								</a>
							</li>
							<li class="m-menu__section">
								<h4 class="m-menu__section-text">
									Database
								</h4>
								<i class="m-menu__section-icon flaticon-more-v3"></i>
							</li>
							<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  data-menu-submenu-toggle="hover">
								<a  href="/certificate-of-registration" class="m-menu__link m-menu__toggle">
									<i class="m-menu__link-icon fa 	fa-database"></i>
									<span class="m-menu__link-text">
										Certificate of Registration
									</span>
								</a>
							</li>
							<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  data-menu-submenu-toggle="hover">
								<a  href="/class-load" class="m-menu__link m-menu__toggle">
									<i class="m-menu__link-icon fa 	fa-database"></i>
									<span class="m-menu__link-text">
										Class Load
									</span>
								</a>
							</li>
							<li class="m-menu__section">
								<h4 class="m-menu__section-text">
									Evaluation Management
								</h4>
								<i class="m-menu__section-icon flaticon-more-v3"></i>
							</li>
							<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  data-menu-submenu-toggle="hover">
								<a  href="/forms" class="m-menu__link m-menu__toggle">
									<i class="m-menu__link-icon fa fa-list-alt"></i>
									<span class="m-menu__link-text">
										Forms
									</span>
								</a>
							</li>
							<li class="m-menu__section">
								<h4 class="m-menu__section-text">
									Data Analysis
								</h4>
								<i class="m-menu__section-icon flaticon-more-v3"></i>
							</li>
							<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  data-menu-submenu-toggle="hover">
								<a  href="/summative-report" class="m-menu__link m-menu__toggle">
									<i class="m-menu__link-icon fa fa-bar-chart-o"></i>
									<span class="m-menu__link-text">
										Summative Report
									</span>
								</a>
							</li>
							<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  data-menu-submenu-toggle="hover">
								<a  href="/rank" class="m-menu__link m-menu__toggle">
									<i class="m-menu__link-icon fa fa-save"></i>
									<span class="m-menu__link-text">
										Rank
									</span>
								</a>
							</li>
							<li class="m-menu__section">
								<h4 class="m-menu__section-text">
									Help
								</h4>
								<i class="m-menu__section-icon flaticon-more-v3"></i>
							</li>
							<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  data-menu-submenu-toggle="hover">
								<a  href="/tutorials" class="m-menu__link m-menu__toggle">
									<i class="m-menu__link-icon fa fa-play"></i>
									<span class="m-menu__link-text">
										Video Tutorials
									</span>
								</a>
							</li>
						</ul>
					</div>
					<!-- END: Aside Menu -->
				</div>
				<!-- END: Left Aside -->
				<div class="m-grid__item m-grid__item--fluid m-wrapper">
					@yield('content')
				</div>
			</div>
			<!-- end:: Body -->
			<!-- begin::Footer -->
			<footer class="m-grid__item	m-footer" style = "background-color:#DDDDDD">
				<div class="m-container m-container--fluid m-container--full-height m-page__container">
					<div class="m-stack m-stack--flex-tablet-and-mobile m-stack--ver m-stack--desktop">
						<div class="m-stack__item m-stack__item--left m-stack__item--middle m-stack__item--last">
							<span class="m-footer__copyright" style = "color:black">
								{{ Carbon\Carbon::now()->format('Y') }} &copy; Faculty Performance Evaluation by
								<a href="https://www.sanbeda-alabang.edu.ph/bede/" target="_blank" class="m-link" style = "color:#800">
									San Beda College Alabang
								</a>
							</span>
						</div>
						<div class="m-stack__item m-stack__item--right m-stack__item--middle m-stack__item--first" id = "foot">
							<ul class="m-footer__nav m-nav m-nav--inline m--pull-right">
								<li class="m-nav__item">
									<a href="#" class="m-nav__link" data-toggle="modal" data-target="#about">
										<span class="m-nav__link-text">
											About
										</span>
									</a>
								</li>
								<li class="m-nav__item">
									<a href="#"  class="m-nav__link" data-toggle="modal" data-target="#privacy">
										<span class="m-nav__link-text">
											Privacy
										</span>
									</a>
								</li>
								<li class="m-nav__item">
									<a href="#" class="m-nav__link" data-toggle="modal" data-target="#TnC">
										<span class="m-nav__link-text">
											T&C
										</span>
									</a>
								</li>
								<li class="m-nav__item">
									<a href="#" class="m-nav__link" data-toggle="modal" data-target="#support">
										<span class="m-nav__link-text">
											Support
										</span>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</footer>
			<!-- end::Footer -->
		</div>
		<!-- end:: Page -->
		<!-- Modal -->
		<div class="modal fade" id="shrtNewForm" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered" role="document">
				<div class="modal-content">
					<div class="modal-header" style = "background-color:#f4f4f4">
						<h5 class="modal-title" id="">
							New Form
						</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true" class="la la-remove"></span>
						</button>
					</div>
					{!! Form::open(['action' => 'Admin\FormController@store', 'method' => 'post']) !!}
						@csrf
						<input type = "hidden" value = "1" name = "quick">
						<div class="modal-body">
							<div class="form-group m-form__group row m--margin-top-20">
								<label class="col-form-label col-lg-3 col-sm-12">
									Title
								</label>
								<div class="col-lg-7 col-md-9 col-sm-12">
									<input type='text' class="form-control" placeholder="Enter Form Title" name = "title" required/>
								</div>
							</div>
							<div class="m-form__group form-group row">
								<label class="col-3 col-form-label">
									Self Evaluation
								</label>
								<div class="col-9">
									<div class="m-checkbox-list">
										<label class="m-checkbox">
											<input type="checkbox" name = "self[]" value = "Dean">
											Dean
											<span></span>
										</label>
										<label class="m-checkbox">
											<input type="checkbox" name = "self[]" value = "Vice Dean">
											Vice Dean
											<span></span>
										</label>
										<label class="m-checkbox">
											<input type="checkbox" name = "self[]" value = "Department Chair">
											Department Chair
											<span></span>
										</label>
										<label class="m-checkbox">
											<input type="checkbox" name = "self[]" value = "Professor">
											Professor
											<span></span>
										</label>
									</div>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<button type="submit" class="m-btn--pill btn btn-success m-btn">
								Submit
							</button>
							<button type="button" class="m-btn--pill btn btn-danger m-btn" data-dismiss="modal">
								Close
							</button>
						</div>
					{!! Form::close() !!}
				</div>
			</div>
		</div>
		<!-- About -->
		<div class="modal fade" id="about" tabindex="-1" role="dialog" aria-labelledby="about" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered" role="document">
				<div class="modal-content">
					<div class="modal-header">
					<h5 class="modal-title" id="about">About</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					</div>
					<div class="modal-body">
						Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
					</div>
					<div class="modal-footer">
					<button type="button" class="btn btn-metal" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
			</div>
		<!-- Privacy -->
		<div class="modal fade" id="privacy" tabindex="-1" role="dialog" aria-labelledby="privacy" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered" role="document">
				<div class="modal-content">
					<div class="modal-header">
					<h5 class="modal-title" id="privacy">Privacy</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					</div>
					<div class="modal-body">
						Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
					</div>
					<div class="modal-footer">
					<button type="button" class="btn btn-metal" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>
		<!-- TnC -->
		<div class="modal fade" id="TnC" tabindex="-1" role="dialog" aria-labelledby="TnC" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered" role="document">
				<div class="modal-content">
					<div class="modal-header">
					<h5 class="modal-title" id="TnC">Terms And Conditions</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					</div>
					<div class="modal-body">
						Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
					</div>
					<div class="modal-footer">
					<button type="button" class="btn btn-metal" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>
		<!-- Support -->
		<div class="modal fade" id="support" tabindex="-1" role="dialog" aria-labelledby="support" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered" role="document">
				<div class="modal-content">
					<div class="modal-header">
					<h5 class="modal-title" id="support">Support</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					</div>
					<div class="modal-body">
						Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
					</div>
					<div class="modal-footer">
					<button type="button" class="btn btn-metal" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>
	    <!-- begin::Scroll Top -->
		<div class="m-scroll-top m-scroll-top--skin-top" data-toggle="m-scroll-top" data-scroll-offset="500" data-scroll-speed="300">
			<i class="la la-arrow-up"></i>
		</div>
		<!-- end::Scroll Top -->
    	<!--begin::Base Scripts -->
		<script src="{{asset('metronic/assets/vendors/base/vendors.bundle.js')}}" type="text/javascript"></script>
		<script src="{{asset('metronic/assets/demo/default/base/scripts.bundle.js')}}" type="text/javascript"></script>
		<!--end::Base Scripts -->   
        <!--begin::Page Vendors -->
		<script src="{{asset('metronic/assets/vendors/custom/fullcalendar/fullcalendar.bundle.js')}}" type="text/javascript"></script>
		<!--end::Page Vendors -->  
		<script src="{{ asset('metronic/assets/vendors/custom/jquery-ui/jquery-ui.bundle.js') }}" type="text/javascript"></script>
		<script src="{{ asset('metronic/assets/demo/default/custom/components/portlets/draggable.js') }}" type="text/javascript"></script>
		<script>
			var path = window.location.pathname;
			path = path.replace(/\/$/, "");
			path = decodeURIComponent(path);
			$("#submenu_active a").each(function () {
				var href = $(this).attr('href');
				if (path.substring(0, href.length) === href) {
					$(this).closest('li').addClass('m-menu__item--active');
				}
			});
			
			$('#m_quicksearch_input').click(function(){
				document.getElementById("myDropdown1").classList.toggle("show");
			});

			$('#m_quicksearch_input').keyup(function(){
				document.getElementById("myDropdown1").classList.toggle("show");
				var input, filter, ul, li, a, i, div;
				input = document.getElementById("m_quicksearch_input");
				filter = input.value.toUpperCase();
				div = document.getElementById("myDropdown1");
				a = div.getElementsByTagName("a");
				for (i = 0; i < a.length; i++) {
					if (a[i].innerHTML.toUpperCase().indexOf(filter) > -1) {
						a[i].style.display = "";
					} else {
						a[i].style.display = "none";
					}
				}
			});
		</script>
	</body>
	<!-- end::Body -->
</html>