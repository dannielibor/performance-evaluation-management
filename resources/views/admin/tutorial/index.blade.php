@extends('admin.layouts.app')

@section('title', 'Tutorials')

@section('content')

<div class="m-content">
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head" style = "background-color:#f4f4f4">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        Video Tutorials
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-portlet__body">
            @if(Session::get('type') == 'Super Admin')
            <!--begin: Search Form -->
            <div class="m-form m-form--label-align-left m--margin-top-20 m--margin-bottom-30">
                <div class="row align-items-center">
                    <div class="col-xl-2 order-1 order-xl-2 m--align-left">
                        <button style = "background-color:#800;border:none" class="btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--pill" data-toggle="modal" data-target="#m_modal_5">
                            <span>
                                <i class="fl flaticon-user-add"></i>
                                <span>
                                    New Tutorial
                                </span>
                            </span>
                        </button>
                        <div class="m-separator m-separator--dashed d-xl-none"></div>
                    </div>
                    <div class="modal fade" id="m_modal_5" tabindex="-1" role="dialog" aria-labelledby="m_modal_5" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                {!! Form::open(['action' => 'Admin\TutorialController@store', 'method' => 'post']) !!}
                                    @csrf
                                <div class="modal-header" style = "background-color:#f4f4f4">
                                    <h5 class="modal-title" id="m_modal_5">
                                        New Tutorial
                                    </h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">
                                            &times;
                                        </span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class = "form-group">
                                        <div class = "row">
                                            <div class = "col">
                                                <label for="firstname">Title</label>
                                                <input type="text" class="form-control m-input m-input--air fname" name = "title" placeholder="Title here">
                                            </div>
                                        </div>
                                    </div>
                                    <div class = "form-group">
                                        <div class = "row">
                                            <div class = "col">
                                                <label for="firstname">Video Link</label>
                                                <input type="text" class="form-control m-input m-input--air fname" name = "link" placeholder="Video link" required>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="submit" class="m-btn--pill btn btn-success">
                                        Submit
                                    </button>
                                    <button type="button" class="m-btn--pill btn btn-danger" data-dismiss="modal">
                                        Close
                                    </button>
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end: Search Form -->
            @endif
            <br><br>
            <!--begin::Content-->
            <div class="tab-content">
                <div class="tab-pane active" id="m_widget5_tab1_content" aria-expanded="true">
                    <!--begin::m-widget5-->
                    @foreach ($videos as $video)
                        <div class="m-widget5">
                            <div class="m-widget5__item">
                                <div class="m-widget5__pic" style = "cursor:pointer" onclick="window.location.href='/tutorials/{{$video->id}}'">
                                    <img class="m-widget7__img" src="{{asset('images/video.png')}}" alt="">
                                </div>
                                <div class="m-widget5__content" style = "cursor:pointer" onclick="window.location.href='/tutorials/{{$video->id}}'">
                                    <h4 class="m-widget5__title">
                                        {{ $video->title }}
                                    </h4>
                                    <div class="m-widget5__info">
                                        <span class="m-widget5__info-label">
                                            Released:
                                        </span>
                                        <span class="m-widget5__info-date m--font-info">
                                            {{ $video->created_at->format('M d, Y | h:m a') }}
                                        </span>
                                    </div>
                                </div>
                                @if(Session::get('type') == 'Super Admin')
                                <div class="m-widget5__stats1">
                                    <span class="m-widget5__number">
                                        <a href="#" data-toggle="modal" data-target="#m_modal_{{ $video->id }}" class="m-portlet__nav-link btn m-btn m-btn--hover-warning m-btn--icon m-btn--icon-only m-btn--pill" title="Edit">
                                            <i class="la la-edit"></i>
                                        </a>
                                        <a href="#" data-toggle="modal" data-target="#m_modal_delete_{{ $video->id }}" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete">
                                            <i class="la la-trash"></i>
                                        </a>
                                    </span>
                                </div>
                                @endif
                            </div>
                        </div>
                        {{-- delete modal --}}
                        <div class="modal fade" id="m_modal_delete_{{ $video->id }}" tabindex="-1" role="dialog" aria-labelledby="delete_{{ $video->id }}" aria-hidden="true">
                            {!! Form::open(['action' => ['Admin\TutorialController@destroy', $video->id], 'method' => 'post']) !!}
                                @csrf
                                {!! Form::hidden('_method', 'DELETE') !!}
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                <div class="modal-header" style = "background-color:#FAA900">
                                    <h5 class="modal-title" id="delete_{{ $video->id }}"><i class = "fa fa-warning"></i> &nbsp; Delete</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body" style = "text-align:center">
                                    This tutorial will be deleted!
                                    <br>
                                    Do you wish to continue?
                                </div>
                                <div class="modal-footer">
                                    <button type="submit" class="m-btn--pill btn btn-danger">
                                        Delete
                                    </button>
                                    <button type="button" class="m-btn--pill btn btn-metal" data-dismiss="modal">
                                        Cancel
                                    </button>
                                </div>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                        {{-- edit modal --}}
                        <div class="modal fade" id="m_modal_{{ $video->id }}" tabindex="-1" role="dialog" aria-labelledby="edit_{{ $video->id }}" aria-hidden="true">
                            {!! Form::open(['action' => ['Admin\TutorialController@update', $video->id], 'method' => 'post']) !!}
                                @csrf
                                {!! Form::hidden('_method', 'PUT') !!}
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-header" style = "background-color:#f4f4f4">
                                        <h5 class="modal-title" id="m_modal_5">
                                            Edit Tutorial
                                        </h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">
                                                &times;
                                            </span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class = "form-group">
                                            <div class = "row">
                                                <div class = "col">
                                                    <label for="firstname">Title</label>
                                                    <input type="text" class="form-control m-input m-input--air fname" name = "title" value = "{{$video->title}}" placeholder="Title here">
                                                </div>
                                            </div>
                                        </div>
                                        <div class = "form-group">
                                            <div class = "row">
                                                <div class = "col">
                                                    <label for="firstname">Video Link</label>
                                                    <input type="text" class="form-control m-input m-input--air fname" name = "link" value = "{{$video->link}}" placeholder="Video link" required>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" class="m-btn--pill btn btn-success">
                                            Submit
                                        </button>
                                        <button type="button" class="m-btn--pill btn btn-danger" data-dismiss="modal">
                                            Close
                                        </button>
                                    </div>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    @endforeach
                    <!--end::m-widget5-->
                </div>
            </div>
            <!--end::Content-->
        </div>
    </div>
</div>

@endsection