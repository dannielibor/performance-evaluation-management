@extends('admin.layouts.app')

@section('title', 'Tutorials')

@section('content')

<div class="m-content">
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head" style = "background-color:#f4f4f4">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        <a href="javascript:history.back()" id = "cancel" style = "background:none;border:none" class="btn btn-secondary m-btn m-btn--custom m-btn--icon">
                            <span>
                                <i class="la la-arrow-left"></i>
                                &nbsp;&nbsp;
                                <span>
                                    Video Tutorials
                                </span>
                            </span>
                        </a>
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-portlet__body">
            <h2>{{ $video->title }}</h2>
            <br>
            <!--begin::Content-->
            <div class="embed-responsive embed-responsive-16by9">
                <iframe class="embed-responsive-item" src="{{ $video->link }}"></iframe>
            </div>
            <!--end::Content-->
        </div>
    </div>
</div>

@endsection