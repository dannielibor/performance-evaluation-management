@extends('admin.layouts.app')

@section('title', 'Database')

@section('content')

<style>

    #html_table button{
        border-radius:50px;
    }

    .user-img{
        border-radius:100%;
        width: 15%;
        margin-right:20px;
    }

    td{
        text-transform: capitalize;
    }

.file-upload{display:block;text-align:center;font-family: Helvetica, Arial, sans-serif;font-size: 12px;}
.file-upload .file-select{display:block;border: 2px solid #dce4ec;color: #34495e;cursor:pointer;height:40px;line-height:40px;text-align:left;background:#FFFFFF;overflow:hidden;position:relative;}
.file-upload .file-select .file-select-button{background:#dce4ec;padding:0 10px;display:inline-block;height:40px;line-height:40px;}
.file-upload .file-select .file-select-name{line-height:40px;display:inline-block;padding:0 10px;}
.file-upload .file-select:hover{border-color:#34495e;transition:all .2s ease-in-out;-moz-transition:all .2s ease-in-out;-webkit-transition:all .2s ease-in-out;-o-transition:all .2s ease-in-out;}
.file-upload .file-select:hover .file-select-button{background:#34495e;color:#FFFFFF;transition:all .2s ease-in-out;-moz-transition:all .2s ease-in-out;-webkit-transition:all .2s ease-in-out;-o-transition:all .2s ease-in-out;}
.file-upload.active .file-select{border-color:#3fa46a;transition:all .2s ease-in-out;-moz-transition:all .2s ease-in-out;-webkit-transition:all .2s ease-in-out;-o-transition:all .2s ease-in-out;}
.file-upload.active .file-select .file-select-button{background:#3fa46a;color:#FFFFFF;transition:all .2s ease-in-out;-moz-transition:all .2s ease-in-out;-webkit-transition:all .2s ease-in-out;-o-transition:all .2s ease-in-out;}
.file-upload .file-select input[type=file]{z-index:100;cursor:pointer;position:absolute;height:100%;width:100%;top:0;left:0;opacity:0;filter:alpha(opacity=0);}
.file-upload .file-select.file-select-disabled{opacity:0.65;}
.file-upload .file-select.file-select-disabled:hover{cursor:default;display:block;border: 2px solid #dce4ec;color: #34495e;cursor:pointer;height:40px;line-height:40px;margin-top:5px;text-align:left;background:#FFFFFF;overflow:hidden;position:relative;}
.file-upload .file-select.file-select-disabled:hover .file-select-button{background:#dce4ec;color:#666666;padding:0 10px;display:inline-block;height:40px;line-height:40px;}
.file-upload .file-select.file-select-disabled:hover .file-select-name{line-height:40px;display:inline-block;padding:0 10px;}

</style>

    @if(count($class) > 0)
        @include('admin.class.show')
    @else 
        @include('admin.class.store')
    @endif

    <script src="{{ asset('metronic/assets/demo/default/custom/components/forms/wizard/wizard.js') }}" type="text/javascript"></script>
    <script src="{{ asset('metronic/assets/demo/default/custom/components/datatables/base/html-table.js') }}" type="text/javascript"></script>

<script>

$('#import').click(function(){
    document.getElementById('m_2').style.display = "block";
    document.getElementById('m_1').style.display = "none";
});

$('#cancel').click(function(){
    document.getElementById('m_2').style.display = "none";
    document.getElementById('m_1').style.display = "block";
});

$('#chooseFile').bind('change', function (e) {
  var filename = $("#chooseFile").val();
  if (/^\s*$/.test(filename)) {
    $(".file-upload").removeClass('active');
    $("#noFile").text("No file chosen...");
  }
  else {
    $(".file-upload").addClass('active');
    $("#noFile").text(filename.replace("C:\\fakepath\\", "")); 
  }

  var name = e.target.files[0].name;
  var modified = e.target.files[0].lastModifiedDate;
  var size = e.target.files[0].size;
  var type = e.target.files[0].type;

    var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
    if (size == 0) return '0 Byte';
    var i = parseInt(Math.floor(Math.log(size) / Math.log(1024)));

  $('#name').html(name);
  $('#modified').html(modified);
  $('#size').html(Math.round(size / Math.pow(1024, i), 2) + ' ' + sizes[i]);
  $('#type').html(type);

});

$('#year').change(function(){
    $('#getYear').html($(this).val());
});

$('#term').change(function(){
    $('#getTerm').html($(this).val());
});

</script>
    
@endsection