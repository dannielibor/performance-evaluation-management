@extends('admin.layouts.app')

@section('title', 'Dashboard')

@section('content')

<style>
    .table-responsive{
        height:250px;
        overflow:auto;
    }
    #hoverProfile:hover{
        background-color: #800;
        border: none;
    }
</style>

<div class="m-content">
    <!--Begin::Main Portlet-->
    <div class="row">
        @if(Session::get('type') == 'Super Admin')
        <div class="col-xl-4">
        @else 
        <div class="col-xl-8">
        @endif
            <!--begin:: Widgets/Latest Updates-->
            <div class="m-portlet m-portlet--full-height m-portlet--fit ">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                Form Schedules
                            </h3>
                        </div>
                    </div>
                </div>
                <div class="m-portlet__body">
                    <div class="m-widget4 m-widget4--chart-bottom" style="min-height: 300px">
                        <div style = "height:300px;overflow:auto;">
                            <table class = "table" style = "text-align:center">
                                <thead>
                                    <tr>
                                        <th>Title</th>
                                        <th>Status</th>
                                        <th>Schedule</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach (App\Form::orderBy('created_at', 'asc')->get() as $listForms)
                                        <tr>
                                            <td style = "text-align:left">
                                                {{ $listForms->title }}
                                            </td>
                                            <td>
                                                <center>
                                                    @if($listForms->status == 'pending')
                                                        <span class="m-badge m-badge--metal m-badge--wide">Pending</span>
                                                    @elseif($listForms->status == 'active')
                                                        <span class="m-badge m-badge--success m-badge--wide">Active</span>
                                                    @elseif($listForms->status == 'inactive')
                                                        <span class="m-badge m-badge--primary m-badge--wide">Inactive</span>
                                                    @endif
                                                </center>
                                            </td>
                                            <td>
                                                <center>
                                                    @if(!empty($listForms->from))
                                                        {{ Carbon\Carbon::parse($listForms->from)->format('M. d, Y h:m A') }} - {{ Carbon\Carbon::parse($listForms->to)->format('M. d, Y h:m A') }}
                                                    @else 
                                                        ---
                                                    @endif
                                                </center>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!--end:: Widgets/Latest Updates-->
        </div>
        @if(Session::get('type') == 'Super Admin')
            <div class="col-xl-4">
                <!--begin:: Widgets/Latest Updates-->
                <div class="m-portlet m-portlet--full-height m-portlet--fit ">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <h3 class="m-portlet__head-text">
                                    Activity Logs
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <div class="tab-content" style = "height:300px;overflow:auto;">
                            <div class="tab-pane active" id="m_widget2_tab1_content">
                                <!--Begin::Timeline 3 -->
                                <div class="m-timeline-3">
                                    <div class="m-timeline-3__items">
                                        @foreach (App\log::orderBy('created_at', 'asc')->get() as $logs)
                                            <div class="m-timeline-3__item m-timeline-3__item--metal">
                                                <span class="m-timeline-3__item-time">
                                                    {{ $logs->created_at->format('g:i a') }}
                                                </span>
                                                <div class="m-timeline-3__item-desc">
                                                    <span class="m-timeline-3__item-text">
                                                        {{ $logs->description }}
                                                    </span>
                                                    <br>
                                                    <span class="m-timeline-3__item-user-name">
                                                        <a href="#" class="m-link m-link--metal m-timeline-3__item-link">
                                                            {{ $logs->created_at->format('M d, Y') }}
                                                            <br>
                                                            @if(empty($logs->user))
                                                                By <i>Deleted</i>
                                                            @else 
                                                                By {{ $logs->user->name }}
                                                            @endif
                                                        </a>
                                                    </span>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                                <!--End::Timeline 3 -->
                            </div>
                        </div>
                    </div>
                </div>
                <!--end:: Widgets/Latest Updates-->
            </div>
        @endif
        <div class="col-xl-4">
        <!--begin:: Widgets/Application Sales-->
        <div class="m-portlet__body">
            <!--begin:: Widgets/Latest Updates-->
            <div class="m-portlet m-portlet--full-height m-portlet--fit ">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                Imports
                            </h3>
                        </div>
                    </div>
                </div>
                <div class="m-portlet__body">
                    @if(empty(App\COR::first()) && empty(App\ClassLoad::first()))
                        <small>No imported files yet</small>
                    @endif
                    @if(!empty(App\COR::first()))
                        <div class="m-widget13">
                            <div class="m-widget13__item">
                                <span class="m-widget13__text m-widget13__text-bolder">
                                    Certificate of Registration
                                </span>
                            </div>
                            <div class="m-widget13__item">
                                <span class="m-widget13__desc m--align-right">
                                    Academic Year & Term:
                                </span>
                                <span class="m-widget13__text">
                                    <?php
                                        $cor = App\COR::first();
                                        echo $cor->year . ' | ' . $cor->term . 'Semester';
                                    ?>
                                </span>
                            </div>
                            <div class="m-widget13__item">
                                <span class="m-widget13__desc m--align-right">
                                    File Size:
                                </span>
                                <span class="m-widget13__text">
                                    <?php
                                        $directories = Storage::files('public/csv');

                                        foreach($directories as $directory){
                                            $explode = explode('/', $directory);
                                            $file = explode('.', $explode[2]);
                                            $cut = explode('_cor_', $file[0]);
                                            if(count($cut) > 1){
                                                $size = Storage::size($directory);
                                                $units = array('B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB');
                                                $power = $size > 0 ? floor(log($size, 1024)) : 0;
                                                echo number_format($size / pow(1024, $power)) . ' ' . $units[$power];
                                            }
                                        }
                                    ?>
                                </span>
                            </div>
                            <div class="m-widget13__item">
                                <span class="m-widget13__desc m--align-right">
                                    Last Modified:
                                </span>
                                <span class="m-widget13__text">
                                    <?php
                                        $directories = Storage::files('public/csv');

                                        foreach($directories as $directory){
                                            $explode = explode('/', $directory);
                                            $file = explode('.', $explode[2]);
                                            $cut = explode('_cor_', $file[0]);
                                            if(count($cut) > 1){
                                                echo date('M d, Y', Storage::lastModified($directory));
                                            }
                                        }
                                        
                                    ?>
                                </span>
                            </div>
                        </div>
                    @endif
                    
                    @if(!empty(App\ClassLoad::first()))
                        <div class="m-widget13">
                            <div class="m-widget13__item">
                                <span class="m-widget13__text m-widget13__text-bolder">
                                    Class Load
                                </span>
                            </div>
                            <div class="m-widget13__item">
                                <span class="m-widget13__desc m--align-right">
                                    Academic Year & Term:
                                </span>
                                <span class="m-widget13__text">
                                    <?php
                                        $class = App\ClassLoad::first();
                                        echo $class->year . ' | ' . $class->term . 'Semester';
                                    ?>
                                </span>
                            </div>
                            <div class="m-widget13__item">
                                <span class="m-widget13__desc m--align-right">
                                    File Size:
                                </span>
                                <span class="m-widget13__text">
                                    <?php
                                        $directories = Storage::files('public/csv');

                                        foreach($directories as $directory){
                                            $explode = explode('/', $directory);
                                            $file = explode('.', $explode[2]);
                                            $cut = explode('_cl_', $file[0]);
                                            if(count($cut) > 1){
                                                $size = Storage::size($directory);
                                                $units = array('B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB');
                                                $power = $size > 0 ? floor(log($size, 1024)) : 0;
                                                echo number_format($size / pow(1024, $power)) . ' ' . $units[$power];
                                            }
                                        }
                                    ?>
                                </span>
                            </div>
                            <div class="m-widget13__item">
                                <span class="m-widget13__desc m--align-right">
                                    Last Modified:
                                </span>
                                <span class="m-widget13__text">
                                    <?php
                                        $directories = Storage::files('public/csv');

                                        foreach($directories as $directory){
                                            $explode = explode('/', $directory);
                                            $file = explode('.', $explode[2]);
                                            $cut = explode('_cl_', $file[0]);
                                            if(count($cut) > 1){
                                                echo date('M d, Y', Storage::lastModified($directory));
                                            }
                                        }
                                    ?>
                                </span>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
            <!--end:: Widgets/Latest Updates-->
        </div>
        <!--end:: Widgets/Application Sales-->
        </div>
    </div>
    <!--End::Main Portlet-->

    <!--Begin::Main Portlet-->
    <div class="m-portlet">
        <div class="m-portlet__body  m-portlet__body--no-padding">
            <div class="row m-row--no-padding m-row--col-separator-xl">
                <div class="col-xl-4">
                    <!--begin:: Widgets/Daily Sales-->
                    <div class="m-widget14">
                        <div class="m-widget14__header m--margin-bottom-30">
                            <h3 class="m-widget14__title">
                                Department Rate
                            </h3>
                            <span class="m-widget14__desc">
                                Check out each collumn for more details
                            </span>
                            <div class = "row">
                                <div class = "col">
                                    <select id="year" class = "form-control">
                                        @foreach (App\FormProgress::distinct()->get(['year']) as $year)
                                            <option value="{{$year->year}}">{{$year->year}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class = "col">
                                    <select id="term" class = "form-control">
                                        @foreach (App\FormProgress::distinct()->get(['term']) as $term)
                                            <option value="{{$term->term}}">{{$term->term}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="m-widget14__chart" style="height:120px;">
                            <canvas  id="m_chart_daily_sales"></canvas>
                        </div>
                    </div>
                    <!--end:: Widgets/Daily Sales-->
                </div>
                <div class="col-xl-4">
                    <!--begin:: Widgets/Profit Share-->
                    <div class="m-widget14">
                        <div class="m-widget14__header">
                            <h3 class="m-widget14__title">
                                Department Progress
                            </h3>
                            <span class="m-widget14__desc">
                                Form progress per department
                            </span>
                        </div>
                        <div class = "table-responsive">
                            <table class = "table">
                                <thead style = "text-align:center">
                                    <tr>
                                        <th width = "50%" scope="col">Department</th>
                                        <th scope="col">Progress</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(count(App\Faculty::distinct()->get(['department'])) > 0)
                                        @foreach (App\Faculty::distinct()->get(['department']) as $department)
                                            <?php
                                                $faculty = App\Faculty::where('department', $department->department)->get();
                                                $total = 0;
                                                $result = 0;
                                                foreach($faculty as $fac){
                                                    if($fac->status == 'completed'){
                                                        $result = ++$result;
                                                    }
                                                    $total = ++$total;
                                                }
                                                $percent = round(($result/$total) * 100);
                                            ?>
                                            <tr>
                                                <td>
                                                    {{$department->department}}
                                                </td>
                                                <td>
                                                    @if($percent < 50)
                                                        <?php $rate = 'danger'; ?>
                                                    @elseif($percent < 100)
                                                        <?php $rate = 'warning'; ?>
                                                    @else 
                                                        <?php $rate = 'success'; ?>
                                                    @endif
                                                    <span class="m-widget1__number m--font-{{$rate}}">
                                                        <strong>
                                                            {{$percent}}%
                                                        </strong>
                                                    </span>
                                                    <div class="progress m-progress--sm">
                                                        <div class="progress-bar bg-{{$rate}} progress-bar-animated" role="progressbar" style="width: {{$percent}}%" aria-valuenow="{{$percent}}" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else 
                                        <tr>
                                            <td colspan="2" style = "text-align:center">No records found</td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!--end:: Widgets/Profit Share-->
                </div>
                <div class="col-xl-4">
                    <!--begin:: Widgets/Profit Share-->
                    <div class="m-widget14">
                        <div class="m-widget14__header">
                            <h3 class="m-widget14__title">
                                Section Progress
                            </h3>
                            <span class="m-widget14__desc">
                                Form progress per section
                            </span>
                            <select id="selectSection" class = "form-control">
                                @foreach (App\Student::distinct()->get(['program']) as $programs)
                                    <option value="{{$programs->program}}">{{$programs->program}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class = "table-responsive">
                            <table class = "table">
                                <thead style = "text-align:center">
                                    <tr>
                                        <th width = "50%" scope="col">Section</th>
                                        <th scope="col">Progress</th>
                                    </tr>
                                </thead>
                                <tbody id = "sectionProgress">
                                    @include('admin.section_progress')
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!--end:: Widgets/Profit Share-->
                </div>
            </div>
        </div>
    </div>
    <!--End::Main Portlet-->

    <!--Begin::Main Portlet-->
    <div class="row">
        <div class="col-xl-6">
            <!--begin:: Widgets/User Progress -->
            <div class="m-portlet m-portlet--full-height ">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                Student Progress
                            </h3>
                        </div>
                    </div>
                    <div class="m-portlet__head-tools">
                        <ul class="m-portlet__nav" style = "list-style:none ">
                            <li class="m-portlet__nav-item">
                                <select id="chooseForm" class = "form-control">
                                    @foreach (App\Form::where('status', 'active')->orderBy('created_at', 'asc')->get() as $form)
                                        <option value="{{$form->id}}">{{$form->title}}</option>
                                    @endforeach
                                </select>
                            </li>
                            <li class="m-portlet__nav-item">
                                <div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" data-dropdown-toggle="hover" aria-expanded="true">
                                    <a href="#" class="m-portlet__nav-link btn btn-lg btn-secondary  m-btn m-btn--icon m-btn--icon-only m-btn--pill  m-dropdown__toggle">
                                        <i class="la la-ellipsis-h m--font-brand"></i>
                                    </a>
                                    <div class="m-dropdown__wrapper">
                                        <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
                                        <div class="m-dropdown__inner">
                                            <div class="m-dropdown__body">
                                                <div class="m-dropdown__content">
                                                    <ul class="m-nav">
                                                        <li class="m-nav__item">
                                                            <a href="#" data-toggle="modal" data-target="#filter" class="m-nav__link">
                                                                <i class="m-nav__link-icon flaticon-info"></i>
                                                                <span class="m-nav__link-text">
                                                                    Filter
                                                                </span>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="m-portlet__body">
                    <div class="tab-content">
                        <div class="tab-pane active" id="m_widget4_tab1_content">
                            <div class="m-widget4 m-widget4--progress" id = "loadStudent">
                                @include('admin.load_students')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end:: Widgets/User Progress -->
        </div>
        <div class="col-xl-6">
            <!--begin:: Widgets/User Progress -->
            <div class="m-portlet m-portlet--full-height ">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                Faculty Progress
                            </h3>
                        </div>
                    </div>
                    <div class="m-portlet__head-tools">
                        <ul class="m-portlet__nav" style = "list-style:none ">
                            <li class="m-portlet__nav-item">
                                <select id="getForm" class = "form-control">
                                    @foreach (App\Form::where('status', 'active')->orderBy('created_at', 'asc')->get() as $form)
                                        <option value="{{$form->id}}">{{$form->title}}</option>
                                    @endforeach
                                </select>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="m-portlet__body">
                    <select class = "form-control" id="chooseDepartment">
                        @foreach (App\Faculty::distinct()->get(['department']) as $dept)
                            <option value="{{$dept->department}}">{{$dept->department}}</option>
                        @endforeach
                    </select>
                    <br>
                    <div class="tab-content">
                        <div class="tab-pane active" id="m_widget4_tab1_content">
                            <div class="m-widget4 m-widget4--progress" id = "loadFaculty">
                                @include('admin.faculties')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end:: Widgets/User Progress -->
        </div>
    </div>
    <!--End::Main Portlet-->
</div>

<!-- Modal -->
<div class="modal fade" id="filter" tabindex="-1" role="dialog" aria-labelledby="filter" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="filter">Filter</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div class = "form-group row">
                <div class = "col">
                    <select id="chooseProgram" class = "form-control">
                        @foreach (App\Student::distinct()->get(['program']) as $programs)
                            <option value="{{$programs->program}}">{{$programs->program}}</option>
                        @endforeach
                    </select>
                </div>
                <div class = "col" id = "inputSection">
                    @include('admin.choose_section')
                </div>
            </div>
            <div class = "form-group" id = "inputSchedule">
                @include('admin.schedule')
            </div>
        </div>
        </div>
    </div>
</div>

<script src="{{asset('metronic/assets/app/js/dashboard.js')}}" type="text/javascript"></script>

<script>

    $('#selectSection').change(function(){
        $.ajax({
            url: '/dashboard/selectSection',
            type: 'get',
            data: { value: $(this).val() },
            success: function(response){
                $('#sectionProgress').html(response);
            }
        });
    });

    $('#chooseProgram').change(function(){
        $.ajax({
            url: '/dashboard/chooseSection',
            type: 'get',
            data: { value: $(this).val() },
            success: function(response){
                $('#inputSection #setSection').html(response);
            }
        });
    });

    $('#inputSection #setSection').change(function(){
        $.ajax({
            url: '/dashboard/setStudents',
            type: 'get',
            data: { section: $(this).val(), program: $('#chooseProgram').val() },
            success: function(response){
                $('#inputSchedule #chooseSchedule').html(response);
            }
        });
    });

    $('#inputSchedule #chooseSchedule').change(function(){
        $.ajax({
            url: '/dashboard/setSchedules',
            type: 'get',
            data: { form: $('#chooseForm').val() ,section: $('#inputSection #setSection').val(), program: $('#chooseProgram').val(), schedule: $(this).val() },
            success: function(response){
                $('#loadStudent').html(response);
            }
        });
    });

    $('#chooseForm').change(function(){
        $.ajax({
            url: '/dashboard/setSchedules',
            type: 'get',
            data: { form: $(this).val(), section: $('#inputSection #setSection').val(), program: $('#chooseProgram').val(), schedule: $('#inputSchedule #chooseSchedule').val() },
            success: function(response){
                $('#loadStudent').html(response);
            }
        });
    });

    $('#chooseDepartment').change(function(){
        $.ajax({
            url: '/dashboard/setFaculty',
            type: 'get',
            data: { form: $('#getForm').val(), department: $(this).val() },
            success: function(response){
                $('#loadFaculty').html(response);
            }
        });
    });

    $('#getForm').change(function(){
        $.ajax({
            url: '/dashboard/setFaculty',
            type: 'get',
            data: { form: $(this).val(), department: $('#chooseDepartment').val() },
            success: function(response){
                $('#loadFaculty').html(response);
            }
        });
    });

    $('.res').click(function(e) {
        e.stopPropagation();
    });

</script>

@endsection