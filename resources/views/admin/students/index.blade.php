@extends('admin.layouts.app')

@section('title', 'User Management')

@section('content')

<style>

  .avatar-upload {
    position: relative;
    max-width: 205px;
    margin: 10px auto;
    }
    .avatar-upload .avatar-edit {
    position: absolute;
    right: 12px;
    z-index: 1;
    top: 10px;
    }
    .avatar-upload .avatar-edit input, #imageUpload {
    display: none;
    }
    .avatar-upload .avatar-edit input + label {
    display: inline-block;
    width: 34px;
    height: 34px;
    margin-bottom: 0;
    border-radius: 100%;
    background: #FFFFFF;
    border: 1px solid transparent;
    box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.12);
    cursor: pointer;
    font-weight: normal;
    transition: all 0.2s ease-in-out;
    }
    .avatar-upload .avatar-edit input + label:hover {
    background: #f1f1f1;
    border-color: #d6d6d6;
    }
    .avatar-upload .avatar-edit input + label:after {
    content: "\f040";
    font-family: 'FontAwesome';
    color: #757575;
    position: absolute;
    top: 10px;
    left: 0;
    right: 0;
    text-align: center;
    margin: auto;
    }
    .avatar-upload .avatar-preview {
    width: 192px;
    height: 192px;
    position: relative;
    border-radius: 100%;
    border: 6px solid #F8F8F8;
    box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.1);
    }
    .avatar-upload .avatar-preview > div {
    width: 100%;
    height: 100%;
    border-radius: 100%;
    background-size: cover;
    background-repeat: no-repeat;
    background-position: center;
    }

    input[type='text']{
        text-transform: capitalize;
    }

    .student-img{
        border-radius:100%;
        width: 15%;
        margin-right:20px;
    }

    #html_table .btn{
        border-radius:50px;
    }
    i.fa.fa-caret-right{
        color: #800 !important;
    }
    i.fa.fa-caret-down{
        color: #800 !important;
    }
</style>

<div class="m-content">
    <!--begin:: Widgets/Stats-->
    <div class="m-portlet ">
        <div class="m-portlet__body  m-portlet__body--no-padding">
            <div class="row m-row--no-padding m-row--col-separator-xl">
                <div class="col-md-12 col-lg-6 col-xl-4">
                    <!--begin::Total Profit-->
                    <div class="m-widget24">
                        <div class="m-widget24__item">
                            <h4 class="m-widget24__title">
                                Students
                            </h4>
                            <br>
                            <span class="m-widget24__desc">
                                All Students
                            </span>
                            <span class="m-widget24__stats m--font-accent">
                                <?php
                                    $allStudent = App\Student::all();
                                ?>
                                {{ count($allStudent) }}
                            </span>
                            <div class="m--space-10"></div>
                            <?php
                                $cor = App\COR::distinct()->get(['studentNo']);
                                $enrolled = 0;
                                if(count($cor) > 0){
                                    foreach($cor as $stud){
                                        $student = App\Student::where('studentNo', $stud->studentNo)->first();
                                        if(!empty($student)){
                                            $enrolled = ++$enrolled;
                                        }
                                    }
                                }else{

                                }
                            ?>
                            @if( count($allStudent) == 0 && $enrolled == 0 )
                                <div class="progress m-progress--sm">
                                    <div class="progress-bar m--bg-accent" role="progressbar" style="width: 0%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <span class="m-widget24__change">
                                    Enrolled Students
                                </span>
                                <span class="m-widget24__number">
                                    0%
                                </span>
                            @else 
                                <div class="progress m-progress--sm">
                                    <div class="progress-bar m--bg-accent" role="progressbar" style="width: {{ ($enrolled/count($allStudent)) * 100 }}%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <span class="m-widget24__change">
                                    Enrolled Students
                                </span>
                                <span class="m-widget24__number">
                                    {{ ($enrolled/count($allStudent)) * 100 }}%
                                </span>
                            @endif
                        </div>
                    </div>
                    <!--end::Total Profit-->
                </div>
                <div class="col-md-12 col-lg-6 col-xl-4">
                    <!--begin::New Feedbacks-->
                    <div class="m-widget24">
                        <div class="m-widget24__item">
                            <h4 class="m-widget24__title">
                                Pending
                            </h4>
                            <br>
                            <span class="m-widget24__desc">
                                Status
                            </span>
                            <span class="m-widget24__stats m--font-danger">
                                <?php
                                    $penRes = 0;
                                    $penTot = 0;
                                    $pending = App\Student::all();
                                    foreach($pending as $pen){
                                        if($pen->status == 'pending'){
                                            $penRes = ++$penRes;
                                        }
                                        $penTot = ++$penTot;
                                    }
                                ?>
                                {{ $penRes }}
                            </span>
                            <div class="m--space-10"></div>
                            @if( $penRes == 0 && $penTot == 0 )
                                <div class="progress m-progress--sm">
                                    <div class="progress-bar m--bg-danger" role="progressbar" style="width: 0%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <span class="m-widget24__change">
                                    Progress
                                </span>
                                <span class="m-widget24__number">
                                    0%
                                </span>
                            @else 
                                <div class="progress m-progress--sm">
                                    <div class="progress-bar m--bg-danger" role="progressbar" style="width: {{ ($penRes/$penTot) * 100 }}%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <span class="m-widget24__change">
                                    Progress
                                </span>
                                <span class="m-widget24__number">
                                    {{ round(($penRes/$penTot) * 100) }}%
                                </span>
                            @endif
                        </div>
                    </div>
                    <!--end::New Feedbacks-->
                </div>
                <div class="col-md-12 col-lg-6 col-xl-4">
                    <!--begin::New Orders-->
                    <div class="m-widget24">
                        <div class="m-widget24__item">
                            <h4 class="m-widget24__title">
                                Completed
                            </h4>
                            <br>
                            <span class="m-widget24__desc">
                                Status
                            </span>
                            <span class="m-widget24__stats m--font-success">
                                <?php
                                    $comRes = 0;
                                    $comTot = 0;
                                    $completed = App\Student::all();
                                    foreach($completed as $com){
                                        if($com->status == 'completed'){
                                            $comRes = ++$comRes;
                                        }
                                        $comTot = ++$comTot;
                                    }
                                ?>
                                {{ $comRes }}
                            </span>
                            <div class="m--space-10"></div>
                            @if( $comRes == 0 && $comTot == 0 )
                                <div class="progress m-progress--sm">
                                    <div class="progress-bar m--bg-success" role="progressbar" style="width: 0%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <span class="m-widget24__change">
                                    Progress
                                </span>
                                <span class="m-widget24__number">
                                    0%
                                </span>
                            @else 
                                <div class="progress m-progress--sm">
                                    <div class="progress-bar m--bg-success" role="progressbar" style="width: {{ ($comRes/$comTot) * 100 }}%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <span class="m-widget24__change">
                                    Progress
                                </span>
                                <span class="m-widget24__number">
                                    {{ round(($comRes/$comTot) * 100) }}%
                                </span>
                            @endif
                        </div>
                    </div>
                    <!--end::New Orders-->
                </div>
            </div>
        </div>
    </div>
    <!--end:: Widgets/Stats--> 
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head" style = "background-color:#f4f4f4">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        Students
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-portlet__body">
            <!--begin: Search Form -->
            <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                <div class="row align-items-center">
                    <div class="col-xl-8 order-2 order-xl-1">
                        <div class="form-group m-form__group row align-items-center">
                            <div class="col-md-3">
                                <div class="m-form__group m-form__group--inline">
                                    <div class="m-form__label">
                                        <label>
                                            Status:
                                        </label>
                                    </div>
                                    <div class="m-form__control">
                                        <select class="form-control m-bootstrap-select" id="m_form_status">
                                            <option value="">
                                                All
                                            </option>
                                            <option value="pending">
                                                Pending
                                            </option>
                                            <option value="completed">
                                                Completed
                                            </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="d-md-none m--margin-bottom-10"></div>
                            </div>
                            <div class="col-md-5">
                                <div class="m-form__group m-form__group--inline">
                                    <div class="m-form__label">
                                        <label>
                                            Program:
                                        </label>
                                    </div>
                                    <div class="m-form__control">
                                        <select class="form-control m-bootstrap-select" id="m_form_program">
                                            <option value="">
                                                All
                                            </option>
                                            <option value="Bachelor of Science in Information Technology">Bachelor of Science in Information Technology</option>
                                            {{-- @foreach ($programs as $item)
                                                <option value="{{$item->program}}">{{$item->program}}</option>
                                            @endforeach --}}
                                        </select>
                                    </div>
                                </div>
                                <div class="d-md-none m--margin-bottom-10"></div>
                            </div>
                            <div class="col-md-4">
                                <div class="m-input-icon m-input-icon--left">
                                    <input type="text" class="form-control m-input" placeholder="Search..." id="generalSearch">
                                    <span class="m-input-icon__icon m-input-icon__icon--left">
                                        <span>
                                            <i class="la la-search"></i>
                                        </span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end: Search Form -->
            <!--begin: Datatable -->
            <div class="m_datatable" id="child_data_local"></div>
            <!--end: Datatable -->
        </div>
    </div>
</div>

<script src="{{ asset('metronic/assets/demo/default/custom/components/datatables/child/data-local.js') }}" type="text/javascript"></script>

@endsection