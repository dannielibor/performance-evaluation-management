<select id="setSection" class = "form-control">
    <?php
        $blocks = [];
        $students = App\Student::where('program', $program)->get();
    ?>
    @foreach ($students as $student)
        <?php array_push($blocks, $student->studentNo); ?>
    @endforeach
    
    <?php $cor = App\COR::whereIn('studentNo', $blocks)->distinct()->get(['section']); ?>
    
    @foreach ($cor as $load)
        <option value="{{$load->section}}">{{$load->section}}</option>
    @endforeach                            
</select>