<select id="chooseSchedule" class = "form-control">
    @foreach (App\COR::where('section', $getSection)->distinct()->get(['code']) as $item)
        <?php $check = App\COR::where('code', $item->code)->first(); $explode = explode(' ', $check->subject); ?>
        @if(last($explode) != 'Laboratory')
            <option value="{{ $check->code }}">{{ $check->code }} | {{ $check->subject }} | {{ $check->schedule }} | {{ $check->room }}</option>
        @endif
    @endforeach            
</select>