{{-- @extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Reset Password') }}</div>

                <div class="card-body">
                    <form method="POST" action="password/reset">
                        @csrf

                        <input type="hidden" name="token" value="{{ $token }}">

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ $email ?? old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Reset Password') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection --}}

<!DOCTYPE html>
<html lang="en" >
	<!-- begin::Head -->
	<head>
		<meta charset="utf-8" />
		<title>
			SBCA - FPES
		</title>
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<meta name="description" content="Latest updates and statistic charts">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!--begin::Web font -->
		<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
		<script>
          WebFont.load({
            google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
          });
		</script>
		<!--end::Web font -->
        <!--begin::Base Styles --> 
		<link href="{{ asset('metronic/assets/vendors/base/vendors.bundle.css') }}" rel="stylesheet" type="text/css" />
		<link href="{{ asset('metronic/assets/demo/default/base/style.bundle.css') }}" rel="stylesheet" type="text/css" />
		<!--end::Base Styles -->
        <link rel="shortcut icon" href="{{ asset('images/sbca.png') }}" />

        <style>

		@media (min-width: 993px) {
            .m-footer--push.m-aside-left--enabled:not(.m-footer--fixed) .m-footer {
                margin-left: 0px !important;
            }
		}
		
            input::-webkit-input-placeholder {
                color: white !important;
            }
            input{
                background-color: #00000040 !important;
                color: white !important;
            }
            label{
                color: white !important;
            }
            a{
                color: white !important;
            }
            .m-login__account-msg{
                color: white !important;
            }
            .m-login__desc{
                color: white !important;
            }
            button{
                color: white !important;
                border: 1px solid white !important;
			}
			.close{
				color:red !important;
			}
			input[type='number']::-webkit-outer-spin-button,
			input[type='number']::-webkit-inner-spin-button {
				-webkit-appearance: none;
			}

			.m-nav .m-nav__item:hover:not(.m-nav__item--disabled) > .m-nav__link .m-nav__link-icon,
			.m-nav .m-nav__item:hover:not(.m-nav__item--disabled) > .m-nav__link .m-nav__link-text,
			.m-nav .m-nav__item:hover:not(.m-nav__item--disabled) > .m-nav__link .m-nav__link-arrow, .m-nav .m-nav__item.m-nav__item--active > .m-nav__link .m-nav__link-icon,
			.m-nav .m-nav__item.m-nav__item--active > .m-nav__link .m-nav__link-text,
			.m-nav .m-nav__item.m-nav__item--active > .m-nav__link .m-nav__link-arrow {
			color: #fff; }

			/* Scrollbar styles */
			::-webkit-scrollbar {
				width: 12px;
				height: 12px;
			}

			::-webkit-scrollbar-track {
				border-radius: 12px;
			}

			::-webkit-scrollbar-track:vertical {
				background: #f1f2f7; 
			}

			::-webkit-scrollbar-thumb {
				background: #5a5a5a;  
				border-radius: 12px;
			}

        </style>

	</head>
	<!-- end::Head -->
	@if(isset($notify))
		<small id = "notify" style = "position:absolute;visibility:hidden">notify</small>
	@endif
    <!-- end::Body -->
	<body class="m--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">
		<!-- begin:: Page -->
		<div class="m-grid m-grid--hor m-grid--root m-page">
			<div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor m-login m-login--signin m-login--2 m-login-2--skin-1" id="m_login" style="background-image: url({{ asset('images/bg.jpg') }});">
				<div class="m-grid__item m-grid__item--fluid m-login__wrapper">
					<div class="m-login__container">
						<div class="m-login__logo">
							<a href="">
								<img src="{{ asset('images/sbca.png') }}" alt = "San Beda College Alabang" width = "100">
							</a>
						</div>
						<div class="m-login__signin">
							<div class="m-login__head">
								<h3 class="m-login__title">
									Reset Password
								</h3>
							</div>
							<form class="m-login__form m-form" role = "form" method="POST" action="{{route('password.reset')}}">
                                @csrf
                                <input type="hidden" name="token" value="{{ $token }}">
								<div class="form-group m-form__group">
                                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }} m-input" name="email" value="{{ $email ?? old('email') }}" required autofocus>

                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
								<div class="form-group m-form__group">
                                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }} m-input m-login__form-input--last" name="password" required placeholder="New Password">

                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group m-form__group">
                                    <input id="password-confirm" type="password" class="form-control m-input m-login__form-input--last" name="password_confirmation" required placeholder="Confirm Password">
                                </div>
								<div class="m-login__form-action">
									<button type = "submit" class="btn m-btn m-btn--pill m-btn--custom m-btn--air m-login__btn m-login__btn--primary">
                                        {{ __('Reset Password') }}
									</button>
									&nbsp;&nbsp;
									<a href = "/" style = "border-color:white" class="btn m-btn m-btn--pill m-btn--custom m-btn--air m-login__btn">
										Cancel
									</a>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
			<footer class="m-grid__item	m-footer" style = "background-color:black">
				<div class="m-container m-container--fluid m-container--full-height m-page__container">
					<div class="m-stack m-stack--flex-tablet-and-mobile m-stack--ver m-stack--desktop">
						<div class="m-stack__item m-stack__item--left m-stack__item--middle m-stack__item--last">
							<span class="m-footer__copyright">
								{{ Carbon\Carbon::now()->format('Y') }} &copy; Faculty Performance Evaluation by
								<a href="https://www.sanbeda-alabang.edu.ph/bede/" target="_blank" class="m-link">
									San Beda College Alabang
								</a>
							</span>
						</div>
						<div class="m-stack__item m-stack__item--right m-stack__item--middle m-stack__item--first">
							<ul class="m-footer__nav m-nav m-nav--inline m--pull-right">
								<li class="m-nav__item">
									<a href="#" class="m-nav__link" data-toggle="modal" data-target="#about">
										<span class="m-nav__link-text">
											About
										</span>
									</a>
								</li>
								<li class="m-nav__item">
									<a href="#"  class="m-nav__link" data-toggle="modal" data-target="#privacy">
										<span class="m-nav__link-text">
											Privacy
										</span>
									</a>
								</li>
								<li class="m-nav__item">
									<a href="#" class="m-nav__link" data-toggle="modal" data-target="#TnC">
										<span class="m-nav__link-text">
											T&C
										</span>
									</a>
								</li>
								<li class="m-nav__item">
									<a href="#" class="m-nav__link" data-toggle="modal" data-target="#support">
										<span class="m-nav__link-text">
											Support
										</span>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</footer>
		</div>
		<!-- end:: Page -->
		<!-- About -->
		<div class="modal fade" id="about" tabindex="-1" role="dialog" aria-labelledby="about" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered" role="document">
				<div class="modal-content">
					<div class="modal-header">
					<h5 class="modal-title" id="about">About</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					</div>
					<div class="modal-body">
						Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
					</div>
					<div class="modal-footer">
					<button type="button" class="btn btn-metal" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		  </div>
		<!-- Privacy -->
		<div class="modal fade" id="privacy" tabindex="-1" role="dialog" aria-labelledby="privacy" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered" role="document">
				<div class="modal-content">
					<div class="modal-header">
					<h5 class="modal-title" id="privacy">Privacy</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					</div>
					<div class="modal-body">
						Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
					</div>
					<div class="modal-footer">
					<button type="button" class="btn btn-metal" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>
		<!-- TnC -->
		<div class="modal fade" id="TnC" tabindex="-1" role="dialog" aria-labelledby="TnC" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered" role="document">
				<div class="modal-content">
					<div class="modal-header">
					<h5 class="modal-title" id="TnC">Terms And Conditions</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					</div>
					<div class="modal-body">
						Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
					</div>
					<div class="modal-footer">
					<button type="button" class="btn btn-metal" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>
		<!-- Support -->
		<div class="modal fade" id="support" tabindex="-1" role="dialog" aria-labelledby="support" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered" role="document">
				<div class="modal-content">
					<div class="modal-header">
					<h5 class="modal-title" id="support">Support</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					</div>
					<div class="modal-body">
						Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
					</div>
					<div class="modal-footer">
					<button type="button" class="btn btn-metal" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>
    	<!--begin::Base Scripts -->
		<script src="{{ asset('metronic/assets/vendors/base/vendors.bundle.js') }}" type="text/javascript"></script>
		<script src="{{ asset('metronic/assets/demo/default/base/scripts.bundle.js') }}" type="text/javascript"></script>
		<!--end::Base Scripts -->   
        <!--begin::Page Snippets -->
		<script src="{{ asset('metronic/assets/snippets/pages/user/login.js') }}" type="text/javascript"></script>
		<!--end::Page Snippets -->

		<script src="{{asset('js/password-strength-meter/dist/password.min.js')}}"></script>
		<link type="text/css" rel="stylesheet" href="{{asset('js/password-strength-meter/dist/password.min.css')}}" />

		<script>

			$('#reg-pass').password({
				shortPass: 'The password is too short',
				badPass: 'Weak, Try combining letters & numbers',
				goodPass: 'Medium, try using special charecters',
				strongPass: 'Strong password',
				enterPass: 'Type your password',
				showPercent: false,
				showText: true, // shows the text tips
				animate: true, // whether or not to animate the progress bar on input blur/focus
				animateSpeed: 'fast', // the above animation speed
				minimumLength: 4 // minimum password length (below this threshold, the score is 0)
			});
			
			$('#reg-pass').on('password.text', (e, text, score) => {
				valid = text;
			})
	
		</script>
		
	</body>
	<!-- end::Body -->
</html>
