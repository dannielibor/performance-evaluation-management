@foreach ($profs as $prof)

    <?php
        $report = 0;

        $elements = App\Element::where('type', $prof->type)->get();

        foreach($elements as $element){

            $subs = App\SubElement::where('elementId', $element->id)->get();
            
            if(count($subs) > 0){

                $totalSub = 0;
                foreach($subs as $sub){
                    $subRate = [];
                    
                    $relations = App\FormRelationship::where('formId', $element->formId)->where('evaluatee', $prof->type)->get();
                    foreach($relations as $relation){

                        $score = 0;

                        if($relation->status == 'Distinct'){
                            $evaluators = App\Faculty::where('type', $relation->evaluator)->where('department', $prof->department)->get();
                        }else{
                            $evaluators = App\Faculty::where('type', $relation->evaluator)->get();
                        }

                        foreach($evaluators as $evaluator){
                        
                            if($sub->type == $evaluator->type){
                                
                                $formProgress = App\FormProgress::where('formId', $element->formId)
                                                                    ->where('evaluatee', $prof->userId)
                                                                    ->where('evaluator', $evaluator->userId)
                                                                    ->where('year', $year)
                                                                    ->where('term', $term)
                                                                    ->where('status', 'completed')->first();
                                if(!empty($formProgress)){
                                    $score = $score + $formProgress->total;
                                }
                                
                                array_push($subRate, $score);

                            }
                            
                        }
                        
                    } 
                    if(array_sum($subRate) == 0 && count($subRate) == 0){
                        $total = 0;
                    }else{
                        $total = array_sum($subRate) / count($subRate);
                    }
                    $totalSub = $totalSub + ($total*($sub->rate/100));
                }
                $report = $report + $totalSub;
            }else{

                if($element->selfEvaluation == '1'){
                    $progress = App\FormProgress::select(DB::raw('SUM(total) as total'))
                                            ->where('formId', $element->formId)
                                            ->where('status', 'completed')
                                            ->where('evaluatee', $prof->userId)
                                            ->where('evaluator', $prof->userId)
                                            ->where('year', $year)
                                            ->where('term', $term)->first();

                    $count = App\FormProgress::where('formId', $element->formId)
                                            ->where('status', 'completed')
                                            ->where('evaluatee', $prof->userId)
                                            ->where('evaluator', $prof->userId)
                                            ->where('year', $year)
                                            ->where('term', $term)->get();
                }else{
                    $progress = App\FormProgress::select(DB::raw('SUM(total) as total'))
                                            ->where('formId', $element->formId)
                                            ->where('status', 'completed')
                                            ->where('evaluatee', $prof->userId)
                                            ->where('evaluator', '!=', $prof->userId)
                                            ->where('year', $year)
                                            ->where('term', $term)->first();

                    $count = App\FormProgress::where('formId', $element->formId)
                                            ->where('status', 'completed')
                                            ->where('evaluatee', $prof->userId)
                                            ->where('evaluator', '!=', $prof->userId)
                                            ->where('year', $year)
                                            ->where('term', $term)->get();
                }

                if(!empty($progress) && count($count) > 0){
                    
                    $report = $report + (($progress->total/count($count))*($element->rate/100));
                    
                }

            }

        }
        $total = round($report, 2);
    ?>

    <tr>
        <td>
            <img src="/storage/profiles/{{$prof->user->image}}" alt="{{$prof->user->image}}" class = "user-img">&nbsp;{{$prof->user->name}}
        </td>
        <td>
            <center>
                {{$prof->department}}
            </center>
        </td>
        <td>
            <center>
                @if($total > 4.99)
                    <span class = "m-badge m-badge--success m-badge--wide">Outstanding</span>
                @elseif($total > 3.99)
                    <span class = "m-badge m-badge--success m-badge--wide">Very Satisfactory</span>
                @elseif($total > 2.99)
                    <span class = "m-badge m-badge--success m-badge--wide">Satisfactory</span>
                @elseif($total > 1.99)
                    <span class = "m-badge m-badge--warning m-badge--wide">Moderately Satisfactory</span>
                @else 
                    <span class = "m-badge m-badge--danger m-badge--wide">Needs Improvement</span>
                @endif
            </center>
        </td>
        <td>
            <center>
                {{ $total }}
            </center>
        </td>
        <td>
            <center>
                {!! Form::open(['action' => ['Guest\RankController@update', $prof->id], 'method' => 'post']) !!}
                @csrf
                {!! Form::hidden('_method', 'PUT') !!}
                <input type="hidden" value = "{{$year}}" name = "year">
                <input type="hidden" value = "{{$term}}" name = "term">
                    <button style = "background:none;border:none" type = "submit" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="View">
                        <i class="la la-angle-right"></i>
                    </button>
                {!! Form::close() !!}
            </center>
        </td>
    </tr>
@endforeach

{{ $profs->links() }}

<script src="{{ asset('metronic/assets/demo/default/custom/components/datatables/base/html-table.js') }}" type="text/javascript"></script>

<script>
    $(document).ready(function(){
        var thIndex = 0,
        curThIndex = null;

        $(function(){
            // $('table thead tr th').click(function(){
            //     thIndex = $(this).index();
            //     if(thIndex != curThIndex){
            //         curThIndex = thIndex;
            //         sorting = [];
            //         tbodyHtml = null;
            //         $('table tbody tr').each(function(){
            //             sorting.push($(this).children('td').eq(curThIndex).html() + ', ' + $(this).index());
            //         });
                    
            //         sorting = sorting.sort();
            //         sortIt();
            //     }
            // });

            thIndex = $('#sort').index();
            if(thIndex != curThIndex){
                curThIndex = thIndex;
                sorting = [];
                tbodyHtml = null;
                $('table tbody tr').each(function(){
                    sorting.push($(this).children('td').eq(curThIndex).html() + ', ' + $(this).index());
                });
                
                sorting = sorting.sort();
                sortIt();
            }
        });

        function sortIt(){
            for(var sortingIndex = 0; sortingIndex < sorting.length; sortingIndex++){
                rowId = parseInt(sorting[sortingIndex].split(', ')[1]);
                tbodyHtml = tbodyHtml + $('table tbody tr').eq(rowId)[0].outerHTML;
            }
            $('table tbody').html(tbodyHtml);
        }
    });
</script>