@extends('guest.layouts.app')

@section('title', 'Data Analysis')

@section('content')

<style>
    .user-img{
        border-radius:100%;
        width: 15%;
        margin-right:20px;
    }

    #html_table button{
        border-radius:50px;
    }
</style>

<div class="m-content">
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head" style = "background-color:#f4f4f4">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        Rank
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-portlet__body table-responsive">
            <div class = "row align-items-center" id = "topbar">
                <div class="col-xl-4 order-2 order-xl-1">
                    <div class="form-group m-form__group row align-items-center">
                        <div class="col-md-12">
                            <div class="m-input-icon m-input-icon--left">
                                <input type="text" class="form-control m-input" placeholder="Search..." id="generalSearch">
                                <span class="m-input-icon__icon m-input-icon__icon--left">
                                    <span>
                                        <i class="la la-search"></i>
                                    </span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-2 order-2 order-xl-1">
                    <div class="form-group m-form__group row align-items-center">
                        <div class="col-md-12">
                            <select class="form-control" id = "dept">
                                <option value="">Select a department</option>
                                @foreach (App\Faculty::distinct()->get(['department']) as $department)
                                    <option value="{{$department->department}}">{{$department->department}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-xl-2 order-2 order-xl-1">
                    <div class="form-group m-form__group row align-items-center">
                        <div class="col-md-12">
                            <select class="form-control" id = "year">
                                <option value="">Select a year</option>
                                @foreach (App\FormProgress::distinct()->get(['year']) as $year)
                                    <option value="{{$year->year}}">{{$year->year}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-xl-2 order-2 order-xl-1">
                    <div class="form-group m-form__group row align-items-center">
                        <div class="col-md-12">
                            <select class="form-control" id = "term">
                                <option value="">Select a semester</option>
                                <option value="1st">1st Semester</option>
                                <option value="2nd">2nd Semester</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <!--begin: Datatable -->
            <table class = "table" width="100%" id = "mytable">
                <thead style = "text-align:center">
                    <tr>
                        <th title="Field #1">
                            Name
                        </th>
                        <th title="Field #2">
                            Department
                        </th>
                        <th title="Field #3">
                            Rank
                        </th>
                        <th title="Field #4" id = "sort">
                            Over-all Rating
                        </th>
                        <th title="Field #5">
                            Action
                        </th>
                    </tr>
                </thead>
                <tbody id = "table1">
                    @include('guest.summary.rank.rank')
                </tbody>
            </table>
            <!--end: Datatable -->
        </div>
    </div>
</div>

<script>
    $("#generalSearch").on("keyup", function() {
        var value = $(this).val().toLowerCase();
        $("tbody tr").filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });
    $('#dept').change(function(){
        $.ajax({
            url: '/summary/rank/load',
            type: 'get',
            data: { dept: $(this).val(), year: $('#year').val(), term: $('#term').val() },
            success: function(response){
                $('tbody').html(response);
            }
        });
    });
    $('#year').change(function(){
        $.ajax({
            url: '/summary/rank/load',
            type: 'get',
            data: { year: $(this).val(), dept: $('#dept').val(), term: $('#term').val() },
            success: function(response){
                $('tbody').html(response);
            }
        });
    });
    $('#term').change(function(){
        $.ajax({
            url: '/summary/rank/load',
            type: 'get',
            data: { term: $(this).val(), year: $('#year').val(), dept: $('#dept').val() },
            success: function(response){
                $('tbody').html(response);
            }
        });
    });
</script>

@endsection