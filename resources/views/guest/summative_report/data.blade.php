@if(isset($elements))
    <?php $overall = 0; $totalElements = 0; $selected = App\Faculty::where('userId', $user)->first(); ?>
    @foreach ($elements as $element) 
        @if(!empty($element->form))
            <tr>
                <td>
                    @if($element->selfEvaluation == 1)
                        Self Evaluation - {{$element->form->title}}
                    @else 
                        {{$element->form->title}}
                    @endif
                </td>
                <td></td>
                <td>
                    {{ $element->rate }}%
                    @if(count(App\SubElement::where('elementId', $element->id)->get()) < 1)
                        <?php $totalElements = $totalElements + $element->rate; ?>
                    @endif
                </td>
            @if(count(App\SubElement::where('elementId', $element->id)->get()) < 1)
                <td colspan="2">
                    <?php
                        if($element->selfEvaluation == 1){

                            $formProgress = App\FormProgress::where('formId', $element->formId)
                                                                ->where('evaluatee', $user)
                                                                ->where('evaluator', $user)
                                                                ->where('year', $year)
                                                                ->where('term', $term)
                                                                ->where('status', 'completed')->first();

                            if(!empty($formProgress)){
                                $total = $formProgress->total;
                            }else{
                                $total = 0;
                            }
                            
                            echo round($total, '2');

                        }else{
                            $rate = [];

                            $relations = App\FormRelationship::where('formId', $element->formId)->where('evaluatee', $selected->type)->get();
                            
                            foreach($relations as $relation){

                                if($relation->evaluator == 'Student'){

                                    $load = App\ClassLoad::where('facultyNo', $selected->facultyNo)->get();

                                    $block  = [];

                                    foreach($load as $class){

                                        $sched = 0;
                                        $students = 0;

                                        $cor = App\COR::where('year', $class->year)
                                                        ->where('term', $class->term)
                                                        ->where('section', $class->section)
                                                        ->where('subject', $class->subject)
                                                        ->where('schedule', $class->schedule)
                                                        ->where('room', $class->room)
                                                        ->where('code', $class->code)->get();
                                        
                                        if(count($cor) > 0){

                                            foreach($cor as $reg){

                                                $formProgress = App\FormProgress::where('formId', $element->formId)
                                                                                ->where('evaluatee', $selected->userId)
                                                                                ->where('evaluator', $reg->student->userId)
                                                                                ->where('year', $year)
                                                                                ->where('term', $term)
                                                                                ->where('status', 'completed')->first();
                                                if(!empty($formProgress)){
                                                    $sched = $sched + $formProgress->total;
                                                    $students = ++$students;
                                                }
                                                
                                            }
                                            if($sched != 0){
                                                array_push($block, ($sched/$students));
                                            }
                                        }
                                    }

                                    if(!empty($block)){
                                        array_push($rate, array_sum($block)/count($block));
                                    }
                                    
                                }else{

                                    $score = 0;

                                    if($relation->status == 'Distinct'){
                                        $faculties = App\Faculty::where('type', $relation->evaluator)->where('department', $selected->department)->where('userId', '!=', $user)->get();
                                    }else{
                                        $faculties = App\Faculty::where('type', $relation->evaluator)->where('userId', '!=', $user)->get();
                                    }
                                    
                                    foreach($faculties as $faculty){
                                        
                                        $formProgress = App\FormProgress::where('formId', $element->formId)
                                                                            ->where('evaluatee', $selected->userId)
                                                                            ->where('evaluator', $faculty->userId)
                                                                            ->where('year', $year)
                                                                            ->where('term', $term)
                                                                            ->where('status', 'completed')->first();
                                        if(!empty($formProgress)){
                                            $score = $score + $formProgress->total;
                                        }

                                        array_push($rate, $score);
                                        
                                    }

                                }

                            }

                            if(count($rate) > 0){
                                $total = array_sum($rate) / count($rate);
                            }else{
                                $total = 0;
                            }

                            echo round($total, '2');
                        }
                    ?>
                </td>
                <td>
                    {{ round($total * ($element->rate/100), '2') }}
                    <?php $overall = $overall + ($total * ($element->rate/100)); ?>
                </td>
            @endif
            </tr>  

            @foreach (App\SubElement::where('elementId', $element->id)->get() as $sub)
                <tr>
                    <td>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ $sub->type }}
                    </td>
                    <td colspan="2">
                        {{ $sub->rate }}%
                        <?php $totalElements = $totalElements + $sub->rate; ?>
                    </td>
                    <td colspan="2">
                        <?php
                        
                            $rate = [];
                            $condition = false;
                            
                            if($sub->type == 'Dean/VDAA'){
                                $relations = App\FormRelationship::where('formId', $element->formId)->where('evaluatee', $selected->type)->get();
                                foreach($relations as $relation){

                                    if($relation->evaluator == 'Dean'){
                                        $score = 0;
                            
                                        if($relation->status == 'Distinct'){
                                            $faculties = App\Faculty::where('type', $relation->evaluator)->where('department', $selected->department)->where('userId', '!=', $user)->get();
                                        }else{
                                            $faculties = App\Faculty::where('type', $relation->evaluator)->where('userId', '!=', $user)->get();
                                        }

                                        foreach($faculties as $faculty){
                                                
                                            $formProgress = App\FormProgress::where('formId', $element->formId)
                                                                                ->where('evaluatee', $selected->userId)
                                                                                ->where('evaluator', $faculty->userId)
                                                                                ->where('year', $year)
                                                                                ->where('term', $term)
                                                                                ->where('status', 'completed')->first();
                                            if(!empty($formProgress)){
                                                $score = $score + $formProgress->total;
                                            }

                                            if($score == 0){
                                                $condition = true;
                                            }

                                            array_push($rate, $score);
                                            
                                        }
                                    }else if($relation->evaluator == 'Vice Dean'){ 
                                        $score = 0;
                            
                                        if($relation->status == 'Distinct'){
                                            $faculties = App\Faculty::where('type', $relation->evaluator)->where('department', $selected->department)->where('userId', '!=', $user)->get();
                                        }else{
                                            $faculties = App\Faculty::where('type', $relation->evaluator)->where('userId', '!=', $user)->get();
                                        }

                                        foreach($faculties as $faculty){
                                        
                                            $formProgress = App\FormProgress::where('formId', $element->formId)
                                                                                ->where('evaluatee', $selected->userId)
                                                                                ->where('evaluator', $faculty->userId)
                                                                                ->where('year', $year)
                                                                                ->where('term', $term)
                                                                                ->where('status', 'completed')->first();
                                            if(!empty($formProgress)){
                                                $score = $score + $formProgress->total;
                                            }

                                            if($score == 0){
                                                $condition = true;
                                            }

                                            array_push($rate, $score);
                                            
                                        }
                                    }
                                    
                                }
                            }else{
                                $relation = App\FormRelationship::where('formId', $element->formId)->where('evaluatee', $selected->type)->where('evaluator', $sub->type)->first();
                                $score = 0;
                                
                                if($relation->status == 'Distinct'){
                                    $faculties = App\Faculty::where('type', $relation->evaluator)->where('department', $selected->department)->where('userId', '!=', $user)->get();
                                }else{
                                    $faculties = App\Faculty::where('type', $relation->evaluator)->where('userId', '!=', $user)->get();
                                }

                                foreach($faculties as $faculty){
                                
                                    if($sub->type == $faculty->type){
                                        
                                        $formProgress = App\FormProgress::where('formId', $element->formId)
                                                                            ->where('evaluatee', $selected->userId)
                                                                            ->where('evaluator', $faculty->userId)
                                                                            ->where('year', $year)
                                                                            ->where('term', $term)
                                                                            ->where('status', 'completed')->first();
                                        if(!empty($formProgress)){
                                            $score = $score + $formProgress->total;
                                        }

                                        array_push($rate, $score);

                                    }
                                    
                                }
                            }
                            
                            if(array_sum($rate) != 0 && count($rate) != 0){
                                if($condition){
                                    $total = array_sum($rate);
                                }else{
                                    $total = array_sum($rate) / count($rate);
                                }
                            }else{
                                $total = 0;
                            }
                            
                            echo round($total, '2');

                        ?>
                    </td>
                    <td>
                        {{ round($total * ($sub->rate/100), '2') }}
                        <?php $overall = $overall + ($total * ($sub->rate/100)); ?>
                    </td>
                </tr>
            @endforeach
        @endif
    @endforeach
    <tr>
        <td></td>
        <td style = "text-align:right" colspan="2">Total: {{ $totalElements }}%</td>
        <td></td>
        <td style = "text-align:right" colspan="2">OVER-ALL RATING: {{ round($overall, '2') }}</td>
    </tr>
@endif  