@extends('guest.layouts.app')

@section('title', 'Summative Report')

@section('content')

<div class="m-content">
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__body">
            <div class = "row align-items-center" id = "topbar">
                <div class="col-xl-4 order-2 order-xl-1">
                    <div class="form-group m-form__group row align-items-center">
                        <div class="col-md-12">
                            <select class="form-control m-select2 faculty" id="m_select2_2" style = "width:100%">
                                <option value="">Choose...</option>
                                @if(Session::get('type') == 'Dean' || Session::get('type') == 'Vice Dean')
                                    @foreach (App\Faculty::distinct()->get(['department']) as $department)
                                        <optgroup label="{{$department->department}}">
                                            @foreach (App\Faculty::where('department', $department->department)->get() as $faculty)
                                                <option value="{{$faculty->userId}}">
                                                    {{$faculty->user->name}}
                                                </option>
                                            @endforeach
                                        </optgroup>
                                    @endforeach
                                @elseif(Session::get('type') == 'Department Chair')
                                    @foreach (App\Faculty::where('department', auth()->user()->faculty->department)->get() as $faculty)
                                        <option value="{{$faculty->userId}}">
                                            {{$faculty->user->name}}
                                        </option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-xl-2 order-2 order-xl-1">
                    <div class="form-group m-form__group row align-items-center">
                        <div class="col-md-12">
                            <select class="form-control" id = "year">
                                <option value="">Select a year</option>
                                <option value="{{ Carbon\Carbon::now()->format('Y')-1 }}-{{ Carbon\Carbon::now()->format('Y') }}">{{ Carbon\Carbon::now()->format('Y')-1 }}-{{ Carbon\Carbon::now()->format('Y') }}</option>
                                <option value="{{ Carbon\Carbon::now()->format('Y')-2 }}-{{ Carbon\Carbon::now()->format('Y')-1 }}">{{ Carbon\Carbon::now()->format('Y')-2 }}-{{ Carbon\Carbon::now()->format('Y')-1 }}</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-xl-2 order-2 order-xl-1">
                    <div class="form-group m-form__group row align-items-center">
                        <div class="col-md-12">
                            <select class="form-control" id = "term">
                                <option value="">Select a semester</option>
                                <option value="1st">1st Semester</option>
                                <option value="2nd">2nd Semester</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 order-1 order-xl-2 m--align-right">
                    <div>
                        <div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" data-dropdown-toggle="hover" aria-expanded="true">
                            <a href="#" class="m-portlet__nav-link btn btn-lg btn-metal  m-btn m-btn--outline-2x m-btn--air m-btn--icon m-btn--icon-only m-btn--pill  m-dropdown__toggle">
                                <i class="la la-plus m--hide"></i>
                                <i class="la la-ellipsis-h"></i>
                            </a>
                            <div class="m-dropdown__wrapper">
                                <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
                                <div class="m-dropdown__inner">
                                    <div class="m-dropdown__body">
                                        <div class="m-dropdown__content">
                                            <ul class="m-nav">
                                                <li class="m-nav__item">
                                                    <a href="#" class="btnprn m-nav__link">
                                                        <i class="m-nav__link-icon la la-print"></i>
                                                        <span class="m-nav__link-text">
                                                            Print
                                                        </span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="m-separator m-separator--dashed d-xl-none"></div>
                </div>
                
            </div>
            <div id = "print">
                <center> 
                    <div class="d-flex justify-content-center align-items-center p-3 my-3">
                        <img class="mr-3" src="/images/sbca.png" alt="" width="50">
                        <div class="lh-100">
                            <h6 class="mb-0 lh-100"><font face="Old English Text MT" size="5">San Beda College Alabang</font></h6>
                            <small>Alabang Hills Village, Muntinlupa City</small>
                        </div>
                    </div>
                    <h2>FACULTY PERFORMANCE APPRAISAL <br> SUMMATIVE REPORT</h2>
                </center>

                <br><br>

                <div class="d-flex">
                    <div class="p-2"><strong>Name:</strong> <span id = "name"></span></div>
                    <div class="ml-auto p-2"><strong>Department:</strong> <span id = "department"></span></div>
                </div>

                <div class="d-flex">
                    <div class="p-2"><strong>Evaluation Period:</strong> <span id = "period"></span></div>
                    <div class="ml-auto p-2"><strong>Date:</strong> {{ Carbon\Carbon::now()->format('M d, Y') }}</div>
                </div>

                <br><br>

                <!--begin: Datatable -->
                <table class = "table table-bordered" id = "summative">
                    <thead>
                        <tr>
                            <th scope="col">
                                Elements
                            </th>
                            <th scope = "col"></th>
                            <th scope = "col"></th>
                            <th scope="col">
                                Rating
                            </th>
                            <th scope="col">
                                %
                            </th>
                            <th scope="col">
                                Total
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @include('admin.data_analysis.summative_report.data')
                    </tbody>
                </table>
                <!--end: Datatable -->
            </div>
        </div>
    </div>
</div>

<script src="{{asset('metronic/assets/demo/default/custom/components/forms/widgets/select2.js')}}" type="text/javascript"></script>

<script>

$(document).ready(function(){
    
    $('.faculty').change(function(){
        $.ajax({
            url: '/summary/summative_report/getDepartment',
            type: 'get',
            data: { id: $(this).val() },
            success: function(response){
                $('#name').text(response.name);
                $('#department').text(response.department);
            }
        });

        $.ajax({
            url: '/summary/summative_report/load',
            type: 'get',
            data: { id:$('.faculty').val(), year: $('#year').val(), term: $('#term').val() },
            success: function(response){ 
                $("#summative tbody").html(response);
            }
        });
    });

});

$('#year').change(function(){
    $.ajax({
        url: '/summary/summative_report/load',
        type: 'get',
        data: { id:$('.faculty').val(), year: $('#year').val(), term: $('#term').val() },
        success: function(response){ 
            $("#summative tbody").html(response);
        }
    });
});

$('#term').change(function(){
    $('#period').text($('#term option:selected').text());
    $.ajax({
        url: '/summary/summative_report/load',
        type: 'get',
        data: { id:$('.faculty').val(), year: $('#year').val(), term: $('#term').val() },
        success: function(response){ 
            $("#summative tbody").html(response);
        }
    });
});

$('.btnprn').click(function(){
    $('#topbar').css('display', 'none');
    $('#headnav').css('display', 'none');
    $('#foot').css('display', 'none');
    $('#alert').css('display', 'none');
    window.print();
    $('#topbar').css('display', '');
    $('#headnav').css('display', '');
    $('#foot').css('display', '');
    $('#foot').css('display', '');
});

</script>

@endsection