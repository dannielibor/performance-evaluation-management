@extends('guest.layouts.app')

@section('title', 'Search')

@section('content')

<style>
        .avatar-upload {
    position: relative;
    max-width: 205px;
    margin: 10px auto;
    }
    .avatar-upload .avatar-edit {
    position: absolute;
    right: 12px;
    z-index: 1;
    top: 10px;
    }
    .avatar-upload .avatar-edit input, #imageUpload {
    display: none;
    }
    .avatar-upload .avatar-edit input + label {
    display: inline-block;
    width: 34px;
    height: 34px;
    margin-bottom: 0;
    border-radius: 100%;
    background: #FFFFFF;
    border: 1px solid transparent;
    box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.12);
    cursor: pointer;
    font-weight: normal;
    transition: all 0.2s ease-in-out;
    }
    .avatar-upload .avatar-edit input + label:hover {
    background: #f1f1f1;
    border-color: #d6d6d6;
    }
    .avatar-upload .avatar-edit input + label:after {
    content: "\f040";
    font-family: 'FontAwesome';
    color: #757575;
    position: absolute;
    top: 10px;
    left: 0;
    right: 0;
    text-align: center;
    margin: auto;
    }
    .avatar-upload .avatar-preview {
    width: 192px;
    height: 192px;
    position: relative;
    border-radius: 100%;
    border: 6px solid #F8F8F8;
    box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.1);
    }
    .avatar-upload .avatar-preview > div {
    width: 100%;
    height: 100%;
    border-radius: 100%;
    background-size: cover;
    background-repeat: no-repeat;
    background-position: center;
    }
    .user-img{
        border-radius:100%;
        width: 15%;
        margin-right:20px;
    }
</style>

<div class="m-content">
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__body">
            <div class="row">
                <div class="col-xl-3 col-lg-4">
                    <div class="m-card-profile">
                        <div class="m-card-profile__title m--hide">
                            Your Profile
                        </div>
                        <div class="m-card-profile__pic">
                            <div class="avatar-upload"> 
                                <div class="avatar-preview">
                                    <div id="imagePreview" style="background-image: url('/storage/profiles/{{ $user->image }}');">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="m-card-profile__details">
                            <span class="m-card-profile__name">
                                <span id = "setFname">{{ $user->name }}</span>
                                @if(!empty($user->student))
                                    @if($user->student->gender == 'M')
                                        <i class = "fa fa-male"></i>
                                    @else 
                                        <i class = "fa fa-female"></i>
                                    @endif
                                @endif
                            </span>
                            <a href="" class="m-card-profile__email m-link" id = "setEmail">
                                <span id = "setEmail">{{ $user->email }}</span>
                            </a>
                        </div>
                    </div>
                    <ul class="m-nav m-nav--hover-bg m-portlet-fit--sides">
                        <li class="m-nav__separator m-nav__separator--fit"></li>
                        <li class="m-nav__section m--hide">
                            <span class="m-nav__section-text">
                                Section
                            </span>
                        </li>
                        <li class="m-nav__item">
                            <div class="m-nav__link">
                                <i class="m-nav__link-icon fa fa-vcard"></i>
                                <span class="m-nav__link-title">
                                    <span class="m-nav__link-wrap">
                                        <span class="m-nav__link-text">
                                            @if(!empty($user->student))
                                                {{$user->student->studentNo}}   
                                            @else 
                                                {{$user->faculty->facultyNo}}
                                            @endif
                                        </span>
                                    </span>
                                </span>
                            </div>
                        </li>
                        <li class="m-nav__item">
                            <div class="m-nav__link">
                                <i class="m-nav__link-icon fa fa-circle"></i>
                                <span class="m-nav__link-title">
                                    <span class="m-nav__link-wrap">
                                        <span class="m-nav__link-text">
                                            @if(!empty($user->student))
                                                {{$user->student->program}}
                                            @else 
                                                {{$user->faculty->department}}
                                            @endif
                                        </span>
                                    </span>
                                </span>
                            </div>
                        </li>
                        <li class="m-nav__item">
                            <div class="m-nav__link">
                                <i class="m-nav__link-icon fa fa-info"></i>
                                <span class="m-nav__link-title">
                                    <span class="m-nav__link-wrap">
                                        <span class="m-nav__link-text">
                                            @if(!empty($user->student))
                                                {{$user->student->yearLvl}}
                                            @else 
                                                {{$user->faculty->type}}
                                            @endif
                                        </span>
                                    </span>
                                </span>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="col-xl-9 col-lg-8" style = "max-height:450px;overflow:scroll;">
                    <div class="tab-content">
                        <div class="tab-pane active" id="m_widget4_tab1_content">
                            <div class="m-widget4 m-widget4--progress">
                                <table class = "table table-hover">
                                    <thead>
                                        <tr>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($forms as $form)
                                            <tr>
                                                <td>
                                                    <?php
                                                        $formProgress = App\FormProgress::where('formId', $form->formId)
                                                                                        ->where('evaluator', auth()->user()->id)
                                                                                        ->where('evaluatee', $user->id)
                                                                                        ->where('year', $user->faculty->class->year)
                                                                                        ->where('term', $user->faculty->class->term)->first();
                                                        $count = 0;
                                                        $ans = 0;
                                                        $bodies = App\Body::where('formId', $form->formId)->get();
                                                        foreach($bodies as $body){
                                                            $contents = App\Content::where('bodyId', $body->id)->where('type', 'question')->get();
                                                            foreach($contents as $content){
                                                                $count = ++$count;
                                                                if(!empty($formProgress)){
                                                                    $entry = App\Entry::where('contentId', $content->id)->where('formProgressId', $formProgress->id)->first();
                                                                    if(!empty($entry)){
                                                                        $ans = ++$ans;
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        $percent = round(($ans/$count)*100);
                                                    ?>
                                                    @if($percent < 50)
                                                        <?php $status = 'danger'; ?>
                                                    @elseif($percent < 100)
                                                        <?php $status = 'warning'; ?>
                                                    @else 
                                                        <?php $status = 'success'; ?>
                                                    @endif
                                                    <div class="m-widget4__item">
                                                        <div class="m-widget4__img m-widget4__img--pic">
                                                            @if(!empty($formProgress) && $formProgress->status == 'completed')
                                                                <i class="fa fa-check-circle" style = "color:green"></i>
                                                            @else 
                                                                <i class="m-nav__link-icon fa fa-exclamation-circle" style = "color:grey"></i>
                                                            @endif
                                                        </div>
                                                        <div class="m-widget4__info">
                                                            <span class="m-widget4__title">
                                                                {{ $form->form->title }}
                                                            </span>
                                                        </div>
                        
                                                        <div class="m-widget4__progress">
                                                            <div class="m-widget4__progress-wrapper">
                                                                <span class="m-widget17__progress-number">
                                                                    {{ $percent }}%
                                                                </span>
                                                            </div>
                                                            <div class="progress">
                                                                <div class="progress-bar progress-bar-striped bg-{{$status}} progress-bar-animated" role="progressbar" style="width: {{$percent}}%;" aria-valuenow="{{$percent}}" aria-valuemin="0" aria-valuemax="100"></div>
                                                            </div>
                                                        </div>
                                                        <div class="m-widget4__ext">
                                                            @if(!empty($formProgress) && $formProgress->status == 'pending')
                                                                <a href="#" class="m-btn m-btn--pill btn btn-sm btn-metal" id = "hoverProfile">
                                                                    Start
                                                                </a>
                                                            @elseif(empty($formProgress))
                                                                <a href="/forms/{{$form->form->title}}/{{$form->form->id}}/faculty/{{$user->faculty->type}}/{{$user->faculty->userId}}" class="m-btn m-btn--pill btn btn-sm btn-metal" id = "hoverProfile">
                                                                    Start
                                                                </a>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__body">
            <!--begin: Datatable -->
            <table class = "m-datatable" id="html_table" width="100%">
                <thead style = "text-align:center">
                    <tr>
                        <th title = "Field #1">
                            Subject Code
                        </th>
                        <th title = "Field #2">
                            Subject Title
                        </th>
                        <th title = "Field #3">
                            Section
                        </th>
                        <th title = "Field #4">
                            Schedule
                        </th>
                        <th title = "Field #5">
                            Class Size
                        </th>
                        <th title = "Field #6">
                            Room
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach (App\ClassLoad::where('facultyNo', $user->faculty->facultyNo)->get() as $faculty)
                        <tr>
                            <td>
                                <center>
                                    {{ $faculty->code }}
                                </center>
                            </td>
                            <td>
                                {{ $faculty->subject }}
                            </td>
                            <td>
                                <center>
                                    {{ $faculty->section }}
                                </center>
                            </td>
                            <td>
                                {{ $faculty->schedule }}
                            </td>
                            <td>
                                <center>
                                    {{ $faculty->size }}
                                </center>
                            </td>
                            <td>
                                <center>
                                    {{ $faculty->room }}
                                </center>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <!--end: Datatable -->
        </div>
    </div>
</div>

<script src="{{ asset('metronic/assets/demo/default/custom/components/datatables/base/html-table.js') }}" type="text/javascript"></script>

@endsection