<?php

	$user = App\User::find(auth()->user()->id);
	
	$restrict = App\FormRelationship::where('evaluator', Session::get('type'))->distinct()->get(['formId']);
	
?>

<!DOCTYPE html>

<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
	<!-- begin::Head -->
	<head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

		<title>
			SBCA - FPES | @yield('title')
        </title>
        
        <!-- Scripts -->
        {{-- <script src="{{ asset('js/app.js') }}"></script> --}}

        <!-- Fonts -->
        <link rel="dns-prefetch" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
		<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
		<link rel="stylesheet" href="/resources/demos/style.css">
        <!-- Styles -->

		<!--begin::Web font -->
		<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
		<script>
			WebFont.load({
				google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
				active: function() {
					sessionStorage.fonts = true;
				}
			});
		</script>
		<!--end::Web font -->
		<link href="{{ asset('metronic/assets/vendors/custom/jquery-ui/jquery-ui.bundle.css') }}" rel="stylesheet" type="text/css" />
		<!--begin::Base Styles -->  
		<link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">
		<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
		<script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
        <!--begin::Page Vendors -->
		<link href="{{asset('metronic/assets/vendors/custom/fullcalendar/fullcalendar.bundle.css')}}" rel="stylesheet" type="text/css" />
		<!--end::Page Vendors -->
		<link href="{{asset('metronic/assets/vendors/base/vendors.bundle.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{asset('metronic/assets/demo/default/base/style.bundle.css')}}" rel="stylesheet" type="text/css" />
		<!--end::Base Styles -->
		<link rel="shortcut icon" href="{{ asset('images/sbca.png') }}" />

		<style>

			@media (min-width: 993px) {
				.m-footer--push.m-aside-left--enabled:not(.m-footer--fixed) .m-footer {
					margin-left: 0px !important;
				}
			}

			.m-header-menu.m-header-menu--skin-light .m-menu__nav > .m-menu__item > .m-menu__link .m-menu__link-icon {
				color: #fff; }

			.m-header-menu.m-header-menu--skin-light .m-menu__nav > .m-menu__item > .m-menu__link .m-menu__link-text {
    			color: #fff; }

			.m-header-menu.m-header-menu--skin-light .m-menu__nav > .m-menu__item > .m-menu__link .m-menu__hor-arrow {
				color: #fff; }

			.m-header-menu.m-header-menu--skin-light .m-menu__nav > .m-menu__item:hover > .m-menu__link .m-menu__link-icon, .m-header-menu.m-header-menu--skin-light .m-menu__nav > .m-menu__item.m-menu__item--hover > .m-menu__link .m-menu__link-icon {
				color: #f5f5dc; }
			
			.m-header-menu.m-header-menu--skin-light .m-menu__nav > .m-menu__item:hover > .m-menu__link .m-menu__link-text, .m-header-menu.m-header-menu--skin-light .m-menu__nav > .m-menu__item.m-menu__item--hover > .m-menu__link .m-menu__link-text {
				color: #f5f5dc; }

			.m-header-menu.m-header-menu--skin-light .m-menu__nav > .m-menu__item:hover > .m-menu__link > .m-menu__hor-arrow, .m-header-menu.m-header-menu--skin-light .m-menu__nav > .m-menu__item.m-menu__item--hover > .m-menu__link > .m-menu__hor-arrow {
				color: #f5f5dc; }

			.m-header-menu.m-header-menu--submenu-skin-light .m-menu__nav > .m-menu__item .m-menu__submenu > .m-menu__subnav > .m-menu__item:hover > .m-menu__link .m-menu__link-text, .m-header-menu.m-header-menu--submenu-skin-light .m-menu__nav > .m-menu__item .m-menu__submenu > .m-menu__subnav > .m-menu__item.m-menu__item--hover > .m-menu__link .m-menu__link-text {
				  color: #800; }
				  
			.m-header-menu.m-header-menu--submenu-skin-light .m-menu__nav > .m-menu__item .m-menu__submenu > .m-menu__subnav > .m-menu__item:hover > .m-menu__link .m-menu__link-icon, .m-header-menu.m-header-menu--submenu-skin-light .m-menu__nav > .m-menu__item .m-menu__submenu > .m-menu__subnav > .m-menu__item.m-menu__item--hover > .m-menu__link .m-menu__link-icon {
				  color: #800; }
				  
			.m-nav .m-nav__item:hover:not(.m-nav__item--disabled) > .m-nav__link .m-nav__link-icon,
			.m-nav .m-nav__item:hover:not(.m-nav__item--disabled) > .m-nav__link .m-nav__link-text,
			.m-nav .m-nav__item:hover:not(.m-nav__item--disabled) > .m-nav__link .m-nav__link-arrow, .m-nav .m-nav__item.m-nav__item--active > .m-nav__link .m-nav__link-icon,
			.m-nav .m-nav__item.m-nav__item--active > .m-nav__link .m-nav__link-text,
			.m-nav .m-nav__item.m-nav__item--active > .m-nav__link .m-nav__link-arrow {
			color: #000 !important; }

			.alert-warning:not(.m-alert--outline) {
			color: white;
			background-color: #ffc241;
			border-color: #ffc241; }

			.m-link:hover:after {
			border-bottom: 1px solid #000;
			opacity: 0.3 ;
			filter: alpha(opacity=30) ; }

			.m-grid__item, .m-grid__item--fluid, .m-wrapper{
				background-color:#F2F3F4;
				box-shadow: inset 5px 0 10px -10px rgba(0,0,0,0.7);
			}

			.alertEmail:hover{
				border-bottom: 1px solid #000;
				opacity: 0.3;
				filter: alpha(opacity=30);
			}

			.alertSetEmail{
				color: white;
				text-decoration: none !important;
			}

			.alertSetEmail:hover{
				color: black !important;
			}

			.m-brand.m-brand--skin-dark .m-brand__tools .m-brand__icon > i {
			  color: #fff; }
			  
			.m-brand.m-brand--skin-dark .m-brand__tools .m-brand__toggler span {
			  background: #fff; }
			  
			.m-brand.m-brand--skin-dark .m-brand__tools .m-brand__toggler span::before, .m-brand.m-brand--skin-dark .m-brand__tools .m-brand__toggler span::after {
			background: #fff; }

			@media (max-width: 992px) {
			.m-aside-header-menu-mobile.m-aside-header-menu-mobile--skin-dark {
			  background-color: #800; }
			  .m-header-menu.m-header-menu--skin-light .m-menu__nav > .m-menu__item:hover > .m-menu__link .m-menu__link-icon, .m-header-menu.m-header-menu--skin-light .m-menu__nav > .m-menu__item.m-menu__item--hover > .m-menu__link .m-menu__link-icon {
				color: #000; }
			
			.m-header-menu.m-header-menu--skin-light .m-menu__nav > .m-menu__item:hover > .m-menu__link .m-menu__link-text, .m-header-menu.m-header-menu--skin-light .m-menu__nav > .m-menu__item.m-menu__item--hover > .m-menu__link .m-menu__link-text {
				color: #000; }

			.m-header-menu.m-header-menu--skin-light .m-menu__nav > .m-menu__item:hover > .m-menu__link > .m-menu__hor-arrow, .m-header-menu.m-header-menu--skin-light .m-menu__nav > .m-menu__item.m-menu__item--hover > .m-menu__link > .m-menu__hor-arrow {
				color: #000; }
			}

			.m-aside-header-menu-mobile.m-aside-header-menu-mobile--skin-dark .m-menu__nav > .m-menu__item.m-menu__item--open {
			-webkit-transition: background-color 0.3s;
			-moz-transition: background-color 0.3s;
			-ms-transition: background-color 0.3s;
			-o-transition: background-color 0.3s;
			transition: background-color 0.3s;
			background-color: #fff; }

			.m-aside-header-menu-mobile.m-aside-header-menu-mobile--skin-dark .m-menu__nav > .m-menu__item > .m-menu__link .m-menu__ver-arrow {
				color: #800; }
				
			.m-aside-header-menu-mobile-close.m-aside-header-menu-mobile-close--skin-dark {
     		 background-color: #fff; }

			.search-img{
				border-radius:100%;
				width: 25px;
				margin-right:10px;
			}

			/* Scrollbar styles */
			::-webkit-scrollbar {
				width: 12px;
				height: 12px;
			}

			::-webkit-scrollbar-track {
				border-radius: 12px;
			}

			::-webkit-scrollbar-track:vertical {
				background: #f1f2f7; 
			}

			::-webkit-scrollbar-thumb {
				background: #5a5a5a;  
				border-radius: 12px;
			}

		</style>

	</head>
	<!-- end::Head -->
    <!-- end::Body -->
	<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
		<!-- begin:: Page -->
		{!! Toastr::render() !!}
		<div class="m-grid m-grid--hor m-grid--root m-page">
			<!-- BEGIN: Header -->
			<header class="m-grid__item    m-header "  data-minimize-offset="200" data-minimize-mobile-offset="200">
				<div class="m-container m-container--fluid m-container--full-height">
					<div class="m-stack m-stack--ver m-stack--desktop" id = "headnav">
						<!-- BEGIN: Brand -->
						<div class="m-stack__item m-brand  m-brand--skin-dark" style = "background-color:#800">
							<div class="m-stack m-stack--ver m-stack--general">
								<div class="m-stack__item m-stack__item--middle m-brand__logo">
									<a href="/" class="m-brand__logo-wrapper" style = "text-decoration:none">
										<img alt="San Beda College Alabang" width = "30" src="{{asset('images/sbca.png')}}"/>&nbsp;
											<span style = "color:#fff;font-size:15px">
												@if(Session::get('type') == 'Student')
													Student
												@else
													Faculty
												@endif
											</span>
									</a>
								</div>
								<div class="m-stack__item m-stack__item--middle m-brand__tools">
									<!-- BEGIN: Responsive Header Menu Toggler -->
									<a id="m_aside_header_menu_mobile_toggle" href="javascript:;" class="m-brand__icon m-brand__toggler m--visible-tablet-and-mobile-inline-block">
										<span></span>
									</a>
									<!-- END -->
									<!-- BEGIN: Topbar Toggler -->
									<a id="m_aside_header_topbar_mobile_toggle" href="javascript:;" class="m-brand__icon m--visible-tablet-and-mobile-inline-block">
										<i class="flaticon-more"></i>
									</a>
									<!-- BEGIN: Topbar Toggler -->
								</div>
							</div>
						</div>
						<!-- END: Brand -->
						<div class="m-stack__item m-stack__item--fluid m-header-head" id="m_header_nav" style = "background-color:#800">
							<!-- BEGIN: Horizontal Menu -->
							<button class="m-aside-header-menu-mobile-close  m-aside-header-menu-mobile-close--skin-dark " id="m_aside_header_menu_mobile_close_btn">
								<i class="la la-close"></i>
							</button>
							<div id="m_header_menu" class="m-header-menu m-aside-header-menu-mobile m-aside-header-menu-mobile--offcanvas  m-header-menu--skin-light m-header-menu--submenu-skin-light m-aside-header-menu-mobile--skin-dark m-aside-header-menu-mobile--submenu-skin-dark "  >
								<ul class="m-menu__nav  m-menu__nav--submenu-arrow">
									<li class="m-menu__item  m-menu__item--submenu m-menu__item--rel" >
										<a  href="/" class="m-menu__link">
											<i class="m-menu__link-icon fa fa-home"></i>
											<span class="m-menu__link-text">
												Home
											</span>
										</a>
									</li>

									<li class="m-menu__item  m-menu__item--submenu m-menu__item--rel" data-menu-submenu-toggle="click" data-redirect="true" aria-haspopup="true">
											<a href="#" class="m-menu__link m-menu__toggle">
												<i class="m-menu__link-icon fa fa-list-alt"></i>
												<span class="m-menu__link-text">
													Forms
												</span>
												<i class="m-menu__hor-arrow la la-angle-down"></i>
												<i class="m-menu__ver-arrow la la-angle-right"></i>
											</a>
											<div class="m-menu__submenu m-menu__submenu--classic m-menu__submenu--left">
												<span class="m-menu__arrow m-menu__arrow--adjust"></span>
												<ul class="m-menu__subnav">

												@if(count($restrict) > 0)

													@foreach ($restrict as $item)

														@if(!empty($item->form) && $item->form->status == 'active')

															<?php
																$type = App\FormRelationship::where('formId', $item->form->id)->where('evaluator', Session::get('type'))->get();
															?>

															@if(count($type) > 1)

																<li class="m-menu__item  m-menu__item--submenu"  data-menu-submenu-toggle="hover" data-redirect="true" aria-haspopup="true">
																	<a  href="#" class="m-menu__link m-menu__toggle">
																		<i class="m-menu__link-icon flaticon-computer"></i>
																		<span class="m-menu__link-text">
																			{{ $item->form->title }}
																		</span>
																		<i class="m-menu__hor-arrow la la-angle-right"></i>
																		<i class="m-menu__ver-arrow la la-angle-right"></i>
																	</a>
																	<div class="m-menu__submenu m-menu__submenu--classic m-menu__submenu--right">
																		<span class="m-menu__arrow "></span>
																		<ul class="m-menu__subnav">

																			@foreach (App\FormRelationship::where('formId', $item->form->id)->where('evaluator', Session::get('type'))->get() as $submenu)

																				<li class="m-menu__item"  data-redirect="true" aria-haspopup="true">
																					<a  href="/forms/{{$item->form->title}}/{{$item->form->id}}/faculty/{{$submenu->evaluatee}}" class="m-menu__link ">
																						<i class="m-menu__link-icon flaticon-users"></i>
																						<span class="m-menu__link-text">
																							{{$submenu->evaluatee}}
																						</span>
																					</a>
																				</li>
																				
																			@endforeach

																			<?php $formRestrict = App\FormRestriction::where('type', Session::get('type'))->where('selfEvaluation', 1)->get(); ?>
													
																			@if(count($formRestrict) > 0)
																				@foreach ($formRestrict as $item)
																					<?php
																						$formProgress = App\FormProgress::where('formId', $item->formId)
																														->where('evaluatee', auth()->user()->id)
																														->where('evaluator', auth()->user()->id)
																														->where('as', Session::get('type'))
																														->where('year', auth()->user()->faculty->class->year)
																														->where('term', auth()->user()->faculty->class->term)->first();
																					?>
																					@if(!empty($formProgress) && $formProgress->status == 'completed')
																						<li class="m-menu__item  m-menu__item--submenu">
																							<a href="#" class="m-menu__link">
																								<i class="m-menu__link-icon	fa fa-file-text-o" style = "color:green"></i>
																								<span class="m-menu__link-text" style = "color:green">
																									Self Evaluation - {{ $item->form->title }}
																								</span>
																							</a>
																						</li>
																					@else 
																						<li class="m-menu__item  m-menu__item--submenu">
																							<a href="/forms/{{$item->form->title}}/{{$item->form->id}}/faculty/{{Session::get('type')}}/{{auth()->user()->id}}" class="m-menu__link">
																								<i class="m-menu__link-icon	fa fa-file-text-o"></i>
																								<span class="m-menu__link-text">
																									Self Evaluation - {{ $item->form->title }}
																								</span>
																							</a>
																						</li>
																					@endif
																				@endforeach
																			@endif

																		</ul>
																	</div>
																</li>
															
															@else

																<li class="m-menu__item  m-menu__item--submenu">
																	<a href="/forms/{{$item->form->title}}/{{$item->form->id}}" class="m-menu__link">
																		<i class="m-menu__link-icon	fa fa-file-text-o"></i>
																		<span class="m-menu__link-text">
																			{{ $item->form->title }}
																		</span>
																	</a>
																</li>

															@endif

														@endif
														
													@endforeach

												@else 

													<?php $formRestrict = App\FormRestriction::where('type', Session::get('type'))->where('selfEvaluation', 1)->get(); ?>
													
													@if(count($formRestrict) > 0)
														@foreach ($formRestrict as $item)
															<?php
																$formProgress = App\FormProgress::where('formId', $item->formId)
																								->where('evaluatee', auth()->user()->id)
																								->where('evaluator', auth()->user()->id)
																								->where('as', Session::get('type'))
																								->where('year', auth()->user()->faculty->class->year)
																								->where('term', auth()->user()->faculty->class->term)->first();
															?>
															@if(!empty($formProgress) && $formProgress->status == 'completed')
																<li class="m-menu__item  m-menu__item--submenu">
																	<a href="#" class="m-menu__link">
																		<i class="m-menu__link-icon	fa fa-file-text-o" style = "color:green"></i>
																		<span class="m-menu__link-text" style = "color:green">
																			Self Evaluation - {{ $item->form->title }}
																		</span>
																	</a>
																</li>
															@else 
																<li class="m-menu__item  m-menu__item--submenu">
																	<a href="/forms/{{$item->form->title}}/{{$item->form->id}}/faculty/{{Session::get('type')}}/{{auth()->user()->id}}" class="m-menu__link">
																		<i class="m-menu__link-icon	fa fa-file-text-o"></i>
																		<span class="m-menu__link-text">
																			Self Evaluation - {{ $item->form->title }}
																		</span>
																	</a>
																</li>
															@endif
														@endforeach

													@else 
														<li class="m-menu__item" data-redirect="true" aria-haspopup="true">
															<span class="m-menu__link">
																No Data Found
															</span>
														</li>
													@endif

												@endif

											</ul>
										</div>
									</li>
												
									{{-- @if(Session::get('type') == 'Dean' || Session::get('type') == 'Vice Dean' || Session::get('type') == 'Department Chair')
										<li class="m-menu__item  m-menu__item--submenu m-menu__item--rel" data-redirect="true" aria-haspopup="true">
											<a  href="/summative_report" class="m-menu__link">
												<i class="m-menu__link-icon fa fa-bar-chart"></i>
												<span class="m-menu__link-title">
													<span class="m-menu__link-wrap">
														<span class="m-menu__link-text">
															Summative Report
														</span>
													</span>
												</span>
											</a>
										</li>
									@endif --}}
									@if(Session::get('type') == 'Dean' || Session::get('type') == 'Vice Dean' || Session::get('type') == 'Department Chair')
										<li class="m-menu__item  m-menu__item--submenu m-menu__item--rel" data-menu-submenu-toggle="click" data-redirect="true" aria-haspopup="true">
											<a href="#" class="m-menu__link m-menu__toggle">
												<i class="m-menu__link-icon fa fa-list-alt"></i>
												<span class="m-menu__link-text">
													Summary
												</span>
												<i class="m-menu__hor-arrow la la-angle-down"></i>
												<i class="m-menu__ver-arrow la la-angle-right"></i>
											</a>
											<div class="m-menu__submenu m-menu__submenu--classic m-menu__submenu--left">
												<span class="m-menu__arrow m-menu__arrow--adjust"></span>
												<ul class="m-menu__subnav">
													<li class="m-menu__item  m-menu__item--submenu">
														<a href="/summary/summative_report" class="m-menu__link">
															<i class="m-menu__link-icon	fa fa-file-text-o"></i>
															<span class="m-menu__link-text">
																Summative Report
															</span>
														</a>
													</li>
													<li class="m-menu__item  m-menu__item--submenu">
														<a href="/summary/rank" class="m-menu__link">
															<i class="m-menu__link-icon	fa fa-file-text-o"></i>
															<span class="m-menu__link-text">
																Rank
															</span>
														</a>
													</li>
												</ul>
											</div>
										</li>
									@endif
								</ul>
							</div>
							<!-- END: Horizontal Menu -->
							<!-- BEGIN: Topbar -->
							<div id="m_header_topbar" class="m-topbar  m-stack m-stack--ver m-stack--general">
								<div class="m-stack__item m-topbar__nav-wrapper">
									<ul class="m-topbar__nav m-nav m-nav--inline">
										<li class="m-nav__item m-dropdown m-dropdown--large m-dropdown--arrow m-dropdown--align-center m-dropdown--mobile-full-width m-dropdown--skin-light m-list-search m-list-search--skin-light" 
											data-dropdown-toggle="click" data-dropdown-persistent="true" id="m_quicksearch" data-search-type="dropdown">
											<a href="#" class="m-nav__link m-dropdown__toggle">
												<span class="m-nav__link-icon">
													<i class="flaticon-search-1"></i>
												</span>
											</a>
											<div class="m-dropdown__wrapper">
												<span class="m-dropdown__arrow m-dropdown__arrow--center"></span>
												<div class="m-dropdown__inner ">
													<div class="m-dropdown__header">
														<form action = "/search" class="m-list-search__form">
															<div class="m-list-search__form-wrapper">
																<span class="m-list-search__form-input-wrapper">
																	<input id="m_quicksearch_input" autocomplete="off" type="text" name = "q" class="m-list-search__form-input" value="" placeholder="Search...">
																</span>
																<span class="m-list-search__form-icon-close" id="m_quicksearch_close">
																	<i class="la la-remove"></i>
																</span>
															</div>
															<div class = "dropdown">
																<div id="myDropdown1" class="dropdown-menu" style = "max-height:200px;overflow:scroll;">
																	@if(Session::get('type') == 'Dean' || Session::get('Vice Dean'))
																		@foreach (App\Faculty::all() as $faculty)
																			<a href="/search?q={{$faculty->user->id}}" class="dropdown-item"><img src="/storage/profiles/{{$faculty->user->image}}" class = "search-img">{{ $faculty->user->name }}</a>
																		@endforeach
																	@elseif(Session::get('type') == 'Student')
																		@foreach (App\Faculty::all() as $faculty)
																			@foreach (App\ClassLoad::where('facultyNo', $faculty->facultyNo)->get() as $class)
																				<?php $cor = App\COR::where('studentNo', auth()->user()->student->studentNo)->where('code', $class->code)->where('section', $class->section)->where('schedule', $class->schedule)->first(); ?>
																				@if(!empty($cor))
																					<?php $explode = explode(' ', $cor->subject); ?>
																					@if(last($explode) != 'Laboratory')
																						<a href="/search?q={{$faculty->user->id}}" class="dropdown-item"><img src="/storage/profiles/{{$faculty->user->image}}" class = "search-img">{{ $faculty->user->name }}</a>
																					@endif
																				@endif
																			@endforeach
																		@endforeach
																	@else 
																		@foreach (App\Faculty::where('department', auth()->user()->faculty->department)->get() as $faculty)
																			<a href="/search?q={{$faculty->user->id}}" class="dropdown-item"><img src="/storage/profiles/{{$faculty->user->image}}" class = "search-img">{{ $faculty->user->name }}</a>
																		@endforeach
																	@endif
																</div>
															</div>
														</form>
													</div>
													<div class="m-dropdown__body">
														<div class="m-dropdown__scrollable m-scrollable" data-max-height="300" data-mobile-max-height="200">
															<div class="m-dropdown__content"></div>
														</div>
													</div>
												</div>
											</div>
										</li>
										<li class="m-nav__item m-topbar__user-profile m-topbar__user-profile--img  m-dropdown m-dropdown--medium m-dropdown--arrow m-dropdown--header-bg-fill m-dropdown--align-right m-dropdown--mobile-full-width m-dropdown--skin-light" data-dropdown-toggle="click">
											<a href="#" class="m-nav__link m-dropdown__toggle">
												<span class="m-topbar__userpic">
													<img src="/storage/profiles/{{$user->image}}" class="m--img-rounded m--marginless m--img-centered" alt=""/>
												</span>
												<span class="m-topbar__username m--hide">
													Nick
												</span>
											</a>
											<div class="m-dropdown__wrapper">
												<span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust" style = "color: #800"></span>
												<div class="m-dropdown__inner">
													<div class="m-dropdown__header m--align-center" style="background-color: #800; background-size: cover;">
														<div class="m-card-user m-card-user--skin-dark">
															<div class="m-card-user__pic">
																<img src="/storage/profiles/{{$user->image}}" class="m--img-rounded m--marginless" alt=""/>
															</div>
															<div class="m-card-user__details">
																<span class="m-card-user__name m--font-weight-500" style = "color: white">
																	{{ $user->name }}
																</span>
																<a href="" class="m-card-user__email m--font-weight-300 m-link" style = "color: white">
																	{{ $user->email }}
																</a>
															</div>
														</div>
													</div>
													<div class="m-dropdown__body">
														<div class="m-dropdown__content">
															<ul class="m-nav m-nav--skin-light">
																<li class="m-nav__section m--hide">
																	<span class="m-nav__section-text">
																		Section
																	</span>
																</li>
																<li class="m-nav__item">
																	<a href="/profile" class="m-nav__link">
																		<i class="m-nav__link-icon fl flaticon-user"></i>
																		<span class="m-nav__link-title">
																			<span class="m-nav__link-wrap">
																				<span class="m-nav__link-text">
																					My Profile
																				</span>
																			</span>
																		</span>
																	</a>
																</li>
																<li class="m-nav__item">
																	<a href = "/profile/settings" class="m-nav__link">
																		<i class="m-nav__link-icon fl flaticon-settings-1"></i>
																		<span class="m-nav__link-title">
																			<span class="m-nav__link-wrap">
																				<span class="m-nav__link-text">
																					Settings
																				</span>
																			</span>
																		</span>
																	</a>
																</li>
																<li class="m-nav__separator m-nav__separator--fit"></li>
																<li class="m-nav__item">
																	<a class="btn m-btn--pill btn-secondary m-btn m-btn--custom m-btn--label-brand m-btn--bolder" style = "color:grey" href="{{ route('logout') }}"
																	onclick="event.preventDefault();
																					document.getElementById('logout-form').submit();">
																		Sign out
																	</a>
																	<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
																		@csrf
																	</form>
																</li>
															</ul>
														</div>
													</div>
												</div>
											</div>
										</li>
									</ul>
								</div>
							</div>
							<!-- END: Topbar -->
						</div>
					</div>
				</div>
			</header>
			<!-- END: Header -->		
			<!-- begin::Body -->
			<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
				<div class="m-grid__item m-grid__item--fluid m-wrapper">
					@if(auth()->user()->activated == 0 && auth()->user()->email == null)
						<div class="alert alert-warning alert-dismissible fade show" role="alert" id = "alert">
							<button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
							<i class="la la-warning"></i>&nbsp;
							You won't be able to evaluate until you <strong class = "alertEmail"> <a href = "/profile" class = "alertSetEmail">set an email </a> </strong> to your profile.
						</div>
					@elseif(auth()->user()->activated == 0)
						<div class="alert alert-warning alert-dismissible fade show" role="alert" id = "alert">
							<button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
							<i class="la la-warning"></i>&nbsp;
							You won't be able to evaluate until you activate this account, click <strong class = "alertEmail"> <a href = "{{route('activation_key_resend')}}" class = "alertSetEmail"> here </a> </strong> to resend email
						</div>
					@endif
					@yield('content')
				</div>
			</div>
			<!-- begin::Footer -->
			<footer class="m-grid__item	m-footer" style = "background-color:white">
				<div class="m-container m-container--fluid m-container--full-height m-page__container">
					<div class="m-stack m-stack--flex-tablet-and-mobile m-stack--ver m-stack--desktop">
						<div class="m-stack__item m-stack__item--left m-stack__item--middle m-stack__item--last">
							<span class="m-footer__copyright" style = "color:black">
								{{ Carbon\Carbon::now()->format('Y') }} &copy; Faculty Performance Evaluation by
								<a href="https://www.sanbeda-alabang.edu.ph/bede/" target="_blank" class="m-link" style = "color:#800">
									San Beda College Alabang
								</a>
							</span>
						</div>
						<div class="m-stack__item m-stack__item--right m-stack__item--middle m-stack__item--first" id = "foot">
							<ul class="m-footer__nav m-nav m-nav--inline m--pull-right">
								<li class="m-nav__item">
									<a href="#" class="m-nav__link" data-toggle="modal" data-target="#about">
										<span class="m-nav__link-text">
											About
										</span>
									</a>
								</li>
								<li class="m-nav__item">
									<a href="#"  class="m-nav__link" data-toggle="modal" data-target="#privacy">
										<span class="m-nav__link-text">
											Privacy
										</span>
									</a>
								</li>
								<li class="m-nav__item">
									<a href="#" class="m-nav__link" data-toggle="modal" data-target="#TnC">
										<span class="m-nav__link-text">
											T&C
										</span>
									</a>
								</li>
								<li class="m-nav__item">
									<a href="#" class="m-nav__link" data-toggle="modal" data-target="#support">
										<span class="m-nav__link-text">
											Support
										</span>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</footer>
			<!-- end::Footer -->
			<!-- end:: Body -->
		</div>
		<!-- end:: Page -->
		<!-- About -->
		<div class="modal fade" id="about" tabindex="-1" role="dialog" aria-labelledby="about" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered" role="document">
				<div class="modal-content">
					<div class="modal-header">
					<h5 class="modal-title" id="about">About</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					</div>
					<div class="modal-body">
						Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
					</div>
					<div class="modal-footer">
					<button type="button" class="btn btn-metal" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>
		<!-- Privacy -->
		<div class="modal fade" id="privacy" tabindex="-1" role="dialog" aria-labelledby="privacy" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered" role="document">
				<div class="modal-content">
					<div class="modal-header">
					<h5 class="modal-title" id="privacy">Privacy</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					</div>
					<div class="modal-body">
						Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
					</div>
					<div class="modal-footer">
					<button type="button" class="btn btn-metal" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>
		<!-- TnC -->
		<div class="modal fade" id="TnC" tabindex="-1" role="dialog" aria-labelledby="TnC" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered" role="document">
				<div class="modal-content">
					<div class="modal-header">
					<h5 class="modal-title" id="TnC">Terms And Conditions</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					</div>
					<div class="modal-body">
						Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
					</div>
					<div class="modal-footer">
					<button type="button" class="btn btn-metal" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>
		<!-- Support -->
		<div class="modal fade" id="support" tabindex="-1" role="dialog" aria-labelledby="support" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered" role="document">
				<div class="modal-content">
					<div class="modal-header">
					<h5 class="modal-title" id="support">Support</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					</div>
					<div class="modal-body">
						Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
					</div>
					<div class="modal-footer">
					<button type="button" class="btn btn-metal" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>
	    <!-- begin::Scroll Top -->
		<div class="m-scroll-top m-scroll-top--skin-top" id = "up" data-toggle="m-scroll-top" data-scroll-offset="500" data-scroll-speed="300">
			<i class="la la-arrow-up"></i>
		</div>
		<!-- end::Scroll Top -->
    	<!--begin::Base Scripts -->
		<script src="{{asset('metronic/assets/vendors/base/vendors.bundle.js')}}" type="text/javascript"></script>
		<script src="{{asset('metronic/assets/demo/default/base/scripts.bundle.js')}}" type="text/javascript"></script>
		<!--end::Base Scripts -->   
		<script src="{{asset('metronic/assets/demo/default/custom/components/forms/widgets/select2.js')}}" type="text/javascript"></script>
        <!--begin::Page Vendors -->
		<script src="{{asset('metronic/assets/vendors/custom/fullcalendar/fullcalendar.bundle.js')}}" type="text/javascript"></script>
		<!--end::Page Vendors -->  
		<script src="{{ asset('metronic/assets/vendors/custom/jquery-ui/jquery-ui.bundle.js') }}" type="text/javascript"></script>
		<script src="{{ asset('metronic/assets/demo/default/custom/components/portlets/draggable.js') }}" type="text/javascript"></script>
        <!--begin::Page Snippets -->
		<script src="{{asset('metronic/assets/app/js/dashboard.js')}}" type="text/javascript"></script>
		<!--end::Page Snippets -->
		<script>
			$('#m_quicksearch_input').click(function(){
				document.getElementById("myDropdown1").classList.toggle("show");
			});

			$('#m_quicksearch_input').keyup(function(){
				document.getElementById("myDropdown1").classList.toggle("show");
				var input, filter, ul, li, a, i, div;
				input = document.getElementById("m_quicksearch_input");
				filter = input.value.toUpperCase();
				div = document.getElementById("myDropdown1");
				a = div.getElementsByTagName("a");
				for (i = 0; i < a.length; i++) {
					if (a[i].innerHTML.toUpperCase().indexOf(filter) > -1) {
						a[i].style.display = "";
					} else {
						a[i].style.display = "none";
					}
				}
			});
		</script>
	</body>
	<!-- end::Body -->
</html>