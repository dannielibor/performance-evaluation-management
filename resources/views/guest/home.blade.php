@extends('guest.layouts.app')

@section('title', 'Home')

@section('content')

<style>

    .avatar-upload {
    position: relative;
    max-width: 205px;
    margin: 10px auto;
    }
    .avatar-upload .avatar-edit {
    position: absolute;
    right: 12px;
    z-index: 1;
    top: 10px;
    }
    .avatar-upload .avatar-edit input, #imageUpload {
    display: none;
    }
    .avatar-upload .avatar-edit input + label {
    display: inline-block;
    width: 34px;
    height: 34px;
    margin-bottom: 0;
    border-radius: 100%;
    background: #FFFFFF;
    border: 1px solid transparent;
    box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.12);
    cursor: pointer;
    font-weight: normal;
    transition: all 0.2s ease-in-out;
    }
    .avatar-upload .avatar-edit input + label:hover {
    background: #f1f1f1;
    border-color: #d6d6d6;
    }
    .avatar-upload .avatar-edit input + label:after {
    content: "\f040";
    font-family: 'FontAwesome';
    color: #757575;
    position: absolute;
    top: 10px;
    left: 0;
    right: 0;
    text-align: center;
    margin: auto;
    }
    .avatar-upload .avatar-preview {
    width: 192px;
    height: 192px;
    position: relative;
    border-radius: 100%;
    border: 6px solid #F8F8F8;
    box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.1);
    }
    .avatar-upload .avatar-preview > div {
    width: 100%;
    height: 100%;
    border-radius: 100%;
    background-size: cover;
    background-repeat: no-repeat;
    background-position: center;
    }

    input[type='number']::-webkit-outer-spin-button,
    input[type='number']::-webkit-inner-spin-button {
        -webkit-appearance: none;
    }

    #fullname, #setFname{
        text-transform: capitalize;
    }

    .user-img{
        border-radius:100%;
        width: 15%;
        margin-right:20px;
    }

</style>

<div class="m-content">

    @if(!empty($restrict))

        <?php
            $total = 0;
            $result = 0;
            $bodies = App\Body::where('formId', $restrict->form->id)->get();
            foreach($bodies as $body){
                $contents = App\Content::where('bodyId', $body->id)->where('type', 'question')->get();
                foreach($contents as $content){
                    $total = ++$total;
                    $cor = App\COR::orderBy('id', 'asc')->first();
                    $formProgress = App\FormProgress::where('formId', $restrict->form->id)
                                                    ->where('evaluatee', auth()->user()->id)
                                                    ->where('evaluator', auth()->user()->id)
                                                    ->where('as', Session::get('type'))
                                                    ->where('term', $cor->term)
                                                    ->where('year', $cor->year)->first();
                    if(!empty($formProgress)){
                        $entries = App\Entry::where('contentId', $content->id)->where('formProgressId', $formProgress->id)->first();
                        if(!empty($entries)){
                            $result = ++$result;
                        }
                    }
                }
            }
            if($total == 0){
                $total = 100;
            }
            $percent = round(($result/$total) * 100);
        ?>

        @if($percent < 50)
            <?php $status = 'danger'; ?>
        @elseif($percent < 100)
            <?php $status = 'warning'; ?>
        @else 
            <?php $status = 'success'; ?>
        @endif
        
        @if(Session::get('type') == 'Dean' || Session::get('type') == 'Vice Dean')
        <div class="row">
            <div class="col-xl-9 col-lg-8">
        @endif
                <div>
                    <div>
                        <div class="row m-row--no-padding m-row--col-separator">
                            <div class="col-xl-4">
                                <div class="m-portlet m-portlet--full-height">
                                    <div class="m-portlet__body">
                                        <div class="m-card-profile">
                                            <div class="m-card-profile__details">
                                                <span class="m-card-profile__name">
                                                    {{ $restrict->form->title }}
                                                </span>
                                                <small>Self Evaluation</small>
                                            </div>
                                        </div>
                                        <ul class="m-nav m-nav--hover-bg m-portlet-fit--sides">
                                            <li class="m-nav__separator m-nav__separator--fit"></li>
                                            <li class="m-nav__item">
                                                <div class="m-nav__link">
                                                    @if($percent != 100)
                                                        <i class="m-nav__link-icon fa fa-exclamation-circle" style = "color:grey"></i>
                                                    @endif
                                                    <span class="m-nav__link-title">
                                                        <span class="m-nav__link-wrap">
                                                            <span class="m-nav__link-text" style = "text-align:right">
                                                                @if($status != 'success')
                                                                    <div class="progress">
                                                                        <div class="progress-bar progress-bar-striped bg-{{$status}} progress-bar-animated" role="progressbar" style="width: {{$percent}}%" aria-valuenow="{{$percent}}" aria-valuemin="0" aria-valuemax="100"></div>
                                                                    </div>
                                                                    {{$percent}}%
                                                                @endif
                                                            </span>
                                                        </span>
                                                    </span>
                                                </div>
                                                @if($status != 'success')
                                                    <center>
                                                        <button onclick="window.location.href='/forms/{{$restrict->form->title}}/{{$restrict->form->id}}/faculty/{{Session::get('type')}}/{{auth()->user()->id}}'" class = "btn btn-accent m-btn--pill">
                                                            @if($percent == 0)
                                                                Start
                                                            @else 
                                                                Continue
                                                            @endif
                                                        </button>
                                                    </center>
                                                @else 
                                                    <center>
                                                        <i class="fa fa-check-circle" style = "color:green;font-size:50px"></i>
                                                    </center>
                                                @endif
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-8">
                                <div class="m-portlet m-portlet--full-height m-portlet--tabs">
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="m_user_profile_tab_1">
                                            <div class="m-portlet__body" style = "padding:50px">
                
                                                <?php
                                                    $get = App\COR::orderBy('id', 'asc')->first();
                                                    $all = 0;
                                                    $data = 0;
                                                ?>
                
                                                <div class="form-group m-form__group row">
                                                    @foreach (App\Body::where('formId', $restrict->formId)->get() as $body)
                                                        <label for="example-text-input" class="col-2 col-form-label">
                                                            {{$body->sort}}. {{$body->header}} ({{$body->rate}}%)
                                                        </label>
                                                        <div class="col-sm-4" style = "text-align:right">
                                                            @foreach (App\Content::where('bodyId', $body->id)->where('type', 'question')->get() as $content)
                                                                <?php 
                                                                    $all = ++$all;
                                                                    $formProgress = App\FormProgress::where('formId', $restrict->form->id)->where('evaluatee', auth()->user()->id)->where('evaluator', auth()->user()->id)->where('as', Session::get('type'))->where('term', $get->term)->where('year', $get->year)->first();
                                                                    if(!empty($formProgress)){
                                                                        $entry = App\Entry::where('contentId', $content->id)->where('formProgressId', $formProgress->id)->first(); 
                                                                        if(!empty($entry)){
                                                                            $data = ++$data;
                                                                        }
                                                                    }
                                                                ?>
                                                            @endforeach
                                                            <?php
                                                                if($all == 0){
                                                                    $all = 100;
                                                                }
                                                                $complete = round(($data/$all) * 100);
                                                            ?>
                                                            @if($complete < 50)
                                                                <?php $state = 'danger'; ?>
                                                            @elseif($complete < 100)
                                                                <?php $state = 'warning'; ?>
                                                            @else 
                                                                <?php $state = 'success'; ?>
                                                            @endif
                                                            <div class="progress">
                                                                <div class="progress-bar progress-bar-striped bg-{{$state}} progress-bar-animated" role="progressbar" style="width: {{$complete}}%" aria-valuenow="{{$complete}}" aria-valuemin="0" aria-valuemax="100"></div>
                                                            </div>
                                                            {{$complete}}%
                                                        </div>      
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @if(Session::get('type') == 'Dean' || Session::get('type') == 'Vice Dean')
            </div>
            <div class="col-xl-3 col-lg-4">
                <div class="m-portlet m-portlet--full-height">
                    <!--begin:: Widgets/Daily Sales-->
                    <div class="m-widget14">
                        <div class="m-widget14__header m--margin-bottom-30">
                            <h3 class="m-widget14__title">
                                Department Rate
                            </h3>
                            <span class="m-widget14__desc">
                                Check out each collumn for more details
                            </span>
                            <div class = "row">
                                <div class = "col">
                                    <select id="year" class = "form-control">
                                        @foreach (App\FormProgress::distinct()->get(['year']) as $year)
                                            <option value="{{$year->year}}">{{$year->year}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class = "col">
                                    <select id="term" class = "form-control">
                                        @foreach (App\FormProgress::distinct()->get(['term']) as $term)
                                            <option value="{{$term->term}}">{{$term->term}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="m-widget14__chart" style="height:120px;">
                            <canvas  id="m_chart_daily_sales"></canvas>
                        </div>
                    </div>
                    <!--end:: Widgets/Daily Sales-->
                </div>
            </div>
        </div>
        @endif
    @endif

    <div class="row">
        <div class="col-xl-3 col-lg-4">
            <div class="m-portlet m-portlet--full-height">
                <div class="m-portlet__body">
                    <div class="m-card-profile">
                        <div class="m-card-profile__title m--hide">
                            Your Profile
                        </div>
                        <div class="m-card-profile__pic">
                            <div class="avatar-upload"> 
                                <div class="avatar-preview">
                                    <div id="imagePreview" style="background-image: url('/storage/profiles/{{ $user->image }}');">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="m-card-profile__details">
                            <span class="m-card-profile__name">
                                <span id = "setFname">{{ $user->name }}</span>
                                @if(Session::get('type') == 'Student')
                                    @if($user->student->gender == 'M')
                                        <i class = "fa fa-male"></i>
                                    @else 
                                        <i class = "fa fa-female"></i>
                                    @endif
                                @endif
                            </span>
                            <a href="" class="m-card-profile__email m-link" id = "setEmail">
                                <span id = "setEmail">{{ $user->email }}</span>
                            </a>
                        </div>
                    </div>
                    <ul class="m-nav m-nav--hover-bg m-portlet-fit--sides">
                        <li class="m-nav__separator m-nav__separator--fit"></li>
                        <li class="m-nav__section m--hide">
                            <span class="m-nav__section-text">
                                Section
                            </span>
                        </li>
                        <li class="m-nav__item">
                            <div class="m-nav__link">
                                <i class="m-nav__link-icon fa fa-vcard"></i>
                                <span class="m-nav__link-title">
                                    <span class="m-nav__link-wrap">
                                        <span class="m-nav__link-text">
                                            @if(Session::get('type') == 'Student')
                                                {{$user->student->studentNo}}   
                                            @else 
                                                {{$user->faculty->facultyNo}}
                                            @endif
                                        </span>
                                    </span>
                                </span>
                            </div>
                        </li>
                        <li class="m-nav__item">
                            <div class="m-nav__link">
                                <i class="m-nav__link-icon fa fa-circle"></i>
                                <span class="m-nav__link-title">
                                    <span class="m-nav__link-wrap">
                                        <span class="m-nav__link-text">
                                            @if(Session::get('type') == 'Student')
                                                {{$user->student->program}}
                                            @else 
                                                {{$user->faculty->department}}
                                            @endif
                                        </span>
                                    </span>
                                </span>
                            </div>
                        </li>
                        <li class="m-nav__item">
                            <div class="m-nav__link">
                                <i class="m-nav__link-icon fa fa-info"></i>
                                <span class="m-nav__link-title">
                                    <span class="m-nav__link-wrap">
                                        <span class="m-nav__link-text">
                                            @if(Session::get('type') == 'Student')
                                                {{$user->student->yearLvl}}
                                            @else 
                                                {{$user->faculty->type}}
                                            @endif
                                        </span>
                                    </span>
                                </span>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-xl-9 col-lg-8">
            <div class="m-portlet m-portlet--full-height m-portlet--tabs">
                <div class="tab-content">
                    <div class="tab-pane active" id="m_user_profile_tab_1">
                        <div class="m-portlet__body">
                            @if(Session::get('type') == 'Student')
                                <table class = "m-datatable" id="html_table" width="100%">
                                    <thead style = "text-align:center">
                                        <tr>
                                            <th title = "Field #1">
                                                Subject Code
                                            </th>
                                            <th title = "Field #2">
                                                Subject Title
                                            </th>
                                            <th title = "Field #3">
                                                Section
                                            </th>
                                            <th title = "Field #4">
                                                Schedule
                                            </th>
                                            <th title = "Field #5">
                                                Room
                                            </th>
                                            <th title = "Field #6">
                                                Faculty
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach (App\COR::where('studentNo', $user->student->studentNo)->get() as $student)
                                            <tr>
                                                <td>
                                                    <center>
                                                        {{ $student->code }}
                                                    </center>
                                                </td>
                                                <td>
                                                    {{ $student->subject }}
                                                </td>
                                                <td>
                                                    <center>
                                                        {{ $student->section }}
                                                    </center>
                                                </td>
                                                <td>
                                                    {{ $student->schedule }}
                                                </td>
                                                <td>
                                                    <center>
                                                        {{ $student->room }}
                                                    </center>
                                                </td>
                                                <td>
                                                    <?php
                                                        $class = App\ClassLoad::where('code', $student->code)
                                                                                ->where('subject', $student->subject)
                                                                                ->where('section', $student->section)
                                                                                ->where('schedule', $student->schedule)
                                                                                ->where('room', $student->room)->first();    
                                                    ?>
                                                    @if( !empty($class->faculty) )
                                                    <img src="/storage/profiles/{{ $class->faculty->user->image }}" alt="" class = "user-img">{{ $class->faculty->user->name }}
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            @else 
                                <table class = "m-datatable" id="html_table" width="100%">
                                    <thead style = "text-align:center">
                                        <tr>
                                            <th title = "Field #1">
                                                Subject Code
                                            </th>
                                            <th title = "Field #2">
                                                Subject Title
                                            </th>
                                            <th title = "Field #3">
                                                Section
                                            </th>
                                            <th title = "Field #4">
                                                Schedule
                                            </th>
                                            <th title = "Field #5">
                                                Class Size
                                            </th>
                                            <th title = "Field #6">
                                                Room
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach (App\ClassLoad::where('facultyNo', $user->faculty->facultyNo)->get() as $faculty)
                                            <tr>
                                                <td>
                                                    <center>
                                                        {{ $faculty->code }}
                                                    </center>
                                                </td>
                                                <td>
                                                    {{ $faculty->subject }}
                                                </td>
                                                <td>
                                                    <center>
                                                        {{ $faculty->section }}
                                                    </center>
                                                </td>
                                                <td>
                                                    {{ $faculty->schedule }}
                                                </td>
                                                <td>
                                                    <center>
                                                        {{ $faculty->size }}
                                                    </center>
                                                </td>
                                                <td>
                                                    <center>
                                                        {{ $faculty->room }}
                                                    </center>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="{{ asset('metronic/assets/demo/default/custom/components/datatables/base/html-table.js') }}" type="text/javascript"></script>

</div>

@endsection