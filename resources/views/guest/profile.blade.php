@extends('guest.layouts.app')

@section('title', 'My Profile')

@section('content')

<style>

    .avatar-upload {
    position: relative;
    max-width: 205px;
    margin: 10px auto;
    }
    .avatar-upload .avatar-edit {
    position: absolute;
    right: 12px;
    z-index: 1;
    top: 10px;
    }
    .avatar-upload .avatar-edit input, #imageUpload {
    display: none;
    }
    .avatar-upload .avatar-edit input + label {
    display: inline-block;
    width: 34px;
    height: 34px;
    margin-bottom: 0;
    border-radius: 100%;
    background: #FFFFFF;
    border: 1px solid transparent;
    box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.12);
    cursor: pointer;
    font-weight: normal;
    transition: all 0.2s ease-in-out;
    }
    .avatar-upload .avatar-edit input + label:hover {
    background: #f1f1f1;
    border-color: #d6d6d6;
    }
    .avatar-upload .avatar-edit input + label:after {
    content: "\f040";
    font-family: 'FontAwesome';
    color: #757575;
    position: absolute;
    top: 10px;
    left: 0;
    right: 0;
    text-align: center;
    margin: auto;
    }
    .avatar-upload .avatar-preview {
    width: 192px;
    height: 192px;
    position: relative;
    border-radius: 100%;
    border: 6px solid #F8F8F8;
    box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.1);
    }
    .avatar-upload .avatar-preview > div {
    width: 100%;
    height: 100%;
    border-radius: 100%;
    background-size: cover;
    background-repeat: no-repeat;
    background-position: center;
    }

    input[type='number']::-webkit-outer-spin-button,
    input[type='number']::-webkit-inner-spin-button {
        -webkit-appearance: none;
    }

    #fullname, #setFname{
        text-transform: capitalize;
    }

</style>

<div class="m-content">
    <div class="row">
        <div class="col-xl-3 col-lg-4">
            <div class="m-portlet m-portlet--full-height  ">
                <div class="m-portlet__body">
                    <div class="m-card-profile">
                        <div class="m-card-profile__title m--hide">
                            Your Profile
                        </div>
                        <div class="m-card-profile__pic">
                            <div class="avatar-upload">
                                <div class="avatar-edit">
                                    <input type='file'/>
                                    <label onclick = "$('.input').click();"></label>
                                </div>
                                <div class="avatar-preview">
                                    <div id="imagePreview" style="background-image: url('/storage/profiles/{{ $user->image }}');">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="m-card-profile__details">
                            <span class="m-card-profile__name">
                                <span id = "setFname">{{ $user->name }}</span>
                                @if(Session::get('type') == 'Student')
                                    @if($user->student->gender == 'M')
                                        <i class = "fa fa-male"></i>
                                    @else
                                        <i class = "fa fa-female"></i>
                                    @endif
                                @endif
                            </span>
                            <a href="" class="m-card-profile__email m-link" id = "setEmail">
                                <span id = "setEmail">{{ $user->email }}</span>
                            </a>
                        </div>
                    </div>
                    <ul class="m-nav m-nav--hover-bg m-portlet-fit--sides">
                        <li class="m-nav__separator m-nav__separator--fit"></li>
                        <li class="m-nav__section m--hide">
                            <span class="m-nav__section-text">
                                Section
                            </span>
                        </li>
                        <li class="m-nav__item">
                            <div class="m-nav__link">
                                <i class="m-nav__link-icon fa fa-vcard"></i>
                                <span class="m-nav__link-title">
                                    <span class="m-nav__link-wrap">
                                        <span class="m-nav__link-text">
                                            @if(Session::get('type') == 'Student')
                                                {{$user->student->studentNo}}   
                                            @else 
                                                {{$user->faculty->facultyNo}}
                                            @endif
                                        </span>
                                    </span>
                                </span>
                            </div>
                        </li>
                        <li class="m-nav__item">
                            <div class="m-nav__link">
                                <i class="m-nav__link-icon fa fa-circle"></i>
                                <span class="m-nav__link-title">
                                    <span class="m-nav__link-wrap">
                                        <span class="m-nav__link-text">
                                            @if(Session::get('type') == 'Student')
                                                {{$user->student->program}}
                                            @else 
                                                {{$user->faculty->department}}
                                            @endif
                                        </span>
                                    </span>
                                </span>
                            </div>
                        </li>
                        <li class="m-nav__item">
                            <div class="m-nav__link">
                                <i class="m-nav__link-icon fa fa-info"></i>
                                <span class="m-nav__link-title">
                                    <span class="m-nav__link-wrap">
                                        <span class="m-nav__link-text">
                                            @if(Session::get('type') == 'Student')
                                                {{$user->student->yearLvl}}
                                            @else 
                                                {{$user->faculty->type}}
                                            @endif
                                        </span>
                                    </span>
                                </span>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-xl-9 col-lg-8">
            <div class="m-portlet m-portlet--full-height m-portlet--tabs  ">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-tools">
                        <ul class="nav nav-tabs m-tabs m-tabs-line   m-tabs-line--left m-tabs-line--default" role="tablist">
                            <li class="nav-item m-tabs__item">
                                <a class="nav-link m-tabs__link <?php if(!isset($settings)){echo 'active';} ?>" data-toggle="tab" href="#m_user_profile_tab_1" role="tab">
                                    <i class="flaticon-share m--hide"></i>
                                    @if(Session::get('type') == 'Student')
                                        Student Profile
                                    @else 
                                        Faculty Profile
                                    @endif
                                </a>
                            </li>
                            <li class="nav-item m-tabs__item">
                                <a class="nav-link m-tabs__link <?php if(isset($settings)){echo 'active';} ?>" data-toggle="tab" href="#m_user_profile_tab_3" role="tab">
                                    Settings
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="tab-content">
                    <div class="tab-pane <?php if(!isset($settings)){echo 'active';} ?>" id="m_user_profile_tab_1">
                        {!! Form::open(['action' => ['Guest\ProfileController@update', $user->id], 'enctype' => 'multipart/form-data', 'method' => 'post', 'class' => 'm-form m-form--fit m-form--label-align-right']) !!}
                            @csrf
                            {!! Form::hidden('_method', 'PUT') !!}
                            <input type="hidden" name = "image" value = "{{ $user->image }}">
                            <input type='file' name = "img" id="imageUpload" accept=".png, .jpg, .jpeg" class = "input" capture/>
                        <div class="m-portlet__body">
                            <div class="form-group m-form__group">
                                <label for="example-text-input" class="form-label">
                                    Username
                                </label>
                                <input class="form-control m-input m-input--air" type="text" id = "username" name = "username" value = "{{$user->username}}" required>
                            </div>
                            <div class="form-group m-form__group">
                                <label for="example-text-input" class="form-label">
                                    Email
                                </label>
                                <input class="form-control m-input m-input--air" id = "email" type="email" value="{{ $user->email }}" name = "email">
                            </div>
                            <div class="form-group m-form__group">
                                <label for="example-text-input" class="form-label">
                                    Change Password
                                </label>
                                <input class="form-control m-input m-input--air" type="password" id = "password" name = "password">
                            </div>
                            <div class="form-group m-form__group">
                                <label for="example-text-input" class="form-label">
                                    Confirm Password
                                </label>
                                <input class="form-control m-input m-input--air" type="password" id = "confirm_password">
                            </div>
                        </div>
                        <div class="m-portlet__foot m-portlet__foot--fit">
                            <div class="m-form__actions">
                                <div class="row">
                                    <div class="col-2"></div>
                                    <div class="col-7">
                                        <button type="submit" class="m-btn--pill btn btn-success m-btn m-btn--air m-btn--custom">
                                            Submit
                                        </button>
                                        &nbsp;&nbsp;
                                        <a href = "javascript:history.back()" class="m-btn--pill btn btn-danger m-btn m-btn--air m-btn--custom">
                                            Cancel
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                    <div class="tab-pane <?php if(isset($settings)){echo 'active';} ?>" id="m_user_profile_tab_3">
                        <div class="m-portlet__body">
                            <div class="form-group m-form__group row">
                                <div class="col-10 ml-auto">
                                    <h3 class="m-form__section">
                                        Privacy
                                    </h3>
                                </div>
                            </div>
                            <div class="m-form__group form-group row">
                                <label class="col-3 col-form-label">
                                    Two-Factor Authentication (2FA)
                                </label>
                                <div class="col-3">
                                    <span class="m-switch m-switch--outline m-switch--icon m-switch--success">
                                        <label>
                                            <input type="checkbox" id = "2fa" 
                                                <?php
                                                    if(auth()->user()->twoFA == 1){
                                                        echo 'checked';
                                                    }
                                                ?>>
                                            <span></span>
                                        </label>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>

    $('#2fa').change(function(){
        $.ajax({
            url: '/2fa',
            type: 'get'
        });
    });

    var password = document.getElementById("password")
    , confirm_password = document.getElementById("confirm_password");

    function validatePassword(){
    if(password.value != confirm_password.value) {
        confirm_password.setCustomValidity("Passwords Don't Match");
    } else {
        confirm_password.setCustomValidity('');
    }
    }

    password.onchange = validatePassword;
    confirm_password.onkeyup = validatePassword;

</script>

@endsection