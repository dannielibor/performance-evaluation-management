<?php
    $total = 0;
    $result = 0;
    $bodies = App\Body::where('formId', $restrict->form->id)->get();
    foreach($bodies as $body){
        $contents = App\Content::where('bodyId', $body->id)->where('type', 'question')->get();
        foreach($contents as $content){
            $total = ++$total;
        }
    }
    $get = App\COR::orderBy('created_at', 'asc')->first();
    $formProgress = App\FormProgress::where('formId', $restrict->form->id)->where('evaluator', auth()->user()->id)->where('evaluatee', $faculty->userId)->where('year', $get->year)->where('term', $get->term)->where('as', $as)->first();
    if(!empty($formProgress)){
        $entries = App\Entry::where('formProgressId', $formProgress->id)->get();
        foreach($entries as $entry){
            $result = ++$result;
        }
    }
    if($total == 0){
        $total = 100;
    }
    $percent = round(($result/$total) * 100);
?>

@if($percent < 50)
    <?php $status = 'danger'; ?>
@elseif($percent < 100)
    <?php $status = 'warning'; ?>
@else 
    <?php $status = 'success'; ?>
@endif

<div class="col-sm-{{$bootstrapColWidth}}">
    <div class="m-portlet m-portlet--creative m-portlet--first m-portlet--bordered-semi">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <br>
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon m--hide">
                        <i class="flaticon-statistics"></i>
                    </span>
                    @if(Session::get('type') == 'Student')
                        <h3 class="m-portlet__head-text">
                            <p><strong>Subject: </strong>{{ $sched->subject }}</p>
                            <p><strong>Section: </strong>{{ $sched->section }}</p>
                            <p><Strong>Schedule: </Strong>{{ $sched->schedule }}</p>
                            <p><strong>Room: </strong>{{ $sched->room }}</p>
                        </h3>
                    @endif
                    @if($status == 'success')
                        <h2 class="m-portlet__head-label m-portlet__head-label--{{$status}}" style = "padding: 15px">
                            <span>
                                <img src="/storage/profiles/{{$faculty->user->image}}" alt="{{$faculty->user->image}}" width = "50" style = "border-radius:50%">
                            </span>
                            &nbsp;
                            <span>{{$faculty->user->name}}</span>
                        </h2>
                    @else 
                        <h2 class="m-portlet__head-label m-portlet__head-label--metal" style = "padding: 15px;cursor:pointer" 
                        onclick="window.location.href='/forms/{{$restrict->form->title}}/{{$restrict->form->id}}/faculty/{{$as}}/{{$faculty->userId}}'">
                            <span>
                                <img src="/storage/profiles/{{$faculty->user->image}}" alt="{{$faculty->user->image}}" width = "50" style = "border-radius:50%">
                            </span>
                            &nbsp;
                            <span>{{$faculty->user->name}}</span>
                        </h2>
                    @endif
                </div>
            </div>
        </div>
        <div class="m-portlet__body">
            @if($status != 'success')
                <div class="progress">
                    <div class="progress-bar progress-bar-striped bg-{{$status}} progress-bar-animated" role="progressbar" style="width: {{$percent}}%" aria-valuenow="{{$percent}}" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
                {{$percent}}%
            @endif
        </div>
    </div>
</div>