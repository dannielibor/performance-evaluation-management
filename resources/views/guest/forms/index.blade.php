@extends('guest.layouts.app')

@section('title', 'Forms')

@section('content')

<?php
    //Columns must be a factor of 12 (1,2,3,4,6,12)
    $numOfCols = 3;
    $rowCount = 0;
    $bootstrapColWidth = 12 / $numOfCols;
    $accordion = 0;
?>

<!-- BEGIN: Subheader -->
<div class="m-subheader ">
    <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title ">
                {{ $restrict->form->title }}
            </h3>
        </div>
    </div>
</div>
<!-- END: Subheader -->

<div class="m-content">

    @if(Session::get('type') == 'Student')

        @if($restrict->status == 'Distinct')

            <div class = "row">

                @foreach (App\COR::where('studentNo', auth()->user()->student->studentNo)->get() as $sched)
                
                    @foreach (App\ClassLoad::where('schedule', $sched->schedule)->where('code', $sched->code)->where('section', $sched->section)->where('year', $sched->year)->where('term', $sched->term)->get() as $load)
                        <?php
                            $explode = explode(' ', $load->subject);
                        ?>
                        @if(last($explode) != 'Laboratory')
                            @if($restrict->evaluatee == 'Professor')
                                <?php
                                    $faculties = App\Faculty::where('facultyNo', $load->facultyNo)->whereIn('type', ['Professor', 'Department Chair', 'Vice Dean', 'Dean'])->get();
                                ?>
                            @else 
                                <?php
                                    $faculties = App\Faculty::where('facultyNo', $load->facultyNo)->where('type', $restrict->evaluatee)->get();
                                ?>
                            @endif

                            @foreach ($faculties as $faculty)
                                @include('guest.forms.faculty')
                            @endforeach
                        @endif

                    @endforeach

                @endforeach

            </div>

        @else 

            @foreach (App\Faculty::distinct()->get(['department']) as $department)

                @if($restrict->evaluatee == 'Professor')
                    <?php $faculties = App\Faculty::where('department', $department->department)
                                                    ->whereIn('type', ['Professor', 'Department Chair', 'Vice Dean', 'Dean'])
                                                    ->where('userId', '!=', auth()->user()->id)->get(); ?>
                @else 
                    <?php $faculties = App\Faculty::where('department', $department->department)
                                                    ->where('type', $restrict->evaluatee)
                                                    ->where('userId', '!=', auth()->user()->id)->get(); ?>
                @endif

                @if(count($faculties) > 1)

                    @include('guest.forms.department')
                    <?php ++$accordion; ?>

                @else 

                    @foreach ($faculties as $faculty)
                        @include('guest.forms.faculty')
                    @endforeach

                @endif

            @endforeach

        @endif

    @else 

        @if($restrict->status == 'Distinct')

            @if($restrict->evaluatee == 'Professor')
                <?php $faculties = App\Faculty::where('department', auth()->user()->faculty->department)
                                                ->whereIn('type', ['Professor', 'Department Chair', 'Vice Dean', 'Dean'])
                                                ->where('userId', '!=', auth()->user()->id)->get(); ?>
            @else 
                <?php $faculties = App\Faculty::where('department', auth()->user()->faculty->department)
                                                ->where('type', $restrict->evaluatee)
                                                ->where('userId', '!=', auth()->user()->id)->get(); ?>
            @endif

            <div class = "row">
                @if(count($faculties) > 0)
                    @foreach ($faculties as $faculty)
                        @include('guest.forms.faculty')
                    @endforeach
                @else 
                    No Data Found
                @endif
            </div>

        @else 

            @foreach (App\Faculty::distinct()->get(['department']) as $department)

                @if($restrict->evaluatee == 'Professor')
                    <?php $faculties = App\Faculty::where('department', $department->department)
                                                    ->whereIn('type', ['Professor', 'Department Chair', 'Vice Dean', 'Dean'])
                                                    ->where('userId', '!=', auth()->user()->id)->get(); ?>
                @else 
                    <?php $faculties = App\Faculty::where('department', $department->department)
                                                    ->where('type', $restrict->evaluatee)
                                                    ->where('userId', '!=', auth()->user()->id)->get(); ?>
                @endif
                
                @if( count($faculties) > 0 )
                    @include('guest.forms.department')
                    <?php ++$accordion; ?>
                @endif

            @endforeach

        @endif

    @endif

</div>

@endsection