<form action="/entry" method = "post">
    @csrf
    <input type="hidden" value = "{{$form->id}}" name = "formId" id = "formId">
    <input type="hidden" value = "{{$user->id}}" name = "userId" id = "evaluatee">
    <input type="hidden" value = "{{$as}}" name = "as" id = "faculty"> 
    @if(count($body) > 0)
        @foreach ($body as $item)
            <div class="m-accordion__item">
                <div class="m-accordion__item-head" role="tab" id="m_accordion_7_item_1_head" aria-expanded="false" style = "text-align:left">
                    <span class="m-accordion__item-title" style = "color:#800">
                        {{$item->sort}}. {{$item->header}} <small>({{$item->rate}}%)</small>
                    </span>
                </div>
                <div class="m-accordion__item-body" id="m_accordion_7_item_1_body" role="tabpanel" aria-labelledby="m_accordion_7_item_1_head">
                    <div class="m-accordion__item-content">
                        @foreach (App\Content::where('bodyId', $item->id)->orderByRaw('LENGTH(position)', 'asc')->orderBy('position', 'asc')->get() as $content)
                            <div class="sv_main sv_default_css">
                                <div class="sv_container">
                                    <div class="sv_body" style = "border:none">
                                        <div class="sv_p_root">

                                            @if($content->type == 'header')
                                                <h5 class="sv_q_title" style = "text-align:left">
                                                    <span style="position: static;">{{$content->sort}}. {{$content->content}}</span>
                                                </h5>
                                            @else 

                                            <?php
                                                $get = App\COR::orderBy('id', 'desc')->first();
                                                $formProgress = App\FormProgress::where('formId', $form->id)->where('evaluatee', $user->id)
                                                                                    ->where('evaluator', auth()->user()->id)
                                                                                    ->where('year', $get->year)
                                                                                    ->where('term', $get->term)
                                                                                    ->where('as', $as)->first();
                                                if(!empty($formProgress)){
                                                    $entry = App\Entry::where('contentId', $content->id)->where('formProgressId', $formProgress->id)->first();
                                                    $entries = App\Entry::where('formProgressId', $formProgress->id)->get();
                                                }
                                            ?>
   
                                                <table  class="table table-striped sv_q_matrix">
                                                    <thead>
                                                        <tr>
                                                            <td></td>
                                                            <th style = "font-size:10px;text-align:center">Outstanding</th>
                                                            <th style = "font-size:10px;text-align:center">Very Satisfactory</th>
                                                            <th style = "font-size:10px;text-align:center">Satisfactory</th>
                                                            <th style = "font-size:10px;text-align:center">Moderatly Satisfactory</th>
                                                            <th style = "font-size:10px;text-align:center">Needs Improvement</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr style = "border-bottom:none; <?php if(empty($entry) && isset($entries) && count($entries) > 0){echo'background-color:#ffcccc';} ?> ">
                                                            <td style = "width: 100%">
                                                                <span style="position: static;">{{$content->content}}</span>
                                                            </td>
                                                            <td>
                                                                <label class="sv_q_m_label">
                                                                    <input type="radio" value = "5" <?php if(!empty($entry) && $entry->result == 5){echo'checked';} ?> name = "radio{{$content->id}}" data-id = "{{$content->id}}">
                                                                    <span class="circle"></span>
                                                                    <span class="check"></span>
                                                                    <span class="matrix-vert">Outstanding</span>
                                                                </label>
                                                            </td>
                                                            <td>
                                                                <label class="sv_q_m_label">
                                                                    <input type="radio" value = "4" <?php if(!empty($entry) && $entry->result == 4){echo'checked';} ?> name = "radio{{$content->id}}" data-id = "{{$content->id}}">
                                                                    <span class="circle"></span>
                                                                    <span class="check"></span>
                                                                    <span class="matrix-vert">Very Satisfactory</span>
                                                                </label>
                                                            </td>
                                                            <td>
                                                                <label class="sv_q_m_label">
                                                                    <input type="radio" value = "3" <?php if(!empty($entry) && $entry->result == 3){echo'checked';} ?> name = "radio{{$content->id}}" data-id = "{{$content->id}}">
                                                                    <span class="circle"></span>
                                                                    <span class="check"></span>
                                                                    <span class="matrix-vert">Satisfactory</span>
                                                                </label>
                                                            </td>
                                                            <td>
                                                                <label class="sv_q_m_label">
                                                                    <input type="radio" value = "2" <?php if(!empty($entry) && $entry->result == 2){echo'checked';} ?> name = "radio{{$content->id}}" data-id = "{{$content->id}}">
                                                                    <span class="circle"></span>
                                                                    <span class="check"></span>
                                                                    <span class="matrix-vert">Moderatly Satisfactory</span>
                                                                </label>
                                                            </td>
                                                            <td class = "radio">
                                                                <label class="sv_q_m_label">
                                                                    <input type="radio" value = "1" <?php if(!empty($entry) && $entry->result == 1){echo'checked';} ?> name = "radio{{$content->id}}" data-id = "{{$content->id}}">
                                                                    <span class="circle"></span>
                                                                    <span class="check"></span>
                                                                    <span class="matrix-vert">Needs Improvement</span>
                                                                </label>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>

                                            @endif

                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        @endforeach
    @else 
        <center>
            No data found
        </center>
        <br>
    @endif

    @if($body->lastPage() == $body->currentPage())

        @foreach (App\Feedback::where('formId', $form->id)->get() as $feedback)
            <?php 
                $get = App\COR::orderBy('id', 'asc')->first();
                $result = App\FeedbackResult::where('formId', $form->id)
                                            ->where('evaluator', auth()->user()->id)
                                            ->where('evaluatee', $user->id)
                                            ->where('year', $get->year)
                                            ->where('as', $as)
                                            ->where('term', $get->term)->get();
            ?>
            @if(count($result) > 1)
                @foreach ($result as $FeedbackResult)
                    <input type="hidden" value = "{{$FeedbackResult->id}}" name = "feedbackId_{{$FeedbackResult->id}}">
                    <div class = "container" style = "margin-bottom:50px">
                        <label>{{$FeedbackResult->header}}</label>
                        <textarea style = "resize:none" name="feedback_{{$FeedbackResult->formId->id}}" placeholder="Text here" cols="30" rows="10" class = "form-control">
                            @if(!empty($FeedbackResult->result))
                                {{ $FeedbackResult->result }}
                            @endif
                        </textarea>
                    </div>
                @endforeach
            @else 
                <div class = "container" style = "margin-bottom:50px">
                    <label>{{$feedback->header}}</label>
                    <textarea style = "resize:none" name="feedback_{{$feedback->id}}" placeholder="Text here" cols="30" rows="10" class = "form-control"></textarea>
                </div>
            @endif

        @endforeach

    @endif
    
        <div style = "float:right">
            {{$body->links()}}
        </div>

    @if($body->lastPage() == $body->currentPage() && count($body) > 0)

        <button class = "btn btn-success" style = "margin-bottom:15px" type = "submit">Submit</button>

    @endif

    <a href = "javascript:history.back()" class = "btn btn-metal" style = "margin-bottom:15px">Cancel</a>

</form>