<div class="m-accordion m-accordion--default" id="m_accordion_1" role="tablist">

    <?php
        $total = 0;
        $result = 0;
        $bodies = App\Body::where('formId', $restrict->form->id)->get();
        foreach($bodies as $body){
            $contents = App\Content::where('bodyId', $body->id)->where('type', 'question')->get();
            foreach($contents as $content){
                if($restrict->evaluatee == 'Professor'){
                    $users = App\Faculty::whereIn('type', ['Professor', 'Department Chair', 'Vice Dean', 'Dean'])->where('department', $department->department)->where('userId', '!=', auth()->user()->id)->get();
                }else{ 
                    $users = App\Faculty::where('type', $restrict->evaluatee)->where('department', $department->department)->where('userId', '!=', auth()->user()->id)->get();
                }
                foreach($users as $user){ 
                    $total = ++$total;
                    $get = App\COR::orderBy('created_at', 'asc')->first();
                    $formProgress = App\FormProgress::where('formId', $restrict->form->id)->where('evaluator', auth()->user()->id)->where('evaluatee', $user->userId)->where('year', $get->year)->where('term', $get->term)->where('as', $as)->first();
                    if(!empty($formProgress)){
                        $entries = App\Entry::where('formProgressId', $formProgress->id)->where('contentId', $content->id)->first();
                        if(!empty($entries)){
                            $result = ++$result;
                        }
                    }
                }
            }
        }
        if($total == 0){
            $total = 100;
        }
        $percent = round(($result/$total) * 100);
    ?>

    <div class="m-accordion__item">
        <div class="m-accordion__item-head collapsed"  role="tab" id="m_accordion_1_item_1_head_{{$accordion}}" data-toggle="collapse" href="#m_accordion_1_item_1_body_{{$accordion}}" aria-expanded="false">
            <span class="m-accordion__item-icon">
                @if($percent == 100)
                    <i class="fa fa-check-circle" style = "color:green"></i>
                @else 
                    <i class="fa fa-exclamation-circle" style = "color:grey"></i>
                @endif
            </span>
            <span class="m-accordion__item-title">
                {{$department->department}}
                @if($percent != 100)
                    @if($percent < 50)
                        <?php $rate = 'danger'; ?>
                    @elseif($percent < 100)
                        <?php $rate = 'warning'; ?>
                    @else 
                        <?php $rate = 'success'; ?>
                    @endif
                    <span class="m-widget1__number m--font-{{$rate}}">
                        {{$percent}}%
                    </span>
                    <div class="progress">
                        <div class="progress-bar progress-bar-striped bg-{{$rate}} progress-bar-animated" role="progressbar" style="width: {{$percent}}%" aria-valuenow="{{$percent}}" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                @endif
            </span>
            <span class="m-accordion__item-mode"></span>
        </div>
        <div class="m-accordion__item-body collapse" id="m_accordion_1_item_1_body_{{$accordion}}" class=" " role="tabpanel" aria-labelledby="m_accordion_1_item_1_head_{{$accordion}}" data-parent="#m_accordion_1">
            <div class="m-accordion__item-content">
                <div class = "row">
                    @foreach ($faculties as $faculty)
                        @include('guest.forms.faculty')
                    @endforeach
                </div>
            </div>
        </div>
    </div>

</div>