@extends('guest.layouts.app')

@section('title', 'Forms')

@section('content')

<style>

    .matrix-vert {
        display: none;
        }
    @media screen and (max-width: 640px) {
        .sv_q_matrix {
        border: 0;
        }
        .question{
        font-weight:bold;
        }
        .option {
        text-align: left;
        }
        .sv_q_matrix caption {
            font-size: 1.3em;
        }
    
        .sv_q_matrix thead {
            border: none;
            clip: rect(0 0 0 0);
            height: 1px;
            margin: -1px;
            overflow: hidden;
            padding: 0;
            position: absolute;
            width: 1px;
        }
    
        .sv_q_matrix tr {
            border: 1px solid #ddd !important;
            display: block;
            margin-bottom: .625em;
        }
    
        .sv_q_matrix td {
            border-bottom: 0;
            display: block;
            text-align: left;
        }
    
            .sv_q_matrix td::after {
            content: attr(title);
            }
    
            .sv_q_matrix td:last-child {
            border-bottom: 0;
            }
            .sv_q_matrix td:first-child {
            background-color:#ddd;
            }
            .matrix-vert {
        display: inline;
        }
    }

</style>

<center>
    <div class="m-content">
        <div class="m-portlet m-portlet--mobile" style = "padding-top:3px">
            <center>
                <div class="d-flex justify-content-center align-items-center p-3 my-3">
                    <img class="mr-3" src="/images/sbca.png" alt="" width="50">
                    <div class="lh-100">
                        <h6 class="mb-0 lh-100"><font face="Old English Text MT" size="5">San Beda College Alabang</font></h6>
                        <small>Alabang Hills Village, Muntinlupa City</small>
                    </div>
                </div>
                <h2>{{$form->title}}</h2>
                <p>Revised {{$form->updated_at->format('Y')}}</p>
            </center>

            @if(Session::get('type') == 'Student')
                <?php $class = App\ClassLoad::where('facultyNo', $user->faculty->facultyNo)->get(); ?>

                @foreach ($class as $load)
                    <?php $cor = App\COR::where('studentNo', auth()->user()->student->studentNo)->where('section', $load->section)->where('code', $load->code)->first(); ?>
                    @if(!empty($cor))
                        <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30" style = "text-align:left;width:80%">
                            <div class="row align-items-center">
                                <div class="col-xl-12 order-2 order-xl-1">
                                    <div class="form-group m-form__group row align-items-center">
                                        <div class="col-md-6">
                                            <div class="m-form__group m-form__group--inline">
                                                <p><strong>Faculty:&nbsp;</strong>{{$user->name}}</p>
                                            </div>
                                            <div class="d-md-none m--margin-bottom-10"></div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="m-form__group m-form__group--inline">
                                                <p><strong>Subject:&nbsp;</strong>{{ $cor->subject }}</p> 
                                            </div>
                                            <div class="d-md-none m--margin-bottom-10"></div>
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row align-items-center">
                                        <div class="col-md-6">
                                            <div class="m-form__group m-form__group--inline">
                                                <p><strong>Section:&nbsp;</strong>{{$cor->section}}</p>
                                            </div>
                                            <div class="d-md-none m--margin-bottom-10"></div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="m-form__group m-form__group--inline">
                                                <p><strong>Schedule:&nbsp;</strong>{{ $cor->schedule }} | {{ $cor->room }}</p> 
                                            </div>
                                            <div class="d-md-none m--margin-bottom-10"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @break
                    @endif
                @endforeach
            @else 
                <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30" style = "text-align:left;width:80%">
                    <div class="row align-items-center">
                        <div class="col-xl-12 order-2 order-xl-1">
                            <div class="form-group m-form__group row align-items-center">
                                <div class="col-md-6">
                                    <div class="m-form__group m-form__group--inline">
                                        <p><strong>Faculty:&nbsp;</strong>{{$user->name}}</p>
                                    </div>
                                    <div class="d-md-none m--margin-bottom-10"></div>
                                </div>
                                <div class="col-md-6">
                                    <div class="m-form__group m-form__group--inline">
                                        <p><strong>Department:&nbsp;</strong>{{$user->faculty->department}}</p> 
                                    </div>
                                    <div class="d-md-none m--margin-bottom-10"></div>
                                </div>
                            </div>
                            <div class="form-group m-form__group row align-items-center">
                                <div class="col-md-6">
                                    <div class="m-form__group m-form__group--inline">
                                        <p><strong>Type:&nbsp;</strong>{{$user->faculty->type}}</p>
                                    </div>
                                    <div class="d-md-none m--margin-bottom-10"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif

            <div id = "table_data"> 
                <div style = "margin-top:50px" class="fpes container m-accordion m-accordion--default m-accordion--solid m-accordion--section  m-accordion--toggle-arrow" id="m_accordion_7" role="tablist">
                    
                    @include('guest.forms.data')
                    
                </div>
            </div>
        </div>
    </div>
</center>

<script>

    $(document).ready(function(){
        $(document).on('click', '.pagination a', function(event){
            event.preventDefault();
            var page = $(this).attr('href').split('page=')[1];
            fetch_data(page);
            var array = [];
            $('input[type="radio"]:checked').each(function(){
                array.push($(this).attr('data-id')+'-'+$(this).val());
            });
            $.ajax({
                url: '/paginate',
                type: 'get',
                data: { id:$('#formId').val(), value: array, evaluatee: $('#evaluatee').val(), as:$('#faculty').val() },
                success: function(response){
                    $('#up').click();
                }
            });
        });
        function fetch_data(page){
            $.ajax({
                url: '/pagination/fetch_data?page='+page,
                type: 'get',
                data: { id:$('#formId').val(), userId:$('#evaluatee').val(), as:$('#faculty').val() },
                success: function(data){ 
                    $('.fpes').html(data);
                }
            });
        }
    });

</script>

@endsection