@component('mail::layout')
    {{-- Header --}}
    @slot('header')
        @component('mail::header', ['url' => config('app.url')])
            <center>
                <div class="d-flex justify-content-center align-items-center p-3 my-3">
                    <img class="mr-3" src="https://upload.wikimedia.org/wikipedia/en/c/c5/San_Beda_College_Alabang_logo.png" alt="" width="50">
                </div>
            </center>
        @endcomponent
    @endslot

    {{-- Body --}}
    {{ $slot }}

    {{-- Subcopy --}}
    @isset($subcopy)
        @slot('subcopy')
            @component('mail::subcopy')
                {{ $subcopy }}
            @endcomponent
        @endslot
    @endisset

    {{-- Footer --}}
    @slot('footer')
        @component('mail::footer')
            © {{ date('Y') }} SBCA. @lang('All rights reserved.')
        @endcomponent
    @endslot
@endcomponent
