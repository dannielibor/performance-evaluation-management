<?php

use Illuminate\Database\Seeder;

class FormProgressTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faculties = App\Faculty::where('department', 'Information Technology')->get();
        foreach($faculties as $faculty){
            $load = App\ClassLoad::where('facultyNo', $faculty->facultyNo)->get();
            foreach($load as $class){
                $cor = App\COR::where('code', $class->code)
                                ->where('subject', $class->subject)
                                ->where('schedule', $class->schedule)
                                ->where('room', $class->room)
                                ->where('section', $class->section)
                                ->where('year', $class->year)
                                ->where('term', $class->term)->take(10)->get();
                foreach($cor as $reg){
                    $explode = explode(' ', $reg->subject);
                    if(last($explode) != 'Laboratory'){
                        if($reg->student->yearLvl == '4th Year'){
                            $new = new App\FormProgress;
                            $new->formId = 1;
                            $new->evaluator = $reg->student->userId;
                            $new->evaluatee = $class->faculty->userId;
                            $new->year = $reg->year;
                            $new->term = $reg->term;
                            $new->as = 'Professor';
                            $new->save();
                        }
                    }
                }
            }
        }

        $faculties = App\Faculty::where('department', 'Information Technology')->get();
        foreach($faculties as $faculty){
            $forms = App\Form::all();
            foreach($forms as $form){
                $relations = App\FormRelationship::where('formId', $form->id)->where('evaluatee', $faculty->type)->get();
                foreach($relations as $relation){
                    if($relation->status == 'Distinct'){
                        $evaluators = App\Faculty::where('type', $relation->evaluator)->where('department', $faculty->department)->where('userId', '!=', $faculty->userId)->get();
                    }else{
                        $evaluators = App\Faculty::where('type', $relation->evaluator)->where('userId', '!=', $faculty->userId)->get();
                    }
                    foreach($evaluators as $evaluator){
                        $progress = new App\FormProgress;
                        $progress->formId = $form->id;
                        $progress->evaluatee = $faculty->userId;
                        $progress->evaluator = $evaluator->userId;
                        $progress->as = $relation->evaluatee;
                        $progress->year = $faculty->class->year;
                        $progress->term = $faculty->class->term;
                        $progress->save();
                    }
                }
                $restrict = App\FormRestriction::where('formId', $form->id)->where('type', $faculty->type)->first();
                if(!empty($restrict) && $restrict->selfEvaluation == 1){
                    $progress = new App\FormProgress;
                    $progress->formId = $form->id;
                    $progress->evaluatee = $faculty->userId;
                    $progress->evaluator = $faculty->userId;
                    $progress->as = 'Professor';
                    $progress->year = $faculty->class->year;
                    $progress->term = $faculty->class->term;
                    $progress->save();
                }
            }
        }
    }
}