<?php

use Illuminate\Database\Seeder;

class ClassLoadTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $csvData = file_get_contents(public_path() . '/storage/csv/classload.csv');
        $rows = array_map('str_getcsv', explode("\n", $csvData));

        for ($i = 1; $i < count($rows)-1; $i++){

            $class = new App\ClassLoad;
            $class->facultyNo = $rows[$i][0];
            $class->code = $rows[$i][2];
            $class->subject = $rows[$i][3];
            $class->section = $rows[$i][4];
            $class->schedule = $rows[$i][5];
            $class->size = $rows[$i][7];
            $class->room = $rows[$i][6];
            $class->year = '2018-2019';
            $class->term = '2nd';
            $class->save();

            $validate = App\User::where('username', $rows[$i][0])->first();

            if(empty($validate)){

                $split = explode(',', $rows[$i][1]);
                $user = new App\User;
                $user->name = $split[1]. ' ' . $split[0];
                $user->username = $rows[$i][0];
                $user->activated = 0;
                $user->password = Hash::make($rows[$i][0]);
                $user->save();

            }

            $validate = App\Faculty::where('facultyNo', $rows[$i][0])->first();

            if(empty($validate)){

                $user = App\User::where('username', $rows[$i][0])->first();

                $faculty = new App\Faculty;
                $faculty->userId = $user->id;
                $faculty->facultyNo = $rows[$i][0];
                $faculty->department = $rows[$i][8];
                $faculty->save();

            }

        }

        $type = App\Faculty::where('facultyNo', '2012102')->first();
        if(!empty($type)){
            $type->type = 'Department Chair';
            $type->save();
        }

        $type = App\Faculty::where('facultyNo', '2011116')->first();
        if(!empty($type)){
            $type->type = 'Vice Dean';
            $type->save();
        }

        $type = App\Faculty::where('facultyNo', '20152160')->first();
        if(!empty($type)){
            $type->type = 'Dean';
            $type->save();
        }

    }
}
