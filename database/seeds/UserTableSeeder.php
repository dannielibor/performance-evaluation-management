<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $new = new App\User;
        $new->username = "s-admin";
        $new->name = "s-admin";
        $new->email = "superadmin@gmail.com";
        $new->password = Hash::make("admin");
        $new->activated = 1;
        $new->isSuperAdmin = 1;
        $new->twoFA = 0;
        $new->save();

        $new = new App\User;
        $new->username = "admin";
        $new->name = "admin";
        $new->email = "admin@gmail.com";
        $new->password = Hash::make("admin");
        $new->activated = 1;
        $new->isAdmin = 1;
        $new->twoFA = 0;
        $new->save();
    }
}
