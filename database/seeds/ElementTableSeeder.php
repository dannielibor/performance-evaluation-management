<?php

use Illuminate\Database\Seeder;

class ElementTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $new = new App\Element;
        $new->type = 'Professor';
        $new->formId = 3;
        $new->rate = 35;
        $new->save();

        $new = new App\Element;
        $new->type = 'Professor';
        $new->formId = 2;
        $new->rate = 40;
        $new->save();

        $new = new App\Element;
        $new->type = 'Professor';
        $new->formId = 1;
        $new->rate = 20;
        $new->save();
        

        $new = new App\Element;
        $new->type = 'Professor';
        $new->formId = 2;
        $new->rate = 5;
        $new->selfEvaluation = 1;
        $new->save();

        /////

        $new = new App\Element;
        $new->type = 'Department Chair';
        $new->formId = 3;
        $new->rate = 35;
        $new->save();

        $new = new App\Element;
        $new->type = 'Department Chair';
        $new->formId = 2;
        $new->rate = 40;
        $new->save();

        $new = new App\Element;
        $new->type = 'Department Chair';
        $new->formId = 1;
        $new->rate = 20;
        $new->save();

        $new = new App\Element;
        $new->type = 'Department Chair';
        $new->formId = 2;
        $new->rate = 5;
        $new->selfEvaluation = 1;
        $new->save();

    }
}