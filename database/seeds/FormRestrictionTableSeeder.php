<?php

use Illuminate\Database\Seeder;

class FormRestrictionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $new = new App\FormRestriction;
        $new->formId = 2;
        $new->type = 'Professor';
        $new->selfEvaluation = 1;
        $new->save();

        $new = new App\FormRestriction;
        $new->formId = 2;
        $new->type = 'Department Chair';
        $new->selfEvaluation = 1;
        $new->save();

        $new = new App\FormRestriction;
        $new->formId = 2;
        $new->type = 'Vice Dean';
        $new->selfEvaluation = 1;
        $new->save();

        $new = new App\FormRestriction;
        $new->formId = 2;
        $new->type = 'Dean';
        $new->selfEvaluation = 1;
        $new->save();
    }
}
