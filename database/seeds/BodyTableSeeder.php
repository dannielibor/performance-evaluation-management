<?php

use Illuminate\Database\Seeder;

class BodyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /* student evaluation */
        $new = new App\Body;
        $new->formId = 1;
        $new->sort = 'A';
        $new->header = 'METHODS AND STRATEGIES OF TEACHING';
        $new->position = '0';
        $new->rate = 40;
        $new->save();

        $new = new App\Body;
        $new->formId = 1;
        $new->sort = 'B';
        $new->header = 'MASTERY OF THE SUBJECT MATTER';
        $new->position = '1';
        $new->rate = 20;
        $new->save();

        $new = new App\Body;
        $new->formId = 1;
        $new->sort = 'C';
        $new->header = 'COMMUNICATION SKILLS';
        $new->position = '2';
        $new->rate = 15;
        $new->save();

        $new = new App\Body;
        $new->formId = 1;
        $new->sort = 'D';
        $new->header = 'CLASSROOM MANAGEMENT';
        $new->position = '3';
        $new->rate = 15;
        $new->save();

        $new = new App\Body;
        $new->formId = 1;
        $new->sort = 'E';
        $new->header = 'PERSONAL AND SOCIAL TRAITS';
        $new->position = '4';
        $new->rate = 10;
        $new->save();

        /* appraisal */

        $new = new App\Body;
        $new->formId = 2;
        $new->sort = 'A';
        $new->header = 'PROFESSIONAL RESPONSIBILITIES';
        $new->position = '0';
        $new->rate = 70;
        $new->save();

        $new = new App\Body;
        $new->formId = 2;
        $new->sort = 'B';
        $new->header = 'PROFESSIONAL RELATIONSHIPS';
        $new->position = '1';
        $new->rate = 20;
        $new->save();

        $new = new App\Body;
        $new->formId = 2;
        $new->sort = 'C';
        $new->header = 'PERSONAL QUALITIES';
        $new->position = '2';
        $new->rate = 10;
        $new->save();

        /* classroom observation */

        $new = new App\Body;
        $new->formId = 3;
        $new->sort = 'A';
        $new->header = 'Teacher';
        $new->position = '0';
        $new->save();

        $new = new App\Body;
        $new->formId = 3;
        $new->sort = 'B';
        $new->header = 'Teaching Procedure';
        $new->position = '1';
        $new->save();

        $new = new App\Body;
        $new->formId = 3;
        $new->sort = 'C';
        $new->header = 'Students';
        $new->position = '2';
        $new->save();

        $new = new App\Body;
        $new->formId = 3;
        $new->sort = 'D';
        $new->header = 'General Observation';
        $new->position = '3';
        $new->save();
    }
}
