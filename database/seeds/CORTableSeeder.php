<?php

use Illuminate\Database\Seeder;

class CORTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $csvData = file_get_contents(public_path() . '/storage/csv/cor.csv');
        $rows = array_map('str_getcsv', explode("\n", $csvData));

        for ($i = 1; $i < count($rows)-1; $i++){

            $cor = new App\COR;
            $cor->studentNo = $rows[$i][0];
            $cor->code = $rows[$i][5];
            $cor->subject = $rows[$i][6];
            $cor->section = $rows[$i][7];
            $cor->schedule = $rows[$i][8];
            $cor->room = $rows[$i][9];
            $cor->year = '2018-2019';
            $cor->term = '2nd';
            $cor->save();

            $validate = App\User::where('username', $rows[$i][0])->first();

            if(empty($validate)){

                $split = explode(',', $rows[$i][1]);
                $user = new App\User;
                $user->name = $split[1]. ' ' . $split[0];
                $user->username = $rows[$i][0];
                $user->password = Hash::make($rows[$i][0]);
                $user->save();

            }

            $validate = App\Student::where('studentNo', $rows[$i][0])->first();

            if(empty($validate)){

                $user = App\User::where('username', $rows[$i][0])->first();

                $student = new App\Student;
                $student->userId = $user->id;
                $student->studentNo = $rows[$i][0];
                $student->gender = $rows[$i][2];
                $student->program = $rows[$i][3];
                $student->yearLvl = $rows[$i][4];
                $student->save();

            }

        }
    }
}
