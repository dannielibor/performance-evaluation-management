<?php

use Illuminate\Database\Seeder;

class FeedbackResultTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $new = new App\FeedbackResult;
        $new->formId = 1;
        $new->feedbackId = 1;
        $new->evaluator = '214';
        $new->evaluatee = 318;
        $new->as = 'Professor';
        $new->result = 'good presentations';
        $new->year = '2018-2019';
        $new->term = '2nd';
        $new->save();

        $new = new App\FeedbackResult;
        $new->formId = 1;
        $new->feedbackId = 1;
        $new->evaluator = '89';
        $new->evaluatee = 318;
        $new->as = 'Professor';
        $new->result = 'motivational advices';
        $new->year = '2018-2019';
        $new->term = '2nd';
        $new->save();

        $new = new App\FeedbackResult;
        $new->formId = 1;
        $new->feedbackId = 1;
        $new->evaluator = '287';
        $new->evaluatee = 318;
        $new->as = 'Professor';
        $new->result = 'cute professor';
        $new->year = '2018-2019';
        $new->term = '2nd';
        $new->save();

        $new = new App\FeedbackResult;
        $new->formId = 3;
        $new->feedbackId = 2;
        $new->evaluator = '343';
        $new->evaluatee = 318;
        $new->as = 'Professor';
        $new->result = 'loud voice';
        $new->year = '2018-2019';
        $new->term = '2nd';
        $new->save();

        $new = new App\FeedbackResult;
        $new->formId = 3;
        $new->feedbackId = 2;
        $new->evaluator = '342';
        $new->evaluatee = 318;
        $new->as = 'Professor';
        $new->result = 'influencer';
        $new->year = '2018-2019';
        $new->term = '2nd';
        $new->save();

        $new = new App\FeedbackResult;
        $new->formId = 3;
        $new->feedbackId = 2;
        $new->evaluator = '341';
        $new->evaluatee = 318;
        $new->as = 'Professor';
        $new->result = 'smart';
        $new->year = '2018-2019';
        $new->term = '2nd';
        $new->save();

        $new = new App\FeedbackResult;
        $new->formId = 3;
        $new->feedbackId = 3;
        $new->evaluator = '343';
        $new->evaluatee = 318;
        $new->as = 'Professor';
        $new->result = 'more visuals';
        $new->year = '2018-2019';
        $new->term = '2nd';
        $new->save();

        $new = new App\FeedbackResult;
        $new->formId = 3;
        $new->feedbackId = 3;
        $new->evaluator = '342';
        $new->evaluatee = 318;
        $new->as = 'Professor';
        $new->result = 'more homework';
        $new->year = '2018-2019';
        $new->term = '2nd';
        $new->save();

        $new = new App\FeedbackResult;
        $new->formId = 3;
        $new->feedbackId = 3;
        $new->evaluator = '341';
        $new->evaluatee = 318;
        $new->as = 'Professor';
        $new->result = 'study more';
        $new->year = '2018-2019';
        $new->term = '2nd';
        $new->save();
    }
}
