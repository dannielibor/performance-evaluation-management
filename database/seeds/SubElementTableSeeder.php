<?php

use Illuminate\Database\Seeder;

class SubElementTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        $new = new App\SubElement;
        $new->elementId = 1;
        $new->type = 'Dean/VDAA';
        $new->rate = 5;
        $new->save();

        $new = new App\SubElement;
        $new->elementId = 1;
        $new->type = 'Department Chair';
        $new->rate = 30;
        $new->save();

        $new = new App\SubElement;
        $new->elementId = 2;
        $new->type = 'Dean/VDAA';
        $new->rate = 20;
        $new->save();

        $new = new App\SubElement;
        $new->elementId = 2;
        $new->type = 'Department Chair';
        $new->rate = 20;
        $new->save();

        /////

        $new = new App\SubElement;
        $new->elementId = 5;
        $new->type = 'Dean/VDAA';
        $new->rate = 35;
        $new->save();

        $new = new App\SubElement;
        $new->elementId = 6;
        $new->type = 'Dean/VDAA';
        $new->rate = 40;
        $new->save();

    }
}
