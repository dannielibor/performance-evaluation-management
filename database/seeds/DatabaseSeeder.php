<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            UserTableSeeder::class,
            CORTableSeeder::class,
            ClassLoadTableSeeder::class,
            FormTableSeeder::class,
            BodyTableSeeder::class,
            ContentTableSeeder::class,
            FormRelationshipTableSeeder::class,
            FormRestrictionTableSeeder::class,
            FormProgressTableSeeder::class,
            // EntryTableSeeder::class,
            StudentEntry::class,
            FeedbackTableSeeder::class,
            FeedbackResultTableSeeder::class,
            ElementTableSeeder::class,
            SubElementTableSeeder::class,
            TutorialTableSeeder::class
        ]);
    }
}