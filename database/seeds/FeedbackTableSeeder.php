<?php

use Illuminate\Database\Seeder;

class FeedbackTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $new = new App\Feedback;
        $new->formId = 1;
        $new->header = 'Comments/Suggestions';
        $new->save();

        $new = new App\Feedback;
        $new->formId = 3;
        $new->header = 'Strengths';
        $new->save();

        $new = new App\Feedback;
        $new->formId = 3;
        $new->header = 'Recommendations';
        $new->save();
    }
}
