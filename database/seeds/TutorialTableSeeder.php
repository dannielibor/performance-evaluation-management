<?php

use Illuminate\Database\Seeder;

class TutorialTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $new = new App\Tutorial;
        $new->title = "How to manage students";
        $new->link = 'https://www.useloom.com/embed/ffe85afaa7b44669a60183fb4c2a56b8';
        $new->save();

        $new = new App\Tutorial;
        $new->title = "How to manage professor";
        $new->link = 'https://www.useloom.com/embed/5e156efaa91c47e49ee833f193449fe9';
        $new->save();

        $new = new App\Tutorial;
        $new->title = "How to import COR";
        $new->link = 'https://www.useloom.com/embed/6f30dbd8ed49444b91617cd11afd1d95';
        $new->save();

        $new = new App\Tutorial;
        $new->title = "How to import Class Load";
        $new->link = 'https://www.useloom.com/embed/530d2025db224168891786fd49dc9076';
        $new->save();

        $new = new App\Tutorial;
        $new->title = "How to publish a new form";
        $new->link = 'https://www.useloom.com/embed/6adf64c243c049d2ba903d9b1658d59f';
        $new->save();

        $new = new App\Tutorial;
        $new->title = "How to edit a summative report";
        $new->link = 'https://www.useloom.com/embed/73e2793adf5846cf9fb656a4f0e4bbb5';
        $new->save();

        $new = new App\Tutorial;
        $new->title = "How to print a summative report";
        $new->link = 'https://www.useloom.com/embed/3f32319cbb334c13881bf3bd81732b01';
        $new->save();
    }
}
