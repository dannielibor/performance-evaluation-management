<?php

use Illuminate\Database\Seeder;

class EntryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $faculties = App\Faculty::where('department', 'Information Technology')->get();

        foreach($faculties as $faculty){

            $load = App\ClassLoad::where('facultyNo', $faculty->facultyNo)->get();

            foreach($load as $class){

                $cor = App\COR::where('code', $class->code)
                                ->where('subject', $class->subject)
                                ->where('section', $class->section)
                                ->where('schedule', $class->schedule)
                                ->where('room', $class->room)
                                ->where('year', $class->year)
                                ->where('term', $class->term)->take(10)->get();
                
                foreach($cor as $sched){

                    $explode = explode(' ', $sched->subject);
                    if(last($explode) != 'Laboratory'){
                        if($sched->student->yearLvl == '4th Year'){

                            $progress = App\FormProgress::where('formId', 1)->where('evaluator', $sched->student->userId)->where('evaluatee', $faculty->userId)->first();

                            $bodies = App\Body::where('formId', 1)->get();

                            foreach($bodies as $body){
    
                                $contents = App\Content::where('bodyId', $body->id)->get();
    
                                foreach($contents as $content){
    
                                    $entry = new App\Entry;
                                    $entry->formProgressId = $progress->id;
                                    $entry->contentId = $content->id;
                                    $entry->result = rand(3,5);
                                    $entry->save();
    
                                }
    
                            }

                        }
                    }

                }

            }

        }
    }
}
