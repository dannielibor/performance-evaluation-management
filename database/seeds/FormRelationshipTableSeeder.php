<?php

use Illuminate\Database\Seeder;

class FormRelationshipTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $new = new App\FormRelationship;
        $new->formId = 1;
        $new->evaluatee = 'Professor';
        $new->evaluator = 'Student';
        $new->save();

        $new = new App\FormRelationship;
        $new->formId = 2;
        $new->evaluatee = 'Professor';
        $new->evaluator = 'Department Chair';
        $new->save();

        $new = new App\FormRelationship;
        $new->formId = 2;
        $new->evaluatee = 'Department Chair';
        $new->evaluator = 'Vice Dean';
        $new->status = 'Indistinct';
        $new->save();

        $new = new App\FormRelationship;
        $new->formId = 2;
        $new->evaluatee = 'Professor';
        $new->evaluator = 'Vice Dean';
        $new->status = 'Indistinct';
        $new->save();

        $new = new App\FormRelationship;
        $new->formId = 2;
        $new->evaluatee = 'Professor';
        $new->evaluator = 'Dean';
        $new->status = 'Indistinct';
        $new->save();

        $new = new App\FormRelationship;
        $new->formId = 2;
        $new->evaluatee = 'Department Chair';
        $new->evaluator = 'Dean';
        $new->status = 'Indistinct';
        $new->save();

        $new = new App\FormRelationship;
        $new->formId = 2;
        $new->evaluatee = 'Vice Dean';
        $new->evaluator = 'Dean';
        $new->status = 'Indistinct';
        $new->save();

        $new = new App\FormRelationship;
        $new->formId = 3;
        $new->evaluatee = 'Professor';
        $new->evaluator = 'Department Chair';
        $new->save();

        $new = new App\FormRelationship;
        $new->formId = 3;
        $new->evaluatee = 'Professor';
        $new->evaluator = 'Vice Dean';
        $new->status = 'Indistinct';
        $new->save();

        $new = new App\FormRelationship;
        $new->formId = 3;
        $new->evaluatee = 'Department Chair';
        $new->evaluator = 'Vice Dean';
        $new->status = 'Indistinct';
        $new->save();

        $new = new App\FormRelationship;
        $new->formId = 3;
        $new->evaluatee = 'Department Chair';
        $new->evaluator = 'Dean';
        $new->status = 'Indistinct';
        $new->save();

        $new = new App\FormRelationship;
        $new->formId = 3;
        $new->evaluatee = 'Professor';
        $new->evaluator = 'Dean';
        $new->status = 'Indistinct';
        $new->save();
    }
}
