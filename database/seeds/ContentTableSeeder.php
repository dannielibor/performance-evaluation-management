<?php

use Illuminate\Database\Seeder;

class ContentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
                /* student evaluation */
                $new = new App\Content;
                $new->bodyId = 1;
                $new->type = 'question';
                $new->position = 0;
                $new->content = '1. Starts the lesson in an interesting manner.';
                $new->save();
        
                $new = new App\Content;
                $new->bodyId = 1;
                $new->type = 'question';
                $new->position = 1;
                $new->content = '2. Clarifies the objective/s of the lesson.';
                $new->save();
        
                $new = new App\Content;
                $new->bodyId = 1;
                $new->type = 'question';
                $new->position = 2;
                $new->content = '3. Makes the lesson practical and relevant by relating it with everyday living.';
                $new->save();
        
                $new = new App\Content;
                $new->bodyId = 1;
                $new->type = 'question';
                $new->position = 3;
                $new->content = '4. Relates the lesson with current issues and other subject areas whenever possible.';
                $new->save();
        
                $new = new App\Content;
                $new->bodyId = 1;
                $new->type = 'question';
                $new->position = 4;
                $new->content = "5. Facilitates student's understanding by rephrasing questions using synonyms, giving examples and the like.";
                $new->save();
        
                $new = new App\Content;
                $new->bodyId = 1;
                $new->type = 'question';
                $new->position = 5;
                $new->content = "6. Formulates questions that challenge the student's imagination or reasoning ability.";
                $new->save();
        
                $new = new App\Content;
                $new->bodyId = 1;
                $new->type = 'question';
                $new->position = 6;
                $new->content = "7. Gives student's learning tasks; exercises related to the topics learned/being learned.";
                $new->save();
        
                $new = new App\Content;
                $new->bodyId = 1;
                $new->type = 'question';
                $new->position = 7;
                $new->content = "8. Uses instructional time productively by properly apportioning it to all class activities.";
                $new->save();
        
                $new = new App\Content;
                $new->bodyId = 1;
                $new->type = 'question';
                $new->position = 8;
                $new->content = "9. Encourages maximum participation of the students in all activities.";
                $new->save();
        
                $new = new App\Content;
                $new->bodyId = 1;
                $new->type = 'question';
                $new->position = 9;
                $new->content = "10. Makes use of the whiteboard and other audiovisual materials like pictures, graphs, and outlines to make the lesson more interesting.";
                $new->save();
        
                $new = new App\Content;
                $new->bodyId = 1;
                $new->type = 'question';
                $new->position = 10;
                $new->content = "11. Provides students with more opportunities for learning by giving assignments, research work, and the like which could be accomplished.";
                $new->save();
        
                $new = new App\Content;
                $new->bodyId = 1;
                $new->type = 'question';
                $new->position = 11;
                $new->content = "12. Encourages students to ask intelligent and relevant questions.";
                $new->save();
        
                $new = new App\Content;
                $new->bodyId = 1;
                $new->type = 'question';
                $new->position = 12;
                $new->content = "13. Establishes continuity and unity of present and previous lessons. Summarizes the lesson to bring out important points.";
                $new->save();
        
                $new = new App\Content;
                $new->bodyId = 1;
                $new->type = 'question';
                $new->position = 13;
                $new->content = "14. Evaluates students performance by giving quizzes from time to time.";
                $new->save();
        
                $new = new App\Content;
                $new->bodyId = 1;
                $new->type = 'question';
                $new->position = 14;
                $new->content = "15. Informs students of test results not later than two weeks after the exam.";
                $new->save();
        
                $new = new App\Content;
                $new->bodyId = 1;
                $new->type = 'question';
                $new->position = 15;
                $new->content = "16. Explains the grading system and gives grades fairly.";
                $new->save();
        
                $new = new App\Content;
                $new->bodyId = 2;
                $new->type = 'question';
                $new->position = 0;
                $new->content = "1. Presents the lessons systematically.";
                $new->save();
        
                $new = new App\Content;
                $new->bodyId = 2;
                $new->type = 'question';
                $new->position = 1;
                $new->content = "2. Explains the lesson/objectives clearly.";
                $new->save();
        
                $new = new App\Content;
                $new->bodyId = 2;
                $new->type = 'question';
                $new->position = 2;
                $new->content = "3. Answers question of the students intelligently and satisfactorily.";
                $new->save();
        
                $new = new App\Content;
                $new->bodyId = 3;
                $new->type = 'question';
                $new->position = 0;
                $new->content = "1. Speaks correct English and/or Filipino and communicates his/her ideas well.";
                $new->save();
        
                $new = new App\Content;
                $new->bodyId = 3;
                $new->type = 'question';
                $new->position = 1;
                $new->content = "2. Has a loud voice enough to be heard by everyone in class.";
                $new->save();
        
                $new = new App\Content;
                $new->bodyId = 4;
                $new->type = 'question';
                $new->position = 0;
                $new->content = "1. Sees to it that the room is clean and orderly at all times.";
                $new->save();
        
                $new = new App\Content;
                $new->bodyId = 4;
                $new->type = 'question';
                $new->position = 1;
                $new->content = "2. Maintains classroom discipline.";
                $new->save();
        
                $new = new App\Content;
                $new->bodyId = 4;
                $new->type = 'question';
                $new->position = 2;
                $new->content = "3. Checks the attendance of students.";
                $new->save();
        
                $new = new App\Content;
                $new->bodyId = 4;
                $new->type = 'question';
                $new->position = 3;
                $new->content = "4. Sees to it that the students wear the prescribed uniform.";
                $new->save();
        
                $new = new App\Content;
                $new->bodyId = 4;
                $new->type = 'question';
                $new->position = 4;
                $new->content = "5. Sees to it that all teaching materials like whiteboard marker and erasers are prepared.";
                $new->save();
        
                $new = new App\Content;
                $new->bodyId = 4;
                $new->type = 'question';
                $new->position = 5;
                $new->content = "6. Turns off airconditioning units, all lights, and make sure that the whiteboard is clean before dismissing the class.";
                $new->save();
        
                $new = new App\Content;
                $new->bodyId = 5;
                $new->type = 'question';
                $new->position = 0;
                $new->content = "1. Projects self-confidence; respects the opinions, suggestions, and comments of students.";
                $new->save();
        
                $new = new App\Content;
                $new->bodyId = 5;
                $new->type = 'question';
                $new->position = 1;
                $new->content = "2. Projects a respectable and a decent image.";
                $new->save();
        
                $new = new App\Content;
                $new->bodyId = 5;
                $new->type = 'question';
                $new->position = 2;
                $new->content = "3. Practices what he/she preaches/advises.";
                $new->save();
        
                $new = new App\Content;
                $new->bodyId = 5;
                $new->type = 'question';
                $new->position = 3;
                $new->content = "4. Manifests and promotes the Benedictine charism of work and prayer.";
                $new->save();
        
                $new = new App\Content;
                $new->bodyId = 5;
                $new->type = 'question';
                $new->position = 4;
                $new->content = "5. Integrates positive values in the lesson.";
                $new->save();
        
                $new = new App\Content;
                $new->bodyId = 5;
                $new->type = 'question';
                $new->position = 5;
                $new->content = "6. Recognizes students' participation in institutional activities by giving proper considerations.";
                $new->save();
        
                $new = new App\Content;
                $new->bodyId = 5;
                $new->type = 'question';
                $new->position = 6;
                $new->content = "7. Comes to class on regularly.";
                $new->save();
        
                $new = new App\Content;
                $new->bodyId = 5;
                $new->type = 'question';
                $new->position = 7;
                $new->content = "8. Comes to class on time.";
                $new->save();
        
                $new = new App\Content;
                $new->bodyId = 5;
                $new->type = 'question';
                $new->position = 8;
                $new->content = "9. Is approachable and encourages students to consult him/her for assistance.";
                $new->save();
        
                /* appraisal */
        
                $new = new App\Content;
                $new->bodyId = 6;
                $new->type = 'header';
                $new->position = 0;
                $new->sort = 1;
                $new->content = "Engages in professional growth activities";
                $new->save();
        
                $new = new App\Content;
                $new->bodyId = 6;
                $new->type = 'question';
                $new->position = 1;
                $new->content = "Attends SBC/CAS seminars, conferences, workshops etc.";
                $new->save();
        
                $new = new App\Content;
                $new->bodyId = 6;
                $new->type = 'question';
                $new->position = 2;
                $new->content = "Attends off-campus seminars, conferences, workshops etc.";
                $new->save();
        
                $new = new App\Content;
                $new->bodyId = 6;
                $new->type = 'question';
                $new->position = 3;
                $new->content = "Engages in scholarly research activities.";
                $new->save();
        
                $new = new App\Content;
                $new->bodyId = 6;
                $new->type = 'header';
                $new->position = 4;
                $new->sort = 2;
                $new->content = "Demonstrates dependability in professional duties";
                $new->save();
        
                $new = new App\Content;
                $new->bodyId = 6;
                $new->type = 'question';
                $new->position = 5;
                $new->content = "Submits course syllabus that conforms with the CAS curriculum(i.e. form, content)";
                $new->save();
        
                $new = new App\Content;
                $new->bodyId = 6;
                $new->type = 'question';
                $new->position = 6;
                $new->content = "Submits a well-constructed examination";
                $new->save();
        
                $new = new App\Content;
                $new->bodyId = 6;
                $new->type = 'question';
                $new->position = 7;
                $new->content = "Submits accurate grades that conforms with CAS standards.";
                $new->save();
        
                $new = new App\Content;
                $new->bodyId = 6;
                $new->type = 'question';
                $new->position = 8;
                $new->content = "Submits other requirements prescribed by the department.";
                $new->save();
        
                $new = new App\Content;
                $new->bodyId = 6;
                $new->type = 'header';
                $new->position = 9;
                $new->sort = 3;
                $new->content = "Works cooperatively in bringing about the success of the school program";
                $new->save();
        
                $new = new App\Content;
                $new->bodyId = 6;
                $new->type = 'question';
                $new->position = 10;
                $new->content = "Cooperates to bring success of the school program(i.e. spiritual, social, outreach, academic)";
                $new->save();
        
                $new = new App\Content;
                $new->bodyId = 6;
                $new->type = 'question';
                $new->position = 11;
                $new->content = "Takes care and makes optimum use of the physical and materials resources that supports Instructional program";
                $new->save();
        
                $new = new App\Content;
                $new->bodyId = 6;
                $new->type = 'header';
                $new->position = 12;
                $new->sort = 4;
                $new->content = "Promptness in meeting obligations";
                $new->save();
        
                $new = new App\Content;
                $new->bodyId = 6;
                $new->type = 'question';
                $new->position = 13;
                $new->content = "Has a good and reasonable attendance record";
                $new->save();
        
                $new = new App\Content;
                $new->bodyId = 6;
                $new->type = 'question';
                $new->position = 14;
                $new->content = "Reports and leaves classes on time";
                $new->save();
        
                $new = new App\Content;
                $new->bodyId = 6;
                $new->type = 'question';
                $new->position = 15;
                $new->content = "Reports to classes regularly";
                $new->save();
        
                $new = new App\Content;
                $new->bodyId = 6;
                $new->type = 'question';
                $new->position = 16;
                $new->content = "Attends meetings convocations and other official school invitations regularly";
                $new->save();
        
                $new = new App\Content;
                $new->bodyId = 7;
                $new->type = 'header';
                $new->position = 0;
                $new->sort = 1;
                $new->content = "Maintains an effective working relationships with staff";
                $new->save();
        
                $new = new App\Content;
                $new->bodyId = 7;
                $new->type = 'question';
                $new->position = 1;
                $new->content = "Respects needs and feelings of his/her colleagues";
                $new->save();
        
                $new = new App\Content;
                $new->bodyId = 7;
                $new->type = 'question';
                $new->position = 2;
                $new->content = "Maintains a positive relationships with all school personnel";
                $new->save();
        
                $new = new App\Content;
                $new->bodyId = 7;
                $new->type = 'header';
                $new->position = 3;
                $new->sort = 2;
                $new->content = "Maintains a relationships with students that is conductive to learning";
                $new->save();
        
                $new = new App\Content;
                $new->bodyId = 7;
                $new->type = 'question';
                $new->position = 4;
                $new->content = "Maintains a supportive, positive and professional relationships with students";
                $new->save();
        
                $new = new App\Content;
                $new->bodyId = 7;
                $new->type = 'question';
                $new->position = 5;
                $new->content = "Exemplifies academic, moral and ethical norms in his/her personal and professional life";
                $new->save();
        
                $new = new App\Content;
                $new->bodyId = 8;
                $new->type = 'header';
                $new->position = 0;
                $new->sort = 1;
                $new->content = "Health and Vigor";
                $new->save();
        
                $new = new App\Content;
                $new->bodyId = 8;
                $new->type = 'question';
                $new->position = 1;
                $new->content = "Displays pleasant personality";
                $new->save();
        
                $new = new App\Content;
                $new->bodyId = 8;
                $new->type = 'question';
                $new->position = 2;
                $new->content = "Displays a sense of humor";
                $new->save();
        
                $new = new App\Content;
                $new->bodyId = 8;
                $new->type = 'question';
                $new->position = 3;
                $new->content = "Open to suggestions, new ideas and accepts constructive criticisms";
                $new->save();
        
                $new = new App\Content;
                $new->bodyId = 8;
                $new->type = 'header';
                $new->position = 9;
                $new->sort = 2;
                $new->content = "Competance and Initiative";
                $new->save();
        
                $new = new App\Content;
                $new->bodyId = 8;
                $new->type = 'question';
                $new->position = 10;
                $new->content = "Exercise tact and objectivity in articulating own ideas and concerns";
                $new->save();
        
                $new = new App\Content;
                $new->bodyId = 8;
                $new->type = 'question';
                $new->position = 11;
                $new->content = "Contributes ideas, inputs for the good of the department and the school";
                $new->save();
        
                $new = new App\Content;
                $new->bodyId = 8;
                $new->type = 'header';
                $new->position = 12;
                $new->sort = 3;
                $new->content = "Grooming and appropriateness of Attire";
                $new->save();
        
                $new = new App\Content;
                $new->bodyId = 8;
                $new->type = 'question';
                $new->position = 13;
                $new->content = "Practice habits of good grooming";
                $new->save();
        
                $new = new App\Content;
                $new->bodyId = 8;
                $new->type = 'header';
                $new->position = 14;
                $new->sort = 4;
                $new->content = "Work attitudes";
                $new->save();
        
                $new = new App\Content;
                $new->bodyId = 8;
                $new->type = 'question';
                $new->position = 15;
                $new->content = "Is enthusiastic and shows initiative beyond the call of duty";
                $new->save();
        
                $new = new App\Content;
                $new->bodyId = 8;
                $new->type = 'question';
                $new->position = 16;
                $new->content = "Accepts leadership and fellowships roles";
                $new->save();
        
                $new = new App\Content;
                $new->bodyId = 8;
                $new->type = 'question';
                $new->position = 17;
                $new->content = "Upholds the good name and integrity of the school in and out of the school premises";
                $new->save();
        
                /* classroom observation */
                $new = new App\Content;
                $new->bodyId = 9;
                $new->type = 'question';
                $new->position = 0;
                $new->content = "1. Teaching personality";
                $new->save();
        
                $new = new App\Content;
                $new->bodyId = 9;
                $new->type = 'question';
                $new->position = 1;
                $new->content = "2. Composure";
                $new->save();
        
                $new = new App\Content;
                $new->bodyId = 9;
                $new->type = 'question';
                $new->position = 2;
                $new->content = "3. Articulation";
                $new->save();
        
                $new = new App\Content;
                $new->bodyId = 9;
                $new->type = 'question';
                $new->position = 3;
                $new->content = "4. Modulation of voice";
                $new->save();
        
                $new = new App\Content;
                $new->bodyId = 9;
                $new->type = 'question';
                $new->position = 4;
                $new->content = "5. Mastery of the medium of instruction";
                $new->save();
        
                $new = new App\Content;
                $new->bodyId = 9;
                $new->type = 'question';
                $new->position = 5;
                $new->content = "6. Mastery of the subject matter";
                $new->save();
        
                $new = new App\Content;
                $new->bodyId = 9;
                $new->type = 'question';
                $new->position = 6;
                $new->content = "7. Ability to answer questions";
                $new->save();
        
                $new = new App\Content;
                $new->bodyId = 9;
                $new->type = 'question';
                $new->position = 7;
                $new->content = "8. Openness to student's opinions";
                $new->save();
        
                $new = new App\Content;
                $new->bodyId = 10;
                $new->type = 'question';
                $new->position = 0;
                $new->content = "1. Organization of subject matter";
                $new->save();
        
                $new = new App\Content;
                $new->bodyId = 10;
                $new->type = 'question';
                $new->position = 1;
                $new->content = "2. Ability to relate subject to previous topics";
                $new->save();
        
                $new = new App\Content;
                $new->bodyId = 10;
                $new->type = 'question';
                $new->position = 2;
                $new->content = "3. Ability to provoke critical thinking";
                $new->save();
        
                $new = new App\Content;
                $new->bodyId = 10;
                $new->type = 'question';
                $new->position = 3;
                $new->content = "4. Ability to motivate";
                $new->save();
        
                $new = new App\Content;
                $new->bodyId = 10;
                $new->type = 'question';
                $new->position = 4;
                $new->content = "5. Ability to manage class";
                $new->save();
        
                $new = new App\Content;
                $new->bodyId = 10;
                $new->type = 'question';
                $new->position = 5;
                $new->content = "6. Questioning techniques";
                $new->save();
        
                $new = new App\Content;
                $new->bodyId = 10;
                $new->type = 'question';
                $new->position = 6;
                $new->content = "7. Use of teaching aids";
                $new->save();
        
                $new = new App\Content;
                $new->bodyId = 11;
                $new->type = 'question';
                $new->position = 0;
                $new->content = "1. Class attention";
                $new->save();
        
                $new = new App\Content;
                $new->bodyId = 11;
                $new->type = 'question';
                $new->position = 1;
                $new->content = "2. Class participation";
                $new->save();
        
                $new = new App\Content;
                $new->bodyId = 12;
                $new->type = 'question';
                $new->position = 0;
                $new->content = "1. Rapport between teacher and students";
                $new->save();
        
                $new = new App\Content;
                $new->bodyId = 12;
                $new->type = 'question';
                $new->position = 1;
                $new->content = "2. Class atmosphere";
                $new->save();
        
                $new = new App\Content;
                $new->bodyId = 12;
                $new->type = 'question';
                $new->position = 2;
                $new->content = "3. Overall teacher impact";
                $new->save();
        
                $new = new App\Content;
                $new->bodyId = 12;
                $new->type = 'question';
                $new->position = 3;
                $new->content = "4. Overall classroom condition";
                $new->save();
    }
}
