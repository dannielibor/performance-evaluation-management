<?php

use Illuminate\Database\Seeder;

class FormTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $new = new App\Form;
        $new->title = 'Student Evaluation';
        $new->status = 'active';
        $new->from = '2019-03-15';
        $new->to = '2019-012-15';
        $new->pagination = '3';
        $new->save();

        $new = new App\Form;
        $new->title = 'Faculty Performance Appraisal';
        $new->status = 'active';
        $new->from = '2019-03-15';
        $new->to = '2019-012-15';
        $new->pagination = '3';
        $new->save();

        $new = new App\Form;
        $new->title = 'Classroom Observation';
        $new->status = 'active';
        $new->from = '2019-03-15';
        $new->to = '2019-012-15';
        $new->pagination = '3';
        $new->save();
    }
}
