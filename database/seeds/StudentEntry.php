<?php

use Illuminate\Database\Seeder;

class StudentEntry extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $blocks = App\COR::where('section', '!=', 'BSIT 1-A')->where('section', '!=', 'BSIT 1-B')->where('section', '!=', 'BSIT 3-A')->where('section', '!=', 'BSIT 3-B')->distinct()->get(['section']);
        foreach($blocks as $block){
            $csvData = file_get_contents(public_path() . '/storage/csv/'.$block->section.'.csv');
            $rows = array_map('str_getcsv', explode("\n", $csvData));
            for ($i = 1; $i < count($rows)-1; $i++){

                $progress = App\FormProgress::where('formId', 1)->where('evaluatee', $rows[$i][4])->where('evaluator', $rows[$i][5])->first();

                if(!empty($progress)){

                    $entry = new App\Entry;
                    $entry->formProgressId = $progress->id;
                    $entry->contentId = $rows[$i][6];
                    $entry->result = $rows[$i][2];
                    $entry->save();
    
                    $status = App\Student::where('userId', $rows[$i][5])->first();
                    $status->status = 'completed';
                    $status->save();
    
                    $user = App\User::find($rows[$i][5]);
                    $user->email = $user->username . '@gmail.com';
                    $user->activated = 1;
                    $user->save();
                    
                    $countContent = 0;
                    $entries = 0;
                    $bodies = App\Body::where('formId', 1)->get();
                    foreach($bodies as $body){
                        $contents = App\Content::where('bodyId', $body->id)->where('type', 'question')->get();
                        $countContent = $countContent + count($contents);
                        foreach($contents as $content){
                            $entry = App\Entry::where('contentId', $content->id)
                                            ->where('formProgressId', $progress->id)->first();
                            if(!empty($entry)){
                                $entries = ++$entries;
                            }
                        }
                    }
            
                    if(($countContent != 0 && $entries != 0) && ($countContent == $entries)){
                        $progress->status = 'completed';
                        $allBody = [];
                        $bodies = App\Body::where('formId', 1)->get();
                        foreach($bodies as $body){
                            $sum = 0;
                            $contents = App\Content::where('bodyId', $body->id)->where('type', 'question')->get();
                            foreach($contents as $content){
                                $entry = App\Entry::where('contentId', $content->id)
                                                ->where('formProgressId', $progress->id)->first();
                                $sum = $sum + $entry->result;
                            }
                            array_push($allBody, ( ($sum/count($contents)) * ($body->rate/100) ));
                        }
                        $progress->total = array_sum($allBody);
                        $progress->save();
                    }

                }
            }
        }

        $csvData = file_get_contents(public_path() . '/storage/csv/appraisal.csv');
        $rows = array_map('str_getcsv', explode("\n", $csvData));

        for ($i = 1; $i < count($rows)-1; $i++){

            $evaluator = App\Faculty::where('facultyNo', $rows[$i][3])->first();
            $evaluatee = App\Faculty::where('facultyNo', $rows[$i][4])->first();

            $progress = App\FormProgress::where('formId', $rows[$i][0])
                                    ->where('evaluator', $evaluator->userId)
                                    ->where('evaluatee', $evaluatee->userId)
                                    ->where('as', $rows[$i][6])->first();

            $entry = new App\Entry;
            $entry->formProgressId = $progress->id;
            $entry->contentId = $rows[$i][2];
            $entry->result = $rows[$i][5];
            $entry->save();

            $status = App\Faculty::where('userId', $evaluator->userId)->first();
            $status->status = 'completed';
            $status->save();

            $user = App\User::find($evaluator->userId);
            $user->email = $user->username . '@gmail.com';
            $user->activated = 1;
            $user->save();
        }

        $csvData = file_get_contents(public_path() . '/storage/csv/classobservation.csv');
        $rows = array_map('str_getcsv', explode("\n", $csvData));

        for ($i = 1; $i < count($rows)-1; $i++){

            $evaluator = App\Faculty::where('facultyNo', $rows[$i][3])->first();
            $evaluatee = App\Faculty::where('facultyNo', $rows[$i][4])->first();

            $progress = App\FormProgress::where('formId', $rows[$i][0])
                                    ->where('evaluator', $evaluator->userId)
                                    ->where('evaluatee', $evaluatee->userId)
                                    ->where('as', $rows[$i][6])->first();

            $entry = new App\Entry;
            $entry->formProgressId = $progress->id;
            $entry->contentId = $rows[$i][2];
            $entry->result = $rows[$i][5];
            $entry->save();

            $status = App\Faculty::where('userId', $evaluator->userId)->first();
            $status->status = 'completed';
            $status->save();

            $user = App\User::find($evaluator->userId);
            $user->email = $user->username . '@gmail.com';
            $user->activated = 1;
            $user->save();
        }

        $faculties = App\Faculty::where('department', 'Information Technology')->get();
        foreach($faculties as $faculty){
            $forms = App\Form::where('id', '!=', 1)->get();
            foreach($forms as $form){
                $relations = App\FormRelationship::where('formId', $form->id)->where('evaluatee', $faculty->type)->get();
                foreach($relations as $relation){
                    if($relation->status == 'Distinct'){
                        $evaluators = App\Faculty::where('type', $relation->evaluator)->where('department', $faculty->department)->where('userId', '!=', $faculty->userId)->get();
                    }else{
                        $evaluators = App\Faculty::where('type', $relation->evaluator)->where('userId', '!=', $faculty->userId)->get();
                    }
                    foreach($evaluators as $evaluator){
                        $progress = App\FormProgress::where('formId', $form->id)->where('evaluatee', $faculty->userId)->where('evaluator', $evaluator->userId)->where('as', $relation->evaluatee)->first();
                        $countContent = 0;
                        $entries = 0;
                        $bodies = App\Body::where('formId', $form->id)->get();
                
                        foreach($bodies as $body){
                            $contents = App\Content::where('bodyId', $body->id)->where('type', 'question')->get();
                            $countContent = $countContent + count($contents);
                            foreach($contents as $content){
                                $entry = App\Entry::where('contentId', $content->id)
                                                ->where('formProgressId', $progress->id)->first();
                                if(!empty($entry)){
                                    $entries = ++$entries;
                                }
                            }
                        }

                        if(($countContent != 0 && $entries != 0) && ($countContent == $entries)){
                            $progress->status = 'completed';
                            $allBody = [];
                            $bool = false;
                            $bodies = App\Body::where('formId', $form->id)->get();
                            foreach($bodies as $body){
                                $sum = 0;
                                $contents = App\Content::where('bodyId', $body->id)->where('type', 'question')->get();
                                foreach($contents as $content){
                                    $entry = App\Entry::where('contentId', $content->id)
                                                    ->where('formProgressId', $progress->id)->first();
                                    $sum = $sum + $entry->result;
                                }
                                if($body->rate == 0){
                                    $bool = true;
                                    array_push($allBody, $sum);
                                }else{
                                    array_push($allBody, ( ($sum/count($contents)) * ($body->rate/100) ));
                                }
                            }
                            if($bool){
                                $progress->total = array_sum($allBody) / $countContent;
                                $bool = false;
                            }else{
                                $progress->total = array_sum($allBody);
                            }
                            $progress->save();
                        }
                    }
                }
                $restrict = App\FormRestriction::where('formId', $form->id)->where('type', $faculty->type)->first();
                if(!empty($restrict) && $restrict->selfEvaluation == 1){
                    $progress = App\FormProgress::where('formId', $form->id)->where('evaluatee', $faculty->userId)->where('evaluator', $faculty->userId)->where('as', 'Professor')->first();
                    $countContent = 0;
                    $entries = 0;
                    $bodies = App\Body::where('formId', $form->id)->get();
            
                    foreach($bodies as $body){
                        $contents = App\Content::where('bodyId', $body->id)->where('type', 'question')->get();
                        $countContent = $countContent + count($contents);
                        foreach($contents as $content){
                            $entry = App\Entry::where('contentId', $content->id)
                                            ->where('formProgressId', $progress->id)->first();
                            if(!empty($entry)){
                                $entries = ++$entries;
                            }
                        }
                    }
        
                    if(($countContent != 0 && $entries != 0) && ($countContent == $entries)){
                        $progress->status = 'completed';
                        $allBody = [];
                        $bool = false;
                        $bodies = App\Body::where('formId', $form->id)->get();
                        foreach($bodies as $body){
                            $sum = 0;
                            $contents = App\Content::where('bodyId', $body->id)->where('type', 'question')->get();
                            foreach($contents as $content){
                                $entry = App\Entry::where('contentId', $content->id)
                                                ->where('formProgressId', $progress->id)->first();
                                $sum = $sum + $entry->result;
                            }
                            if($body->rate == 0){
                                $bool = true;
                                array_push($allBody, $sum);
                            }else{
                                array_push($allBody, ( ($sum/count($contents)) * ($body->rate/100) ));
                            }
                        }
                        if($bool){
                            $progress->total = array_sum($allBody) / $countContent;
                            $bool = false;
                        }else{
                            $progress->total = array_sum($allBody);
                        }
                        $progress->save();
                    }
                }
            }
        }

    }
}