<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeedbackResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('feedback_results', function (Blueprint $table) {
            $table->increments('id');
            $table->string('formId');
            $table->string('feedbackId');
            $table->string('evaluator');
            $table->string('evaluatee');
            $table->string('as');
            $table->text('result');
            $table->string('year');
            $table->string('term');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('feedback_results');
    }
}
