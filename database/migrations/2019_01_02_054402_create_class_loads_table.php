<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClassLoadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('class_loads', function (Blueprint $table) {
            $table->increments('id');
            $table->string('facultyNo');
            $table->string('code');
            $table->string('subject');
            $table->string('section');
            $table->string('schedule');
            $table->string('size');
            $table->string('room');
            $table->string('year');
            $table->string('term');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('class_loads');
    }
}