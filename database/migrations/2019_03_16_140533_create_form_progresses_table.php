<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormProgressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('form_progresses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('formId');
            $table->string('evaluator');
            $table->string('evaluatee');
            $table->string('status')->default('pending');
            $table->string('as');
            $table->string('year');
            $table->string('term');
            $table->string('total')->default(0);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('form_progresses');
    }
}
