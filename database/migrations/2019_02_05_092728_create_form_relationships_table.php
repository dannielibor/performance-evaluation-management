<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormRelationshipsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('form_relationships', function (Blueprint $table) {
            $table->increments('id');
            $table->string('formId');
            $table->string('evaluatee')->nullable();
            $table->string('evaluator')->nullable();
            $table->string('status')->default('Distinct');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('form_relationships');
    }
}
