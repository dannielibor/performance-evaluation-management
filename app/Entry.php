<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Entry extends Model
{
    use SoftDeletes;

    public function content(){
        return $this->hasOne('App\Content', 'id', 'contentId');
    }

    public function progress(){
        return $this->hasOne('App\FormProgress', 'id', 'formProgressId');
    }

    public function contentTrashed(){
        return $this->hasOne('App\Content', 'id', 'contentId')->withTrashed();
    }

    public function progressTrashed(){
        return $this->hasOne('App\FormProgress', 'id', 'formProgressId')->withTrashed();
    }
}
