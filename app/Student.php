<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Student extends Model
{
    use SoftDeletes;

    public function user(){
        return $this->hasOne('App\User', 'id', 'userId');
    }

    public function userTrashed(){
        return $this->hasOne('App\User', 'id', 'userId')->withTrashed();
    }

    public function cor(){
        return $this->hasOne('App\COR', 'studentNo', 'studentNo');
    }
}
