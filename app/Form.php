<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Form extends Model
{
    use SoftDeletes;
    protected $table = 'forms';

    public function body(){
        return $this->hasOne('App\Body', 'formId', 'id');
    }

    public function bodyTrashed(){
        return $this->hasOne('App\Body', 'formId', 'id')->withTrashed();
    }
}
    