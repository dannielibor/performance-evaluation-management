<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class cor extends Model
{
    use SoftDeletes;
    
    public function student(){
        return $this->hasOne('App\Student', 'studentNo', 'studentNo');
    }

}
