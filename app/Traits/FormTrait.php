<?php

namespace App\Traits;

use App\Form;
use App\FormRelationship;
use App\Student;
use App\Faculty;
use App\Body;
use App\Content;
use App\Entry;
use App\User;
use App\EmailNotification;
use App\ClassLoad;
use App\COR;
use App\FormProgress;
use Carbon;

trait FormTrait {

    public function schedule() {
        
        $forms = Form::withTrashed()->get();

        foreach($forms as $form){

            if(strtotime($form->from) > strtotime(Carbon\Carbon::now()->format('Y-m-d'))){
                $form->status = 'pending';
            }else if(strtotime($form->from) < strtotime(Carbon\Carbon::now()->format('Y-m-d')) && strtotime($form->to) > strtotime(Carbon\Carbon::now()->format('Y-m-d'))){
                $form->status = 'active';
            }else{
                $form->status = 'inactive';
            }

            $form->save();
            
        }

    }

}