<?php

namespace App\Traits;

use App\TwoAuth;
use App\Form;
use App\User;
use Carbon;

trait AuthTrait {

    public function auth() {
        
        $twoauth = TwoAuth::all();
        foreach($twoauth as $item){
            if($item->created_at < Carbon\Carbon::now()->subMinutes(1)->toDateTimeString()){
                $item->delete();
            }
        }

    }

}