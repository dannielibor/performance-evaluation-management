<?php

namespace App;
 
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use App\Notifications\PasswordResetNotification;
use Illuminate\Database\Eloquent\SoftDeletes;
 
class User extends Model implements AuthenticatableContract, CanResetPasswordContract
{
    use Authenticatable, CanResetPassword, Notifiable, SoftDeletes;
 
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';
 
    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];
 
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];
     
    /**
     * Overriding the exiting sendPasswordResetNotification so that I can customize it
     *
     * @var array
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new PasswordResetNotification($token));
    }
     
    public function student(){
        return $this->hasOne('App\Student', 'userId', 'id');
    }

    public function activate(){
        return $this->hasOne('App\ActivationKey', 'user_id', 'id');
    }

    public function faculty(){
        return $this->hasOne('App\Faculty', 'userId', 'id');
    }

    public function twofa(){
        return $this->hasOne('App\TwoAuth', 'userId', 'id');
    }

    public function studentTrashed(){
        return $this->hasOne('App\Student', 'userId', 'id')->withTrashed();
    }

    public function facultyTrashed(){
        return $this->hasOne('App\Faculty', 'userId', 'id')->withTrashed();
    }
 
}
