<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\EmailNotification;
use Carbon;

class Notify extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:notify';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clear email notifications';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $notify = EmailNotification::all();
        foreach($notify as $item){

            if(strtotime($item->created_at->format('Y-m-d')) > strtotime(Carbon\Carbon::now()->format('Y-m-d'))){
                $item->delete();
            }

        }
    }
}
