<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Notifications\FormNotification;
use App\Form;
use App\FormRelationship;
use App\Faculty;
use App\Student;
use App\COR;
use App\ClassLoad;
use App\FormProgress;
use App\EmailNotification;
use App\User;
use Carbon;

class FormCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:form';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send email to all user';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        ini_set('max_execution_time', 0);

        $id = [];

        $users = User::where('isAdmin', 0)->where('isSuperAdmin', 0)->where('email', '!=', null)->where('activated', 1)->get();
        foreach($users as $user){
            
            $sent = EmailNotification::where('userId', $user->id)->get();
            if(count($sent) == 0){

                $type = Student::where('userId', $user->id)->first();
                if(!empty($type)){
    
                    $profs = 0;
                    $completed = 0;
    
                    $forms = Form::where('status', 'active')->get();
                    foreach($forms as $form){
    
                        if( strtotime($form->to->format('Y-m-d')) == strtotime(Carbon\Carbon::now()->addDays(1)->format('Y-m-d')) ){
                         
                            $relation = FormRelationship::where('formId', $form->id)->where('evaluator', 'Student')->get();
                            foreach($relation as $item){
        
                                $faculties = Faculty::all();
                                foreach($faculties as $faculty){
        
                                    $load = ClassLoad::where('facultyNo', $faculty->facultyNo)->get();
                                    foreach($load as $class){
        
                                        $sched = COR::where('studentNo', '2015300332')
                                                        ->where('code', $class->code)
                                                        ->where('subject', $class->subject)
                                                        ->where('section', $class->section)
                                                        ->where('schedule', $class->schedule)
                                                        ->where('room', $class->room)
                                                        ->where('year', $class->year)
                                                        ->where('term', $class->term)->first();
        
                                        if(!empty($sched)){
        
                                            $profs = ++$profs;
                                            $progress = FormProgress::where('formId', $form->id)
                                                                        ->where('evaluator', $sched->student->userId)
                                                                        ->where('evaluatee', $faculty->userId)
                                                                        ->where('as', $item->evaluatee)
                                                                        ->where('year', $class->year)
                                                                        ->where('term', $class->term)->first();
        
                                            if(!empty($progress)){
        
                                                if($progress->status == 'completed'){
                                                    $completed = ++$completed;
                                                }
        
                                            }
        
                                        }
        
                                    }   
        
                                }                     
        
                            }

                        }
    
                    }
    
                }else{
    
                    $profs = 0;
                    $completed = 0;
    
                    $forms = Form::where('status', 'active')->get();
                    foreach($forms as $form){
    
                        if( strtotime($form->to->format('Y-m-d')) == strtotime(Carbon\Carbon::now()->addDays(1)->format('Y-m-d')) ){

                            $relation = FormRelationship::where('formId', $form->id)->where('evaluator', '!=' , 'Student')->get();
                            foreach($relation as $item){
        
                                if($item->status == 'Distinct'){
                                    if($item->evaluator == 'Professor'){
                                        $facs = Faculty::whereIn('type', ['Professor', 'Department Chair', 'Vice Dean', 'Dean'])->where('department', $user->faculty->department)->get();
                                    }else{
                                        $facs = Faculty::where('type', $item->evaluator)->where('department', $user->faculty->department)->get();
                                    }
                                }else{
                                    if($item->evaluator == 'Professor'){
                                        $facs = Faculty::whereIn('type', ['Professor', 'Department Chair', 'Vice Dean', 'Dean'])->get();
                                    }else{
                                        $facs = Faculty::where('type', $item->evaluator)->get();
                                    }
                                }
        
                                foreach($facs as $fac){
        
                                    $profs = ++$profs;
                                    $progress = FormProgress::where('formId', $form->id)
                                                        ->where('evaluatee', $fac->userId)
                                                        ->where('evaluator', $user->id)
                                                        ->where('as', $item->evaluatee)
                                                        ->where('year', $user->faculty->class->year)
                                                        ->where('term', $user->faculty->class->term)->first();
        
                                    if(!empty($progress)){
        
                                        if($progress->status == 'completed'){
                                            $completed = ++$completed;
                                        }
        
                                    }
        
                                }
        
                            }

                        }
    
                    }
    
                }
    
                if($completed != $profs){
                    $notify = new EmailNotification;
                    $notify->userId = $user->id;
                    $notify->isSent = '1';
                    $notify->save();
                    $user->notify(new FormNotification($user));
                }

            }

        }
    }
}
