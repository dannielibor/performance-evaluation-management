<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Feedback extends Model
{
    use SoftDeletes;

    public function formTrashed(){
        return $this->hasOne('App\Form', 'id', 'formId')->withTrashed();
    }
}
