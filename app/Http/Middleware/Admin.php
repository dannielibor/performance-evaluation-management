<?php

namespace App\Http\Middleware;

use App\User;

use Closure;
use Auth;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check() && (auth()->user()->isAdmin == 1 || auth()->user()->isSuperAdmin == 1) && auth()->user()->activated == 1){
            return $next($request);
        }elseif(Auth::check() && (auth()->user()->isAdmin == 0 || auth()->user()->isSuperAdmin == 0)){
            abort(404);
        }elseif(Auth::check() && (auth()->user()->isAdmin == 1 || auth()->user()->isSuperAdmin == 1) && auth()->user()->activated == 0){
            $this->queueActivationKeyNotification(User::find(auth()->user()->id));
            Auth::logout();
            return response(view('admin.layouts.login')->with('notify', 1));
        }else{
            Auth::logout();
            return response(view('admin.layouts.login'));
        }
    }
}
