<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class User
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    { 
        if(Auth::check() && auth()->user()->isAdmin == 0){
            return $next($request);
        }elseif(Auth::check() && auth()->user()->isAdmin == 1){
            abort(404);
        }else{
            Auth::logout();
            return response(view('authenticate'));
        }
        
    }
}
