<?php

namespace App\Http\Controllers\Guest;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\FormTrait;
use App\Traits\InternetTrait;
use App\Traits\AuthTrait;

use Session;
use App\Faculty;
use App\Element;
use App\User;

class SummativeReportController extends Controller
{
    use FormTrait, InternetTrait, AuthTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Element $element)
    {
        $this->connection();
        $this->auth();
        $this->schedule();
        $this->authorize('view', $element);

        return view('guest.summative_report.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $this->connection();
        $this->schedule();
        $this->auth();
        
        $user = User::find($request->id);
        return response()->json(['name' => $user->name, 'department' => $user->faculty->department]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $this->connection();
        $this->auth();
        $this->schedule();

        if(empty($request->year) || empty($request->term) || empty($request->id)){
            return '';
        }

        $faculty = Faculty::where('userId', $request->id)->first();

        $elements = Element::where('type', $faculty->type)->get();

        return view('guest.summative_report.data', ['elements' => $elements, 'year' => $request->year, 'term' => $request->term, 'user' => $request->id])->render();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
