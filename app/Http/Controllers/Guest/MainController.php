<?php

namespace App\Http\Controllers\Guest;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\FormTrait;
use App\Traits\AuthTrait;
use App\Traits\InternetTrait;

use Session;
use App\FormRestriction;
use App\FormRelationship;
use App\User;
use App\Student;
use App\Faculty;
use App\COR;
use App\Form;
use App\TwoAuth;
use Carbon;
use Auth;

class MainController extends Controller
{
    use FormTrait, InternetTrait, AuthTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->connection();
        $this->schedule();
        $this->auth();

        $restrict = FormRestriction::where('type', Session::get('type'))->where('selfEvaluation', 1)->first();
        $user = User::find(auth()->user()->id);

        $programs = Student::distinct()->first(['program']);
    
        $blocks = [];
        $students = Student::where('program', $programs->program)->get();
    
        foreach($students as $student){
            array_push($blocks, $student->studentNo);
        }
        
        $cor = COR::whereIn('studentNo', $blocks)->first();

        $form = Form::where('status', 'active')->orderBy('created_at', 'asc')->first();

        $faculty = Faculty::distinct()->first(['department']);

        if(!empty($form)){
            return view('guest.home')->with('restrict', $restrict)->with('user', $user)
                                    ->with('program', $programs->program)
                                    ->with('setForm', $form->id)
                                    ->with('getSection', $cor->section)
                                    ->with('getCode', $cor->code)
                                    ->with('formId', $form->id)
                                    ->with('getDepartment', $faculty->department);
        }else{
            return view('guest.home')->with('restrict', $restrict)->with('user', $user)
                                    ->with('program', $programs->program)
                                    ->with('getSection', $cor->section)
                                    ->with('getCode', $cor->code)
                                    ->with('getDepartment', $faculty->department);
        }

    }

    public function search(Request $request){
        $this->connection();
        $this->schedule();
        $this->auth();
        
        $user = User::find($request->q);
        $forms = FormRelationship::where('evaluator', Session::get('type'))->where('evaluatee', $user->faculty->type)->get();
        if(Auth::check()){
            return view('guest.search')->with('user', $user)->with('forms', $forms);
        }else{
            return view('authenticate');
        }
    }
    
}
