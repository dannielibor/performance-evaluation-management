<?php

namespace App\Http\Controllers\Guest;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\FormTrait;
use App\Traits\AuthTrait;
use App\Traits\InternetTrait;

use App\Faculty;
use App\User;
use Carbon;

class RankController extends Controller
{
    use FormTrait, InternetTrait, AuthTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->connection();
        $this->schedule();
        $this->auth();

        $profs = Faculty::paginate(10);
        return view('guest.summary.rank.index')->with('profs', $profs)->with('year', Carbon\Carbon::now()->format('Y')-1 . '-' . Carbon\Carbon::now()->format('Y'))->with('term', '1st');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $faculty = Faculty::find($id);
        return view('guest.summary.rank.show')->with('faculty', $faculty)->with('year', $request->year)->with('term', $request->term);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function load(Request $request){

        $this->connection();
        $this->schedule();
        $this->auth();
        
        if(empty($request->year) || empty($request->term) || empty($request->dept)){
            return '';
        }

        $profs = Faculty::where('department', $request->dept)->paginate(10);
        
        return view('guest.summary.rank.rank', ['profs' => $profs, 'term' => $request->term, 'year' => $request->year])->render();

    }

    public function entry(Request $request){
        $this->connection();
        $this->schedule();
        $this->auth();

        $user = User::find($request->id);
        $explode = explode('|', $request->form);
        if(count($explode) > 1){
            return view('guest.summary.rank.entries', ['user' => $user, 'form' => $explode[0], 'passYear' => $request->year, 'passTerm' => $request->term, 'self' => '1'])->render();
        }else{
            return view('guest.summary.rank.entries', ['user' => $user, 'form' => $request->form, 'passYear' => $request->year, 'passTerm' => $request->term])->render();
        }
    }

    public function feedback(Request $request){
        $this->connection();
        $this->schedule();
        $this->auth();
        
        $user = User::find($request->id);
        return view('guest.summary.rank.feedback', ['user' => $user, 'form' => $request->form, 'year' => $request->year, 'term' => $request->term])->render();
    }
}
