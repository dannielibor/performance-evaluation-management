<?php

namespace App\Http\Controllers\Guest;

use Illuminate\Http\Request;
use App\Traits\FormTrait;
use App\Http\Controllers\Controller;
use App\Traits\InternetTrait;
use App\Traits\AuthTrait;

use App\User;
use App\Faculty;
use App\Student;
use App\Body;
use App\Content;
use App\Form;
use App\Entry;
use App\FormRelationship;
use App\FeedbackResult;
use App\FormRestriction;
use App\Feedback;
use App\FormProgress;
use App\COR;
use App\EntryTotal;
use App\ClassLoad;
use App\Log;
use Session;

use Toastr;

class FormsController extends Controller
{
    use FormTrait, InternetTrait, AuthTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Form $form)
    {
        $this->connection();
        $this->schedule();
        $this->auth();
        $this->authorize('view', $form);
        
        $restrict = FormRelationship::where('formId', $request->formId)->where('evaluator', Session::get('type'))->first();
        return view('guest.forms.index')->with('restrict', $restrict)->with('as', $restrict->evaluatee);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { 
        $this->connection();
        $this->schedule();
        $this->auth();
        
        $get = COR::orderBy('id', 'desc')->first();
        $this->progress($request->formId, $request->userId, $get->year, $get->term, $request->as);
        $progress = FormProgress::where('formId', $request->formId)
                                ->where('evaluator', auth()->user()->id)
                                ->where('evaluatee', $request->userId)
                                ->where('year', $get->year)
                                ->where('term', $get->term)
                                ->where('as', $request->as)->first();
        $body = Body::select('id')->where('formId', $request->formId)->get();

        $content = Content::whereIn('bodyId', $body)->where('type', 'question')->get();

        for($x = 0; $x < count($content); $x++){

            $a = 'radio'.$content[$x]->id;

            $validate = Entry::where('formProgressId', $progress->id)
                            ->where('contentId', $content[$x]->id)->first();

            if(empty($validate)){
                if(!empty($request->$a)){
                    $entry = new Entry;
                    $entry->formProgressId = $progress->id;
                    $entry->contentId = $content[$x]->id;
                    $entry->result = $request->$a;
                    $entry->save();
                }
            }else{
                if(!empty($request->$a)){
                    $validate->result = $request->$a;
                    $validate->save();
                }
            }

        }

        $feedback = Feedback::where('formId', $request->formId)->get();

        for($y = 0; $y < count($feedback); $y++){
            $b = 'feedback_'.$feedback[$y]->id;
            $comment = new FeedbackResult;
            $comment->formId = $request->formId;
            $comment->feedbackId = $feedback[$y]->id;
            $comment->evaluator = auth()->user()->id;
            $comment->evaluatee = $request->userId;
            $comment->as = $request->as;
            $comment->result = $request->$b;
            $comment->year = $get->year;
            $comment->term = $get->term;
            $comment->save();
        }

        $countContent = 0;
        $entries = 0;
        $bool = false;
        $bodies = Body::where('formId', $request->formId)->get();
        foreach($bodies as $body){
            $contents = Content::where('bodyId', $body->id)->where('type', 'question')->get();
            $countContent = $countContent + count($contents);
            foreach($contents as $content){
                $entry = Entry::where('contentId', $content->id)->where('formProgressId', $progress->id)->first();
                if(!empty($entry)){
                    $entries = ++$entries;
                }
            }
        }

        if(($countContent != 0 && $entries != 0) && ($countContent == $entries)){
            $progress->status = 'completed';
            $allBody = [];
            $bodies = Body::where('formId', $request->formId)->get();
            foreach($bodies as $body){
                $sum = 0;
                $contents = Content::where('bodyId', $body->id)->where('type', 'question')->get();
                foreach($contents as $content){
                    $entry = Entry::where('contentId', $content->id)
                                    ->where('formProgressId', $progress->id)->first();
                    $sum = $sum + $entry->result;
                }
                if($body->rate != 0){
                    array_push($allBody, ( ($sum/count($contents)) * ($body->rate/100) ));
                }else{
                    $bool = true;
                    array_push($allBody, $sum);
                }
                
            }

            if($bool){
                $progress->total = array_sum($allBody) / $countContent;
                $bool = false;
            }else{
                $progress->total = array_sum($allBody);
            }
            $progress->save();
            $log = new Log;
            $log->userId = auth()->user()->id;
            $log->description = 'An evaluator has been submitted';
            $log->save();
        }

        $completed = 0;
        $count = 0;

        $relation = FormRelationship::where('evaluator', Session::get('type'))->get();

        foreach($relation as $item){

            if(Session::get('type') == 'Student'){

                if($item->evaluatee == 'Professor'){
                    $faculties = Faculty::whereIn('type', ['Professor', 'Department Chair', 'Vice Dean', 'Dean'])->get();
                }else{
                    $faculties = Faculty::where('type', $item->evaluatee)->get();
                }

                foreach($faculties as $faculty){
    
                    $load = ClassLoad::where('facultyNo', $faculty->facultyNo)->get();

                    foreach($load as $class){

                        $sched = COR::where('studentNo', auth()->user()->student->studentNo)
                                    ->where('code', $class->code)
                                    ->where('schedule', $class->schedule)
                                    ->where('section', $class->section)
                                    ->where('room', $class->room)
                                    ->where('year', $class->year)
                                    ->where('subject', $class->subject)
                                    ->where('term', $class->term)->first();

                        if(!empty($sched)){

                            $explode = explode(' ', $sched->subject);

                            if(last($explode) != 'Laboratory'){
    
                                $progress = FormProgress::where('formId', $item->formId)
                                                        ->where('evaluator', auth()->user()->id)
                                                        ->where('evaluatee', $class->faculty->userId)
                                                        ->where('as', $class->faculty->type)
                                                        ->where('year', $class->year)
                                                        ->where('term', $class->term)->first();

                                if(!empty($progress) && $progress->status == 'completed'){
                                    $completed = ++$completed;
                                }
    
                                $count = ++$count;

                            }

                        }

                    }
    
                }

            }else{

                if($item->status == 'Distinct'){
                    $faculties = Faculty::where('type', $item->evaluatee)->where('department', auth()->user()->faculty->department)->get();
                }else{
                    $faculties = Faculty::where('type', $item->evaluatee)->get();
                }
                
                foreach($faculties as $faculty){

                    $progress = FormProgress::where('formId', $item->formId)
                                            ->where('evaluator', auth()->user()->id)
                                            ->where('evaluatee', $faculty->userId)
                                            ->where('as', $faculty->type)
                                            ->where('year', $faculty->class->year)
                                            ->where('status', 'completed')
                                            ->where('term', $faculty->class->term)->first();
                    
                    if(!empty($progress)){
                        $completed = ++$completed;
                    }
                
                    $count = ++$count;

                }

            }

        }
        
        if($completed == $count){
            if(Session::get('type') == 'Student'){
                $user = Student::where('userId', auth()->user()->id)->first();
                $user->status = 'completed';
                $user->save();
                $log = new Log;
                $log->userId = auth()->user()->id;
                $log->description = 'An evaluator has been completed';
                $log->save();
            }else{
                $user = Faculty::where('userId', auth()->user()->id)->first();
                $user->status = 'completed';
                $user->save();
                $log = new Log;
                $log->userId = auth()->user()->id;
                $log->description = 'An evaluator has been completed';
                $log->save();
            }
        }

        $restrict = FormRelationship::where('formId', $request->formId)->where('evaluator', Session::get('type'))->first();
        if(empty($restrict)){
            return redirect('/');
        }else{
            return view('guest.forms.index')->with('restrict', $restrict)->with('as', $restrict->evaluatee);
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id, Form $form)
    {
        $this->connection();
        $this->schedule();
        $this->auth();
        $this->authorize('show', $form);

        $form = Form::find($request->formId);
        $body = Body::where('formId', $request->formId)->orderBy('position', 'asc')->paginate($form->pagination);
        $user = User::find($request->userId);
        return view('guest.forms.show')->with('form', $form)->with('body', $body)->with('user', $user)->with('as', $request->as);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id, Form $form)
    { 
        $this->connection();
        $this->schedule();
        $this->auth();
        $this->authorize('update', $form);

        $restrict = FormRelationship::where('formId', $request->formId)->where('evaluator', Session::get('type'))->where('evaluatee', $request->as)->first();
        return view('guest.forms.index')->with('restrict', $restrict)->with('as', $request->as);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function fetch_data(Request $request){
        $this->connection();
        $this->schedule();
        $this->auth();

        if($request->ajax()){ 
            $user = User::find($request->userId);
            $form = Form::find($request->id); 
            $body = Body::where('formId', $form->id)->orderBy('position', 'asc')->paginate($form->pagination); 
            return view('guest.forms.data', ['form' => $form, 'user' => $user, 'body' => $body, 'as' => $request->as])->render();
        }
    }

    public function paginate(Request $request){
        $this->connection();
        $this->schedule();
        $this->auth();

        $get = COR::orderBy('id', 'desc')->first();
        $this->progress($request->id, $request->evaluatee, $get->year, $get->term, $request->as);
        $progress = FormProgress::where('formId', $request->id)
                                ->where('evaluator', auth()->user()->id)
                                ->where('evaluatee', $request->evaluatee)
                                ->where('year', $get->year)
                                ->where('term', $get->term)
                                ->where('as', $request->as)->first();
        if(!empty($request->value) && count($request->value) > 0){
            foreach($request->value as $value){
                $explode = explode('-', $value);
                $check = Entry::where('formProgressId', $progress->id)
                                ->where('contentId', $explode[0])->first();
                if(empty($check)){
                    $content = new Entry;
                    $content->formProgressId = $progress->id;
                    $content->contentId = $explode[0];
                    $content->result = $explode[1];
                    $content->save();
                }else{
                    $check->result = $explode[1];
                    $check->save();
                }
            }
        }

    }

    public function progress($formId, $evaluatee, $year, $term, $as){
        $this->connection();
        $this->schedule();
        $this->auth();

        $progress = FormProgress::where('formId', $formId)
                ->where('evaluator', auth()->user()->id)
                ->where('evaluatee', $evaluatee)
                ->where('year', $year)
                ->where('term', $term)
                ->where('as', $as)->first();
        if(empty($progress)){
            $progress = new FormProgress;
            $progress->formId = $formId;
            $progress->evaluator = auth()->user()->id;
            $progress->evaluatee = $evaluatee;
            $progress->as = $as;
            $progress->year = $year;
            $progress->term = $term;
            $progress->save();
        }
    }

}
