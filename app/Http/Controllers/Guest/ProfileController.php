<?php

namespace App\Http\Controllers\Guest;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\FormTrait;
use App\Traits\ActivationKeyTrait;
use App\Traits\InternetTrait;
use App\Traits\AuthTrait;

use App\User;
use Hash;
use Toastr;

class ProfileController extends Controller
{
    use FormTrait, ActivationKeyTrait, InternetTrait, AuthTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->connection();
        $this->auth();
        $this->schedule();

        $user = User::find(auth()->user()->id);
        return view('guest.profile')->with('user', $user);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $this->connection();
        $this->schedule();
        $this->auth();

        $user = User::find(auth()->user()->id);
        return view('guest.profile')->with('user', $user)->with('settings', 1);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $this->connection();
        $this->schedule();
        $this->auth();

        $user = User::find(auth()->user()->id);
        if($user->twoFA == 1){
            $user->twoFA = 0;
        }else{
            $user->twoFA = 1;
        }
        $user->save();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    { 
        $this->connection();
        $this->auth();
        $this->schedule();

        $validate = User::where('username', $request->username)->where('id', '!=', auth()->user()->id)->get();
        if(count($validate) > 0){
            Toastr::warning("Username already exist!", $title = null, $options = []);
            return back();
        }

        $validate = User::where('email', $request->email)->where('id', '!=', auth()->user()->id)->get();
        if(count($validate) > 0 && !empty($request->email)){
            Toastr::warning("Email already exist!", $title = null, $options = []);
            return back();
        }
        $willSent = 1;
        $user = User::find($id);
        $user->username = $request->username;
        if($user->email == $request->email){
            $willSent = 0;
        }
        $user->email = $request->email;

        if(!empty($request->password)){
            $user->password = Hash::make($request->password);
        }

        $user->save();

        if($willSent == 1){
            $user = User::find($id);
            $user->activated = 0;
            $user->save();
            $this->queueActivationKeyNotification($user);
            return redirect('/logout');
        }

        Toastr::success("Profile successfully updated!", $title = null, $options = []);
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
