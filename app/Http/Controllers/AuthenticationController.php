<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Notifications\TwoAuthNotification;
use App\TwoAuth;
use App\User;
use App\Faculty;
use App\Student;
use App\Log;
use Session;
use Auth;

class AuthenticationController extends Controller
{
    public function index(Request $request){
        $auth = TwoAuth::where('code', $request->code)->first();
        if(!empty($auth)){
            $user = User::find($auth->userId);
            $faculty = Faculty::where('userId', $auth->userId)->first();

            if( count(Student::where('userId', $auth->userId)->get()) > 0 ){
                Session::put('type', "Student");
            }else if ( !empty($faculty) ){
                Session::put('type', $faculty->type);
            }else{
                Session::put('type', 'Admin');
            }
            $auth->delete();
            Auth::login($user);
            $log = new Log;
            $log->userId = auth()->user()->id;
            $log->description = 'A user has been signed in';
            $log->save();
            return redirect('/');
        }
        return redirect('/');
    }

    public function resend(Request $request){
        $user = User::where('email', $request->email)->first();
        $user->notify(new TwoAuthNotification($user));
    }
}
