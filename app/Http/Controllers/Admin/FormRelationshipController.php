<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\FormTrait;
use App\Traits\InternetTrait;
use App\Traits\AuthTrait;

use App\FormRelationship;
use App\FormRestriction;
use App\Form;
use App\Log;

use Toastr;
use Session;

class FormRelationshipController extends Controller
{
    use FormTrait, InternetTrait, AuthTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->connection();
        $this->schedule();
        $this->auth();

        $form = new FormRelationship;
        $form->formId = $request->formId;
        $form->save();
        Toastr::success("Relationship successfully created!", $title = null, $options = []);
        $log = new Log;
        $log->userId = auth()->user()->id;
        $log->description = 'A form has been updated';
        $log->save();
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->connection();
        $this->schedule();
        $this->auth();

        if(Session::get('type') == 'Super Admin'){
            $relation = FormRelationship::withTrashed()->where('formId', $id)->get();
            $form = Form::withTrashed()->find($id); 
        }else{
            $relation = FormRelationship::where('formId', $id)->get();
            $form = Form::find($id); 
        }

        return view('admin.forms.relationship.index')->with('relation', $relation)->with('form', $form);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    { 
        $this->connection();
        $this->schedule();
        $this->auth();

        if(!empty($request->type)){
            $restrict = FormRestriction::withTrashed()->where('formId', $request->id)->whereIn('type', $request->type)->get();
            if(count($restrict) > 0){
                foreach($restrict as $item){
                    $item->selfEvaluation = 1;
                    $item->save();
                }
            }else{
                foreach($request->type as $type){
                    $new = new FormRestriction;
                    $new->formId = $request->id;
                    $new->type = $type;
                    $new->selfEvaluation = 1;
                    $new->save();
                }
            }
    
            $restrict = FormRestriction::withTrashed()->where('formId', $request->id)->whereNotIn('type', $request->type)->get();
            if(count($request->type) > 0){
                foreach($restrict as $item){
                    $item->selfEvaluation = 0;
                    $item->save();
                }
            }
        }else{
            $restrict = FormRestriction::withTrashed()->where('formId', $request->id)->get();
            foreach($restrict as $item){
                $item->selfEvaluation = 0;
                $item->save();
            }
        }
        $log = new Log;
        $log->userId = auth()->user()->id;
        $log->description = 'A form has been updated';
        $log->save();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->connection();
        $this->schedule();
        $this->auth();

        $form = FormRelationship::find($id)->forcedelete();
        Toastr::success("Relationship successfully delete!", $title = null, $options = []);
        $log = new Log;
        $log->userId = auth()->user()->id;
        $log->description = 'A form has been updated';
        $log->save();
        return back();
    }

    public function status(Request $request)
    {
        $this->connection();
        $this->schedule();
        $this->auth();

        $form = FormRelationship::withTrashed()->find($request->id);
        $form->status = $request->value;
        $form->save();
    }

    public function evaluator(Request $request)
    {
        $this->connection();
        $this->schedule();
        $this->auth();

        if(!empty($request->compare)){
            $validate = FormRelationship::withTrashed()->where('formId', $request->formId)->where('evaluator', $request->value)->where('evaluatee', $request->compare)->first();
            if(!empty($validate)){
                return 'false';
            }
        }
        $form = FormRelationship::withTrashed()->find($request->id);
        $form->evaluator = $request->value;
        $form->save();
        $log = new Log;
        $log->userId = auth()->user()->id;
        $log->description = 'A form has been updated';
        $log->save();
    }

    public function evaluatee(Request $request)
    {
        $this->connection();
        $this->schedule();
        $this->auth();

        if(!empty($request->compare)){
            $validate = FormRelationship::withTrashed()->where('formId', $request->formId)->where('evaluatee', $request->value)->where('evaluator', $request->compare)->first();
            if(!empty($validate)){
                return 'false';
            }
        }
        $form = FormRelationship::withTrashed()->find($request->id);
        $form->evaluatee = $request->value;
        $form->save();
        $log = new Log;
        $log->userId = auth()->user()->id;
        $log->description = 'A form has been updated';
        $log->save();
    }

    public function selfEval(Request $request)
    {
        $this->connection();
        $this->schedule();
        $this->auth();

        $form = FormRelationship::withTrashed()->find($request->id);
        if($form->isSelfEvaluation == 'on'){
            $form->isSelfEvaluation = 'off';
        }else{
            $form->isSelfEvaluation = 'on';
        }
        $form->save();
        $log = new Log;
        $log->userId = auth()->user()->id;
        $log->description = 'A form has been updated';
        $log->save();
    }

}
