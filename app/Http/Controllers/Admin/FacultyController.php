<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\FormTrait;
use App\Traits\ActivationKeyTrait;
use App\Traits\InternetTrait;
use App\Traits\AuthTrait;

use App\User;
use App\Faculty;
use App\Log;
use App\ClassLoad;
use Hash;
use Toastr;
use Storage;
use Session;

class FacultyController extends Controller
{
    use FormTrait, ActivationKeyTrait, InternetTrait, AuthTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->connection();
        $this->schedule();
        $this->auth();

        $user = User::where('isAdmin', 0)->get();
        $departments = Faculty::distinct()->get(['department']);
        return view('admin.faculty.index')->with('profs', $user)->with('departments', $departments);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->connection();
        $this->schedule();
        $this->auth();

        return view('admin.faculty.store');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->connection();
        $this->schedule();
        $this->auth();

        if(Session::get('type') == 'Super Admin'){
            $user = User::withTrashed()->find($id);
            $departments = Faculty::withTrashed()->distinct()->get(['department']);
        }else{
            $user = User::find($id);
            $departments = Faculty::distinct()->get(['department']);
        }
        
        return view('admin.faculty.edit')->with('user', $user)->with('departments', $departments);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->connection();
        $this->schedule();
        $this->auth();

        try{
            $this->validate($request, [
                'img' => 'image|nullable|max:1999'
            ]);
        }catch(\Exception $e){
            Toastr::warning("Image file size is too big", $title = null, $options = []);
            return back();
        }

        $validate = User::withTrashed()->where('username', $request->username)->where('id', '!=', $id)->get();
        if(count($validate) > 0){
            Toastr::warning("Username already exist!", $title = null, $options = []);
            return back();
        }

        $validate = User::withTrashed()->where('email', $request->email)->where('id', '!=', $id)->get();
        if(count($validate) > 0 && !empty($request->email)){
            Toastr::warning("Email already exist!", $title = null, $options = []);
            return back();
        }
        $willSent = 1;
        $user = User::withTrashed()->find($id);
        $user->name = $request->name;
        if($user->email == $request->email){
            $willSent = 0;
        }
        $user->email = $request->email;
        
        if(!empty($request->password)){
            $user->password = Hash::make($request->password);
        }

        if($request->hasFile('img')){
            
            if($request->image != 'default_profile.jpg'){
                Storage::delete('public/profiles/'.$user->image);
            }

            // get file name with the extension
            $filenameWithExt = $request->file('img')->getClientOriginalName();
            // get just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            // get just ext
            $extension = $request->file('img')->getClientOriginalExtension();
            // filename to store
            $filenameToStore = $filename.'_'.time().'.'.$extension;
            // upload image
            $path = $request->file('img')->storeAs('public/profiles', $filenameToStore);

            $user->image = $filenameToStore;
        }else{
            $user->image = $request->image;
        }

        $log = new Log;
        $log->userId = auth()->user()->id;
        $log->description = 'A faculty member has been updated';
        $log->save();

        $user->save();

        $faculty = Faculty::withTrashed()->where('userId', $id)->first();
        $faculty->facultyNo = $request->facultyNo;
        $faculty->department = $request->department;
        $faculty->save();

        if($willSent == 1){
            $user = User::withTrashed()->find($id);
            $user->activated = 0;
            $user->save();
            $this->queueActivationKeyNotification($user);
        }

        Toastr::success("Faculty successfully updated!", $title = null, $options = []);
        return redirect('/faculty');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    { 
        $this->connection();
        $this->schedule();
        $this->auth();

        $faculty = Faculty::where('userId', $id)->first();
        $class = ClassLoad::where('facultyNo', $faculty->facultyNo)->delete();
        $faculty->delete();
        $user = User::find($id)->delete();
        Toastr::success("Faculty successfully deleted!", $title = null, $options = []);
        $log = new Log;
        $log->userId = auth()->user()->id;
        $log->description = 'A faculty member has been temporarily deleted';
        $log->save();
        return back();
    }

    public function load(){
        $this->connection();
        $this->schedule();
        $this->auth();
        
        if(Session::get('type') == 'Super Admin'){
            $faculty = Faculty::withTrashed()->get();
            $array = [];
    
            for($y = 0; $y < count($faculty); $y++){
                if($faculty[$y]->trashed()){
                    array_push($array, array('id' => $faculty[$y]->userTrashed->id, 'name' => $faculty[$y]->userTrashed->name,
                    'facultyNo' => $faculty[$y]->facultyNo, 'status' => 'Soft Deleted', 'department' => $faculty[$y]->department, 'type' => $faculty[$y]->type, 'img' => $faculty[$y]->userTrashed->image, 'children' => array()));
        
                    $class = ClassLoad::where('facultyNo', $faculty[$y]->facultyNo)->get();
        
                    for($x = 0; $x < count($class); $x++){
                        array_push($array[$y]['children'], array('ID' => $class[$x]->id, 'Code' => $class[$x]->code, 'Subject' => $class[$x]->subject, 'Section' => $class[$x]->section, 'Schedule' => $class[$x]->schedule, 'Room' => $class[$x]->room, 'Size' => $class[$x]->size));
                    }
                }else{
                    array_push($array, array('id' => $faculty[$y]->user->id, 'name' => $faculty[$y]->user->name,
                    'facultyNo' => $faculty[$y]->facultyNo, 'status' => $faculty[$y]->status, 'department' => $faculty[$y]->department, 'type' => $faculty[$y]->type, 'img' => $faculty[$y]->user->image, 'children' => array()));
        
                    $class = ClassLoad::where('facultyNo', $faculty[$y]->facultyNo)->get();
        
                    for($x = 0; $x < count($class); $x++){
                        array_push($array[$y]['children'], array('ID' => $class[$x]->id, 'Code' => $class[$x]->code, 'Subject' => $class[$x]->subject, 'Section' => $class[$x]->section, 'Schedule' => $class[$x]->schedule, 'Room' => $class[$x]->room, 'Size' => $class[$x]->size));
                    }
                }
            }
            return $array;
        }else{
            $faculty = Faculty::all();
            $array = [];
    
            for($y = 0; $y < count($faculty); $y++){
    
                array_push($array, array('id' => $faculty[$y]->user->id, 'name' => $faculty[$y]->user->name,
                'facultyNo' => $faculty[$y]->facultyNo, 'status' => $faculty[$y]->status, 'department' => $faculty[$y]->department, 'type' => $faculty[$y]->type, 'img' => $faculty[$y]->user->image, 'children' => array()));
    
                $class = ClassLoad::where('facultyNo', $faculty[$y]->facultyNo)->get();
    
                for($x = 0; $x < count($class); $x++){
                    array_push($array[$y]['children'], array('ID' => $class[$x]->id, 'Code' => $class[$x]->code, 'Subject' => $class[$x]->subject, 'Section' => $class[$x]->section, 'Schedule' => $class[$x]->schedule, 'Room' => $class[$x]->room, 'Size' => $class[$x]->size));
                }
            }
            return $array;
        }
    }

    public function type(Request $request){
        $this->connection();
        $this->schedule();
        $this->auth();
        
        for($x = 0; $x < count($request->array); $x++){
            $type = Faculty::withTrashed()->where('userId', $request->array[$x]['id'])->first();
            $type->type = $request->array[$x]['type'];
            $type->save();
        }

        $log = new Log;
        $log->userId = auth()->user()->id;
        $log->description = 'A faculty member has been updated';
        $log->save();
    }

    public function forcedelete(Request $request){
        $user = User::onlyTrashed()->find($request->id);
        $faculty = Faculty::onlyTrashed()->where('userId', $user->id)->first();
        $cor = ClassLoad::onlyTrashed()->where('facultyNo', $faculty->facultyNo)->forcedelete();
        $faculty->forcedelete();
        $user->forcedelete();
        Toastr::success("Student successfully deleted!", $title = null, $options = []);
        $log = new Log;
        $log->userId = auth()->user()->id;
        $log->description = 'A faculty member has been permanently deleted';
        $log->save();
        return back();
    }

    public function restore(Request $request){
        $user = User::onlyTrashed()->find($request->id)->restore();
        $faculty = Faculty::onlyTrashed()->where('userId', $request->id)->restore();
        $faculty = Faculty::where('userId', $request->id)->first();
        $cor = ClassLoad::onlyTrashed()->where('facultyNo', $faculty->facultyNo)->restore();
        Toastr::success("Student successfully restored!", $title = null, $options = []);
        $log = new Log;
        $log->userId = auth()->user()->id;
        $log->description = 'A faculty member has been restored';
        $log->save();
        return back();
    }

    public function twoFa(Request $request){
        $this->connection();
        $this->schedule();
        $this->auth();

        $user = User::find($request->id);
        if($user->twoFA == 1){
            $user->twoFA = 0;
        }else{
            $user->twoFA = 1;
        }
        $user->save();
    }

}
