<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\FormTrait;
use App\Traits\InternetTrait;
use App\Traits\AuthTrait;

use App\ClassLoad;
use App\Faculty;
use App\User;
use App\Log;
use Zip;
use Storage;
use DB;
use Toastr;
use Hash;

class ClassLoadController extends Controller
{
    use FormTrait, InternetTrait, AuthTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->connection();
        $this->auth();
        $this->schedule();

        $class = ClassLoad::all();
        return view('admin.class.index')->with('class', $class);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->connection();
        $this->auth();
        $this->schedule();

        ini_set('max_execution_time', 0);

        $return = "";

        $directories = Storage::files('public/csv');

        foreach($directories as $directory){
            $explode = explode('/', $directory);
            $file = explode('.', $explode[2]);
            $cut = explode('_cl_', $file[0]);
            if(count($cut) > 1){
                Storage::delete($directory);
            }
        }
        
        if($request->hasFile('chooseFile')){

            // get file name with the extension
            $filenameWithExt = $request->file('chooseFile')->getClientOriginalName();
            // get just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            // get just ext
            $extension = $request->file('chooseFile')->getClientOriginalExtension();
            // filename to store
            $filenameToStore = $filename.'_cl_'.time().'.'.$extension;
            // upload image
            $path = $request->file('chooseFile')->storeAs('public/csv', $filenameToStore);
                        
            $csvData = file_get_contents(public_path () . '/storage/csv/' .$filenameToStore);

            $rows = array_map('str_getcsv', explode("\n", $csvData));

            if(count($rows[0]) != 9){
                Storage::delete('public/csv/'.$filenameToStore);
                return 'false';
            }

            ClassLoad::truncate();
            
            for ($i = 1; $i < count($rows)-1; $i++){

                $class = new ClassLoad;
                $class->facultyNo = $rows[$i][0];
                $class->code = $rows[$i][2];
                $class->subject = $rows[$i][3];
                $class->section = $rows[$i][4];
                $class->schedule = $rows[$i][5];
                $class->size = $rows[$i][7];
                $class->room = $rows[$i][6];
                $class->year = $request->year;
                $class->term = $request->term;
                $class->save();

                $validate = Faculty::withTrashed()->where('facultyNo', $rows[$i][0])->first();

                if(empty($validate)){
                    $split = explode(',', $rows[$i][1]);
                    $user = new User;
                    $user->name = $split[1]. ' ' . $split[0];
                    $user->username = $rows[$i][0];
                    $user->password = Hash::make($rows[$i][0]);
                    $user->save();
        
                    $faculty = new Faculty;
                    $faculty->userId = $user->id;
                    $faculty->facultyNo = $rows[$i][0];
                    $faculty->department = $rows[$i][8];
                    $faculty->save();
                }else{
                    $validate->status = 'pending';
                    $validate->save();
                }

            } 

            $log = new Log;
            $log->userId = auth()->user()->id;
            $log->description = 'Class Load has been imported';
            $log->save();
            
        }
        
        return $return;

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
