<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\FormTrait;
use App\Traits\InternetTrait;
use App\Traits\AuthTrait;

use App\Feedback;
use App\FeedbackResult;
use App\Log;

class FeedbackController extends Controller
{
    use FormTrait, InternetTrait, AuthTrait;

    public function store(Request $request){
        $this->connection();
        $this->schedule();
        $this->auth();

        $feedback = new Feedback;
        $feedback->formId = $request->id;
        $feedback->save();

        $log = new Log;
        $log->userId = auth()->user()->id;
        $log->description = 'A form has been updated';
        $log->save();
        return response()->json(['id' => $feedback->id]);
    }

    public function update(Request $request){
        $this->connection();
        $this->schedule();
        $this->auth();

        $feedback = Feedback::withTrashed()->find($request->id);
        $feedback->header = $request->value;
        $feedback->save();

        $log = new Log;
        $log->userId = auth()->user()->id;
        $log->description = 'A form has been updated';
        $log->save();
    }

    public function edit(Request $request){
        $this->connection();
        $this->schedule();
        $this->auth();

        $feedback = Feedback::withTrashed()->find($request->id);
        $feedback->position = $request->value;
        $feedback->save();

        $log = new Log;
        $log->userId = auth()->user()->id;
        $log->description = 'A form has been updated';
        $log->save();
    }
    
    public function destroy(Request $request){
        $this->connection();
        $this->schedule();
        $this->auth();
        
        $feedback = Feedback::withTrashed()->find($request->id)->forcedelete();

        $log = new Log;
        $log->userId = auth()->user()->id;
        $log->description = 'A form has been updated';
        $log->save();

        return $request->id;
    }
}
