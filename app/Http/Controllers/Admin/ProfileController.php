<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\FormTrait;
use App\Traits\ActivationKeyTrait;
use App\Traits\InternetTrait;
use App\Traits\AuthTrait;

use App\User;
use Hash;
use Toastr;
use Storage;

class ProfileController extends Controller
{
    use FormTrait, ActivationKeyTrait, InternetTrait, AuthTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { 
        $this->connection();
        $this->schedule();
        $this->auth();

        $user = User::find(auth()->user()->id);
        return view('admin.profile')->with('user', $user);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $this->connection();
        $this->schedule();
        $this->auth();

        $user = User::find(auth()->user()->id);
        return view('admin.profile')->with('user', $user)->with('settings', 1);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $this->connection();
        $this->schedule();
        $this->auth();

        $user = User::find(auth()->user()->id);
        if($user->twoFA == 1){
            $user->twoFA = 0;
        }else{
            $user->twoFA = 1;
        }
        $user->save();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->connection();
        $this->schedule();
        $this->auth();

        try{
            $this->validate($request, [
                'img' => 'image|nullable|max:1999'
            ]);
        }catch(\Exception $e){
            Toastr::warning("Image file size is too big", $title = null, $options = []);
            return back();
        }

        $validate = User::where('username', $request->username)->where('id', '!=', $id)->get();
        if(count($validate) > 0){
            Toastr::warning("Username already exist!", $title = null, $options = []);
            return back();
        }

        $validate = User::where('email', $request->email)->where('id', '!=', $id)->get();
        if(count($validate) > 0 && !empty($request->email)){
            Toastr::warning("Email already exist!", $title = null, $options = []);
            return back();
        }

        $user = User::find($id);
        $willSent = 1;
        $user->name = $request->name;
        if($user->email == $request->email){
            $willSent = 0;
        }
        $user->email = $request->email;
        $user->username = $request->username;

        if(!empty($request->password)){
            $user->password = Hash::make($request->password);
        }

        if($request->hasFile('img')){
            
            if($request->image != 'default_profile.jpg'){
                Storage::delete('public/profiles/'.$user->image);
            }

            // get file name with the extension
            $filenameWithExt = $request->file('img')->getClientOriginalName();
            // get just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            // get just ext
            $extension = $request->file('img')->getClientOriginalExtension();
            // filename to store
            $filenameToStore = $filename.'_'.time().'.'.$extension;
            // upload image
            $path = $request->file('img')->storeAs('public/profiles', $filenameToStore);

            $user->image = $filenameToStore;
        }else{
            $user->image = $request->image;
        }

        $user->save();

        if($willSent == 1){
            $user = User::find($id);
            $user->activated = 0;
            $user->save();
            $this->queueActivationKeyNotification($user);
            return redirect('/logout');
        }

        Toastr::success('Profile has been updated successfully', $title = null, $options = []);

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}
