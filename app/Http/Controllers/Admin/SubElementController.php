<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\FormTrait;
use App\Traits\InternetTrait;
use App\Traits\AuthTrait;

use App\Faculty;
use App\Form;
use App\FormRelationship;
use App\Element;
use App\SubElement;
use App\Log;
use DB;

class SubElementController extends Controller
{
    use FormTrait, InternetTrait, AuthTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->connection();
        $this->schedule();
        $this->auth();

        $sub = new SubElement;
        $sub->elementId = $request->id;
        $sub->save();
        $types = Faculty::distinct()->get(['type']);
        return view('admin.data_analysis.summative_report.sub_element', ['elementId' => $request->id, 'types' => $types])->render();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $this->connection();
        $this->schedule();
        $this->auth();

        $sub = SubElement::withTrashed()->find($request->id);
        $sub->rate = $request->value;
        $sub->save();

        $log = new Log;
        $log->userId = auth()->user()->id;
        $log->description = 'A form has been updated';
        $log->save();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $this->connection();
        $this->schedule();
        $this->auth();

        $sub = SubElement::withTrashed()->find($request->id);
        $sub->type = $request->value;
        $sub->save();

        $log = new Log;
        $log->userId = auth()->user()->id;
        $log->description = 'A form has been updated';
        $log->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $this->connection();
        $this->schedule();
        $this->auth();
        
        SubElement::withTrashed()->find($request->id)->forcedelete();

        $log = new Log;
        $log->userId = auth()->user()->id;
        $log->description = 'A form has been updated';
        $log->save();
    }
}
