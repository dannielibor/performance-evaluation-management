<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\FormTrait;
use App\Traits\InternetTrait;
use App\Traits\AuthTrait;

use App\Form;
use App\COR;
use App\User;

class EntryController extends Controller
{
    use FormTrait, InternetTrait, AuthTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->connection();
        $this->schedule();
        $this->auth();

        $forms = Form::all();
        $get = COR::orderBy('id', 'desc')->first();
        if(empty($get)){
            return view('admin.data_analysis.entries.index')->with('forms', $forms)->with('getYear', 0)->with('getTerm', 0);
        }else{
            return view('admin.data_analysis.entries.index')->with('forms', $forms)->with('getYear', $get->year)->with('getTerm', $get->term);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {  
        $this->connection();
        $this->schedule();
        $this->auth();

        $form = Form::find($request->id);
        return view('admin.data_analysis.entries.show')->with('form', $form)->with('year', $request->year)->with('term', $request->term);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $this->connection();
        $this->schedule();
        $this->auth();

        $forms = Form::where('status', 'active')->get();
        return view('admin.data_analysis.entries.data', ['year' => $request->year, 'term' => $request->term, 'forms' => $forms])->render();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function entry(Request $request){
        $this->connection();
        $this->schedule();
        $this->auth();

        $user = User::find($request->id);
        $explode = explode('|', $request->form);
        if(count($explode) > 1){
            return view('admin.data_analysis.entries.entries', ['user' => $user, 'form' => $explode[0], 'passYear' => $request->year, 'passTerm' => $request->term, 'self' => '1'])->render();
        }else{
            return view('admin.data_analysis.entries.entries', ['user' => $user, 'form' => $request->form, 'passYear' => $request->year, 'passTerm' => $request->term])->render();
        }
    }

    public function feedback(Request $request){
        $this->connection();
        $this->schedule();
        $this->auth();
        
        $user = User::find($request->id);
        return view('admin.data_analysis.entries.feedback', ['user' => $user, 'form' => $request->form, 'year' => $request->year, 'term' => $request->term])->render();
    }

}
