<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\FormTrait;
use App\Traits\InternetTrait;
use App\Traits\AuthTrait;

use DB;
use League\Csv\Reader;
use Toastr;
use Hash;
use App\User;
use App\Student;
use App\COR;
use App\Log;
use Zip;
use Storage;

class CorController extends Controller
{
    use FormTrait, InternetTrait, AuthTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $this->auth();
        $this->connection();
        $this->schedule();

        $cor = COR::all();
        return view('admin.cor.index')->with('cor', $cor);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->auth();
        $this->connection();
        $this->schedule();

        return view('admin.cor.store');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->auth();
        $this->connection();
        $this->schedule();

        ini_set('max_execution_time', 0);
        
        $return = "";

        $directories = Storage::files('public/csv');

        foreach($directories as $directory){
            $explode = explode('/', $directory);
            $file = explode('.', $explode[2]);
            $cut = explode('_cor_', $file[0]);
            if(count($cut) > 1){
                Storage::delete($directory);
            }
        }
        
        if($request->hasFile('chooseFile')){
            
            // get file name with the extension
            $filenameWithExt = $request->file('chooseFile')->getClientOriginalName();
            // get just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            // get just ext
            $extension = $request->file('chooseFile')->getClientOriginalExtension();
            // filename to store
            $filenameToStore = $filename.'_cor_'.time().'.'.$extension;
            // upload image
            $path = $request->file('chooseFile')->storeAs('public/csv', $filenameToStore);
                        
            $csvData = file_get_contents(public_path() . '/storage/csv/' .$filenameToStore);

            $rows = array_map('str_getcsv', explode("\n", $csvData));

            if(count($rows[0]) != 10){
                Storage::delete('public/csv/'.$filenameToStore);
                return 'false';
            }
            
            COR::truncate();
            
            for ($i = 1; $i < count($rows)-1; $i++){

                $cor = new COR;
                $cor->studentNo = $rows[$i][0];
                $cor->code = $rows[$i][5];
                $cor->subject = $rows[$i][6];
                $cor->section = $rows[$i][7];
                $cor->schedule = $rows[$i][8];
                $cor->room = $rows[$i][9];
                $cor->year = $request->year;
                $cor->term = $request->term;
                $cor->save();

                $validate = Student::withTrashed()->where('studentNo', $rows[$i][0])->first();

                if(empty($validate)){
                    $split = explode(',', $rows[$i][1]);
                    $user = new User;
                    $user->name = $split[1]. ' ' . $split[0];
                    $user->username = $rows[$i][0];
                    $user->password = Hash::make($rows[$i][0]);
                    $user->save();
        
                    $student = new Student;
                    $student->userId = $user->id;
                    $student->studentNo = $rows[$i][0];
                    $student->gender = $rows[$i][2];
                    $student->program = $rows[$i][3];
                    $student->yearLvl = $rows[$i][4];
                    $student->save();
                }else{
                    $validate->status = 'pending';
                    $validate->save();
                }

            }

            $log = new Log;
            $log->userId = auth()->user()->id;
            $log->description = 'COR has been imported';
            $log->save();
            
        }

        return $return;

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        
    }

}
