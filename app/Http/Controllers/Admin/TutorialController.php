<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Traits\FormTrait;
use App\Traits\InternetTrait;
use App\Traits\AuthTrait;

use App\Tutorial;
use Toastr;

class TutorialController extends Controller
{
    use FormTrait, InternetTrait, AuthTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->connection();
        $this->schedule();
        $this->auth();
        $videos = Tutorial::orderBy('created_at', 'asc')->get();
        return view('admin.tutorial.index')->with('videos', $videos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Tutorial $video)
    {
        $this->authorize('view', $video);
        $this->connection();
        $this->schedule();
        $this->auth();
        $video = new Tutorial;
        $video->title = $request->title;
        $video->link = str_replace("share","embed",$request->link);
        $video->save();
        Toastr::success("Tutorial successfully added!", $title = null, $options = []);
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->connection();
        $this->schedule();
        $this->auth();
        $video = Tutorial::find($id);
        return view('admin.tutorial.show')->with('video', $video);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id, Tutorial $video)
    {
        $this->authorize('view', $video);
        $this->connection();
        $this->schedule();
        $this->auth();
        $video = Tutorial::find($id);
        $video->title = $request->title;
        $video->link = str_replace("share","embed",$request->link);
        $video->save();
        Toastr::success("Tutorial successfully updated!", $title = null, $options = []);
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Tutorial $video)
    {
        $this->authorize('view', $video);
        $this->connection();
        $this->schedule();
        $this->auth();
        Tutorial::find($id)->delete();
        Toastr::success("Tutorial successfully deleted!", $title = null, $options = []);
        return back();
    }
}
