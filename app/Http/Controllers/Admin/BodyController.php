<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\FormTrait;
use App\Traits\InternetTrait;
use App\Traits\AuthTrait;

use App\Form;
use App\Body;
use App\Content;
use App\Log;

class BodyController extends Controller
{
    use FormTrait, InternetTrait, AuthTrait;

    public function store(Request $request){
        $this->connection();
        $this->auth();
        $this->schedule();

        $log = new Log;
        $log->userId = auth()->user()->id;
        $log->description = 'A form has been updated';
        $log->save();

        $letters = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];

        if(!empty($request->position)){

            $body = new Body;
            $body->formId = $request->formId;
            $body->save();
    
            for($x = 0; $x < count($request->position); $x++){
    
                $position = Body::withTrashed()->find($request->position[$x]);
                $position->position = $x;
                $position->sort = $letters[$x];
                $position->save();
                
            }
    
            $position = Body::withTrashed()->where('formId', $request->formId)->where('sort','!=',null)->orderByRaw('LENGTH(position)', 'desc')->orderBy('position', 'desc')->first();
            
            $update = Body::withTrashed()->find($body->id);
            $update->position = ++$position->position;
            $update->sort = ++$position->sort;
            $update->save();
            
            return response()->json([ 'id' => $body->id, 'sort' => $update->sort ]);

        }else{

            $body = new Body;
            $body->formId = $request->formId;
            $body->sort = 'A';
            $body->position = 0;
            $body->save();

            return response()->json([ 'id' => $body->id, 'sort' => $body->sort ]);

        }

    }

    public function edit(Request $request){
        $this->connection();
        $this->auth();
        $this->schedule();

        $log = new Log;
        $log->userId = auth()->user()->id;
        $log->description = 'A form has been updated';
        $log->save();

        $header = Body::withTrashed()->find($request->id);
        
        if(!empty($request->header)){
            $header->header = $request->header;
            $header->save();
        }
    }

    public function destroy(Request $request){
        $this->connection();
        $this->auth();
        $this->schedule();

        $log = new Log;
        $log->userId = auth()->user()->id;
        $log->description = 'A form has been updated';
        $log->save();

        Body::wtihTrashed()->find($request->id)->forcedelete();
        Content::withTrashed()->where('bodyId', $request->id)->forcedelete();
        return $request->id;
    }

    public function update(Request $request){
        $this->connection();
        $this->auth();
        $this->schedule();

        $log = new Log;
        $log->userId = auth()->user()->id;
        $log->description = 'A form has been updated';
        $log->save();
        
        $body = Body::withTrashed()->find($request->id);
        $body->sort = $request->sort;
        $body->position = $request->position;
        $body->save();
    }

}