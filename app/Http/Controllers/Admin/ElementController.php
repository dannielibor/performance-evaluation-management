<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\FormTrait;
use App\Traits\InternetTrait;
use App\Traits\AuthTrait;

use App\Faculty;
use App\Form;
use App\FormRelationship;
use App\Element;
use App\SubElement;
use App\Log;
use DB;

class ElementController extends Controller
{
    use FormTrait, InternetTrait, AuthTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->auth();
        $this->connection();
        $this->schedule();

        $departments = Faculty::distinct()->get(['department']);
        return view('admin.data_analysis.summative_report.index')->with('departments', $departments);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->auth();
        $this->connection();
        $this->schedule();

        if(!empty($request->type)){
            $new = new Element;
            $new->type = $request->type;
            $new->save();
            $forms = Form::where('status', 'active')->get();
            $elements = Element::where('type', $request->type)->get();
            $total = Element::select(DB::raw('SUM(rate) as total'))->where('type', $request->type)->first();
            $types = Faculty::distinct()->get(['type']);
            $log = new Log;
            $log->userId = auth()->user()->id;
            $log->description = 'Summative Report has been updated';
            $log->save();
            return view('admin.data_analysis.summative_report.element', ['elements' => $elements, 'forms' => $forms, 'total' => $total, 'types' => $types])->render();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $this->auth();
        $this->connection();
        $this->schedule();

        $forms = Form::where('status', 'active')->get();
        $elements = Element::where('type', $request->type)->get();
        $total = Element::select(DB::raw('SUM(rate) as total'))->where('type', $request->type)->first();
        $types = Faculty::distinct()->get(['type']);
        return view('admin.data_analysis.summative_report.element', ['elements' => $elements, 'forms' => $forms, 'total' => $total, 'types' => $types])->render();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $this->auth();
        $this->connection();
        $this->schedule();

        $types = Faculty::distinct()->get(['type']);
        return view('admin.data_analysis.summative_report.edit')->with('types', $types);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $this->auth();
        $this->connection();
        $this->schedule();

        Element::find($request->id)->forcedelete();
        SubElement::whereIn('elementId', [$request->id])->forcedelete();

        $log = new Log;
        $log->userId = auth()->user()->id;
        $log->description = 'Summative Report has been updated';
        $log->save();
    }

    public function title(Request $request){
        $this->auth();
        $this->connection();
        $this->schedule();

        $element = Element::find($request->id);
        $explode = explode('|', $request->value);
        if(!empty($explode[1])){
            $element->formId = $explode[0];
            $element->selfEvaluation = '1';
        }else{
            $element->formId = $request->value;
        }
        $element->save();

        $log = new Log;
        $log->userId = auth()->user()->id;
        $log->description = 'Summative Report has been updated';
        $log->save();

    }

    public function rate(Request $request){
        $this->auth();
        $this->connection();
        $this->schedule();

        $element = Element::find($request->id);
        $element->rate = $request->value;
        $element->save();

        $log = new Log;
        $log->userId = auth()->user()->id;
        $log->description = 'Summative Report has been updated';
        $log->save();
        
    }
}
