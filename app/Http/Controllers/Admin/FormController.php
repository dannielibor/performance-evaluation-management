<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\FormTrait;
use App\Traits\InternetTrait;
use App\Traits\AuthTrait;
use App\User;
use App\Form;
use App\Body;
use App\Content;
use App\Entry;
use App\FormRelationship;
use App\FormRestriction;
use App\Feedback;
use App\FeedbackResult;
use App\FormProgress;
use App\Element;
use App\SubElement;
use App\Log;
use Carbon;
use Toastr;
use DB;
use Session;

class FormController extends Controller
{

    use FormTrait, InternetTrait, AuthTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $this->connection();
        $this->auth();
        $this->schedule();

        if(Session::get('type') == 'Super Admin'){
            $form = Form::withTrashed()->get();
        }else{
            $form = Form::all();
        }

        return view('admin.forms.index')->with('forms', $form);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        Form::onlyTrashed()->find($request->id)->forcedelete();
        $bodies = Body::onlyTrashed()->where('formId', $request->id)->get();
        foreach($bodies as $body){
            $contents = Content::onlyTrashed()->where('bodyId', $body->id)->get();
            foreach($contents as $content){
                $entries = Entry::onlyTrashed()->where('contentId', $content->id)->forcedelete();
                $content->forcedelete();
            }
            $body->forcedelete();
        }
        $elements = Element::onlyTrashed()->where('formId', $request->id)->get();
        foreach($elements as $element){
            $sub = SubElement::onlyTrashed()->where('elementId', $element->id)->forcedelete();
            $element->forcedelete();
        }
        Feedback::onlyTrashed()->where('formId', $request->id)->forcedelete();
        FeedbackResult::onlyTrashed()->where('formId', $request->id)->forcedelete();
        FormProgress::onlyTrashed()->where('formId', $request->id)->forcedelete();
        FormRelationship::onlyTrashed()->where('formId', $request->id)->forcedelete();
        FormRestriction::onlyTrashed()->where('formId', $request->id)->forcedelete();
        Toastr::success("Form successfully deleted!", $title = null, $options = []);
        $log = new Log;
        $log->userId = auth()->user()->id;
        $log->description = 'A form has been permanently deleted';
        $log->save();
        return back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { 
        $this->connection();
        $this->schedule();
        $this->auth();

        $form = new Form;
        $form->title = $request->title;
        $form->status = "inactive";
        $form->pagination = '3';
        $form->save();

        $body = new Body;
        $body->formId = $form->id;
        $body->sort = "A";
        $body->save();

        $position = Body::find($body->id);
        $position->position = --$body->id;
        $position->save();

        if(!empty($request->self)){
            for($x = 0; $x < count($request->self); $x++){
                $self = new FormRestriction;
                $self->formId = $form->id;
                $self->type = $request->self[$x];
                $self->selfEvaluation = 1;
                $self->save();
            }
        }

        $log = new Log;
        $log->userId = auth()->user()->id;
        $log->description = 'New form has been created';
        $log->save();

        Toastr::success("Form successfully created!", $title = null, $options = []);
        if(isset($request->quick)){
            return redirect('/forms/edit/'.$form->id);
        }else{
            return back();
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // $this->connection();
        // $this->schedule();
        // $this->auth();

        // $form = Form::find($id);
        // $body = Body::where('formId', $id)->orderByRaw('LENGTH(position)', 'asc')->orderBy('position', 'asc')->get();
        // return view('admin.forms.preview')->with('form', $form)->with('body', $body);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->connection();
        $this->schedule();
        $this->auth();

        if(Session::get('type') == 'Super Admin'){
            $form = Form::withTrashed()->find($id);
            $feedback = Feedback::withTrashed()->where('formId', $id)->orderByRaw('LENGTH(position)', 'asc')->orderBy('position', 'asc')->get();
        }else{
            $form = Form::find($id);
            $feedback = Feedback::where('formId', $id)->orderByRaw('LENGTH(position)', 'asc')->orderBy('position', 'asc')->get();
        }

        return view('admin.forms.create')->with('form', $form)->with('feedback', $feedback);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->connection();
        $this->schedule();
        $this->auth();

        $form = Form::withTrashed()->find($request->id);
        $form->title = $request->value;
        $form->save();

        $log = new Log;
        $log->userId = auth()->user()->id;
        $log->description = 'A form has been updated';
        $log->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->connection();
        $this->auth();
        $this->schedule();

        Form::find($id)->delete();
        $bodies = Body::where('formId', $id)->get();
        foreach($bodies as $body){
            $contents = Content::where('bodyId', $body->id)->get();
            foreach($contents as $content){
                $entries = Entry::where('contentId', $content->id)->delete();
                $content->delete();
            }
            $body->delete();
        }
        $elements = Element::where('formId', $id)->get();
        foreach($elements as $element){
            $sub = SubElement::where('elementId', $element->id)->delete();
            $element->delete();
        }
        Feedback::where('formId', $id)->delete();
        FeedbackResult::where('formId', $id)->delete();
        FormProgress::where('formId', $id)->delete();
        FormRelationship::where('formId', $id)->delete();
        FormRestriction::where('formId', $id)->delete();
        Toastr::success("Form successfully deleted!", $title = null, $options = []);
        $log = new Log;
        $log->userId = auth()->user()->id;
        $log->description = 'A form has been temporarily deleted';
        $log->save();
        return back();
    }

    public function publish(Request $request){
        $this->connection();
        $this->schedule();
        $this->auth();

        $publish = Form::withTrashed()->find($request->id);
        $publish->pagination = $request->paginate;

        $body = Body::withTrashed()->where('formId', $request->id)->orderByRaw('LENGTH(position)', 'asc')->orderBy('position', 'asc')->get();

        for($x = 0; $x < count($request->rate); $x++){
            $body[$x]->rate = $request->rate[$x];
            $body[$x]->save();
        }

        if(!empty($request->schedule)){

            $split = explode('/', $request->schedule);

            $publish->from = $split[0];
            $publish->to = $split[1];
    
            if(strtotime($split[0]) > strtotime(Carbon\Carbon::now()->format('Y-m-d'))){
                $publish->status = 'pending';
            }else if(strtotime($split[0]) < strtotime(Carbon\Carbon::now()->format('Y-m-d')) && strtotime($split[1]) > strtotime(Carbon\Carbon::now()->format('Y-m-d'))){
                $publish->status = 'active';
            }else{
                $publish->status = 'inactive';
            }
    
            $publish->save();
    
            Toastr::success("Form successfully publish!", $title = null, $options = []);
    
        }

        $log = new Log;
        $log->userId = auth()->user()->id;
        $log->description = 'A form has been published';
        $log->save();
        
        return redirect('/forms');

    }

    public function paginate(Request $request){
        $this->connection();
        $this->schedule();
        $this->auth();
        
        $paginate = Form::withTrashed()->find($request->id);
        $paginate->pagination = $request->value;
        $paginate->save();

        $log = new Log;
        $log->userId = auth()->user()->id;
        $log->description = 'A form has been updated';
        $log->save();
    }

    public function restore($id){
        $this->connection();
        $this->auth();
        $this->schedule();

        Form::onlyTrashed()->find($id)->restore();
        $bodies = Body::onlyTrashed()->where('formId', $id)->get();
        foreach($bodies as $body){
            $contents = Content::onlyTrashed()->where('bodyId', $body->id)->get();
            foreach($contents as $content){
                $entries = Entry::onlyTrashed()->where('contentId', $content->id)->restore();
                $content->restore();
            }
            $body->restore();
        }
        $elements = Element::onlyTrashed()->where('formId', $id)->get();
        foreach($elements as $element){
            $sub = SubElement::onlyTrashed()->where('elementId', $element->id)->restore();
            $element->restore();
        }
        Feedback::onlyTrashed()->where('formId', $id)->restore();
        FeedbackResult::onlyTrashed()->where('formId', $id)->restore();
        FormProgress::onlyTrashed()->where('formId', $id)->restore();
        FormRelationship::onlyTrashed()->where('formId', $id)->restore();
        FormRestriction::onlyTrashed()->where('formId', $id)->restore();
        Toastr::success("Form successfully restored!", $title = null, $options = []);

        $log = new Log;
        $log->userId = auth()->user()->id;
        $log->description = 'A form has been restored';
        $log->save();

        return back();
    }

}