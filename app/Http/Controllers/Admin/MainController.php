<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\FormTrait;
use App\Traits\InternetTrait;
use App\Traits\AuthTrait;
use App\Notifications\FormNotification;

use App\Student;
use App\Faculty;
use App\Element;
use App\SubElement;
use App\Entry;
use App\FormRelationship;
use App\ClassLoad;
use App\COR;
use App\Content;
use App\Body;
use App\EntryTotal;
use App\FormProgress;
use App\Form;
use App\FormRestriction;
use App\User;
use Carbon;
use DB;
use Response;
use Zip;
use Session;

class MainController extends Controller
{
    use FormTrait, InternetTrait, AuthTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { 
        $this->connection();
        $this->schedule();
        $this->auth();

        $programs = Student::distinct()->first(['program']);
        
        if(!empty($programs)){
            $blocks = [];
            $students = Student::where('program', $programs->program)->get();
        
            foreach($students as $student){
                array_push($blocks, $student->studentNo);
            }
            
            $cor = COR::whereIn('studentNo', $blocks)->first();
            $section = $cor->section;
            $code = $cor->code;
            $programs = $programs->program;
        }else{
            $section = '';
            $code = '';
        }

        $form = Form::where('status', 'active')->orderBy('created_at', 'asc')->first();
        if(!empty($form)){
            $form = $form->id;
        }else{
            $form = '';
        }

        $faculty = Faculty::distinct()->first(['department']);
        if(!empty($faculty)){
            $faculty = $faculty->department;
        }else{
            $faculty = '';
        }

        return view('admin.dashboard')->with('program', $programs)->with('setForm', $form)->with('getSection', $section)->with('getCode', $code)->with('formId', $form)->with('getDepartment', $faculty);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function departmentRate(Request $request){
        $this->connection();
        $this->schedule();
        $this->auth();

        $depts = [];
        $rate = [];

        $departments = Faculty::distinct()->get(['department']);

        foreach($departments as $department){

            $perDept = [];

            $faculties = Faculty::where('department', $department->department)->get();

            foreach($faculties as $faculty){

                $report = 0;

                $elements = Element::where('type', $faculty->type)->get();

                foreach($elements as $element){

                    $subs = SubElement::where('elementId', $element->id)->get();
                    
                    if(count($subs) > 0){
                        $totalSub = 0;
                        foreach($subs as $sub){
                            $subRate = [];
                            
                            $relations = FormRelationship::where('formId', $element->formId)->where('evaluatee', $faculty->type)->get();
                            foreach($relations as $relation){
    
                                $score = 0;
    
                                if($relation->status == 'Distinct'){
                                    $evaluators = Faculty::where('type', $relation->evaluator)->where('department', $faculty->department)->get();
                                }else{
                                    $evaluators = Faculty::where('type', $relation->evaluator)->get();
                                }
    
                                foreach($evaluators as $evaluator){
                                
                                    if($sub->type == $evaluator->type){
                                        
                                        $formProgress = FormProgress::where('formId', $element->formId)
                                                                            ->where('evaluatee', $faculty->userId)
                                                                            ->where('evaluator', $evaluator->userId)
                                                                            ->where('year', $request->year)
                                                                            ->where('term', $request->term)
                                                                            ->where('status', 'completed')->first();
                                        if(!empty($formProgress)){
                                            $score = $score + $formProgress->total;
                                        }
                                        
                                        array_push($subRate, $score);
    
                                    }
                                    
                                }
                                
                            } 
                            if(array_sum($subRate) == 0 && count($subRate) == 0){
                                $total = 0;
                            }else{
                                $total = array_sum($subRate) / count($subRate);
                            }
                            $totalSub = $totalSub + ($total*($sub->rate/100));
                        }
                        $report = $report + $totalSub;
                    }else{

                        if($element->selfEvaluation == '1'){
                            $progress = FormProgress::select(DB::raw('SUM(total) as total'))
                                                    ->where('formId', $element->formId)
                                                    ->where('status', 'completed')
                                                    ->where('evaluatee', $faculty->userId)
                                                    ->where('evaluator', $faculty->userId)
                                                    ->where('year', $request->year)
                                                    ->where('term', $request->term)->first();

                            $count = FormProgress::where('formId', $element->formId)
                                                    ->where('status', 'completed')
                                                    ->where('evaluatee', $faculty->userId)
                                                    ->where('evaluator', $faculty->userId)
                                                    ->where('year', $request->year)
                                                    ->where('term', $request->term)->get();
                        }else{
                            $progress = FormProgress::select(DB::raw('SUM(total) as total'))
                                                    ->where('formId', $element->formId)
                                                    ->where('status', 'completed')
                                                    ->where('evaluatee', $faculty->userId)
                                                    ->where('evaluator', '!=', $faculty->userId)
                                                    ->where('year', $request->year)
                                                    ->where('term', $request->term)->first();

                            $count = FormProgress::where('formId', $element->formId)
                                                    ->where('status', 'completed')
                                                    ->where('evaluatee', $faculty->userId)
                                                    ->where('evaluator', '!=', $faculty->userId)
                                                    ->where('year', $request->year)
                                                    ->where('term', $request->term)->get();
                        }

                        if(!empty($progress) && count($count) > 0){
                            
                            $report = $report + (($progress->total/count($count))*($element->rate/100));
                            
                        }

                    }
                    
                }
                array_push($perDept, $report);
            }

            array_push($depts, $department->department);
            array_push($rate, (round(array_sum($perDept)/count($perDept))));
        }

        return response()->json(['departments' => $depts, 'rate' => $rate]);

    }

    public function selectSection(Request $request){
        $this->connection();
        $this->schedule();
        $this->auth();

        return view('admin.section_progress', ['program' => $request->value])->render();
    }

    public function chooseSection(Request $request){
        $this->connection();
        $this->schedule();
        $this->auth();

        return view('admin.choose_section', ['program' => $request->value])->render();
    }

    public function setStudents(Request $request){
        $this->connection();
        $this->schedule();
        $this->auth();

        return view('admin.schedule', ['getSection' => $request->section])->render();
    }

    public function setSchedules(Request $request){
        $this->connection();
        $this->schedule();
        $this->auth();

        return view('admin.load_students', ['program' => $request->program, 'getSection' => $request->section, 'getCode' => $request->schedule, 'formId' => $request->form])->render();
    }

    public function setFaculty(Request $request){
        $this->connection();
        $this->schedule();
        $this->auth();
        
        return view('admin.faculties', ['getDepartment' => $request->department, 'setForm' => $request->form])->render();
    }

    public function getSession(){
        return Session::get('type');
    }

    public function export(){

        // $filename = 'mean.csv';
        // $handle = fopen($filename, 'w+');
        // fputcsv($handle, array('Respondents','FUNCTIONAL COMPLETENESS', 'PERFORMANCE EFFICIENCY', 'COMPATIBILITY', 'USABILITY', 'RELIABILITY', 'SECURITY', 'MAINTAINABILITY', 'PORTABILITY'));
        // for($x = 1; $x < 51; $x++){
        //     fputcsv($handle, array($x, mt_rand(3.29,4.67)/10, mt_rand(3.29,4.67)/10, mt_rand(3.29,4.67)/10, mt_rand(3.29,4.67)/10, mt_rand(3.29,4.67)/10, mt_rand(3.29,4.67)/10, mt_rand(3.29,4.67)/10, mt_rand(3.29,4.67)/10));
        // }
        // fclose($handle);
        // $headers = array(
        //     'Content-Type' => 'text/csv',
        // );
        // return Response::download($filename, 'mean.csv', $headers);
        
        // $filename = 'template.csv';
        // $handle = fopen($filename, 'w+');
        // fputcsv($handle, array('Form','Body', 'Content'));
        // $forms = Form::all();
        // foreach($forms as $form){
        //     $bodies = Body::where('formId', $form->id)->get();
        //     foreach($bodies as $body){
        //         $contents = Content::where('bodyId', $body->id)->where('type', 'question')->get();
        //         foreach($contents as $content){
        //             fputcsv($handle, array($form->id, $body->id, $content->id));
        //         }
        //     }
        // }
        // fclose($handle);
        // $headers = array(
        //     'Content-Type' => 'text/csv',
        // );
        // return Response::download($filename, 'template.csv', $headers);

        // $zip = Zip::create('file.zip');

        // $blocks = COR::distinct()->get(['section']);

        // foreach($blocks as $block){

        //     $filename = $block->section . '.csv';
        //     $handle = fopen($filename, 'w+');
        //     fputcsv($handle, array('Professor','Question', 'Result', 'Section', 'evaluatee', 'evaluator', 'contentId'));

        //     $faculties = Faculty::where('department', 'Information Technology')->get();

        //     foreach($faculties as $faculty){

        //         $load = ClassLoad::where('facultyNo', $faculty->facultyNo)->where('section', $block->section)->get();

        //         foreach($load as $class){

        //             $cor = COR::where('section', $block->section)->where('schedule', $class->schedule)->where('code', $class->code)->where('room', $class->room)->take(10)->get();

        //             foreach($cor as $reg){

        //                 $explode = explode(' ', $reg->subject);

        //                 if(last($explode) != 'Laboratory'){ 
        //                     if($reg->student->yearLvl == '4th Year'){
        //                         $progress = FormProgress::where('formId', 1)->where('evaluatee', $class->faculty->userId)->where('evaluator', $reg->student->userId)->first();
        //                         $entries = Entry::where('formProgressId', $progress->id)->get();

        //                         foreach($entries as $entry){
        
        //                             fputcsv($handle, array($class->faculty->user->name, $entry->content->content, $entry->result, $block->section, $class->faculty->userId, $reg->student->userId, $entry->contentId));
        
        //                         }

        //                     }

        //                 }

        //             }
                    
        //         }

        //     }

        //     fclose($handle);
        //     $zip->add('/storage/csv/'.$block->section . '.csv');

        // }

    }

}
