<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\FormTrait;
use App\Traits\ActivationKeyTrait;
use App\Traits\InternetTrait;
use App\Traits\AuthTrait;

use App\User;
use App\Log;

use Hash;
use Storage;
use Toastr;
use Session;

class AdminsController extends Controller
{
    use FormTrait, ActivationKeyTrait, InternetTrait, AuthTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(User $user)
    {
        $this->connection();
        $this->auth();
        $this->schedule();
        $this->authorize('view', $user);

        $user = User::withTrashed()->where('isAdmin', 1)->orWhere('isSuperAdmin', 1)->get();
        return view('admin.admins.index')->with('users', $user);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, User $user)
    {
        $this->connection();
        $this->auth();
        $this->schedule();
        $this->authorize('view', $user);

        try{
            $this->validate($request, [
                'img' => 'image|nullable|max:1999'
            ]);
        }catch(\Exception $e){
            Toastr::warning("Image file size is too big", $title = null, $options = []);
            return back();
        }

        $email = User::withTrashed()->where('email', $request->email)->first();
        $username = User::withTrashed()->where('username', $request->username)->first();
        
        if(empty($email) && empty($username)){
            $user = new User;
            $user->username = $request->username;
            $user->name = $request->name;
            $user->email = $request->email;
            $user->password = Hash::make($request->password);
            if(isset($request->isSuperAdmin)){
                $user->isSuperAdmin = 1;
            }else{
                $user->isAdmin = 1;
            }
    
            if($request->hasFile('img')){
                // get file name with the extension
                $filenameWithExt = $request->file('img')->getClientOriginalName();
                // get just filename
                $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
                // get just ext
                $extension = $request->file('img')->getClientOriginalExtension();
                // filename to store
                $filenameToStore = $filename.'_'.time().'.'.$extension;
                // upload image
                $path = $request->file('img')->storeAs('public/profiles', $filenameToStore);
    
                $user->image = $filenameToStore;
            }
    
            $user->save();
    
            $this->queueActivationKeyNotification($user);

            $log = new Log;
            $log->userId = auth()->user()->id;
            $log->description = 'New admin user has been created';
            $log->save();

            Toastr::success("User successfully created!", $title = null, $options = []);
        }elseif(!empty($email)){
            Toastr::error("Email already exist!", $title = null, $options = []);
        }elseif(!empty($username)){
            Toastr::error("Username already exist!", $title = null, $options = []);
        }

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, User $user)
    {
        $this->connection();
        $this->auth();
        $this->schedule();
        $this->authorize('view', $user);

        User::onlyTrashed()->find($id)->restore();
        Toastr::success("User successfully restored!", $title = null, $options = []);
        $log = new Log;
        $log->userId = auth()->user()->id;
        $log->description = 'An admin user has been restored';
        $log->save();
        return back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id, User $user)
    {
        $this->connection();
        $this->auth();
        $this->schedule();

        $this->authorize('view', $user);

        try{
            $this->validate($request, [
                'img' => 'image|nullable|max:1999'
            ]);
        }catch(\Exception $e){
            Toastr::warning("Image file size is too big", $title = null, $options = []);
            return back();
        }

        $validate = User::where('username', $request->username)->where('id', '!=', $id)->get();
        if(count($validate) > 0){
            Toastr::warning("Username already exist!", $title = null, $options = []);
            return back();
        }

        $validate = User::where('email', $request->email)->where('id', '!=', $id)->get();
        if(count($validate) > 0 && !empty($request->email)){
            Toastr::warning("Email already exist!", $title = null, $options = []);
            return back();
        }
        $willSent = 1;
        $user = User::withTrashed()->find($id);
        $user->name = $request->name;
        if($user->email == $request->email){
            $willSent = 0;
        }
        $user->email = $request->email;
        $user->username = $request->username;
        
        if(!empty($request->password)){
            $user->password = Hash::make($request->password);
        }

        if($request->hasFile('img')){
            
            if($request->image != 'default_profile.jpg'){
                Storage::delete('public/profiles/'.$user->image);
            }

            // get file name with the extension
            $filenameWithExt = $request->file('img')->getClientOriginalName();
            // get just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            // get just ext
            $extension = $request->file('img')->getClientOriginalExtension();
            // filename to store
            $filenameToStore = $filename.'_'.time().'.'.$extension;
            // upload image
            $path = $request->file('img')->storeAs('public/profiles', $filenameToStore);

            $user->image = $filenameToStore;
        }else{
            $user->image = $request->image;
        }

        $log = new Log;
        $log->userId = auth()->user()->id;
        $log->description = 'An admin user has been updated';
        $log->save();

        $user->save();

        if($willSent == 1){
            $user = User::withTrashed()->find($id);
            $user->activated = 0;
            $user->save();
            $this->queueActivationKeyNotification($user);
            if(auth()->user()->id == $id){
                return redirect('/logout');
            }
        }

        Toastr::success("User successfully updated!", $title = null, $options = []);

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id, User $user)
    {
        $this->connection();
        $this->auth();
        $this->schedule();
        $this->authorize('view', $user);

        if(isset($request->force)){
            User::onlyTrashed()->find($id)->forcedelete();
            $log = new Log;
            $log->userId = auth()->user()->id;
            $log->description = 'An admin user has been permanently deleted';
            $log->save();
        }else{
            User::find($id)->delete();
            $log = new Log;
            $log->userId = auth()->user()->id;
            $log->description = 'An admin user has been temporarily deleted';
            $log->save();
        }

        Toastr::success("User successfully deleted!", $title = null, $options = []);
        return back();
    }

}
