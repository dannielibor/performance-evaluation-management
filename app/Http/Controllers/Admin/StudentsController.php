<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\FormTrait;
use App\Traits\ActivationKeyTrait;
use App\Traits\InternetTrait;
use App\Traits\AuthTrait;

use App\Student;
use App\User;
use App\COR;
use App\Entry;
use App\FormRelationship;
use App\Body;
use App\Content;
use App\Log;

use Hash;
use Toastr;
use Session;
use Storage;

class StudentsController extends Controller
{
    use FormTrait, ActivationKeyTrait, InternetTrait, AuthTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->connection();
        $this->schedule();
        $this->auth();

        $student = Student::all();
        $programs = Student::distinct()->get(['program']);
        return view('admin.students.index')->with('students', $student)->with('programs', $programs);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->connection();
        $this->schedule();
        $this->auth();

        return view('admin.students.store');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->connection();
        $this->schedule();
        $this->auth();

        if(Session::get('type') == 'Super Admin'){
            $user = User::withTrashed()->find($id);
            $year = Student::withTrashed()->select('yearLvl')->distinct()->orderBy('yearLvl', 'asc')->get();
            $program = Student::withTrashed()->select('program')->distinct()->orderBy('program', 'asc')->get();
        }else{
            $user = User::find($id);
            $year = Student::select('yearLvl')->distinct()->orderBy('yearLvl', 'asc')->get();
            $program = Student::select('program')->distinct()->orderBy('program', 'asc')->get();
        }

        return view('admin.students.edit')->with('user', $user)->with('program', $program)->with('year', $year);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->connection();
        $this->schedule();
        $this->auth();

        try{
            $this->validate($request, [
                'img' => 'image|nullable|max:1999'
            ]);
        }catch(\Exception $e){
            Toastr::warning("Image file size is too big", $title = null, $options = []);
            return back();
        }

        $validate = User::where('username', $request->username)->where('id', '!=', $id)->get();
        if(count($validate) > 0){
            Toastr::warning("Username already exist!", $title = null, $options = []);
            return back();
        }

        $validate = User::withTrashed()->where('email', $request->email)->where('id', '!=', $id)->get();
        if(count($validate) > 0 && !empty($request->email)){
            Toastr::warning("Email already exist!", $title = null, $options = []);
            return back();
        }
        $willSent = 1;
        $user = User::withTrashed()->find($id);
        $user->name = $request->name;
        $user->username = $request->username;
        if($user->email == $request->email){
            $willSent = 0;
        }
        $user->email = $request->email;
        
        if(!empty($request->password)){
            $user->password = Hash::make($request->password);
        }

        if($request->hasFile('img')){
            
            if($request->image != 'default_profile.jpg'){
                Storage::delete('public/profiles/'.$user->image);
            }

            // get file name with the extension
            $filenameWithExt = $request->file('img')->getClientOriginalName();
            // get just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            // get just ext
            $extension = $request->file('img')->getClientOriginalExtension();
            // filename to store
            $filenameToStore = $filename.'_'.time().'.'.$extension;
            // upload image
            $path = $request->file('img')->storeAs('public/profiles', $filenameToStore);

            $user->image = $filenameToStore;
        }else{
            $user->image = $request->image;
        }

        $log = new Log;
        $log->userId = auth()->user()->id;
        $log->description = 'A student has been updated';
        $log->save();

        $user->save();

        $student = Student::withTrashed()->where('userId', $id)->first();
        $student->studentNo = $request->studentNo;
        $student->gender = $request->gender;
        $student->yearLvl = $request->yearLvl;
        $student->program = $request->program;
        $student->save();

        if($willSent == 1){
            $user = User::withTrashed()->find($id);
            $user->activated = 0;
            $user->save();
            $this->queueActivationKeyNotification($user);
        }

        Toastr::success("Student successfully updated!", $title = null, $options = []);
        return redirect('/students');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->connection();
        $this->schedule();
        $this->auth();

        $user = User::find($id)->delete();
        $student = Student::where('userId', $id)->first();
        $cor = COR::where('studentNo', $student->studentNo)->delete();
        $student->delete();

        Toastr::success("Student successfully deleted!", $title = null, $options = []);
        $log = new Log;
        $log->userId = auth()->user()->id;
        $log->description = 'A student has been temporarily deleted';
        $log->save();
        return back();
    }

    public function load(){
        $this->connection();
        $this->schedule();
        $this->auth();
        
        if(Session::get('type') == 'Super Admin'){
            $students = Student::withTrashed()->get();
            $array = [];
    
            for($y = 0; $y < count($students); $y++){
                
                if($students[$y]->trashed()){
                    array_push($array, array('id' => $students[$y]->userTrashed->id, 'name' => $students[$y]->userTrashed->name, 
                    'studentNo' => $students[$y]->studentNo, 'program' => $students[$y]->program, 'status' => 'Soft Deleted', 'img' => $students[$y]->userTrashed->image, 'children' => array()));
        
                    $cor = COR::onlyTrashed()->where('studentNo', $students[$y]->studentNo)->get();
                    
                    for($x = 0; $x < count($cor); $x++){
                        array_push($array[$y]['children'], array('Code' => $cor[$x]->code, 'Subject' => $cor[$x]->subject, 'Section' => $cor[$x]->section, 'Schedule' => $cor[$x]->schedule, 'Room' => $cor[$x]->room));
                    }
                }else{
                    array_push($array, array('id' => $students[$y]->user->id, 'name' => $students[$y]->user->name, 
                    'studentNo' => $students[$y]->studentNo, 'program' => $students[$y]->program, 'status' => $students[$y]->status, 'img' => $students[$y]->user->image, 'children' => array()));
        
                    $cor = COR::where('studentNo', $students[$y]->studentNo)->get();
        
                    for($x = 0; $x < count($cor); $x++){
                        array_push($array[$y]['children'], array('Code' => $cor[$x]->code, 'Subject' => $cor[$x]->subject, 'Section' => $cor[$x]->section, 'Schedule' => $cor[$x]->schedule, 'Room' => $cor[$x]->room));
                    }
                }
                
            }
            return $array;
        }else{
            $students = Student::all();
            $array = [];
    
            for($y = 0; $y < count($students); $y++){
    
                array_push($array, array('id' => $students[$y]->user->id, 'name' => $students[$y]->user->name, 
                'studentNo' => $students[$y]->studentNo, 'program' => $students[$y]->program, 'status' => $students[$y]->status, 'img' => $students[$y]->user->image, 'children' => array()));
    
                $cor = COR::where('studentNo', $students[$y]->studentNo)->get();
    
                for($x = 0; $x < count($cor); $x++){
                    array_push($array[$y]['children'], array('Code' => $cor[$x]->code, 'Subject' => $cor[$x]->subject, 'Section' => $cor[$x]->section, 'Schedule' => $cor[$x]->schedule, 'Room' => $cor[$x]->room));
                }
            }
            return $array;
        }
    }

    public function forcedelete(Request $request){
        $user = User::onlyTrashed()->find($request->id);
        $student = Student::onlyTrashed()->where('userId', $user->id)->first();
        $cor = COR::onlyTrashed()->where('studentNo', $student->studentNo)->forcedelete();
        $student->forcedelete();
        $user->forcedelete();
        Toastr::success("Student successfully deleted!", $title = null, $options = []);
        $log = new Log;
        $log->userId = auth()->user()->id;
        $log->description = 'A student has been permanently deleted';
        $log->save();
        return back();
    }

    public function restore(Request $request){
        $user = User::onlyTrashed()->find($request->id)->restore();
        $student = Student::onlyTrashed()->where('userId', $request->id)->restore();
        $student = Student::where('userId', $request->id)->first();
        $cor = COR::onlyTrashed()->where('studentNo', $student->studentNo)->restore();
        Toastr::success("Student successfully restored!", $title = null, $options = []);
        $log = new Log;
        $log->userId = auth()->user()->id;
        $log->description = 'A student has been restored';
        $log->save();
        return back();
    }

    public function twoFa(Request $request){
        $this->connection();
        $this->schedule();
        $this->auth();

        $user = User::find($request->id);
        if($user->twoFA == 1){
            $user->twoFA = 0;
        }else{
            $user->twoFA = 1;
        }
        $user->save();
    }

}
