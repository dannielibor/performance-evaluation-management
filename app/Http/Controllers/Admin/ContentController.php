<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\FormTrait;
use App\Traits\InternetTrait;
use App\Traits\AuthTrait;

use App\Content;
use App\Entry;
use App\Log;

class ContentController extends Controller
{
    use FormTrait, InternetTrait, AuthTrait;

    public function store(Request $request){
        $this->connection();
        $this->auth();
        $this->schedule();

        $position = Content::withTrashed()->where('bodyId', $request->id)->orderByRaw('LENGTH(position)', 'desc')->orderBy('position', 'desc')->first();
        $content = new Content;
        $content->bodyId = $request->id;
        $content->type = $request->type;
        if(!empty($position)){
            $content->position = ++$position->position;
        }
        $content->save();

        $sort = null;

        $sortable = Content::withTrashed()->where('bodyId', $request->id)->where('type', 'header')->orderByRaw('LENGTH(position)', 'asc')->orderBy('position', 'asc')->get();

        for($y = 0; $y < count($sortable); $y++){
            $sortable[$y]->sort = $y+1;
            $sortable[$y]->save();
            $sort = $sortable[$y]->sort;
        }

        $log = new Log;
        $log->userId = auth()->user()->id;
        $log->description = 'A form has been updated';
        $log->save();

        return response()->json([ 'id' => $content->id, 'sort' => $sort ]);
    }

    public function destroy(Request $request){
        $this->connection();
        $this->auth();
        $this->schedule();

        Content::withTrashed()->find($request->id)->forcedelete();

        $sortable = Content::withTrashed()->where('bodyId', $request->base)->where('type', 'header')->orderByRaw('LENGTH(position)', 'asc')->orderBy('position', 'asc')->get();

        for($y = 0; $y < count($sortable); $y++){
            $sortable[$y]->sort = $y+1;
            $sortable[$y]->save();
        }

        $log = new Log;
        $log->userId = auth()->user()->id;
        $log->description = 'A form has been updated';
        $log->save();
    }

    public function update(Request $request){
        $this->connection();
        $this->auth();
        $this->schedule();

        $update = Content::withTrashed()->find($request->id);
        $update->position = $request->position;
        $update->save();

        $sortable = Content::withTrashed()->where('bodyId', $update->bodyId)->where('type', 'header')->orderByRaw('LENGTH(position)', 'asc')->orderBy('position', 'asc')->get();

        for($y = 0; $y < count($sortable); $y++){
            $sortable[$y]->sort = $y+1;
            $sortable[$y]->save();
        }

        $log = new Log;
        $log->userId = auth()->user()->id;
        $log->description = 'A form has been updated';
        $log->save();
    }

    public function edit(Request $request){
        $this->connection();
        $this->auth();
        $this->schedule();

        $edit = Content::withTrashed()->find($request->id);
        $edit->content = $request->value;
        $edit->save();

        $log = new Log;
        $log->userId = auth()->user()->id;
        $log->description = 'A form has been updated';
        $log->save();
    }
}