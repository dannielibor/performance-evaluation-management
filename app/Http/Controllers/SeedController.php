<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\Student;
use App\Faculty;
use App\COR;
use App\ClassLoad;
use App\Form;
use App\FormRestriction;
use App\FormProgress;
use App\FormRelationship;
use App\Element;
use App\SubElement;
use App\Feedback;
use App\FeedbackResult;
use App\Body;
use App\Content;
use App\TwoAuth;
use App\Log;
use App\Entry;
use App\EmailNotification;
use Hash;

class SeedController extends Controller
{
    public function index(){

        ini_set('max_execution_time', 0);

        $new = new User;
        $new->username = "s-admin";
        $new->name = "s-admin";
        $new->email = "superadmin@gmail.com";
        $new->password = Hash::make("admin");
        $new->activated = 1;
        $new->isSuperAdmin = 1;
        $new->twoFA = 0;
        $new->save();

        $new = new User;
        $new->username = "admin";
        $new->name = "admin";
        $new->email = "admin@gmail.com";
        $new->password = Hash::make("admin");
        $new->activated = 1;
        $new->isAdmin = 1;
        $new->twoFA = 0;
        $new->save();

        $csvData = file_get_contents(public_path() . '/storage/csv/cor.csv');
        $rows = array_map('str_getcsv', explode("\n", $csvData));

        for ($i = 1; $i < count($rows)-1; $i++){

            $cor = new COR;
            $cor->studentNo = $rows[$i][0];
            $cor->code = $rows[$i][5];
            $cor->subject = $rows[$i][6];
            $cor->section = $rows[$i][7];
            $cor->schedule = $rows[$i][8];
            $cor->room = $rows[$i][9];
            $cor->year = '2018-2019';
            $cor->term = '2nd';
            $cor->save();

            $validate = User::where('username', $rows[$i][0])->first();

            if(empty($validate)){

                $split = explode(',', $rows[$i][1]);
                $user = new User;
                $user->name = $split[1]. ' ' . $split[0];
                $user->username = $rows[$i][0];
                $user->password = Hash::make($rows[$i][0]);
                $user->save();

            }

            $validate = Student::where('studentNo', $rows[$i][0])->first();

            if(empty($validate)){

                $user = User::where('username', $rows[$i][0])->first();

                $student = new Student;
                $student->userId = $user->id;
                $student->studentNo = $rows[$i][0];
                $student->gender = $rows[$i][2];
                $student->program = $rows[$i][3];
                $student->yearLvl = $rows[$i][4];
                $student->save();

            }

        }

        $csvData = file_get_contents(public_path() . '/storage/csv/classload.csv');
        $rows = array_map('str_getcsv', explode("\n", $csvData));

        for ($i = 1; $i < count($rows)-1; $i++){

            $class = new ClassLoad;
            $class->facultyNo = $rows[$i][0];
            $class->code = $rows[$i][2];
            $class->subject = $rows[$i][3];
            $class->section = $rows[$i][4];
            $class->schedule = $rows[$i][5];
            $class->size = $rows[$i][7];
            $class->room = $rows[$i][6];
            $class->year = '2018-2019';
            $class->term = '2nd';
            $class->save();

            $validate = User::where('username', $rows[$i][0])->first();

            if(empty($validate)){

                $split = explode(',', $rows[$i][1]);
                $user = new User;
                $user->name = $split[1]. ' ' . $split[0];
                $user->username = $rows[$i][0];
                $user->activated = 0;
                $user->password = Hash::make($rows[$i][0]);
                $user->save();

            }

            $validate = Faculty::where('facultyNo', $rows[$i][0])->first();

            if(empty($validate)){

                $user = User::where('username', $rows[$i][0])->first();

                $faculty = new Faculty;
                $faculty->userId = $user->id;
                $faculty->facultyNo = $rows[$i][0];
                $faculty->department = $rows[$i][8];
                $faculty->save();

            }

        }

        $type = Faculty::where('facultyNo', '2012102')->first();
        if(!empty($type)){
            $type->type = 'Department Chair';
            $type->save();
        }

        $type = Faculty::where('facultyNo', '2011116')->first();
        if(!empty($type)){
            $type->type = 'Vice Dean';
            $type->save();
        }

        $type = Faculty::where('facultyNo', '20152160')->first();
        if(!empty($type)){
            $type->type = 'Dean';
            $type->save();
        }

        $new = new Form;
        $new->title = 'Student Evaluation';
        $new->status = 'active';
        $new->from = '2019-03-15';
        $new->to = '2019-04-15';
        $new->pagination = '3';
        $new->save();

        $new = new Form;
        $new->title = 'Faculty Performance Appraisal';
        $new->status = 'active';
        $new->from = '2019-03-15';
        $new->to = '2019-04-15';
        $new->pagination = '3';
        $new->save();

        $new = new Form;
        $new->title = 'Classroom Observation';
        $new->status = 'active';
        $new->from = '2019-03-15';
        $new->to = '2019-04-15';
        $new->pagination = '3';
        $new->save();

                /* student evaluation */
                $new = new Body;
                $new->formId = 1;
                $new->sort = 'A';
                $new->header = 'METHODS AND STRATEGIES OF TEACHING';
                $new->position = '0';
                $new->rate = 40;
                $new->save();
        
                $new = new Body;
                $new->formId = 1;
                $new->sort = 'B';
                $new->header = 'MASTERY OF THE SUBJECT MATTER';
                $new->position = '1';
                $new->rate = 20;
                $new->save();
        
                $new = new Body;
                $new->formId = 1;
                $new->sort = 'C';
                $new->header = 'COMMUNICATION SKILLS';
                $new->position = '2';
                $new->rate = 15;
                $new->save();
        
                $new = new Body;
                $new->formId = 1;
                $new->sort = 'D';
                $new->header = 'CLASSROOM MANAGEMENT';
                $new->position = '3';
                $new->rate = 15;
                $new->save();
        
                $new = new Body;
                $new->formId = 1;
                $new->sort = 'E';
                $new->header = 'PERSONAL AND SOCIAL TRAITS';
                $new->position = '4';
                $new->rate = 10;
                $new->save();
        
                /* appraisal */
        
                $new = new Body;
                $new->formId = 2;
                $new->sort = 'A';
                $new->header = 'PROFESSIONAL RESPONSIBILITIES';
                $new->position = '0';
                $new->rate = 70;
                $new->save();
        
                $new = new Body;
                $new->formId = 2;
                $new->sort = 'B';
                $new->header = 'PROFESSIONAL RELATIONSHIPS';
                $new->position = '1';
                $new->rate = 20;
                $new->save();
        
                $new = new Body;
                $new->formId = 2;
                $new->sort = 'C';
                $new->header = 'PERSONAL QUALITIES';
                $new->position = '2';
                $new->rate = 10;
                $new->save();
        
                /* classroom observation */
        
                $new = new Body;
                $new->formId = 3;
                $new->sort = 'A';
                $new->header = 'Teacher';
                $new->position = '0';
                $new->save();
        
                $new = new Body;
                $new->formId = 3;
                $new->sort = 'B';
                $new->header = 'Teaching Procedure';
                $new->position = '1';
                $new->save();
        
                $new = new Body;
                $new->formId = 3;
                $new->sort = 'C';
                $new->header = 'Students';
                $new->position = '2';
                $new->save();
        
                $new = new Body;
                $new->formId = 3;
                $new->sort = 'D';
                $new->header = 'General Observation';
                $new->position = '3';
                $new->save();

                /* student evaluation */
                $new = new Content;
                $new->bodyId = 1;
                $new->type = 'question';
                $new->position = 0;
                $new->content = '1. Starts the lesson in an interesting manner.';
                $new->save();
        
                $new = new Content;
                $new->bodyId = 1;
                $new->type = 'question';
                $new->position = 1;
                $new->content = '2. Clarifies the objective/s of the lesson.';
                $new->save();
        
                $new = new Content;
                $new->bodyId = 1;
                $new->type = 'question';
                $new->position = 2;
                $new->content = '3. Makes the lesson practical and relevant by relating it with everyday living.';
                $new->save();
        
                $new = new Content;
                $new->bodyId = 1;
                $new->type = 'question';
                $new->position = 3;
                $new->content = '4. Relates the lesson with current issues and other subject areas whenever possible.';
                $new->save();
        
                $new = new Content;
                $new->bodyId = 1;
                $new->type = 'question';
                $new->position = 4;
                $new->content = "5. Facilitates student's understanding by rephrasing questions using synonyms, giving examples and the like.";
                $new->save();
        
                $new = new Content;
                $new->bodyId = 1;
                $new->type = 'question';
                $new->position = 5;
                $new->content = "6. Formulates questions that challenge the student's imagination or reasoning ability.";
                $new->save();
        
                $new = new Content;
                $new->bodyId = 1;
                $new->type = 'question';
                $new->position = 6;
                $new->content = "7. Gives student's learning tasks; exercises related to the topics learned/being learned.";
                $new->save();
        
                $new = new Content;
                $new->bodyId = 1;
                $new->type = 'question';
                $new->position = 7;
                $new->content = "8. Uses instructional time productively by properly apportioning it to all class activities.";
                $new->save();
        
                $new = new Content;
                $new->bodyId = 1;
                $new->type = 'question';
                $new->position = 8;
                $new->content = "9. Encourages maximum participation of the students in all activities.";
                $new->save();
        
                $new = new Content;
                $new->bodyId = 1;
                $new->type = 'question';
                $new->position = 9;
                $new->content = "10. Makes use of the whiteboard and other audiovisual materials like pictures, graphs, and outlines to make the lesson more interesting.";
                $new->save();
        
                $new = new Content;
                $new->bodyId = 1;
                $new->type = 'question';
                $new->position = 10;
                $new->content = "11. Provides students with more opportunities for learning by giving assignments, research work, and the like which could be accomplished.";
                $new->save();
        
                $new = new Content;
                $new->bodyId = 1;
                $new->type = 'question';
                $new->position = 11;
                $new->content = "12. Encourages students to ask intelligent and relevant questions.";
                $new->save();
        
                $new = new Content;
                $new->bodyId = 1;
                $new->type = 'question';
                $new->position = 12;
                $new->content = "13. Establishes continuity and unity of present and previous lessons. Summarizes the lesson to bring out important points.";
                $new->save();
        
                $new = new Content;
                $new->bodyId = 1;
                $new->type = 'question';
                $new->position = 13;
                $new->content = "14. Evaluates students performance by giving quizzes from time to time.";
                $new->save();
        
                $new = new Content;
                $new->bodyId = 1;
                $new->type = 'question';
                $new->position = 14;
                $new->content = "15. Informs students of test results not later than two weeks after the exam.";
                $new->save();
        
                $new = new Content;
                $new->bodyId = 1;
                $new->type = 'question';
                $new->position = 15;
                $new->content = "16. Explains the grading system and gives grades fairly.";
                $new->save();
        
                $new = new Content;
                $new->bodyId = 2;
                $new->type = 'question';
                $new->position = 0;
                $new->content = "1. Presents the lessons systematically.";
                $new->save();
        
                $new = new Content;
                $new->bodyId = 2;
                $new->type = 'question';
                $new->position = 1;
                $new->content = "2. Explains the lesson/objectives clearly.";
                $new->save();
        
                $new = new Content;
                $new->bodyId = 2;
                $new->type = 'question';
                $new->position = 2;
                $new->content = "3. Answers question of the students intelligently and satisfactorily.";
                $new->save();
        
                $new = new Content;
                $new->bodyId = 3;
                $new->type = 'question';
                $new->position = 0;
                $new->content = "1. Speaks correct English and/or Filipino and communicates his/her ideas well.";
                $new->save();
        
                $new = new Content;
                $new->bodyId = 3;
                $new->type = 'question';
                $new->position = 1;
                $new->content = "2. Has a loud voice enough to be heard by everyone in class.";
                $new->save();
        
                $new = new Content;
                $new->bodyId = 4;
                $new->type = 'question';
                $new->position = 0;
                $new->content = "1. Sees to it that the room is clean and orderly at all times.";
                $new->save();
        
                $new = new Content;
                $new->bodyId = 4;
                $new->type = 'question';
                $new->position = 1;
                $new->content = "2. Maintains classroom discipline.";
                $new->save();
        
                $new = new Content;
                $new->bodyId = 4;
                $new->type = 'question';
                $new->position = 2;
                $new->content = "3. Checks the attendance of students.";
                $new->save();
        
                $new = new Content;
                $new->bodyId = 4;
                $new->type = 'question';
                $new->position = 3;
                $new->content = "4. Sees to it that the students wear the prescribed uniform.";
                $new->save();
        
                $new = new Content;
                $new->bodyId = 4;
                $new->type = 'question';
                $new->position = 4;
                $new->content = "5. Sees to it that all teaching materials like whiteboard marker and erasers are prepared.";
                $new->save();
        
                $new = new Content;
                $new->bodyId = 4;
                $new->type = 'question';
                $new->position = 5;
                $new->content = "6. Turns off airconditioning units, all lights, and make sure that the whiteboard is clean before dismissing the class.";
                $new->save();
        
                $new = new Content;
                $new->bodyId = 5;
                $new->type = 'question';
                $new->position = 0;
                $new->content = "1. Projects self-confidence; respects the opinions, suggestions, and comments of students.";
                $new->save();
        
                $new = new Content;
                $new->bodyId = 5;
                $new->type = 'question';
                $new->position = 1;
                $new->content = "2. Projects a respectable and a decent image.";
                $new->save();
        
                $new = new Content;
                $new->bodyId = 5;
                $new->type = 'question';
                $new->position = 2;
                $new->content = "3. Practices what he/she preaches/advises.";
                $new->save();
        
                $new = new Content;
                $new->bodyId = 5;
                $new->type = 'question';
                $new->position = 3;
                $new->content = "4. Manifests and promotes the Benedictine charism of work and prayer.";
                $new->save();
        
                $new = new Content;
                $new->bodyId = 5;
                $new->type = 'question';
                $new->position = 4;
                $new->content = "5. Integrates positive values in the lesson.";
                $new->save();
        
                $new = new Content;
                $new->bodyId = 5;
                $new->type = 'question';
                $new->position = 5;
                $new->content = "6. Recognizes students' participation in institutional activities by giving proper considerations.";
                $new->save();
        
                $new = new Content;
                $new->bodyId = 5;
                $new->type = 'question';
                $new->position = 6;
                $new->content = "7. Comes to class on regularly.";
                $new->save();
        
                $new = new Content;
                $new->bodyId = 5;
                $new->type = 'question';
                $new->position = 7;
                $new->content = "8. Comes to class on time.";
                $new->save();
        
                $new = new Content;
                $new->bodyId = 5;
                $new->type = 'question';
                $new->position = 8;
                $new->content = "9. Is approachable and encourages students to consult him/her for assistance.";
                $new->save();
        
                /* appraisal */
        
                $new = new Content;
                $new->bodyId = 6;
                $new->type = 'header';
                $new->position = 0;
                $new->sort = 1;
                $new->content = "Engages in professional growth activities";
                $new->save();
        
                $new = new Content;
                $new->bodyId = 6;
                $new->type = 'question';
                $new->position = 1;
                $new->content = "Attends SBC/CAS seminars, conferences, workshops etc.";
                $new->save();
        
                $new = new Content;
                $new->bodyId = 6;
                $new->type = 'question';
                $new->position = 2;
                $new->content = "Attends off-campus seminars, conferences, workshops etc.";
                $new->save();
        
                $new = new Content;
                $new->bodyId = 6;
                $new->type = 'question';
                $new->position = 3;
                $new->content = "Engages in scholarly research activities.";
                $new->save();
        
                $new = new Content;
                $new->bodyId = 6;
                $new->type = 'header';
                $new->position = 4;
                $new->sort = 2;
                $new->content = "Demonstrates dependability in professional duties";
                $new->save();
        
                $new = new Content;
                $new->bodyId = 6;
                $new->type = 'question';
                $new->position = 5;
                $new->content = "Submits course syllabus that conforms with the CAS curriculum(i.e. form, content)";
                $new->save();
        
                $new = new Content;
                $new->bodyId = 6;
                $new->type = 'question';
                $new->position = 6;
                $new->content = "Submits a well-constructed examination";
                $new->save();
        
                $new = new Content;
                $new->bodyId = 6;
                $new->type = 'question';
                $new->position = 7;
                $new->content = "Submits accurate grades that conforms with CAS standards.";
                $new->save();
        
                $new = new Content;
                $new->bodyId = 6;
                $new->type = 'question';
                $new->position = 8;
                $new->content = "Submits other requirements prescribed by the department.";
                $new->save();
        
                $new = new Content;
                $new->bodyId = 6;
                $new->type = 'header';
                $new->position = 9;
                $new->sort = 3;
                $new->content = "Works cooperatively in bringing about the success of the school program";
                $new->save();
        
                $new = new Content;
                $new->bodyId = 6;
                $new->type = 'question';
                $new->position = 10;
                $new->content = "Cooperates to bring success of the school program(i.e. spiritual, social, outreach, academic)";
                $new->save();
        
                $new = new Content;
                $new->bodyId = 6;
                $new->type = 'question';
                $new->position = 11;
                $new->content = "Takes care and makes optimum use of the physical and materials resources that supports Instructional program";
                $new->save();
        
                $new = new Content;
                $new->bodyId = 6;
                $new->type = 'header';
                $new->position = 12;
                $new->sort = 4;
                $new->content = "Promptness in meeting obligations";
                $new->save();
        
                $new = new Content;
                $new->bodyId = 6;
                $new->type = 'question';
                $new->position = 13;
                $new->content = "Has a good and reasonable attendance record";
                $new->save();
        
                $new = new Content;
                $new->bodyId = 6;
                $new->type = 'question';
                $new->position = 14;
                $new->content = "Reports and leaves classes on time";
                $new->save();
        
                $new = new Content;
                $new->bodyId = 6;
                $new->type = 'question';
                $new->position = 15;
                $new->content = "Reports to classes regularly";
                $new->save();
        
                $new = new Content;
                $new->bodyId = 6;
                $new->type = 'question';
                $new->position = 16;
                $new->content = "Attends meetings convocations and other official school invitations regularly";
                $new->save();
        
                $new = new Content;
                $new->bodyId = 7;
                $new->type = 'header';
                $new->position = 0;
                $new->sort = 1;
                $new->content = "Maintains an effective working relationships with staff";
                $new->save();
        
                $new = new Content;
                $new->bodyId = 7;
                $new->type = 'question';
                $new->position = 1;
                $new->content = "Respects needs and feelings of his/her colleagues";
                $new->save();
        
                $new = new Content;
                $new->bodyId = 7;
                $new->type = 'question';
                $new->position = 2;
                $new->content = "Maintains a positive relationships with all school personnel";
                $new->save();
        
                $new = new Content;
                $new->bodyId = 7;
                $new->type = 'header';
                $new->position = 3;
                $new->sort = 2;
                $new->content = "Maintains a relationships with students that is conductive to learning";
                $new->save();
        
                $new = new Content;
                $new->bodyId = 7;
                $new->type = 'question';
                $new->position = 4;
                $new->content = "Maintains a supportive, positive and professional relationships with students";
                $new->save();
        
                $new = new Content;
                $new->bodyId = 7;
                $new->type = 'question';
                $new->position = 5;
                $new->content = "Exemplifies academic, moral and ethical norms in his/her personal and professional life";
                $new->save();
        
                $new = new Content;
                $new->bodyId = 8;
                $new->type = 'header';
                $new->position = 0;
                $new->sort = 1;
                $new->content = "Health and Vigor";
                $new->save();
        
                $new = new Content;
                $new->bodyId = 8;
                $new->type = 'question';
                $new->position = 1;
                $new->content = "Displays pleasant personality";
                $new->save();
        
                $new = new Content;
                $new->bodyId = 8;
                $new->type = 'question';
                $new->position = 2;
                $new->content = "Displays a sense of humor";
                $new->save();
        
                $new = new Content;
                $new->bodyId = 8;
                $new->type = 'question';
                $new->position = 3;
                $new->content = "Open to suggestions, new ideas and accepts constructive criticisms";
                $new->save();
        
                $new = new Content;
                $new->bodyId = 8;
                $new->type = 'header';
                $new->position = 9;
                $new->sort = 2;
                $new->content = "Competance and Initiative";
                $new->save();
        
                $new = new Content;
                $new->bodyId = 8;
                $new->type = 'question';
                $new->position = 10;
                $new->content = "Exercise tact and objectivity in articulating own ideas and concerns";
                $new->save();
        
                $new = new Content;
                $new->bodyId = 8;
                $new->type = 'question';
                $new->position = 11;
                $new->content = "Contributes ideas, inputs for the good of the department and the school";
                $new->save();
        
                $new = new Content;
                $new->bodyId = 8;
                $new->type = 'header';
                $new->position = 12;
                $new->sort = 3;
                $new->content = "Grooming and appropriateness of Attire";
                $new->save();
        
                $new = new Content;
                $new->bodyId = 8;
                $new->type = 'question';
                $new->position = 13;
                $new->content = "Practice habits of good grooming";
                $new->save();
        
                $new = new Content;
                $new->bodyId = 8;
                $new->type = 'header';
                $new->position = 14;
                $new->sort = 4;
                $new->content = "Work attitudes";
                $new->save();
        
                $new = new Content;
                $new->bodyId = 8;
                $new->type = 'question';
                $new->position = 15;
                $new->content = "Is enthusiastic and shows initiative beyond the call of duty";
                $new->save();
        
                $new = new Content;
                $new->bodyId = 8;
                $new->type = 'question';
                $new->position = 16;
                $new->content = "Accepts leadership and fellowships roles";
                $new->save();
        
                $new = new Content;
                $new->bodyId = 8;
                $new->type = 'question';
                $new->position = 17;
                $new->content = "Upholds the good name and integrity of the school in and out of the school premises";
                $new->save();
        
                /* classroom observation */
                $new = new Content;
                $new->bodyId = 9;
                $new->type = 'question';
                $new->position = 0;
                $new->content = "1. Teaching personality";
                $new->save();
        
                $new = new Content;
                $new->bodyId = 9;
                $new->type = 'question';
                $new->position = 1;
                $new->content = "2. Composure";
                $new->save();
        
                $new = new Content;
                $new->bodyId = 9;
                $new->type = 'question';
                $new->position = 2;
                $new->content = "3. Articulation";
                $new->save();
        
                $new = new Content;
                $new->bodyId = 9;
                $new->type = 'question';
                $new->position = 3;
                $new->content = "4. Modulation of voice";
                $new->save();
        
                $new = new Content;
                $new->bodyId = 9;
                $new->type = 'question';
                $new->position = 4;
                $new->content = "5. Mastery of the medium of instruction";
                $new->save();
        
                $new = new Content;
                $new->bodyId = 9;
                $new->type = 'question';
                $new->position = 5;
                $new->content = "6. Mastery of the subject matter";
                $new->save();
        
                $new = new Content;
                $new->bodyId = 9;
                $new->type = 'question';
                $new->position = 6;
                $new->content = "7. Ability to answer questions";
                $new->save();
        
                $new = new Content;
                $new->bodyId = 9;
                $new->type = 'question';
                $new->position = 7;
                $new->content = "8. Openness to student's opinions";
                $new->save();
        
                $new = new Content;
                $new->bodyId = 10;
                $new->type = 'question';
                $new->position = 0;
                $new->content = "1. Organization of subject matter";
                $new->save();
        
                $new = new Content;
                $new->bodyId = 10;
                $new->type = 'question';
                $new->position = 1;
                $new->content = "2. Ability to relate subject to previous topics";
                $new->save();
        
                $new = new Content;
                $new->bodyId = 10;
                $new->type = 'question';
                $new->position = 2;
                $new->content = "3. Ability to provoke critical thinking";
                $new->save();
        
                $new = new Content;
                $new->bodyId = 10;
                $new->type = 'question';
                $new->position = 3;
                $new->content = "4. Ability to motivate";
                $new->save();
        
                $new = new Content;
                $new->bodyId = 10;
                $new->type = 'question';
                $new->position = 4;
                $new->content = "5. Ability to manage class";
                $new->save();
        
                $new = new Content;
                $new->bodyId = 10;
                $new->type = 'question';
                $new->position = 5;
                $new->content = "6. Questioning techniques";
                $new->save();
        
                $new = new Content;
                $new->bodyId = 10;
                $new->type = 'question';
                $new->position = 6;
                $new->content = "7. Use of teaching aids";
                $new->save();
        
                $new = new Content;
                $new->bodyId = 11;
                $new->type = 'question';
                $new->position = 0;
                $new->content = "1. Class attention";
                $new->save();
        
                $new = new Content;
                $new->bodyId = 11;
                $new->type = 'question';
                $new->position = 1;
                $new->content = "2. Class participation";
                $new->save();
        
                $new = new Content;
                $new->bodyId = 12;
                $new->type = 'question';
                $new->position = 0;
                $new->content = "1. Rapport between teacher and students";
                $new->save();
        
                $new = new Content;
                $new->bodyId = 12;
                $new->type = 'question';
                $new->position = 1;
                $new->content = "2. Class atmosphere";
                $new->save();
        
                $new = new Content;
                $new->bodyId = 12;
                $new->type = 'question';
                $new->position = 2;
                $new->content = "3. Overall teacher impact";
                $new->save();
        
                $new = new Content;
                $new->bodyId = 12;
                $new->type = 'question';
                $new->position = 3;
                $new->content = "4. Overall classroom condition";
                $new->save();

                $new = new FormRelationship;
        $new->formId = 1;
        $new->evaluatee = 'Professor';
        $new->evaluator = 'Student';
        $new->save();

        $new = new FormRelationship;
        $new->formId = 2;
        $new->evaluatee = 'Professor';
        $new->evaluator = 'Department Chair';
        $new->save();

        $new = new FormRelationship;
        $new->formId = 2;
        $new->evaluatee = 'Department Chair';
        $new->evaluator = 'Vice Dean';
        $new->status = 'Indistinct';
        $new->save();

        $new = new FormRelationship;
        $new->formId = 2;
        $new->evaluatee = 'Professor';
        $new->evaluator = 'Vice Dean';
        $new->status = 'Indistinct';
        $new->save();

        $new = new FormRelationship;
        $new->formId = 2;
        $new->evaluatee = 'Professor';
        $new->evaluator = 'Dean';
        $new->status = 'Indistinct';
        $new->save();

        $new = new FormRelationship;
        $new->formId = 2;
        $new->evaluatee = 'Department Chair';
        $new->evaluator = 'Dean';
        $new->status = 'Indistinct';
        $new->save();

        $new = new FormRelationship;
        $new->formId = 2;
        $new->evaluatee = 'Vice Dean';
        $new->evaluator = 'Dean';
        $new->status = 'Indistinct';
        $new->save();

        $new = new FormRelationship;
        $new->formId = 3;
        $new->evaluatee = 'Professor';
        $new->evaluator = 'Department Chair';
        $new->save();

        $new = new FormRelationship;
        $new->formId = 3;
        $new->evaluatee = 'Professor';
        $new->evaluator = 'Vice Dean';
        $new->status = 'Indistinct';
        $new->save();

        $new = new FormRelationship;
        $new->formId = 3;
        $new->evaluatee = 'Department Chair';
        $new->evaluator = 'Vice Dean';
        $new->status = 'Indistinct';
        $new->save();

        $new = new FormRelationship;
        $new->formId = 3;
        $new->evaluatee = 'Department Chair';
        $new->evaluator = 'Dean';
        $new->status = 'Indistinct';
        $new->save();

        $new = new FormRelationship;
        $new->formId = 3;
        $new->evaluatee = 'Professor';
        $new->evaluator = 'Dean';
        $new->status = 'Indistinct';
        $new->save();

        $new = new FormRestriction;
        $new->formId = 2;
        $new->type = 'Professor';
        $new->selfEvaluation = 1;
        $new->save();

        $new = new FormRestriction;
        $new->formId = 2;
        $new->type = 'Department Chair';
        $new->selfEvaluation = 1;
        $new->save();

        $new = new FormRestriction;
        $new->formId = 2;
        $new->type = 'Vice Dean';
        $new->selfEvaluation = 1;
        $new->save();

        $new = new FormRestriction;
        $new->formId = 2;
        $new->type = 'Dean';
        $new->selfEvaluation = 1;
        $new->save();

        $faculties = Faculty::where('department', 'Information Technology')->get();
        foreach($faculties as $faculty){
            $load = ClassLoad::where('facultyNo', $faculty->facultyNo)->get();
            foreach($load as $class){
                $cor = COR::where('code', $class->code)
                                ->where('subject', $class->subject)
                                ->where('schedule', $class->schedule)
                                ->where('room', $class->room)
                                ->where('section', $class->section)
                                ->where('year', $class->year)
                                ->where('term', $class->term)->take(10)->get();
                foreach($cor as $reg){
                    $explode = explode(' ', $reg->subject);
                    if(last($explode) != 'Laboratory'){
                        if($reg->student->yearLvl == '4th Year'){
                            $new = new FormProgress;
                            $new->formId = 1;
                            $new->evaluator = $reg->student->userId;
                            $new->evaluatee = $class->faculty->userId;
                            $new->year = $reg->year;
                            $new->term = $reg->term;
                            $new->as = 'Professor';
                            $new->save();
                        }
                    }
                }
            }
        }

        $faculties = Faculty::where('department', 'Information Technology')->get();
        foreach($faculties as $faculty){
            $forms = Form::all();
            foreach($forms as $form){
                $relations = FormRelationship::where('formId', $form->id)->where('evaluatee', $faculty->type)->get();
                foreach($relations as $relation){
                    if($relation->status == 'Distinct'){
                        $evaluators = Faculty::where('type', $relation->evaluator)->where('department', $faculty->department)->where('userId', '!=', $faculty->userId)->get();
                    }else{
                        $evaluators = Faculty::where('type', $relation->evaluator)->where('userId', '!=', $faculty->userId)->get();
                    }
                    foreach($evaluators as $evaluator){
                        $progress = new FormProgress;
                        $progress->formId = $form->id;
                        $progress->evaluatee = $faculty->userId;
                        $progress->evaluator = $evaluator->userId;
                        $progress->as = $relation->evaluatee;
                        $progress->year = $faculty->class->year;
                        $progress->term = $faculty->class->term;
                        $progress->save();
                    }
                }
                $restrict = FormRestriction::where('formId', $form->id)->where('type', $faculty->type)->first();
                if(!empty($restrict) && $restrict->selfEvaluation == 1){
                    $progress = new FormProgress;
                    $progress->formId = $form->id;
                    $progress->evaluatee = $faculty->userId;
                    $progress->evaluator = $faculty->userId;
                    $progress->as = 'Professor';
                    $progress->year = $faculty->class->year;
                    $progress->term = $faculty->class->term;
                    $progress->save();
                }
            }
        }

        $blocks = COR::where('section', '!=', 'BSIT 1-A')
                        ->where('section', '!=', 'BSIT 1-B')
                        ->where('section', '!=', 'BSIT 3-A')
                        ->where('section', '!=', 'BSIT 3-B')->distinct()->get(['section']);
                        
        foreach($blocks as $block){
            $csvData = file_get_contents(public_path() . '/storage/csv/'.$block->section.'.csv');
            $rows = array_map('str_getcsv', explode("\n", $csvData));
            for ($i = 1; $i < count($rows)-1; $i++){

                $progress = FormProgress::where('formId', 1)->where('evaluatee', $rows[$i][4])->where('evaluator', $rows[$i][5])->first();

                if(!empty($progress)){

                    $entry = new Entry;
                    $entry->formProgressId = $progress->id;
                    $entry->contentId = $rows[$i][6];
                    $entry->result = $rows[$i][2];
                    $entry->save();
    
                    $status = Student::where('userId', $rows[$i][5])->first();
                    $status->status = 'completed';
                    $status->save();
    
                    $user = User::find($rows[$i][5]);
                    $user->email = $user->username . '@gmail.com';
                    $user->activated = 1;
                    $user->save();
                    
                    $countContent = 0;
                    $entries = 0;
                    $bodies = Body::where('formId', 1)->get();
                    foreach($bodies as $body){
                        $contents = Content::where('bodyId', $body->id)->where('type', 'question')->get();
                        $countContent = $countContent + count($contents);
                        foreach($contents as $content){
                            $entry = Entry::where('contentId', $content->id)
                                            ->where('formProgressId', $progress->id)->first();
                            if(!empty($entry)){
                                $entries = ++$entries;
                            }
                        }
                    }
            
                    if(($countContent != 0 && $entries != 0) && ($countContent == $entries)){
                        $progress->status = 'completed';
                        $allBody = [];
                        $bodies = Body::where('formId', 1)->get();
                        foreach($bodies as $body){
                            $sum = 0;
                            $contents = Content::where('bodyId', $body->id)->where('type', 'question')->get();
                            foreach($contents as $content){
                                $entry = Entry::where('contentId', $content->id)
                                                ->where('formProgressId', $progress->id)->first();
                                $sum = $sum + $entry->result;
                            }
                            array_push($allBody, ( ($sum/count($contents)) * ($body->rate/100) ));
                        }
                        $progress->total = array_sum($allBody);
                        $progress->save();
                    }

                }
            }
        }

        $csvData = file_get_contents(public_path() . '/storage/csv/appraisal.csv');
        $rows = array_map('str_getcsv', explode("\n", $csvData));

        for ($i = 1; $i < count($rows)-1; $i++){

            $evaluator = Faculty::where('facultyNo', $rows[$i][3])->first();
            $evaluatee = Faculty::where('facultyNo', $rows[$i][4])->first();

            $progress = FormProgress::where('formId', $rows[$i][0])
                                    ->where('evaluator', $evaluator->userId)
                                    ->where('evaluatee', $evaluatee->userId)
                                    ->where('as', $rows[$i][6])->first();

            $entry = new Entry;
            $entry->formProgressId = $progress->id;
            $entry->contentId = $rows[$i][2];
            $entry->result = $rows[$i][5];
            $entry->save();

            $status = Faculty::where('userId', $evaluator->userId)->first();
            $status->status = 'completed';
            $status->save();

            $user = User::find($evaluator->userId);
            $user->email = $user->username . '@gmail.com';
            $user->activated = 1;
            $user->save();
        }

        $csvData = file_get_contents(public_path() . '/storage/csv/classobservation.csv');
        $rows = array_map('str_getcsv', explode("\n", $csvData));

        for ($i = 1; $i < count($rows)-1; $i++){

            $evaluator = Faculty::where('facultyNo', $rows[$i][3])->first();
            $evaluatee = Faculty::where('facultyNo', $rows[$i][4])->first();

            $progress = FormProgress::where('formId', $rows[$i][0])
                                    ->where('evaluator', $evaluator->userId)
                                    ->where('evaluatee', $evaluatee->userId)
                                    ->where('as', $rows[$i][6])->first();

            $entry = new Entry;
            $entry->formProgressId = $progress->id;
            $entry->contentId = $rows[$i][2];
            $entry->result = $rows[$i][5];
            $entry->save();

            $status = Faculty::where('userId', $evaluator->userId)->first();
            $status->status = 'completed';
            $status->save();

            $user = User::find($evaluator->userId);
            $user->email = $user->username . '@gmail.com';
            $user->activated = 1;
            $user->save();
        }

        $faculties = Faculty::where('department', 'Information Technology')->get();
        foreach($faculties as $faculty){
            $forms = Form::where('id', '!=', 1)->get();
            foreach($forms as $form){
                $relations = FormRelationship::where('formId', $form->id)->where('evaluatee', $faculty->type)->get();
                foreach($relations as $relation){
                    if($relation->status == 'Distinct'){
                        $evaluators = Faculty::where('type', $relation->evaluator)->where('department', $faculty->department)->where('userId', '!=', $faculty->userId)->get();
                    }else{
                        $evaluators = Faculty::where('type', $relation->evaluator)->where('userId', '!=', $faculty->userId)->get();
                    }
                    foreach($evaluators as $evaluator){
                        $progress = FormProgress::where('formId', $form->id)->where('evaluatee', $faculty->userId)->where('evaluator', $evaluator->userId)->where('as', $relation->evaluatee)->first();
                        $countContent = 0;
                        $entries = 0;
                        $bodies = Body::where('formId', $form->id)->get();
                
                        foreach($bodies as $body){
                            $contents = Content::where('bodyId', $body->id)->where('type', 'question')->get();
                            $countContent = $countContent + count($contents);
                            foreach($contents as $content){
                                $entry = Entry::where('contentId', $content->id)
                                                ->where('formProgressId', $progress->id)->first();
                                if(!empty($entry)){
                                    $entries = ++$entries;
                                }
                            }
                        }

                        if(($countContent != 0 && $entries != 0) && ($countContent == $entries)){
                            $progress->status = 'completed';
                            $allBody = [];
                            $bool = false;
                            $bodies = Body::where('formId', $form->id)->get();
                            foreach($bodies as $body){
                                $sum = 0;
                                $contents = Content::where('bodyId', $body->id)->where('type', 'question')->get();
                                foreach($contents as $content){
                                    $entry = Entry::where('contentId', $content->id)
                                                    ->where('formProgressId', $progress->id)->first();
                                    $sum = $sum + $entry->result;
                                }
                                if($body->rate == 0){
                                    $bool = true;
                                    array_push($allBody, $sum);
                                }else{
                                    array_push($allBody, ( ($sum/count($contents)) * ($body->rate/100) ));
                                }
                            }
                            if($bool){
                                $progress->total = array_sum($allBody) / $countContent;
                                $bool = false;
                            }else{
                                $progress->total = array_sum($allBody);
                            }
                            $progress->save();
                        }
                    }
                }
                $restrict = FormRestriction::where('formId', $form->id)->where('type', $faculty->type)->first();
                if(!empty($restrict) && $restrict->selfEvaluation == 1){
                    $progress = FormProgress::where('formId', $form->id)->where('evaluatee', $faculty->userId)->where('evaluator', $faculty->userId)->where('as', 'Professor')->first();
                    $countContent = 0;
                    $entries = 0;
                    $bodies = Body::where('formId', $form->id)->get();
            
                    foreach($bodies as $body){
                        $contents = Content::where('bodyId', $body->id)->where('type', 'question')->get();
                        $countContent = $countContent + count($contents);
                        foreach($contents as $content){
                            $entry = Entry::where('contentId', $content->id)
                                            ->where('formProgressId', $progress->id)->first();
                            if(!empty($entry)){
                                $entries = ++$entries;
                            }
                        }
                    }
        
                    if(($countContent != 0 && $entries != 0) && ($countContent == $entries)){
                        $progress->status = 'completed';
                        $allBody = [];
                        $bool = false;
                        $bodies = Body::where('formId', $form->id)->get();
                        foreach($bodies as $body){
                            $sum = 0;
                            $contents = Content::where('bodyId', $body->id)->where('type', 'question')->get();
                            foreach($contents as $content){
                                $entry = Entry::where('contentId', $content->id)
                                                ->where('formProgressId', $progress->id)->first();
                                $sum = $sum + $entry->result;
                            }
                            if($body->rate == 0){
                                $bool = true;
                                array_push($allBody, $sum);
                            }else{
                                array_push($allBody, ( ($sum/count($contents)) * ($body->rate/100) ));
                            }
                        }
                        if($bool){
                            $progress->total = array_sum($allBody) / $countContent;
                            $bool = false;
                        }else{
                            $progress->total = array_sum($allBody);
                        }
                        $progress->save();
                    }
                }
            }
        }

        $new = new Element;
        $new->type = 'Professor';
        $new->formId = 3;
        $new->rate = 35;
        $new->save();

        $new = new Element;
        $new->type = 'Department Chair';
        $new->formId = 3;
        $new->rate = 35;
        $new->save();

        $new = new Element;
        $new->type = 'Vice Dean';
        $new->formId = 3;
        $new->rate = 35;
        $new->save();

        $new = new Element;
        $new->type = 'Dean';
        $new->formId = 3;
        $new->rate = 35;
        $new->save();

        $new = new Element;
        $new->type = 'Professor';
        $new->formId = 2;
        $new->rate = 40;
        $new->save();

        $new = new Element;
        $new->type = 'Department Chair';
        $new->formId = 2;
        $new->rate = 40;
        $new->save();

        $new = new Element;
        $new->type = 'Vice Dean';
        $new->formId = 2;
        $new->rate = 40;
        $new->save();

        $new = new Element;
        $new->type = 'Dean';
        $new->formId = 2;
        $new->rate = 40;
        $new->save();

        $new = new Element;
        $new->type = 'Professor';
        $new->formId = 1;
        $new->rate = 20;
        $new->save();

        $new = new Element;
        $new->type = 'Department Chair';
        $new->formId = 1;
        $new->rate = 20;
        $new->save();

        $new = new Element;
        $new->type = 'Vice Dean';
        $new->formId = 1;
        $new->rate = 20;
        $new->save();

        $new = new Element;
        $new->type = 'Dean';
        $new->formId = 1;
        $new->rate = 20;
        $new->save();

        $new = new Element;
        $new->type = 'Professor';
        $new->formId = 2;
        $new->rate = 5;
        $new->selfEvaluation = 1;
        $new->save();

        $new = new Element;
        $new->type = 'Department Chair';
        $new->formId = 2;
        $new->rate = 5;
        $new->selfEvaluation = 1;
        $new->save();

        $new = new Element;
        $new->type = 'Vice Dean';
        $new->formId = 2;
        $new->rate = 5;
        $new->selfEvaluation = 1;
        $new->save();

        $new = new Element;
        $new->type = 'Dean';
        $new->formId = 2;
        $new->rate = 5;
        $new->selfEvaluation = 1;
        $new->save();

        $new = new SubElement;
        $new->elementId = 1;
        $new->type = 'Vice Dean';
        $new->rate = 5;
        $new->save();

        $new = new SubElement;
        $new->elementId = 1;
        $new->type = 'Dean';
        $new->rate = 5;
        $new->save();

        $new = new SubElement;
        $new->elementId = 1;
        $new->type = 'Department Chair';
        $new->rate = 25;
        $new->save();

        $new = new SubElement;
        $new->elementId = 5;
        $new->type = 'Dean';
        $new->rate = 10;
        $new->save();

        $new = new SubElement;
        $new->elementId = 5;
        $new->type = 'Vice Dean';
        $new->rate = 10;
        $new->save();

        $new = new SubElement;
        $new->elementId = 5;
        $new->type = 'Department Chair';
        $new->rate = 20;
        $new->save();
        
        $new = new Tutorial;
        $new->title = "How to manage students";
        $new->link = 'https://www.useloom.com/embed/ffe85afaa7b44669a60183fb4c2a56b8';
        $new->save();

        $new = new Tutorial;
        $new->title = "How to manage professor";
        $new->link = 'https://www.useloom.com/embed/5e156efaa91c47e49ee833f193449fe9';
        $new->save();

        $new = new Tutorial;
        $new->title = "How to import COR";
        $new->link = 'https://www.useloom.com/embed/6f30dbd8ed49444b91617cd11afd1d95';
        $new->save();

        $new = new Tutorial;
        $new->title = "How to import Class Load";
        $new->link = 'https://www.useloom.com/embed/530d2025db224168891786fd49dc9076';
        $new->save();

        $new = new Tutorial;
        $new->title = "How to publish a new form";
        $new->link = 'https://www.useloom.com/embed/6adf64c243c049d2ba903d9b1658d59f';
        $new->save();

        $new = new Tutorial;
        $new->title = "How to edit a summative report";
        $new->link = 'https://www.useloom.com/embed/73e2793adf5846cf9fb656a4f0e4bbb5';
        $new->save();

        $new = new Tutorial;
        $new->title = "How to print a summative report";
        $new->link = 'https://www.useloom.com/embed/3f32319cbb334c13881bf3bd81732b01';
        $new->save();

    }
}
