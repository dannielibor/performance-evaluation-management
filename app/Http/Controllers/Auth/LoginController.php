<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Notifications\TwoAuthNotification;
use Illuminate\Http\Request;
use App\Traits\FormTrait;
use App\Traits\InternetTrait;
use App\Traits\AuthTrait;
use App\TwoAuth;

use App\User;
use App\Student;
use App\Faculty;
use App\Log;
use Auth;
use Session;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers, InternetTrait, FormTrait, AuthTrait;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/index';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function username()
    {
        $this->connection();
        $this->schedule();
        $this->auth();
        
        $identity  = request()->get('identity');
        $fieldName = filter_var($identity, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';
        request()->merge([$fieldName => $identity]);
        return $fieldName;
    }

    public function logout(Request $request) {
        $this->connection();
        $this->schedule();
        $this->auth();

        $path = '';
        if(Auth::check() && (auth()->user()->isAdmin == 1 || auth()->user()->isSuperAdmin == 1)){
            $path = '/admin';
        }else{
            $path = '/';
        }
        $log = new Log;
        $log->userId = auth()->user()->id;
        $log->description = 'A user has been signed out';
        $log->save();
        Auth::logout();
        Session::flush();
        return redirect($path);
    }

    public function login(Request $request){

        $user = User::where('username', $request->identity)->orWhere('email', $request->identity)->first();

        if(password_verify($request->password, $user->password)){
            
            $path = url()->previous();
            $explode = explode('/', $path);

            if(last($explode) == 'admin'){
                
                if($user->isAdmin == 1){

                    Session::put('type', 'Admin');
                    if($user->twoFA == 1){
                        $fa = TwoAuth::where('userId', $user->id)->first();
                        if(empty($fa)){
                            $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
                            $charactersLength = strlen($characters);
                            $randomString = '';
                            for ($i = 0; $i < 5; $i++) {
                                $randomString .= $characters[rand(0, $charactersLength - 1)];
                            }
                            $new = new TwoAuth();
                            $new->userId = $user->id;
                            $new->code = $randomString;
                            $new->save();
                            $user->notify(new TwoAuthNotification($user));
                            return response()->json(['sent' => '1', 'email' => $user->email]);
                        }else{
                            return response()->json(['sent' => '1', 'email' => $user->email]);
                        }

                    }else{
                        Auth::login($user);
                        $log = new Log;
                        $log->userId = auth()->user()->id;
                        $log->description = 'A user has been signed in';
                        $log->save();
                        return response()->json(['path' => '/']);
                    }

                }else if($user->isSuperAdmin == 1){
                    Session::put('type', 'Super Admin'); 
                    if($user->twoFA == 1){
                        $fa = TwoAuth::where('userId', $user->id)->first();
                        if(empty($fa)){
                            $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
                            $charactersLength = strlen($characters);
                            $randomString = '';
                            for ($i = 0; $i < 5; $i++) {
                                $randomString .= $characters[rand(0, $charactersLength - 1)];
                            }
                            $new = new TwoAuth();
                            $new->userId = $user->id;
                            $new->code = $randomString;
                            $new->save();
                            $user->notify(new TwoAuthNotification($user));
                            return response()->json(['sent' => '1', 'email' => $user->email]);
                        }else{
                            return response()->json(['sent' => '1', 'email' => $user->email]);
                        }

                    }else{
                        Auth::login($user);
                        $log = new Log;
                        $log->userId = auth()->user()->id;
                        $log->description = 'A user has been signed in';
                        $log->save();
                        return response()->json(['path' => '/']);
                    }
                }else{
                    return response()->json(['invalid_portal' => '1']);
                }

            }else{ 
                if($user->isAdmin == 1 || $user->isSuperAdmin == 1){
                    return response()->json(['invalid_path' => '1']);
                }else{
                    if($user->twoFA == 1){
                        $fa = TwoAuth::where('userId', $user->id)->first();
                        if(empty($fa)){
                            $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
                            $charactersLength = strlen($characters);
                            $randomString = '';
                            for ($i = 0; $i < 5; $i++) {
                                $randomString .= $characters[rand(0, $charactersLength - 1)];
                            }
                            $new = new TwoAuth();
                            $new->userId = $user->id;
                            $new->code = $randomString;
                            $new->save();
                            $user->notify(new TwoAuthNotification($user));
                            return response()->json(['sent' => '1', 'email' => $user->email]);
                        }else{
                            return response()->json(['sent' => '1', 'email' => $user->email]);
                        }

                    }else{
                        Auth::login($user);
                        $log = new Log;
                        $log->userId = auth()->user()->id;
                        $log->description = 'A user has been signed in';
                        $log->save();
                        $faculty = Faculty::where('userId', $user->id)->first();
                        if( count(Student::where('userId', $user->id)->get()) > 0 ){
                            Session::put('type', "Student");
                        }else if ( !empty($faculty) ){
                            Session::put('type', $faculty->type);
                        }
                        return response()->json(['path' => '/']);
                    }
                }
            }

        }else{
            return response()->json(['invalid_credentials' => '1']);
        }

    }
}
