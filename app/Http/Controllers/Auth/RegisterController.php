<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Traits\ActivationKeyTrait;
use App\Traits\FormTrait;
use App\Traits\AuthTrait;
use Hash;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers, ActivationKeyTrait, AuthTrait, FormTrait;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $this->connection();
        $this->schedule();
        $this->auth();

        return Validator::make($data, 
            [
                'id_number' => 'required|string|max:255',
                'email' => 'required|string|max:255|unique:users',
                'password' => 'required|min:6|max:20',
            ]
        );
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    { 
        $this->connection();
        $this->schedule();
        $this->auth();

        return User::create([
            'name' => 'anonymous',
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'activated' => !config('settings.send_activation_email')
        ]);
    }

    public function register(Request $request)
    {
        $this->connection();
        $this->schedule();
        $this->auth();
        
        $this->schedule();
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        }

        $connected = @fsockopen("www.google.com", 80);

        if($connected){
            // create the user
            $user = $this->create($request->all());
            
            // process the activation email for the user
            $this->queueActivationKeyNotification($user);

            // we do not want to login the new user
            return redirect('/');
        }else{
            return response()->json(['path' => '/failed']);
        }

    }
}
