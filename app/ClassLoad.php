<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ClassLoad extends Model
{
    use SoftDeletes;
    
    public function faculty(){
        return $this->hasOne('App\Faculty', 'facultyNo', 'facultyNo');
    }
}
