<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FormRestriction extends Model
{
    use SoftDeletes;

    public function form(){
        return $this->hasOne('App\Form', 'id', 'formId');
    }
}
