<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Content extends Model
{
    use SoftDeletes;

    public function body(){
        return $this->hasOne('App\Body', 'id', 'bodyId');
    }

    public function bodyTrashed(){
        return $this->hasOne('App\Body', 'id', 'bodyId')->withTrashed();
    }
}
