<?php

namespace App\Providers;

use App\Form;
use App\User;
use App\Element;
use App\Tutorial;
use App\Policies\FormPolicy;
use App\Policies\AdminPolicy;
use App\Policies\SummativeReportPolicy;
use App\Policies\TutorialPolicy;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
        Form::class => FormPolicy::class,
        Element::class => SummativeReportPolicy::class,
        User::class => AdminPolicy::class,
        Tutorial::class => TutorialPolicy::class
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
    }
}
