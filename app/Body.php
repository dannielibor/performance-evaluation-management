<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Body extends Model
{
    use SoftDeletes;

    public function content(){
        return $this->hasMany('App\Content', 'bodyId', 'id');
    }

    public function form(){
        return $this->hasOne('App\Form', 'formId', 'id');
    }

    public function formTrashed(){
        return $this->hasOne('App\Form', 'formId', 'id')->withTrashed();
    }

    public function contentTrashed(){
        return $this->hasOne('App\Content', 'bodyId', 'id')->withTrashed();
    }
}
