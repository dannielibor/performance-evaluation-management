<?php

namespace App\Policies;

use App\User;
use App\Form;
use App\FormRelationship;
use App\FormRestriction;
use App\Faculty;
use App\FormProgress;
use Session;
use Illuminate\Auth\Access\HandlesAuthorization;

class FormPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the form.
     *
     * @param  \App\User  $user
     * @param  \App\Form  $form
     * @return mixed
     */
    public function view(User $user, Form $form)
    {   
        $url = url()->current();
        $split = explode('/', $url);
        $title = str_replace('%20', ' ', $split[4]);
        $form = Form::where('id', $split[5])->where('title', $title)->where('status', 'active')->first();
        if(!empty($form)){
            $restrict = FormRelationship::where('formId', $form->id)->where('evaluator', Session::get('type'))->get();
            if(count($restrict) > 0){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }

    /**
     * Determine whether the user can create forms.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    public function show(User $user, Form $form)
    { 
        if($user->activated == 1){ 
            $url = url()->current();
            $split = explode('/', $url);
            $faculty = Faculty::where('userId', $split[8])->first();
            $title = str_replace('%20', ' ', $split[4]); 
            $form = Form::where('id', $split[5])->where('title', $title)->where('status', 'active')->first();
            if(!empty($form)){ 
                $restrict = FormRelationship::where('formId', $form->id)->where('evaluator', Session::get('type'))->where('evaluatee', $faculty->type)->first(); 
                if(!empty($restrict)){
                    $user = Faculty::where('type', $faculty->type)->where('userId', $split[8])->first();
                    if(!empty($user)){
                        $progress = FormProgress::where('formId', $split[5])->where('evaluatee', $user->userId)->where('evaluator', auth()->user()->id)->where('year', $user->class->year)->where('term', $user->class->term)->first();
                        if(!empty($progress) && $progress->status == 'completed'){
                            return false;
                        }else{
                            return true;
                        }
                    }
                    return false;
                }else{
                    $user = Faculty::where('type', $faculty->type)->where('userId', $split[8])->first();
                    if(!empty($user)){
                        if($user->type = 'Department Chair' || $user->type = 'Vice Dean' || $user->type = 'Dean'){
                            $restrict = FormRelationship::where('formId', $form->id)->where('evaluator', Session::get('type'))->where('evaluatee', 'Professor')->first();
                            if(!empty($restrict)){
                                return true;
                            }else{
                                $self = FormRestriction::where('formId', $form->id)->where('type', Session::get('type'))->where('selfEvaluation', 1)->first();
                                if(!empty($self)){
                                    $progress = FormProgress::where('formId', $split[5])->where('evaluatee', $user->userId)->where('evaluator', auth()->user()->id)->where('year', $user->class->year)->where('term', $user->class->term)->first();
                                    if(!empty($progress) && $progress->status == 'completed'){
                                        return false;
                                    }else{
                                        return true;
                                    }
                                }else{
                                    return false;
                                }
                            }
                        }
                    }
                }
            }else{
                return false;
            }
        }else{
            return false;
        }
    }

    /**
     * Determine whether the user can update the form.
     *
     * @param  \App\User  $user
     * @param  \App\Form  $form
     * @return mixed
     */
    public function update(User $user, Form $form)
    {
        $url = url()->current();
        $split = explode('/', $url);
        $faculty = str_replace('%20', ' ', $split[7]);
        $title = str_replace('%20', ' ', $split[4]);
        $form = Form::where('id', $split[5])->where('title', $title)->where('status', 'active')->first();
        if(!empty($form)){
            $restrict = FormRelationship::where('formId', $form->id)->where('evaluator', Session::get('type'))->where('evaluatee', $faculty)->get();
            if(count($restrict) > 0){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }

    /**
     * Determine whether the user can delete the form.
     *
     * @param  \App\User  $user
     * @param  \App\Form  $form
     * @return mixed
     */
    public function delete(User $user, Form $form)
    {
        //
    }

    /**
     * Determine whether the user can restore the form.
     *
     * @param  \App\User  $user
     * @param  \App\Form  $form
     * @return mixed
     */
    public function restore(User $user, Form $form)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the form.
     *
     * @param  \App\User  $user
     * @param  \App\Form  $form
     * @return mixed
     */
    public function forceDelete(User $user, Form $form)
    {
        //
    }
}
