<?php

namespace App\Policies;

use App\User;
use App\Element;
use Session;
use Illuminate\Auth\Access\HandlesAuthorization;

class SummativeReportPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the element.
     *
     * @param  \App\User  $user
     * @param  \App\Element  $element
     * @return mixed
     */
    public function view(User $user, Element $element)
    {
        if(Session::get('type') == 'Dean' || Session::get('type') == 'Vice Dean' || Session::get('type') == 'Department Chair'){
            return true;
        }else{
            return false;
        }
    }

    /**
     * Determine whether the user can create elements.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the element.
     *
     * @param  \App\User  $user
     * @param  \App\Element  $element
     * @return mixed
     */
    public function update(User $user, Element $element)
    {
        //
    }

    /**
     * Determine whether the user can delete the element.
     *
     * @param  \App\User  $user
     * @param  \App\Element  $element
     * @return mixed
     */
    public function delete(User $user, Element $element)
    {
        //
    }

    /**
     * Determine whether the user can restore the element.
     *
     * @param  \App\User  $user
     * @param  \App\Element  $element
     * @return mixed
     */
    public function restore(User $user, Element $element)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the element.
     *
     * @param  \App\User  $user
     * @param  \App\Element  $element
     * @return mixed
     */
    public function forceDelete(User $user, Element $element)
    {
        //
    }
}
