<?php

namespace App\Policies;

use App\User;
use App\Tutorial;
use Session;
use Illuminate\Auth\Access\HandlesAuthorization;

class TutorialPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the tutorial.
     *
     * @param  \App\User  $user
     * @param  \App\Tutorial  $tutorial
     * @return mixed
     */
    public function view(User $user, Tutorial $tutorial)
    {
        if(Session::get('type') == 'Super Admin'){
            return true;
        }else{
            return false;
        }
    }

    /**
     * Determine whether the user can create tutorials.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the tutorial.
     *
     * @param  \App\User  $user
     * @param  \App\Tutorial  $tutorial
     * @return mixed
     */
    public function update(User $user, Tutorial $tutorial)
    {
        //
    }

    /**
     * Determine whether the user can delete the tutorial.
     *
     * @param  \App\User  $user
     * @param  \App\Tutorial  $tutorial
     * @return mixed
     */
    public function delete(User $user, Tutorial $tutorial)
    {
        //
    }

    /**
     * Determine whether the user can restore the tutorial.
     *
     * @param  \App\User  $user
     * @param  \App\Tutorial  $tutorial
     * @return mixed
     */
    public function restore(User $user, Tutorial $tutorial)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the tutorial.
     *
     * @param  \App\User  $user
     * @param  \App\Tutorial  $tutorial
     * @return mixed
     */
    public function forceDelete(User $user, Tutorial $tutorial)
    {
        //
    }
}
