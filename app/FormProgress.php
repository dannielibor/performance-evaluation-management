<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FormProgress extends Model
{
    use SoftDeletes;
    
    public function evaluator(){
        return $this->hasOne('App\User', 'id', 'evaluator');
    }

    public function evaluatee(){
        return $this->hasOne('App\User', 'id', 'evaluatee');
    }
}
