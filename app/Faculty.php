<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Faculty extends Model
{
    use SoftDeletes;

    public function user(){
        return $this->hasOne('App\User', 'id', 'userId');
    }
    public function class(){
        return $this->hasOne('App\ClassLoad', 'facultyNo', 'facultyNo');
    }
    public function userTrashed(){
        return $this->hasOne('App\User', 'id', 'userId')->withTrashed();
    }
}
