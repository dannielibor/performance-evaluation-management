<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FeedbackResult extends Model
{
    use SoftDeletes;

    public function feedbackTrashed(){
        return $this->hasOne('App\FeedbackResult', 'id', 'formId')->withTrashed();
    }
}
