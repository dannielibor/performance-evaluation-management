<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tutorial extends Model
{
    public function user(){
        return $this->hasOne('App\User', 'id', 'userId');
    }
}
