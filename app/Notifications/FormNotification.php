<?php

namespace App\Notifications;
 
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

use Carbon;
 
class FormNotification extends Notification implements ShouldQueue
{
    use Queueable;
 
    protected $user;
 
    /**
     * Create a new notification instance.
     *
     * SendActivationEmail constructor.
     * @param $activationKey
     */
    public function __construct($user)
    {
        $this->user = $user;
    }
 
    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }
 
    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        if($notifiable->isAdmin == 1 || $notifiable->isSuperAdmin == 1){
            return (new MailMessage)
                ->subject('Hurry! Evaluation Period will end soon')
                ->greeting('Hello, '.$notifiable->name)
                ->line('You need to finish all evaluation form(s) until tomorrow ('.Carbon\Carbon::now()->addDays(1)->format('M d, Y').' - 12:00am).')
                ->action('Sign in', url('/admin'))
                ->line('Note: Failure to comply, no clearance policy will be implemented. No furthur extension of dates.')
                ->line('Thank you for using '. "SBCA's Faculty Performance Evaluation System");
        }else{
            return (new MailMessage)
                ->subject('Hurry! Evaluation Period will end soon')
                ->greeting('Hello, '.$notifiable->name)
                ->line('You need to finish all evaluation form(s) until tomorrow ('.Carbon\Carbon::now()->addDays(1)->format('M d, Y').' - 12:00am).')
                ->action('Sign in', url('/'))
                ->line('Note: Failure to comply, no clearance policy will be implemented. No furthur extension of dates.')
                ->line('Thank you for using '. "SBCA's Faculty Performance Evaluation System");
        }
    }
 
    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}