<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class TwoAuthNotification extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        $this->user = $user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        if($notifiable->isAdmin == 1 || $notifiable->isSuperAdmin == 1){
            return (new MailMessage)
                ->subject('Authentication Code')
                ->greeting('Hello, '.$notifiable->name)
                ->line('Your authentication code is ' . $notifiable->twofa->code)
                ->action('Sign in', url('/admin'))
                ->line("Note: As an additional security measure, you'll need to enter the given code.")
                ->line('Thank you for using '. "SBCA's Faculty Performance Evaluation System");
        }else{
            return (new MailMessage)
                ->subject('Authentication Code')
                ->greeting('Hello, '.$notifiable->name)
                ->line('Your authentication code is ' . $notifiable->twofa->code)
                ->action('Sign in', url('/'))
                ->line("Note: As an additional security measure, you'll need to enter the given code.")
                ->line('Thank you for using '. "SBCA's Faculty Performance Evaluation System");
        }
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
