<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FormRelationship extends Model
{
    use SoftDeletes;
    
    protected $table = 'form_relationships';

    public function form(){
        return $this->hasOne('App\Form', 'id', 'formId');
    }
}
