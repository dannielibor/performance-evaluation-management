$('#feedback #del-feedback[data-id]').click(function(){
    $.ajax({
        url: '/feedback/destroy',
        type: 'get',
        async: false,
        data: { id: $(this).attr('data-id') },
        success: function(response){
            $('tr[data-id="'+response+'"]').detach();
            toastr.options = {
                "closeButton": false,
                "debug": false,
                "newestOnTop": false,
                "progressBar": false,
                "positionClass": "toast-bottom-right",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            };
            toastr.success("Feedback has been successfully deleted!");
        }
    });
});

$('#textArea').click(function(){
    $.ajax({
        url: '/feedback/store',
        type: 'get',
        async: false,
        data: { id: $('#formId').val() },
        success: function(response){
            var html = '<tr data-id = "'+response.id+'">' +
                            '<td>' +
                                '<i class = "fa fa-sort" id = "drag"></i>' +
                            '</td>' +
                            '<td>' +
                                '<input style = "text-align:center" type = "text" class = "form-control" placeholder = "Text here" id = "feedback_header" data-id = "'+response.id+'">' +
                            '</td>' +
                            '<td>' +
                                '<center>' +
                                    '<a id = "del-feedback" data-id = "'+response.id+'" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete">' +
                                        '<i class="la la-trash"></i>' +
                                    '</a>' +
                                '</center>' +
                            '</td>' +
                        '</tr>';
            $('#feedback').append(html);
            toastr.options = {
                "closeButton": false,
                "debug": false,
                "newestOnTop": false,
                "progressBar": false,
                "positionClass": "toast-bottom-right",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            };
            toastr.success("New feedback has been successfully added!");
        }
    });
});

$('#feedback').on('change', '#feedback_header[data-id]', function(){
    $.ajax({
        url: '/feedback/update',
        type: 'get',
        async: false,
        data: { id: $(this).attr('data-id'), value: $(this).val() }
    });
});

$("#sort tbody").sortable({
    update: function(a, b){
        $('#sort tbody tr').each(function(i){
            $.ajax({
                url: '/feedback/edit',
                type: 'get',
                async: false,
                data: { id:$(this).attr('data-id'), value: i  }
            });
        });
    }
}).disableSelection();