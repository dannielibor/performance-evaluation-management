
// change header title via rate
$('#touchspin #header_title[data-id]').change(function(){
    $('#m_sortable_portlets #chapter_title[data-id="'+$(this).attr('data-id')+'"]').val($(this).val());
    $.ajax({
        url: '/edit/chapter',
        type: 'get',
        data: { id: $(this).attr('data-id'), header: $(this).val() }
    });
});

// append question
$('#m_sortable_portlets').on('click', '#question[data-id]', function(){
    var base = $(this).attr('data-id');
    $.ajax({
        url: '/content/store',
        type: 'get',
        data: { id: base, type: 'question' },
        success: function(response){
            var html = '<tr id = "row" data-id = "'+base+'" data = "'+response.id+'" data-type = "question">' +
                            '<td style = "width:1%;cursor:move"></td>' +
                            '<td style = "width:1%;cursor:move">' +
                                '<center>' +
                                    '<i class = "fa fa-sort"></i>' +
                                '</center>' +
                            '</td>' +
                            '<td style = "width:50%;" colspan = "2">' +
                                '<input type="text" id = "input_title" data-id = "'+response.id+'" class="form-control m-input--pill" placeholder="Text Here" style = "width:100%">' +
                            '</td>' +
                            '<td style = "width:3%;cursor:move">' +
                                '<center>' +
                                    '<div id = "del" data-type = "question" data-id = "'+response.id+'" data = "'+base+'" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill">' +
                                        '<i class="la la-trash-o"></i>' +
                                    '</div>' +
                                '</center>' +
                            '</td>' +
                        '</tr>';
            $('#m_sortable_portlets #input[data-id="'+base+'"]').append(html);
            toastr.options = {
                "closeButton": false,
                "debug": false,
                "newestOnTop": false,
                "progressBar": false,
                "positionClass": "toast-bottom-right",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            };
            toastr.success("New question has been successfully added!");
        }
    });
});

// edit content
$('#m_sortable_portlets').on('change', '#input_title[data-id]', function(){
    $.ajax({
        url: '/content/edit',
        type: 'get',
        data: { id: $(this).attr('data-id'), value: $(this).val() }
    });
});

// sort table
$('#m_sortable_portlets #input[data-id]').sortable({
    update: function(b, c){ 
        var num = 0;
        $('#row[data-id="'+$(this).attr('data-id')+'"]').each(function(i){
            // $('.m-loader').css('display','block');
            // $('.m-wrapper').addClass( "ui-state-disabled" );
            $.ajax({
                url: '/content/update',
                type: 'get',
                async: false,
                data: { id: $(this).attr('data'), position: i, sort: i+1 },
                success: function(){
                    // $('.m-loader').css('display','none');
                    // $('.m-wrapper').removeClass( "ui-state-disabled" );
                }
            });
            if($(this).attr('data-type') == 'header'){
                $('#label[data="'+$(this).attr('data')+'"]').text((++num) + '.');
            }
        });
    }
});

// detach content
$('#m_sortable_portlets').on('click', '#del[data-id]', function(){
    var id = $(this).attr('data-id');
    var data = $(this).attr('data');
    var type = $(this).attr('data-type');
    $.ajax({
        url: '/content/destroy',
        type: 'get',
        data: { id: id, base: data, type: $(this).attr('data-type') },
        success: function(response){
            $('#row[data="'+id+'"]').detach();
            $('#row[data-id="'+data+'"] #label').each(function(i){
                $(this).text(i+1);
            });
            toastr.options = {
                "closeButton": false,
                "debug": false,
                "newestOnTop": false,
                "progressBar": false,
                "positionClass": "toast-bottom-right",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            };
            if(type == 'header'){
                toastr.success("Header has been successfully deleted!");
            }else{
                toastr.success("Question has been successfully deleted!");
            }
        }
    });
});

// append header
$('#m_sortable_portlets').on('click', '#header[data-id]', function(){
    var base = $(this).attr('data-id');
    $.ajax({
        url: '/content/store',
        type: 'get',
        data: { id: base, type: 'header' },
        success: function(response){
            var html = '<tr id = "row" data-id = "'+base+'" data = "'+response.id+'" data-type = "header">' +
                            '<td style = "width:1%;cursor:move">' +
                                '<center>' +
                                    '<i class = "fa fa-sort"></i>' +
                                '</center>' +
                            '</td>' +
                            '<td style = "width:50%" colspan = "3">' +
                                '<div class="m-input-icon m-input-icon--left">' +
                                    '<input type="text" id = "input_title" data-id = "'+response.id+'" class="form-control m-input--pill" placeholder="Text Here" style = "width:100%">' +
                                    '<span class="m-input-icon__icon m-input-icon__icon--left">' +
                                        '<span>' +
                                            '<i class="la" id = "label" data = "'+response.id+'">'+response.sort+'.</i>' +
                                        '</span>' +
                                    '</span>' +
                                '</div>' +
                            '</td>' +
                            '<td style = "width:3%;fa fa-sort">' +
                                '<center>' +
                                    '<div id = "del" data-type = "header" data-id = "'+response.id+'" data = "'+base+'" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill">' +
                                        '<i class="la la-trash-o"></i>' +
                                    '</div>' +
                                '</center>' +
                            '</td>' +
                        '</tr>';
            $('#m_sortable_portlets #input[data-id="'+base+'"]').append(html);
            toastr.options = {
                "closeButton": false,
                "debug": false,
                "newestOnTop": false,
                "progressBar": false,
                "positionClass": "toast-bottom-right",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            };
            toastr.success("New header has been successfully added!");
        }
    });
});

// on load
$(document).ready(function(){

    $('#m_touchspin_3[data-id]').change(function(){

        var array = [];

        $('#m_touchspin_3[data-id]').each(function(){
            array.push($(this).val());
        });

        var total = 0;

        for(var x = 0; x < array.length; x++){
            total += parseInt(array[x]);
        }

        if(total > 100){
            var html = '<span style = "color:red" id = "total">'+total+'</span>';
            $('#publishBtn').attr('disabled', true);
            $('#publishBtn').attr('style', 'cursor: not-allowed');
            $('#total').html(html);
        }else{
            var html = '<span id = "total">'+total+'</span>';
            $('#publishBtn').attr('disabled', false);
            $('#publishBtn').attr('style', 'cursor: pointer');
            $('#total').html(html);
        }

    });

    $('#item[data-id]').each(function(){
        $('#m_accordion_1_item_1_head_' + $(this).attr('data-id')).click();
    });
    
});

// chapter title
$('#m_sortable_portlets').on('change', '#chapter_title[data-id]', function(){
    $('#touchspin #spin[data-id="'+$(this).attr('data-id')+'"] #touchspin_title[data-id="'+$(this).attr('data-id')+'"]').text($(this).val());
    $.ajax({
        url: '/edit/chapter',
        type: 'get',
        data: { id: $(this).attr('data-id'), header: $(this).val() }
    });
});

// delete chapter
$('#m_sortable_portlets').on('click', '#delete[data-id]', function(){ 
    $.ajax({
        url: '/destroy/chapter',
        type: 'get',
        data: { id: $(this).attr('data-id') },
        success: function(response){
            $('#item[data-id="'+response+'"]').detach();
            $('#spin[data-id="'+response+'"]').detach();
            var letters = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
            $('#sort[data-id]').each(function(i){
                $(this).text(letters[i] + '.');
                $.ajax({
                    url: '/update/chapter',
                    type: 'get',
                    data: { id: $(this).attr('data-id'), sort: letters[i], position: i }
                });
            });
            toastr.options = {
                "closeButton": false,
                "debug": false,
                "newestOnTop": false,
                "progressBar": false,
                "positionClass": "toast-bottom-right",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            };
            toastr.success("Chapter has been successfully deleted!");
        }
    });
});

// append chapter
$('#chapter').click(function(){

    var array = [];

    $('#sort[data-id]').each(function(){

        array.push($(this).attr('data-id'));

    });

    $.ajax({
        url: '/store/chapter',
        type: 'get',
        data: { formId: $('#formId').val(), position: array },
        success: function(response){
            toastr.options = {
                "closeButton": false,
                "debug": false,
                "newestOnTop": false,
                "progressBar": false,
                "positionClass": "toast-bottom-right",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            };
            toastr.success("New chapter has been successfully added!");
            var html = '<div id = "item" data-id = "'+response.id+'"><div class="m-portlet m-portlet--mobile m-portlet--sortable m-accordion__item">' +
                            '<a href="#" style = "float:right" data-toggle="modal" data-target="#m_modal_delete_'+response.id+'" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete">' +
                                '<i class="la la-trash"></i>' +
                            '</a>' +
                            '<div style = "background-color:#F2F3F4" class="m-accordion__item-head collapsed" role="tab" id="m_accordion_1_item_1_head_'+response.id+'" data-toggle="collapse" href="#m_accordion_1_item_1_body_'+response.id+'" aria-expanded="false">' +
                                '<span class="m-accordion__item-icon" style = "cursor:move">' +
                                    '<i class = "fa fa-sort"></i>' +
                                '</span>' +
                                '<span class="m-accordion__item-title">' +
                                    '<div class="m-input-icon m-input-icon--left">' +
                                        '<input type="text" id = "chapter_title" data-id = "'+response.id+'" class="form-control m-input--pill" placeholder="Text Here" style = "width:50%">' +
                                        '<span class="m-input-icon__icon m-input-icon__icon--left">' +
                                            '<span>' +
                                                '<i class="la" id = "sort" data = "'+response.sort+'" data-id = "'+response.id+'">'+response.sort+'.</i>' +
                                            '</span>' +
                                        '</span>' +
                                    '</div>' +
                                '</span>' +
                                '<span class="m-accordion__item-mode"></span>' +
                            '</div>' +
                            '<div class="m-accordion__item-body collapse" id="m_accordion_1_item_1_body_'+response.id+'" class=" " role="tabpanel" aria-labelledby="m_accordion_1_item_1_head_'+response.id+'" data-parent="#m_accordion_1_'+response.id+'">' +
                                '<div class="m-accordion__item-content">' +
                                    '<div class="row align-items-center">' +
                                        '<div class="col-xl-8 order-2 order-xl-1">' +
                                            '<div class="form-group m-form__group row align-items-center">' +
                                                '<div class="col-md-3">' +
                                                    '<div class="m-form__group m-form__group--inline">' +
                                                        '<div class="m-input-icon m-input-icon--left">' +
                                                            '<button id = "header" data-id = "'+response.id+'" style = "background-color:#800;border:none" class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--pill">' +
                                                                '<span>' +
                                                                    '<i class="fl flaticon-signs"></i>' +
                                                                    '<span>' +
                                                                        'New Header' +
                                                                    '</span>' +
                                                                '</span>' +
                                                            '</button>' +
                                                        '</div>' +
                                                    '</div>' +
                                                    '<div class="d-md-none m--margin-bottom-10"></div>' +
                                                '</div>' +
                                                '<div class="col-md-4">' +
                                                    '<div class="m-form__group m-form__group--inline">' +
                                                        '<div class="m-input-icon m-input-icon--left">' +
                                                            '<button id = "question" data-id = "'+response.id+'" style = "background-color:#800;border:none" class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--pill">' +
                                                                '<span>' +
                                                                    '<i class="fl flaticon-signs"></i>' +
                                                                    '<span>' +
                                                                        'New Question' +
                                                                    '</span>' +
                                                                '</span>' +
                                                            '</button>' +
                                                        '</div>' +
                                                    '</div>' +
                                                    '<div class="d-md-none m--margin-bottom-10"></div>' +
                                                '</div>' +
                                            '</div>' +
                                        '</div>' +
                                    '</div>' +
                                    '<table class = "table table-striped">' +
                                        '<thead>' +
                                            '<tr>' +
                                                '<th></th>' +
                                                '<th></th>' +
                                                '<th></th>' +
                                                '<th></th>' +
                                            '</tr>' +
                                        '</thead>' +
                                        '<tbody id = "input" data-id = "'+response.id+'">' +
                                        '</tbody>' +
                                    '</table>' +
                                '</div>' +
                            '</div>' +
                        '</div>' +
                        
                        '<div class="modal fade" id="m_modal_delete_'+response.id+'" tabindex="-1" role="dialog" aria-labelledby="delete_'+response.id+'" aria-hidden="true">' +
                            '<div class="modal-dialog" role="document">' +
                                '<div class="modal-content">' +
                                '<div class="modal-header">' +
                                    '<h5 class="modal-title" id="delete_'+response.id+'">Warnig</h5>' +
                                    '<button type="button" class="close" data-dismiss="modal" aria-label="Close">' +
                                    '<span aria-hidden="true">&times;</span>' +
                                    '</button>' +
                                '</div>' +
                                '<div class="modal-body">' +
                                    'Are you sure?' +
                                '</div>' +
                                '<div class="modal-footer">' +
                                    '<button type="button" data-dismiss="modal" id = "delete" data-id = "'+response.id+'" class="btn m-btn--pill btn-danger">' +
                                        'Delete' +
                                    '</button>' +
                                    '<button type="button" class="btn btn-metal m-btn--pill" data-dismiss="modal">' +
                                        'Cancel' +
                                    '</button>' +
                                '</div>' +
                                '</div>' +
                            '</div>' +
                        '</div></div>';

            $('#m_sortable_portlets').append(html);

            var touchspin = '<tr id = "spin" data-id = "'+response.id+'">' +
                                '<td colspan="3" style = "width:45%" id = "touchspin_title" data-id = "'+response.id+'">' +
                                    '<input type="text" class = "form-control" id = "header_title" data-id = "'+response.id+'" placeholder="Text here">' +
                                '</td>' +
                                '<td>' +
                                    '<div class = "input-group bootstrap-touchspin">' +
                                        '<span class = "input-group-btn">' +
                                            '<button class="btn btn-secondary bootstrap-touchspin-down" type="button">-</button>' +
                                        '</span>' +
                                        '<span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span>' +
                                        '<input id="m_touchspin_3" readonly type="text" class="form-control" value="0" name="rate[]" placeholder="Input Rate" data-id = "'+response.id+'" type="text">' +
                                        '<span class="input-group-addon bootstrap-touchspin-postfix">%</span>' +
                                        '<span class="input-group-btn"><button class="btn btn-secondary bootstrap-touchspin-up" type="button">+</button></span>' +
                                    '</div>' +
                                '</td>' +
                            '</tr>';

            $('#touchspin').append(touchspin);
        }
    });

});

// edit button for form title
$('#editBtn').click(function(){
    $('#default').attr('style', 'display:none');
    $('#edit').attr('style', 'display:block');
});
// cancel button for form title
$('#cancelBtn').click(function(){
    $('#default').attr('style', 'display:block');
    $('#edit').attr('style', 'display:none');
});
// save button for form title
$('#saveBtn').click(function(){ 
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: '/forms/update/' + $('#formId').val(),
        type: 'post',
        data: { id: $('#formId').val(), value: $('#edit_title').val() },
        success: function(){
            $('#default').attr('style', 'display:block');
            $('#edit').attr('style', 'display:none');
            $('#default_title').text($('#edit_title').val());
        }
    });
});