//== Class definition

var SummernoteDemo = function () {    
    //== Private functions
    var demos = function () {
        $('#m_summernote_1').summernote({
            height: 250
        });
    }

    return {
        // public functions
        init: function() {
            demos(); 
        }
    };
}();

//== Initialization
jQuery(document).ready(function() {
    SummernoteDemo.init();
});