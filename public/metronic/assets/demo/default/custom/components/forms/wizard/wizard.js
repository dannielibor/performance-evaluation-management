//== Class definition
var WizardDemo = function () {
    //== Base elements
    var wizardEl = $('#m_wizard');
    var formEl = $('#m_form');
    var validator;
    var wizard;
    
    //== Private functions
    var initWizard = function () {
        //== Initialize form wizard
        wizard = wizardEl.mWizard({
            startStep: 1
        });

        //== Validation before going to next page
        wizard.on('beforeNext', function(wizard) {
            if (validator.form() !== true) {
                return false;  // don't go to the next step
            }
        })

        //== Change event
        wizard.on('change', function(wizard) {
            mApp.scrollTop();
        });
    }

    var initValidation = function() {
        validator = formEl.validate({
            //== Validate only visible fields
            ignore: ":hidden",

            //== Validation rules
            rules: {
                year: {
                    required: true 
                },
                term: {
                    required: true
                },       
                chooseFile: {
                    required: true,
                    extension: "csv"
                },
                accept: {
                    required: true
                }
            },

            //== Validation messages
            messages: {
                accept: {
                    required: "You must accept the Terms and Conditions agreement!"
                } 
            },
            
            //== Display error  
            invalidHandler: function(event, validator) {     
                mApp.scrollTop();

                swal({
                    "title": "", 
                    "text": "There are some errors in your submission. Please correct them.", 
                    "type": "error",
                    "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
                });
            },

            //== Submit valid form
            submitHandler: function (form) {
                
            }
        });   
    }

    var initSubmit = function() {
        var btn = formEl.find('[data-wizard-action="submit"]');
        
        btn.on('click', function(e) {
            e.preventDefault();
            
            if (validator.form()) {
                //== See: src\js\framework\base\app.js
                mApp.progress(btn);
                mApp.block(formEl); 

                //== See: http://malsup.com/jquery/form/#ajaxSubmit
                formEl.ajaxSubmit({
                    success: function(response) { 
                        console.log(response);
                        mApp.unprogress(btn);
                        mApp.unblock(formEl);
                        
                        if(response == "count"){
                            toastr.error('Too many list of files found!');
                        }else if(response == "xext"){
                            toastr.error('Invalid extension found!');
                        }else if(response == "xpsw"){
                            toastr.error('Decryption failed!');
                        }else if(response == 'false'){
                            toastr.error('Wrong csv file!');
                        }else{
                            swal({
                                "title": "", 
                                "text": "The application has been successfully submitted!", 
                                "type": "success",
                                "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
                            }).then(function(){
                                document.getElementById('m_1').style.display = "block";
                                document.getElementById('m_2').style.display = "none";
                                window.location.reload();
                            });
                        }

                    }
                });
            }
        });
    }

    return {
        // public functions
        init: function() {
            wizardEl = $('#m_wizard');
            formEl = $('#m_form');

            initWizard(); 
            initValidation();
            initSubmit();
        }
    };
}();

jQuery(document).ready(function() {    
    WizardDemo.init();
});