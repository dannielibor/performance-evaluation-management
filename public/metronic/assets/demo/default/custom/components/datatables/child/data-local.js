//== Class definition
var DatatableChildDataLocalStudent = function() {
  //== Private functions

  var subTableInit = function(e) {
    $('<div/>').attr('id', 'child_data_local_' + e.data.id).appendTo(e.detailCell).mDatatable({
      data: {
        type: 'local',
        source: e.data.children,
        pageSize: 10
      },

      // layout definition
      layout: {
        theme: 'default',
        scroll: true,
        height: 300,
        footer: false,

        // enable/disable datatable spinner.
        spinner: {
          type: 1,
          theme: 'default'
        },
      },

      sortable: true,

      // columns definition
      columns: [
        {
          field: 'Code',
          title: 'Code',
          textAlign: 'center'
        }, {
          field: 'Subject',
          title: 'Subject',
          textAlign: 'center'
        },
        {
          field: 'Section',
          title: 'Section',
          textAlign: 'center'
        },{
          field: 'Schedule',
          title: 'Schedule',
          textAlign: 'center'
        }, {
          field: 'Room',
          title: 'Room',
          textAlign: 'center'
        }]
    });
  };

  // demo initializer
  var mainTableInit = function() {

    $.ajax({
      url: '/load/students',
      type: 'get',
      success: function(response){ 
        
        var dataJSONArray = response;

        var datatable = $('.m_datatable').mDatatable({
          // datasource definition
          data: {
            type: 'local',
            source: dataJSONArray,
            pageSize: 10, // display 20 records per page
          },
    
          // layout definition
          layout: {
            theme: 'default',
            scroll: false,
            height: null,
            footer: false,
          },
    
          sortable: true,
    
          filterable: false,
    
          pagination: true,
    
          detail: {
            title: 'Load sub table',
            content: subTableInit
          },
    
          search: {
            input: $('#generalSearch')
          },
    
          // columns definition
          columns: [
            {
              field: 'id',
              title: '',
              sortable: false,
              width: 20,
              textAlign: 'center'
            },
            {
              field: 'img',
              title: '',
              sortable: false,
              width: 0,
              textAlign: 'left'
            },
            {
              field: 'name',
              title: 'Name',
              textAlign: 'center',
              template: function(row){
                return '\
                  <div style = "text-align:left"><img src = "/storage/profiles/'+row.img+'" alt = "'+row.img+'" class = "student-img">&nbsp;'+row.name+'</div>\
                ';
              }
            }, {
              field: 'studentNo',
              title: 'Student Number',
              textAlign: 'center'
            }, {
              field: 'program',
              title: 'Program',
              textAlign: 'center'
            },{
              field: 'status',
              title: 'Status',
              textAlign: 'center',
              // callback function support for column rendering
              template: function(row) {
                var status = {
                  'pending': {'title': 'Pending', 'class': 'm-badge--metal'},
                  'completed': {'title': 'Completed', 'class': ' m-badge--success'},
                  'Soft Deleted': {'title': 'Soft Deleted', 'class': ' m-badge--danger'},
                };
                return '<span class="m-badge ' + status[row.status].class + ' m-badge--wide">' + status[row.status].title + '</span>';
              }
            }, {
              field: 'Actions',
              width: 110,
              textAlign: 'center',
              title: 'Actions',
              sortable: false,
              overflow: 'visible',
              template: function (row, index, datatable) {
                var dropup = (datatable.getPageSize() - index) <= 4 ? 'dropup' : '';
                if(row.status == 'Soft Deleted'){
                  return '\
                    <a href="/students/edit/'+row.id+'" class="m-portlet__nav-link btn m-btn m-btn--hover-warning m-btn--icon m-btn--icon-only m-btn--pill" title="Edit details">\
                      <i class="la la-edit"></i>\
                    </a>\
                    <a href="#" data-toggle="modal" data-target="#m_modal_forceDel_'+row.id+'" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Force Delete">\
                        <i class="la la-trash"></i>\
                    </a>\
                    <a href="#" data-toggle="modal" data-target="#m_modal_restore_'+row.id+'" class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill" title="Restore">\
                        <i class="la la-undo"></i>\
                    </a>\
                    <div class="modal fade" id="m_modal_forceDel_'+row.id+'" tabindex="-1" role="dialog" aria-labelledby="m_modal_forceDel_'+row.id+'" aria-hidden="true">\
                        <form action = "/students/forceDel/'+row.id+'" method = "get">\
                        <div class="modal-dialog" role="document">\
                            <div class="modal-content">\
                            <div class="modal-header" style = "background-color:#FAA900">\
                                <h5 class="modal-title" id="m_modal_forceDel_'+row.id+'"><i class = "fa fa-warning"></i> &nbsp; Delete Student</h5>\
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">\
                                <span aria-hidden="true">&times;</span>\
                                </button>\
                            </div>\
                            <div class="modal-body">\
                              This user will be deleted including its certificate of registration permanently!\
                              <br>\
                              Do you wish to continue?\
                            </div>\
                            <div class="modal-footer">\
                                <button type="submit" class="m-btn--pill btn btn-danger">\
                                    Delete\
                                </button>\
                                <button type="button" class="m-btn--pill btn btn-metal" data-dismiss="modal">\
                                    Cancel\
                                </button>\
                            </div>\
                            </div>\
                        </div>\
                        </form>\
                    </div>\
                    <div class="modal fade" id="m_modal_restore_'+row.id+'" tabindex="-1" role="dialog" aria-labelledby="m_modal_restore_'+row.id+'" aria-hidden="true">\
                    <form action = "/students/restore/'+row.id+'" method = "get">\
                    <div class="modal-dialog" role="document">\
                        <div class="modal-content">\
                        <div class="modal-header" style = "background-color:#FAA900">\
                            <h5 class="modal-title" id="m_modal_restore_'+row.id+'"><i class = "fa fa-warning"></i> &nbsp; Restore</h5>\
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">\
                            <span aria-hidden="true">&times;</span>\
                            </button>\
                        </div>\
                        <div class="modal-body" style = "text-align:center">\
                            This user will be restored!\
                            <br>\
                            Do you wish to continue?\
                        </div>\
                        <div class="modal-footer">\
                            <button type="submit" class="m-btn--pill btn btn-warning">\
                                Restore\
                            </button>\
                            <button type="button" class="m-btn--pill btn btn-metal" data-dismiss="modal">\
                                Cancel\
                            </button>\
                        </div>\
                        </div>\
                    </div>\
                    </form>\
                </div>';
                }else{
                  return '\
                  <a href="/students/edit/'+row.id+'" class="m-portlet__nav-link btn m-btn m-btn--hover-warning m-btn--icon m-btn--icon-only m-btn--pill" title="Edit details">\
                    <i class="la la-edit"></i>\
                  </a>\
                  <a href="#" data-toggle="modal" data-target="#m_modal_delete_'+row.id+'" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete">\
                    <i class="la la-trash"></i>\
                  </a>\
                  <div class="modal fade" id="m_modal_delete_'+row.id+'" tabindex="-1" role="dialog" aria-labelledby="delete_'+row.id+'" aria-hidden="true">\
                      <form action = "/students/delete/'+row.id+'" method = "get">\
                      <div class="modal-dialog" role="document">\
                          <div class="modal-content">\
                          <div class="modal-header" style = "background-color:#FAA900">\
                              <h5 class="modal-title" id="delete_'+row.id+'"><i class = "fa fa-warning"></i> &nbsp; Delete Student</h5>\
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">\
                              <span aria-hidden="true">&times;</span>\
                              </button>\
                          </div>\
                          <div class="modal-body">\
                            This user will be deleted including its certificate of registration!\
                            <br>\
                            Do you wish to continue?\
                          </div>\
                          <div class="modal-footer">\
                              <button type="submit" class="m-btn--pill btn btn-danger">\
                                  Delete\
                              </button>\
                              <button type="button" class="m-btn--pill btn btn-metal" data-dismiss="modal">\
                                  Cancel\
                              </button>\
                          </div>\
                          </div>\
                      </div>\
                      </form>\
                  </div>';
                }
              },
            }],
        });

        var query = datatable.getDataSourceQuery();

        $('#m_form_status').on('change', function () {
          datatable.search($(this).val(), 'status');
        }).val(typeof query.Status !== 'undefined' ? query.Status : '');

        $('#m_form_program').on('change', function () { 
          datatable.search($(this).val(), 'program');
        }).val(typeof query.Program !== 'undefined' ? query.Program : '');
    
        $('#m_form_status, #m_form_program').selectpicker();
        //$('#m_form_status').selectpicker();

      }
    });

  };

  return {
    //== Public functions
    init: function() {
      // init dmeo
      mainTableInit();
    },
  };
}();

//== Class definition
var DatatableChildDataLocalFaculty = function() {
  //== Private functions

  var subTableInit = function(e) {
    $('<div/>').attr('id', 'child_data_local_' + e.data.id).appendTo(e.detailCell).mDatatable({
      data: {
        type: 'local',
        source: e.data.children,
        pageSize: 10
      },

      // layout definition
      layout: {
        theme: 'default',
        scroll: true,
        height: 300,
        footer: false,

        // enable/disable datatable spinner.
        spinner: {
          type: 1,
          theme: 'default'
        },
      },

      sortable: true,

      // columns definition
      columns: [
        {
          field: 'ID',
          title: 'ID',
          textAlign: 'center'
        }, {
          field: 'Code',
          title: 'Code',
          textAlign: 'center'
        }, {
          field: 'Subject',
          title: 'Subject',
          textAlign: 'center'
        },
        {
          field: 'Section',
          title: 'Section',
          textAlign: 'center'
        },{
          field: 'Schedule',
          title: 'Schedule',
          textAlign: 'center'
        },
        {
          field: 'Size',
          title: 'Class Size',
          textAlign: 'center'
        }, {
          field: 'Room',
          title: 'Room',
          textAlign: 'center'
        }]
    });
  };

  // demo initializer
  var mainTableInit = function() {

    $.ajax({
      url: '/load/faculty',
      type: 'get',
      success: function(response){

        var dataJSONArray = response;

        var datatable = $('.m_datatable_faculty').mDatatable({
          // datasource definition
          data: {
            type: 'local',
            source: dataJSONArray,
            pageSize: 10, // display 20 records per page
          },
    
          // layout definition
          layout: {
            theme: 'default',
            scroll: false,
            height: null,
            footer: false,
          },
    
          sortable: true,
    
          filterable: false,
    
          pagination: true,
    
          detail: {
            title: 'Load sub table',
            content: subTableInit
          },
    
          search: {
            input: $('#generalSearch')
          },
    
          // columns definition
          columns: [
            {
              field: 'id',
              title: '',
              sortable: false,
              width: 20,
              textAlign: 'center'
            },
            {
              field: 'img',
              title: '',
              sortable: false,
              width: 20,
              textAlign: 'left',
              template: function(row){
                return '<input type = "checkbox" name = "select" data-id = "'+row.id+'" data-type = "'+row.type+'" value = "'+row.name+'">';
              }
            },
            {
              field: 'name',
              title: 'Name',
              textAlign: 'center',
              template: function(row){
                return '\
                  <div style = "text-align:left"><img src = "/storage/profiles/'+row.img+'" alt = "'+row.img+'" class = "faculty-img">&nbsp;'+row.name+'</div>\
                ';
              }
            }, {
              field: 'facultyNo',
              title: 'Faculty Number',
              textAlign: 'center'
            }, 
            {
              field: 'department',
              title: 'Department',
              textAlign: 'center'
            },
            {
              field: 'status',
              title: 'Status',
              textAlign: 'center',
              // callback function support for column rendering
              template: function(row) {
                var status = {
                  'pending': {'title': 'Pending', 'class': 'm-badge--metal'},
                  'completed': {'title': 'Completed', 'class': ' m-badge--success'},
                  'Soft Deleted': {'title': 'Soft Deleted', 'class': ' m-badge--danger'},
                };
                return '<span class="m-badge ' + status[row.status].class + ' m-badge--wide">' + status[row.status].title + '</span>';
              }
            },
            {
              field: 'type',
              title: 'Type',
              textAlign: 'center',
              template: function(row) {
                var status = { 
                  'Dean': {'title': 'Dean', 'class': 'm-badge--metal'},
                  'Department Chair': {'title': 'Department Chair', 'class': ' m-badge--success'},
                  'Vice Dean': {'title': 'Vice Dean', 'class': ' m-badge--primary'},
                  'Professor': {'title': 'Professor', 'class': ' m-badge--info'}
                };
                return '<span class="m-badge ' + status[row.type].class + ' m-badge--wide">' + status[row.type].title + '</span>';
              }
            }, {
              field: 'Actions',
              width: 110,
              textAlign: 'center',
              title: 'Actions',
              sortable: false,
              overflow: 'visible',
              template: function (row, index, datatable) {
                var dropup = (datatable.getPageSize() - index) <= 4 ? 'dropup' : '';
                if(row.status == 'Soft Deleted'){
                  return '\
                    <a href="/faculty/edit/'+row.id+'" class="m-portlet__nav-link btn m-btn m-btn--hover-warning m-btn--icon m-btn--icon-only m-btn--pill" title="Edit details">\
                      <i class="la la-edit"></i>\
                    </a>\
                    <a href="#" data-toggle="modal" data-target="#m_modal_forceDel_'+row.id+'" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Force Delete">\
                        <i class="la la-trash"></i>\
                    </a>\
                    <a href="#" data-toggle="modal" data-target="#m_modal_restore_'+row.id+'" class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill" title="Restore">\
                        <i class="la la-undo"></i>\
                    </a>\
                    <div class="modal fade" id="m_modal_forceDel_'+row.id+'" tabindex="-1" role="dialog" aria-labelledby="m_modal_forceDel_'+row.id+'" aria-hidden="true">\
                        <form action = "/faculty/forceDel/'+row.id+'" method = "get">\
                        <div class="modal-dialog" role="document">\
                            <div class="modal-content">\
                            <div class="modal-header" style = "background-color:#FAA900">\
                                <h5 class="modal-title" id="m_modal_forceDel_'+row.id+'"><i class = "fa fa-warning"></i> &nbsp; Delete Student</h5>\
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">\
                                <span aria-hidden="true">&times;</span>\
                                </button>\
                            </div>\
                            <div class="modal-body">\
                              This user will be deleted including its class load permanently!\
                              <br>\
                              Do you wish to continue?\
                            </div>\
                            <div class="modal-footer">\
                                <button type="submit" class="m-btn--pill btn btn-danger">\
                                    Delete\
                                </button>\
                                <button type="button" class="m-btn--pill btn btn-metal" data-dismiss="modal">\
                                    Cancel\
                                </button>\
                            </div>\
                            </div>\
                        </div>\
                        </form>\
                    </div>\
                    <div class="modal fade" id="m_modal_restore_'+row.id+'" tabindex="-1" role="dialog" aria-labelledby="m_modal_restore_'+row.id+'" aria-hidden="true">\
                    <form action = "/faculty/restore/'+row.id+'" method = "get">\
                    <div class="modal-dialog" role="document">\
                        <div class="modal-content">\
                        <div class="modal-header" style = "background-color:#FAA900">\
                            <h5 class="modal-title" id="m_modal_restore_'+row.id+'"><i class = "fa fa-warning"></i> &nbsp; Restore</h5>\
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">\
                            <span aria-hidden="true">&times;</span>\
                            </button>\
                        </div>\
                        <div class="modal-body" style = "text-align:center">\
                            This user will be restored!\
                            <br>\
                            Do you wish to continue?\
                        </div>\
                        <div class="modal-footer">\
                            <button type="submit" class="m-btn--pill btn btn-warning">\
                                Restore\
                            </button>\
                            <button type="button" class="m-btn--pill btn btn-metal" data-dismiss="modal">\
                                Cancel\
                            </button>\
                        </div>\
                        </div>\
                    </div>\
                    </form>\
                </div>';
                }else{
                  return '\
                  <a href="/faculty/edit/'+row.id+'" class="m-portlet__nav-link btn m-btn m-btn--hover-warning m-btn--icon m-btn--icon-only m-btn--pill" title="Edit details">\
                    <i class="la la-edit"></i>\
                  </a>\
                  <a href="#" data-toggle="modal" data-target="#m_modal_delete_'+row.id+'" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete">\
                    <i class="la la-trash"></i>\
                  </a>\
                  <div class="modal fade" id="m_modal_delete_'+row.id+'" tabindex="-1" role="dialog" aria-labelledby="delete_'+row.id+'" aria-hidden="true">\
                      <form action = "/faculty/delete/'+row.id+'" method = "get">\
                      <div class="modal-dialog" role="document">\
                          <div class="modal-content">\
                          <div class="modal-header" style = "background-color:#FAA900">\
                              <h5 class="modal-title" id="delete_'+row.id+'"><i class = "fa fa-warning"></i> &nbsp; Delete Faculty</h5>\
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">\
                              <span aria-hidden="true">&times;</span>\
                              </button>\
                          </div>\
                          <div class="modal-body">\
                            This user will be deleted including its class load!\
                            <br>\
                            Do you wish to continue?\
                          </div>\
                          <div class="modal-footer">\
                              <button type="submit" class="m-btn--pill btn btn-danger">\
                                  Delete\
                              </button>\
                              <button type="button" class="m-btn--pill btn btn-metal" data-dismiss="modal">\
                                  Cancel\
                              </button>\
                          </div>\
                          </div>\
                      </div>\
                      </form>\
                  </div>';
                }
              },
            }],
        });

        var query = datatable.getDataSourceQuery();

        $('#m_form_type').on('change', function () {
          datatable.search($(this).val(), 'type');
        }).val(typeof query.Type !== 'undefined' ? query.Type : '');

        $('#m_form_department').on('change', function () {
          datatable.search($(this).val(), 'department');
        }).val(typeof query.Department !== 'undefined' ? query.Department : '');
    
        $('#m_form_type, #m_form_department').selectpicker();
        //$('#m_form_type').selectpicker();

      }
    });

  };

  return {
    //== Public functions
    init: function() {
      // init dmeo
      mainTableInit();
    },
  };
}();

jQuery(document).ready(function() {
  DatatableChildDataLocalStudent.init();
  DatatableChildDataLocalFaculty.init();
});