//== Class definition

var DatatableHtmlTableDemo = function() {
	//== Private functions

	// demo initializer
	var demo = function() {

		var datatable = $('.m-datatable').mDatatable({
			data: {
				saveState: {cookie: false},
			},
			search: {
				input: $('#generalSearch'),
			},
			columns: [
				{
					field: 'Status',
					title: 'Status',
					// callback function support for column rendering
					template: function(row) {
						var status = {
							1: {'title': 'Pending', 'class': 'm-badge--metal'},
							2: {'title': 'Active', 'class': ' m-badge--success'},
							3: {'title': 'Inactive', 'class': ' m-badge--primary'},
							4: {'title': 'Activated', 'class': ' m-badge--success'},
							5: {'title': 'Soft Deleted', 'class': ' m-badge--danger'},
							6: {'title': 'Admin', 'class': ' m-badge--accent'},
							7: {'title': 'Super Admin', 'class': ' m-badge--primary'},
							8: {'title': 'Outstanding', 'class': 'm-badge--success'},
							9: {'title': 'Very Satisfactory', 'class': 'm-badge--success'},
							10: {'title': 'Satisfactory', 'class': 'm-badge--success'},
							11: {'title': 'Moderately Satisfactory', 'class': 'm-badge--warning'},
							12: {'title': 'Needs Improvement', 'class': 'm-badge--danger'}
						};	
						return '<center><span class="m-badge ' + status[row.Status].class + ' m-badge--wide">' + status[row.Status].title + '</span></center>';
					},
				},
				{
					field: 'Type',
					title: 'Type',
					// callback function support for column rendering
					template: function(row) {
						var type = {
							1: {'title': 'Admin', 'class': 'm-badge--accent'},
							2: {'title': 'Super Admin', 'class': ' m-badge--info'}
						};	
						return '<center><span class="m-badge ' + type[row.Type].class + ' m-badge--wide">' + type[row.Type].title + '</span></center>';
					},
				},
			],
		});

		$('#m_form_status').on('change', function() {
			datatable.search($(this).val().toLowerCase(), 'Status');
		});

		$('#m_form_status').selectpicker();

	};

	var studentProg = function() {

		var datatable = $('.m-datatable_studentProgress').mDatatable({
			data: {
				saveState: {cookie: false},
			},
		});

	};

	var facultyProg = function() {

		var datatable = $('.m-datatable_facultyProgress').mDatatable({
			data: {
				saveState: {cookie: false},
			},
		});

	};

	return {
		//== Public functions
		init: function() {
			try {
				demo();
			} catch (error) {
				console.log('');
			}
			try {
				studentProg();
			} catch (error) {
				console.log('');
			}
			try {
				facultyProg();
			} catch (error) {
				console.log('');
			}
		},
	};
}();

jQuery(document).ready(function() {
	DatatableHtmlTableDemo.init();
});