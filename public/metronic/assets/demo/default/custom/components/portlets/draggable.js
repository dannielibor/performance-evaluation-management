var PortletDraggable = function () {

    return {
        //main function to initiate the module
        init: function () {
            $("#m_sortable_portlets").sortable({
                connectWith: ".m-accordion__item-icon",
                items: ".m-portlet", 
                opacity: 0.8,
                handle : '.m-accordion__item-icon',
                coneHelperSize: true,
                placeholder: 'm-portlet--sortable-placeholder',
                forcePlaceholderSize: true,
                tolerance: "pointer",
                helper: "clone",
                tolerance: "pointer",
                forcePlaceholderSize: !0,
                helper: "clone",
                cancel: ".m-portlet--sortable-empty", // cancel dragging if portlet is in fullscreen mode
                revert: 250, // animation in milliseconds
                update: function(b, c) {
                    var letters = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
                    $('#sort[data-id]').each(function(i){
                        $(this).text(letters[i] + '.');
                        $.ajax({
                            url: '/update/chapter',
                            type: 'get',
                            data: { id: $(this).attr('data-id'), sort: letters[i], position: i }
                        });
                    });
                    if (c.item.prev().hasClass("m-portlet--sortable-empty")) {
                        c.item.prev().before(c.item);
                    } 
                }
            });
        }
    };
}();

jQuery(document).ready(function() {
    PortletDraggable.init();
});